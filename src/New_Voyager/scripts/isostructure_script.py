import sys,os,shutil,copy,json
import argparse
import iotbx
import cctbx
import traceback
import numpy as np
import iotbx.pdb.fetch

try:
  from libtbx.phil import parse
  from phasertng.phil.master_phil_file import build_phil_str
  from phasertng.phil.converter_registry import converter_registry
  from phasertng.phil.phil_to_cards import phil_to_cards
  from phasertng.phil.phil_to_cards import phil_to_cards_change_mode
  from phasertng import Phasertng
  from phasertng import PhaserError
  from phasertng import Output
  from phasertng import PhaserError
  from phasertng import enum_out_stream, enum_err_code
  from libtbx.utils import null_out, multi_out
  from scitbx.array_family import flex
  from cctbx.crystal import reindex
  from cctbx import crystal
  import iotbx.pdb
  from cctbx.array_family import flex
  from cctbx.sgtbx import change_of_basis_op
  from six.moves import cStringIO as StringIO

  # NOTE CM temporary till I figure the structure of the repo and how to do the imports
  module_path = os.path.dirname(os.path.abspath(__file__))
  sys.path.append(os.path.join(module_path, "../"))
  from New_Voyager import voyager_base
  from New_Voyager import simple_tools
  from New_Voyager import system_utility
except:
  print('There was an error with the imports')
  traceback.print_exc(file=sys.stdout)





def isostructure_code(user_phil_str=""):

  try:
    # Instantiate the basetng object and create the directory
    basetng = Phasertng("PHENIX")
    phasertng_dir = os.path.abspath(simple_tools.create_directory_timestamp("phasertng_"))
    # Get the masterphil with all the defaults from phasertng
    initparams = build_phil_str(program_name="InputCard")
    assert (len(initparams))
    master_phil_tng = parse(initparams, converter_registry=converter_registry)
    # Now instantiate the basevoyager class.One of its attributes is the master phil with all the voyager scope keywords except the tng
    basevoyager = voyager_base.VoyagerBase()
    # Now we merge both of them
    merged_phil_str = 'voyager { ' + basevoyager.voyager_phil_str + initparams + '}'
    master_phil_global = parse(merged_phil_str, converter_registry=converter_registry)
    # And get those values which have been set by the user
    user_phil = iotbx.phil.parse(user_phil_str)
    working_params_global = master_phil_global.fetch(sources=[user_phil]).extract()
    modified_phil = master_phil_global.format(python_object=working_params_global)
    working_parameters_tng = master_phil_tng.fetch(sources=[modified_phil]).extract()
    # Not sure about whether we will actually run tng but maybe use the object
    #print('sherlock dir(working_parameters_tng.phasertng)',dir(working_parameters_tng.phasertng))
    #

   # NOTE, this code comes from the action isostructure_search that Airlie had already modified. If something in terms
   # of output style does not work I will leave it commented at the moment.
    out_wrapper = multi_out()
    # log_file = open(self.params.phaser.keywords.general.root + ".log", "w")
    primary_logfile = open("voyager.log", "w")
    primary_stream = sys.stdout
    out_wrapper.register("Primary output stream", primary_stream)
    #    out_wrapper.register("Primary logfile", primary_logfile)
    phaser_out = Output("VOYAGER")
    phaser_out.SetLevel("logfile")  # set both
    phaser_out.SetStore("logfile")
    phaser_out.logHeader(enum_out_stream.concise, "ISOSTRUCTURE SEARCH")
    # at the end close file log_file.close()
    phaser_out.logUnderLine(enum_out_stream.logfile, "Query")

    if working_params_global.voyager.isostructure_search.tutorial_mode:
      # return self.initial_dag_nodes
      phaser_out.logWarning(enum_out_stream.logfile, "Running in tutorial mode")
      phaser_out.logBlank(enum_out_stream.logfile)

    mtz = working_params_global.voyager.reflections

    if mtz is None:
      phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fileset, ""))
      phaser_out.logTrailer(enum_out_stream.concise)
      raise Exception("FATAL ERROR. File not set")
    elif not os.path.exists(mtz):
      phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fileopen, str(mtz)))
      phaser_out.logTrailer(enum_out_stream.concise)
      raise Exception(
        "FATAL ERROR. The path to the mtz inserted does not correspond to a valid file. Please correct and restart the program.")
    else:
      phaser_out.logTab(enum_out_stream.logfile, "File: " + str(mtz))
      mtzobj = iotbx.file_reader.any_file(mtz)

    if mtzobj.file_type == "pdb" and mtzobj.crystal_symmetry() is None:
      message = "pdb file without CRYST1 record"
      phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fatal, message))
      phaser_out.logTrailer(enum_out_stream.concise)
      raise Exception("FATAL ERROR: " + message)

    MAXNCDIST = working_params_global.voyager.isostructure_search.max_ncdist  # NOTE: 2.5   This is the default of SAUC server
    TOPEXPLORE = working_params_global.voyager.isostructure_search.top_explore  # 10
    MINCCTHRESH = working_params_global.voyager.isostructure_search.min_corr_coeff  # 0.60
    original_crystal = mtzobj.crystal_symmetry()

    niggli_cell = mtzobj.crystal_symmetry().niggli_cell().unit_cell().parameters()
    reciprocal_cell = mtzobj.crystal_symmetry().unit_cell().reciprocal_parameters()
    unit_cell_volume = mtzobj.crystal_symmetry().unit_cell().niggli_cell().volume()

    if (mtzobj.file_type == 'hkl'):
      phaser_out.logTab(enum_out_stream.logfile, "File type:     reflections")
    elif (mtzobj.file_type == 'pdb'):
      phaser_out.logTab(enum_out_stream.logfile, "File type:     coordinates")
    phaser_out.logTab(enum_out_stream.logfile, "Space Group:   " + str(
      mtzobj.crystal_symmetry().space_group().match_tabulated_settings().hermann_mauguin()))
    phaser_out.logTab(enum_out_stream.logfile, "Original Cell: " + str(mtzobj.crystal_symmetry().unit_cell()))
    niggli_cell_str = mtzobj.crystal_symmetry().niggli_cell().unit_cell()
    phaser_out.logTab(enum_out_stream.logfile, "Niggli Cell:   " + str(niggli_cell_str))
    #  niggli_op = reindex.reindexing_operators(mtzobj.crystal_symmetry(), mtzobj.crystal_symmetry().niggli_cell())
    niggli_op = mtzobj.crystal_symmetry().change_of_basis_op_to_niggli_cell()
    phaser_out.logTab(enum_out_stream.logfile, "Reindexing:    " + str(niggli_op.as_hkl()))

    nodes_to_return = []
    if working_params_global.voyager.isostructure_search.reindex_as is None:
      phaser_out.logUnderLine(enum_out_stream.logfile, "Isostructure Search")

      delta_dim = working_params_global.voyager.isostructure_search.delta_dim
      delta_angle = working_params_global.voyager.isostructure_search.delta_angle

      min_a = str(niggli_cell[0] - delta_dim)
      max_a = str(niggli_cell[0] + delta_dim)
      min_b = str(niggli_cell[1] - delta_dim)
      max_b = str(niggli_cell[1] + delta_dim)
      min_c = str(niggli_cell[2] - delta_dim)
      max_c = str(niggli_cell[2] + delta_dim)
      min_alpha = str(niggli_cell[3] - delta_angle)
      max_alpha = str(niggli_cell[3] + delta_angle)
      min_beta = str(niggli_cell[4] - delta_angle)
      max_beta = str(niggli_cell[4] + delta_angle)
      min_gamma = str(niggli_cell[5] - delta_angle)
      max_gamma = str(niggli_cell[5] + delta_angle)

      # print("TEST")
      # UC1 = cctbx.uctbx.unit_cell([80.36, 80.36, 99.44, 90, 90, 120]).niggli_cell().parameters() #1U4J
      # UC2 = cctbx.uctbx.unit_cell([80.360 ,  80.360  , 99.440 , 90.00 , 90.00 ,120.00]).niggli_cell().parameters() #1G07
      # UC3 = cctbx.uctbx.unit_cell([55.120,   55.120 ,  54.810 , 90.00 , 90.00 , 90.00]).niggli_cell().parameters() #2subgroup_analysis
      # UC4 = cctbx.uctbx.unit_cell([80.949 ,  80.572  , 57.098 , 90.00 , 90.35 , 90.00]).niggli_cell().parameters() #1G2X
      #
      # print("1U4J",UC1,"1G07",UC2)
      # print("NCDist",simple_tools.NCDist_metric(UC1).NCDist(UC2),"annotated",0.0)
      # print("cctbx NCDist",np.sqrt(NCDist(simple_tools.g6_form(UC1),simple_tools.g6_form(UC2)))*(np.sqrt(6.0)/np.mean(UC1[:3]+UC2[:3])))
      # print("V7",simple_tools.V7(UC1).V7_distance(UC2),"annotated",0.0)
      # print()
      # print("1U4J", UC1, "2SGA", UC3)
      # print("NCDist", simple_tools.NCDist_metric(UC1).NCDist(UC3),"annotated",3.1)
      # print("cctbx NCDist",np.sqrt(NCDist(simple_tools.g6_form(UC1),simple_tools.g6_form(UC3)))*(np.sqrt(6.0)/np.mean(UC1[:3]+UC3[:3])))
      # print("V7", simple_tools.V7(UC1).V7_distance(UC3),"annotated",4.9)
      # print()
      # print("1U4J", UC1, "1G2X", UC4)
      # print("NCDist", simple_tools.NCDist_metric(UC1).NCDist(UC4),"annotated",0.9)
      # print("cctbx NCDist",np.sqrt(NCDist(simple_tools.g6_form(UC1),simple_tools.g6_form(UC4)))*(np.sqrt(6.0)/np.mean(UC1[:3]+UC4[:3])))
      # print("V7", simple_tools.V7(UC1).V7_distance(UC4),"annotated",0.2)
      # print()

      # cell_search = "https://www.ebi.ac.uk/pdbe/search/pdb/select?q=nigli_cell_a:[" + min_a + "%20TO%20" + max_a + "]%20AND%20nigli_cell_b:[" + min_b + "%20TO%20" + max_b + "]%20AND%20nigli_cell_c:[" + min_c + "%20TO%20" + max_c + "]%20AND%20nigli_cell_alpha:[" + min_alpha + "%20TO%20" + max_alpha + "]%20AND%20nigli_cell_beta:[" + min_beta + "%20TO%20" + max_beta + "]%20AND%20nigli_cell_gamma:[" + min_gamma + "%20TO%20" + max_gamma + "]&group=true&group.field=pdb_id&rows=100000&group.ngroups=true&omitHeader=true&fl=*"
      cell_search = "https://wwwdev.ebi.ac.uk/pdbe/search/pdb/select?q=nigli_cell_a:[" + min_a + "%20TO%20" + max_a + "]%20AND%20nigli_cell_b:[" + min_b + "%20TO%20" + max_b + "]%20AND%20nigli_cell_c:[" + min_c + "%20TO%20" + max_c + "]%20AND%20nigli_cell_alpha:[" + min_alpha + "%20TO%20" + max_alpha + "]%20AND%20nigli_cell_beta:[" + min_beta + "%20TO%20" + max_beta + "]%20AND%20nigli_cell_gamma:[" + min_gamma + "%20TO%20" + max_gamma + "]&group=true&group.field=pdb_id&rows=100000&group.ngroups=true&omitHeader=true&fl=*"
      server = "www.ebi.ac.uk"
      phaser_out.logEllipsisOpen(enum_out_stream.logfile, "Unit cell search performed on server " + server)
      z = system_utility.request_url(cell_search, server)
      phaser_out.logEllipsisShut(enum_out_stream.logfile)

      if not z:
        result_list = []
      else:
        a1, b1, c1, alpha1, beta1, gamma1 = niggli_cell
        resu = json.loads(z)

        result_list = [[gr["groupValue"], [(s["nigli_cell_a"], s["nigli_cell_b"], s["nigli_cell_c"],
                                            s["nigli_cell_alpha"], s["nigli_cell_beta"], s["nigli_cell_gamma"]) for
                                           s in gr["doclist"]["docs"]][0]] for gr in
                       resu["grouped"]["pdb_id"]["groups"]]

        UC1 = [a1, b1, c1, alpha1 / 57.2957795130823, beta1 / 57.2957795130823, gamma1 / 57.2957795130823]
        # v7 = simple_tools.V7(UC1)
        rsn = []

        for resu in result_list:
          pdbid, niggli = resu
          a2, b2, c2, alpha2, beta2, gamma2 = niggli
          # ncdist = simple_tools.NCDist_metric([a1, b1, c1, alpha1, beta1, gamma1])
          # dist = ncdist.NCDist([a2, b2, c2, alpha2, beta2, gamma2])
          UC2 = [a2, b2, c2, alpha2 / 57.2957795130823, beta2 / 57.2957795130823, gamma2 / 57.2957795130823]
          # UC1 = [145.9, 145.9, 191.645, 90, 112.374, 120]
          # UC2 = [145.097, 145.097, 190.011, 90, 112.446, 120]
          # distv7 = v7.V7_distance(UC2)
          # cctbx_ncdist = np.sqrt(NCDist(simple_tools.g6_form(UC1),simple_tools.g6_form(UC2)))*(np.sqrt(6.0)/np.mean(UC1[:3]+UC2[:3]))
          # cctbx_ncdist = NCDist(UC1, UC2)  # /np.mean(np.abs(np.array(UC1[:3])-np.array(UC2[:3])))
          cctbx_ncdist = 0.1 * np.sqrt(cctbx.uctbx.determine_unit_cell.NCDist(simple_tools.g6_form(UC1), simple_tools.g6_form(UC2)))
          # self.logger.info("pdbid: "+pdbid+" "+str(niggli))
          # print("NCDIST",dist)
          # print("V7" ,distv7)
          # print(UC1)
          # print(UC2)
          # print("CCTBX NCDIST", cctbx_ncdist, NCDist(simple_tools.g6_form(UC1), simple_tools.g6_form(UC2)), NCDist(UC1, UC2))
          # self.logger.info("CCTBX NCDIST "+str(cctbx_ncdist))
          # self.logger.info("")
          # phaser_out.logTab(enum_out_stream.logfile,"pdbid: "+str(pdbid)+" ncdist: "+str("{:.3f}".format(cctbx_ncdist)) +" cell: "+str(niggli))
          # quit()
          rsn.append((pdbid, cctbx_ncdist, niggli))

        phaser_out.logBlank(enum_out_stream.logfile)
        phaser_out.logEllipsisOpen(enum_out_stream.logfile, "Sorting")
        #  result_list = sorted(rsn, key=lambda x: x[1])[:TOPEXPLORE+5]
        result_list = sorted(rsn, key=lambda x: x[1])
        phaser_out.logEllipsisShut(enum_out_stream.logfile)

        first1 = True
        first2 = True
        count = 1
        maxprint = 100
        nebi = len(result_list)
        phaser_out.logTab(enum_out_stream.logfile, "Number returned from server = " + str(nebi))
        for resu in result_list:
          pdbid, cctbx_ncdist, niggli = resu
          if (cctbx_ncdist > MAXNCDIST and first1):
            phaser_out.logTab(enum_out_stream.logfile, "      ----------- Niggli-cone distance limit")
            first1 = False;
          where = enum_out_stream.logfile
          if (count >= maxprint + 1 and cctbx_ncdist > MAXNCDIST and nebi > maxprint):
            where = enum_out_stream.verbose
          phaser_out.logTab(where, str(count).ljust(5) + " pdbid: " + str(pdbid) + " Niggli-cone distance: " + str(
            round(cctbx_ncdist, 3)) + " cell: " + str(niggli))
          if first2 and count >= maxprint and nebi > maxprint and cctbx_ncdist > MAXNCDIST:
            phaser_out.logTab(enum_out_stream.logfile, "      ----------- and " + str(nebi - count) + " more")
            first2 = False
          count = count + 1

      if mtzobj.file_type != "hkl":
        phaser_out.logBlank(enum_out_stream.logfile)
        phaser_out.logTab(enum_out_stream.logfile, "No data for correlation")
        phaser_out.logBlank(enum_out_stream.logfile)
      #  phaser_out.logTrailer(enum_out_stream.concise)
      #  return

      #ic = simple_tools.Intensity_Correlation(mtz, logger=self.logger)
      ic = simple_tools.Intensity_Correlation(mtz)
      coutxt = flex.std_string(ic.outxt)
      phaser_out.logTableTop(enum_out_stream.logfile, "Query Indexing", True)
      phaser_out.logTabArray(enum_out_stream.logfile, coutxt)
      phaser_out.logTableEnd(enum_out_stream.logfile)

      foundpdb = None
      best_pdbid = None
      best_maxcc = -1.0
      best_crystal = None
      best_niggli = None
      best_ope = None

      topnlist = []
      phaser_out.logBlank(enum_out_stream.logfile)
      phaser_out.logTab(enum_out_stream.logfile, "Maximum Niggli-cone distance: " + str("{:.3f}".format(MAXNCDIST)))
      phaser_out.logTab(enum_out_stream.logfile, "Minimum correlation (CC): " + str("{:.3f}".format(MINCCTHRESH)))
      phaser_out.logTab(enum_out_stream.logfile, "Maximum number analysed over minimum CC: " + str(TOPEXPLORE))
      nfoundpdb = 0
      for r, (pdbid, dist, tmp) in enumerate(result_list):
        if dist >= MAXNCDIST:
          phaser_out.logBlank(enum_out_stream.logfile)
          phaser_out.logTab(enum_out_stream.logfile, "----------- Niggli-cone distance limit")
          break
        phaser_out.logBlank(enum_out_stream.logfile)
        phaser_out.logUnderLine(enum_out_stream.logfile, "Evaluating #" + str(r + 1) + " " + str(pdbid))
        phaser_out.logTab(enum_out_stream.logfile, "Niggli-cone distance = " + str("{:.3f}".format(dist)))
        z = system_utility.request_url("https://www.ebi.ac.uk/pdbe/api/pdb/entry/summary/" + str(pdbid.lower()),
                                       "www.ebi.ac.uk")
        if not z:
          phaser_out.logTab(enum_out_stream.logfile, "Failed server request, skipping...")
          continue
        else:
          infol = json.loads(z)

        cannot_download = False
        phaser_out.logTab(enum_out_stream.logfile, "title: " + str(infol[pdbid.lower()][0]["title"]))

        if working_params_global.voyager.isostructure_search.tutorial_mode:
          if r == 0 or dist < 0.0001:
            phaser_out.logTab(enum_out_stream.logfile, "Top solution ignored in tutorial mode")
            phaser_out.logBlank(enum_out_stream.logfile)
            continue

        try:
          # note cm: changed mirror from self.mirror to 'pdbe'
          datap = iotbx.pdb.fetch.fetch(pdbid.lower(), data_type="xray", format="cif", mirror="pdbe", log=None,
                                   force_download=False, local_cache=None).read().decode(system_utility.get_encoding())
          # datap = iotbx.pdb.fetch.fetch(pdbid.lower(), data_type="xray", format="cif", mirror="pdbe", log=None,
          #                          force_download=False, local_cache=None).read().decode(
          #  system_utility.get_encoding())
        except:
          # import time
          # time.sleep(10)
          # print(sys.exc_info())
          # traceback.print_exc(file=sys.stdout)
          try:
            datap = iotbx.pdb.fetch.fetch(pdbid.lower(), data_type="xray", format="cif", mirror="pdbe", log=None,
                                     force_download=False, local_cache=None).read().decode(
              system_utility.get_encoding())
          except:
            try:
              # can get a pdb file and calculate structure factors
              datap = iotbx.pdb.fetch.fetch(pdbid.lower(), data_type="pdb", format="cif", mirror="pdbe", log=None,
                                       force_download=False, local_cache=None).read().decode(
                system_utility.get_encoding())
              # print(sys.exc_info())
              # traceback.print_exc(file=sys.stdout)
            except:
              cannot_download = True

        # NOTE: It looks like cctbx needs to a filename associated with the cif file string
        # dp = cStringIO.StringIO(datap)
        if not cannot_download:
          phaser_out.logTab(enum_out_stream.logfile, "Download successful")
          #  pdb_input = iotbx.pdb.input(source_info=str(pdbid),lines=flex.split_lines(str(datap)))
          cifile = str(pdbid + ".cif")
          with open(cifile, "w") as fg:
            fg.write(datap)
          try:
            # CALCULATE CC HERE
            goodcc, result = ic.compare(cifile, thresh=MINCCTHRESH)
            # end calculate
            phaser_out.logTab(enum_out_stream.logfile, "Niggli Basis: " + ic.niggli2.as_hkl())
            phaser_out.logTab(enum_out_stream.logfile, "Space Group: " + ic.spg2)
            phaser_out.logTab(enum_out_stream.logfile, "Unit Cell: " + str(ic.cell2))

            k = 1
            unique_dict = {}
            unique_list = {}
            for item in result:
              if (item["data"] in unique_dict):
                continue
              else:
                unique_dict[item["data"]] = int(k)
                unique_list[int(k)] = item["data"]
                k = k + 1

            header = True
            for item in result:
              if header:
                phaser_out.logTableTop(enum_out_stream.logfile, "Correlations for " + str(pdbid), False)
                for label in unique_list:
                  phaser_out.logTab(enum_out_stream.logfile,
                                    "Labels: #%-3d=%s" % (int(label), str(unique_list[label])))
                header = False
                phaser_out.logTab(enum_out_stream.logfile,
                                  "%6s %-12s %-15s %5s %5s %7s" % (
                                    "Labels",
                                    "Reindex",
                                    "RotationGroup",
                                    "CC",
                                    "Res'n",
                                    "#Refl"
                                  ))
              phaser_out.logTab(enum_out_stream.logfile,
                                "#%-5d %-12s %-15s %+5.2f %5.2f %7d" % (
                                  int(unique_dict[item["data"]]),
                                  item["reindex"],
                                  ic.spg1.split("(")[0],
                                  item["cc"],
                                  item["reso"],
                                  item["nref"]))
            if header == False:
              phaser_out.logTableEnd(enum_out_stream.logfile)
            if best_maxcc > -1.0:
              if best_pdbid is None:  # hack, set the first one if not set
                best_pdbid = str(pdbid)
              phaser_out.logTab(enum_out_stream.logfile, "Current best deposited data CC: " + str(
                round(best_maxcc, 2)) + " for " + best_pdbid)
            if ic.cc > best_maxcc:
              best_pdbid = str(pdbid)
              best_maxcc = ic.cc
              phaser_out.logTab(enum_out_stream.logfile, "New best CC " + str(round(best_maxcc, 2)))
              if goodcc:
                foundpdb = infol
                best_ope = ic.operation
                best_crystal = ic.crystal_match
                best_niggli = ic.niggli_match
                # miller_indexed = copy.deepcopy(ic.miller_indexed)
                # phaser_out.logTab(enum_out_stream.logfile,"  The deposited data associated to the structure shows a CC of "+str(ic.cc)+" against the given data")
                phaser_out.logChevron(enum_out_stream.logfile, "New isostructure match")
                topnlist.append((pdbid, copy.deepcopy(ic.cc), copy.deepcopy(ic.operation)))
          except:
            phaser_out.logTab(enum_out_stream.logfile,
                              "CIF of PDB: " + str(pdbid) + " cannot be correctly interpreted")
            phaser_out.logTab(enum_out_stream.logfile, "Skipping...")

          if os.path.exists(pdbid + ".cif"):
            os.remove(pdbid + ".cif")
        else:
          phaser_out.logTab(enum_out_stream.logfile, "Download unsuccessful")

        if foundpdb:
          nfoundpdb = nfoundpdb + 1
        if nfoundpdb >= TOPEXPLORE:  # if there are many matches, just look at the first 10
          phaser_out.logBlank(enum_out_stream.logfile)
          phaser_out.logBlank(enum_out_stream.logfile)
          phaser_out.logTab(enum_out_stream.logfile, "Reached limit of isostructures to explore")
          phaser_out.logBlank(enum_out_stream.logfile)
          break

      phaser_out.logBlank(enum_out_stream.logfile)
      phaser_out.logUnderLine(enum_out_stream.logfile, "Isostructure")
      if not foundpdb:
        phaser_out.logTab(enum_out_stream.logfile, "No isostructure matching criteria")
      if foundpdb:
        tpnl = [(v[0], v[1], v[2], True if v[1] == best_maxcc else False) for v in topnlist]
        tpn = {"type": "celldim", "list": tpnl}
        dictio = {"foundpdb": foundpdb, "cc": best_maxcc, "operation": best_ope, "topndict": tpn}

        phaser_out.logTab(enum_out_stream.logfile, "Found Isostructure: " + str(list(foundpdb.keys())[0]))
        phaser_out.logTab(enum_out_stream.logfile, "CC: " + str(round(dictio["cc"], 2)))
        # the operator returned from the compare algorithm is that for the setting of the crystal

        cannot_download = False
        datap = None
        pdbid = list(foundpdb.keys())[0]
        verbose = True
        try:
          # must use cif format, pdb format is not guaranteed eg 1bos
          datap = iotbx.pdb.fetch(pdbid.lower(), data_type="pdb", format="cif", mirror=self.mirror, log=None,
                                   force_download=False, local_cache=None).read().decode(
            system_utility.get_encoding())
          if verbose:
            print(sys.exc_info())
            traceback.print_exc(file=sys.stdout)
        except:
          try:
            datap = iotbx.pdb.fetch.fetch(pdbid.lower(), data_type="pdb", format="cif", mirror="pdbe", log=None,
                                     force_download=False, local_cache=None).read().decode(
              system_utility.get_encoding())
          except:
            if verbose:
              print(sys.exc_info())
              traceback.print_exc(file=sys.stdout)
            cannot_download = True

        if cannot_download:
          phaser_out.logTab(enum_out_stream.logfile, "Cannot download")
        if not cannot_download:
          pdbout = str(pdbid + ".pdb")
          phaser_out.logTab(enum_out_stream.logfile, "Write isostructure to file: " + (pdbout))
          pdbpath = os.path.join(phasertng_dir, pdbout)
          with open(pdbpath, "w") as fg:
            fg.write(datap)

          directd = working_params_global.voyager.isostructure_search.output_isostructure_to_directory
          if directd is not None:
            if not os.path.exists(directd): os.makedirs(directd)
            shutil.copyfile(pdbpath, os.path.join(directd, os.path.basename(pdbpath)))


        if not cannot_download:
          pdb_input = iotbx.pdb.input(source_info=str(pdbid), lines=flex.split_lines(str(datap)))
          #      ncop = reindex.reindexing_operators(mtzobj.crystal_symmetry(), pdb_input.crystal_symmetry().niggli_cell())
          #      above is undocumented and does not appear to be ronseal
          miller_arrays = mtzobj.file_object.as_miller_arrays(crystal_symmetry=mtzobj.crystal_symmetry())
          mtz_dataset = None
          left_out_labels = []
          for ma in miller_arrays:
            if ma.observation_type() is not None and \
                    not ma.is_complex_array() and \
                    not ma.is_hendrickson_lattman_array() and \
                    ma.is_real_array():
              try:
                labels = ma.info().labels
                if not mtz_dataset:
                  mtz_dataset = ma.as_mtz_dataset(column_root_label=labels[0])
                else:
                  mtz_dataset.add_miller_array(ma, column_root_label=labels[0])
              except:
                raise Exception("Cannot add miller array 1")
            else:
              # AJM TODO don't know how to add miller arrays that are not as above
              # looks like you have to use add column with flex array of data
              # there might be an easier way (?)
              labels = ma.info().labels
              left_out_labels.append(str(labels))  # str labels because it is an array
          #  mtz_dataset.add_miller_array(ma, column_root_label=labels)
          mtz_object = mtz_dataset.mtz_object()
          phaser_out.logTab(enum_out_stream.logfile, "Query to Niggli:    " + str(niggli_op.as_hkl()))
          phaser_out.logTab(enum_out_stream.logfile, "Niggli to Isostructure: " + str(best_niggli.inverse().as_hkl()))
          #      phaser_out.logTab(enum_out_stream.logfile,"Niggli to Isostructure: "+str(best_niggli.as_hkl()))
          phaser_out.logTab(enum_out_stream.logfile, "Reindexing operator:    " + str(best_ope))
          mtz_object.change_basis_in_place(cb_op=niggli_op)
          mtz_object.change_basis_in_place(cb_op=change_of_basis_op(best_ope))
          mtz_object.change_basis_in_place(cb_op=best_niggli.inverse())
          cbref = mtz_object.space_group_info().change_of_basis_op_to_reference_setting()
          phaser_out.logTab(enum_out_stream.logfile, "Query setting:      " + str(
            cbref.as_abc()))  # can't be hkl if there are translational elements to the operation
          mtz_object.change_basis_in_place(cb_op=cbref)
          # set the spacegroup to the match space group, which should just be transalation operators
          phaser_out.logTab(enum_out_stream.logfile, "Original Crystal:")
          for line in flex.split_lines(str(mtzobj.crystal_symmetry())):
            phaser_out.logTab(enum_out_stream.logfile, "  " + line)
          # the point group of the original data (mtz_object) will not change under cbop
          pg1 = mtz_object.space_group().build_derived_point_group().info().type().lookup_symbol()
          phaser_out.logTab(enum_out_stream.logfile, "  Point Group: " + str(pg1))
          # pdb_input is the new crystal
          phaser_out.logTab(enum_out_stream.logfile, "Isostructure Crystal:")
          for line in flex.split_lines(str(pdb_input.crystal_symmetry())):
            phaser_out.logTab(enum_out_stream.logfile, "  " + line)
          pg2 = pdb_input.crystal_symmetry().space_group().build_derived_point_group().info().type().lookup_symbol()
          phaser_out.logTab(enum_out_stream.logfile, "  Point Group: " + str(pg2))
          import re
          pg1str = re.sub(r'\([^)]*\)', '', pg1).strip()
          pg2str = re.sub(r'\([^)]*\)', '', pg2).strip()
          if (pg1str != pg2str):
            phaser_out.logBlank(enum_out_stream.logfile)
            phaser_out.logWarning(enum_out_stream.summary,
                                  "Point groups are not the same between structure and isostructure, expand or merge input data")
          else:
            mtz_object.set_space_group(pdb_input.crystal_symmetry().space_group())
            mtz_object = mtz_dataset.mtz_object()
            phaser_out.logTab(enum_out_stream.logfile,
                              "Reindexed Space Group : " + str(mtz_object.space_group_info()))

            newmtzname = os.path.basename(mtz)[:-4] + "_reindexed.mtz"
            newmtzname = os.path.join(phasertng_dir, newmtzname)
            phaser_out.logTab(enum_out_stream.logfile, "Columns removed from mtz file: ")
            if len(left_out_labels) > 0:
              phaser_out.logTabArray(enum_out_stream.logfile, left_out_labels)
            else:
              phaser_out.logTab(enum_out_stream.logfile, "(none)")
            phaser_out.logTab(enum_out_stream.logfile, "Write reindexed mtz: " + str(newmtzname))
            mtz_object.write(newmtzname)

            sio = StringIO()
            mtz_object.show_summary(out=sio, prefix="")
            lines = flex.split_lines(str(sio.getvalue()))
            # need to convert to standard setting
            outxt = []
            for line in lines:
              outxt.append(line)
            show_summary_where = enum_out_stream.verbose
            phaser_out.logBlank(show_summary_where)
            phaser_out.logTab(show_summary_where, "Mtz file summary")
            phaser_out.logTabArray(show_summary_where, outxt)
    #               save_copy_main_dir=False
    #               if save_copy_main_dir:
    #                   shutil.copyfile(mtz, os.path.join(".", newmtzname))
    #                   fileout = os.path.abspath(os.path.join(".", newmtzname))
    #                   phaser_out.logTab(enum_out_stream.logfile,"Writing reindexed file: " + fileout)
    #                   self.logger.info("Reindexed copy of input data saved at "+fileout)
    #               else:
    #                   phaser_out.logTab(enum_out_stream.logfile,"Isostructure not found")
    #                   nodes_to_return += [root]

    else:

      if mtzobj.file_type != "hkl":
        phaser_out.logBlank(enum_out_stream.logfile)
        phaser_out.logTab(enum_out_stream.logfile, "No data for correlation")
        phaser_out.logBlank(enum_out_stream.logfile)
      #  phaser_out.logTrailer(enum_out_stream.concise)
      #  return

      ic = simple_tools.Intensity_Correlation(mtz)
      coutxt = flex.std_string(ic.outxt)
      phaser_out.logTableTop(enum_out_stream.logfile, "Query Indexing", True)
      phaser_out.logTabArray(enum_out_stream.logfile, coutxt)
      phaser_out.logTableEnd(enum_out_stream.logfile)

      best_maxcc = -1.0
      best_crystal = None
      best_niggli = None
      best_ope = None

      phaser_out.logUnderLine(enum_out_stream.logfile, "Reference")
      compare_mtz = working_params_global.voyager.isostructure_search.reindex_as

      if compare_mtz is None:
        phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fileset, ""))
        phaser_out.logTrailer(enum_out_stream.concise)
        raise Exception("FATAL ERROR. File not set")
      elif not os.path.exists(compare_mtz):
        phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fileopen, str(compare_mtz)))
        phaser_out.logTrailer(enum_out_stream.concise)
        raise Exception(
          "FATAL ERROR. The path to the reindex file not correspond to a valid file. Please correct and restart the program.")
      else:
        phaser_out.logTab(enum_out_stream.logfile, "File: " + str(compare_mtz))

      #  pdb_input = iotbx.pdb.input(source_info=str(pdbid),lines=flex.split_lines(str(datap)))
      try:
        # CALCULATE CC HERE
        goodcc, result = ic.compare(compare_mtz, thresh=MINCCTHRESH)
        # end calculate
        phaser_out.logTab(enum_out_stream.logfile, "Niggli Basis: " + ic.niggli2.as_hkl())
        phaser_out.logTab(enum_out_stream.logfile, "Space Group: " + ic.spg2)
        phaser_out.logTab(enum_out_stream.logfile, "Unit Cell: " + str(ic.cell2))

        k = 1
        unique_dict = {}
        unique_list = {}
        for item in result:
          if (item["data"] in unique_dict):
            continue
          else:
            unique_dict[item["data"]] = int(k)
            unique_list[int(k)] = item["data"]
            k = k + 1

        header = True
        for item in result:
          if header:
            phaser_out.logTableTop(enum_out_stream.logfile, "Correlations", False)
            for label in unique_list:
              phaser_out.logTab(enum_out_stream.logfile, "Labels: #%-3d=%s" % (int(label), str(unique_list[label])))
            header = False
            phaser_out.logTab(enum_out_stream.logfile,
                              "%6s %-12s %-15s %5s %5s %7s" % (
                                "Labels",
                                "Reindex",
                                "RotationGroup",
                                "CC",
                                "Res'n",
                                "#Refl"
                              ))
          phaser_out.logTab(enum_out_stream.logfile,
                            "#%-5d %-12s %-15s %+5.2f %5.2f %7d" % (
                              int(unique_dict[item["data"]]),
                              item["reindex"],
                              ic.spg1.split("(")[0],
                              item["cc"],
                              item["reso"],
                              item["nref"]))
        if header == False:
          phaser_out.logTableEnd(enum_out_stream.logfile)
        best_maxcc = ic.cc
        phaser_out.logTab(enum_out_stream.logfile, "CC: " + str(round(best_maxcc, 2)))
        best_ope = ic.operation
        best_crystal = ic.crystal_match
        best_niggli = ic.niggli_match
      except:
        message = "Comparison cannot be correctly interpreted"
        phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fatal, message))
        phaser_out.logTrailer(enum_out_stream.concise)
        return

      if not goodcc:
        phaser_out.logWarning(enum_out_stream.summary, "Reindexing target does not match")
        phaser_out.logTrailer(enum_out_stream.concise)
        return

      miller_arrays = mtzobj.file_object.as_miller_arrays(crystal_symmetry=mtzobj.crystal_symmetry())
      mtz_dataset = None
      left_out_labels = []
      for ma in miller_arrays:
        if ma.observation_type() is not None and \
                not ma.is_complex_array() and \
                not ma.is_hendrickson_lattman_array() and \
                ma.is_real_array():
          try:
            labels = ma.info().labels
            if not mtz_dataset:
              mtz_dataset = ma.as_mtz_dataset(column_root_label=labels[0])
            else:
              mtz_dataset.add_miller_array(ma, column_root_label=labels[0])
          except:
            raise Exception("Cannot add miller array for reindexing")
        else:
          # AJM TODO don't know how to add miller arrays that are not as above
          # looks like you have to use add column with flex array of data
          # there might be an easier way (?)
          labels = ma.info().labels
          left_out_labels.append(
            str(labels) + " " + str(ma.observation_type()))  # str labels because it is an array
      #  mtz_dataset.add_miller_array(ma, column_root_label=labels)
      mtz_object = mtz_dataset.mtz_object()
      phaser_out.logTab(enum_out_stream.logfile, "Query to Niggli:    " + str(niggli_op.as_hkl()))
      phaser_out.logTab(enum_out_stream.logfile, "Niggli to Isostructure: " + str(best_niggli.inverse().as_hkl()))
      #      phaser_out.logTab(enum_out_stream.logfile,"Niggli to Isostructure: "+str(best_niggli.as_hkl()))
      phaser_out.logTab(enum_out_stream.logfile, "Reindexing operator:    " + str(best_ope))
      mtz_object.change_basis_in_place(cb_op=niggli_op)
      mtz_object.change_basis_in_place(cb_op=change_of_basis_op(best_ope))
      mtz_object.change_basis_in_place(cb_op=best_niggli.inverse())
      cbref = mtz_object.space_group_info().change_of_basis_op_to_reference_setting()
      phaser_out.logTab(enum_out_stream.logfile, "Query setting:      " + str(
        cbref.as_abc()))  # can't be hkl if there are translational elements to the operation
      mtz_object.change_basis_in_place(cb_op=cbref)
      # set the spacegroup to the match space group, which should just be transalation operators
      phaser_out.logTab(enum_out_stream.logfile, "Original Crystal:")
      for line in flex.split_lines(str(mtzobj.crystal_symmetry())):
        phaser_out.logTab(enum_out_stream.logfile, "  " + line)
      # this is the query data, won't change under cb_op
      pg1 = mtz_object.space_group().build_derived_point_group().info().type().lookup_symbol()
      phaser_out.logTab(enum_out_stream.logfile, "  Point Group: " + str(pg1))
      phaser_out.logTab(enum_out_stream.logfile, "Reference Crystal:")
      mtz2obj = iotbx.file_reader.any_file(str(compare_mtz))
      crystal2 = mtz2obj.crystal_symmetry()
      for line in flex.split_lines(str(crystal2)):
        phaser_out.logTab(enum_out_stream.logfile, "  " + line)
      pg2 = crystal2.space_group().build_derived_point_group().info().type().lookup_symbol()
      phaser_out.logTab(enum_out_stream.logfile, "  Point Group: " + str(pg2))

      import re
      pg1str = re.sub(r'\([^)]*\)', '', pg1).strip()
      pg2str = re.sub(r'\([^)]*\)', '', pg2).strip()
      if (pg1str != pg2str):
        phaser_out.logBlank(enum_out_stream.logfile)
        phaser_out.logWarning(enum_out_stream.summary,
                              "Point groups are not the same between structure and isostructure, expand or merge input data")
        phaser_out.logTrailer(enum_out_stream.concise)
        return

      mtz_object.set_space_group(crystal2.space_group())
      mtz_object = mtz_dataset.mtz_object()
      phaser_out.logTab(enum_out_stream.logfile, "Reindexed Space Group : " + str(mtz_object.space_group_info()))

      newmtzname = os.path.basename(mtz)[:-4] + "_reindexed.mtz"
      newmtzname = os.path.join(phasertng_dir, newmtzname)
      phaser_out.logTab(enum_out_stream.logfile, "Columns removed from mtz file: ")
      if len(left_out_labels) > 0:
        phaser_out.logTabArray(enum_out_stream.logfile, left_out_labels)
      else:
        phaser_out.logTab(enum_out_stream.logfile, "(none)")
      phaser_out.logTab(enum_out_stream.logfile, "Write reindexed mtz: " + str(newmtzname))
      mtz_object.write(newmtzname)

      sio = StringIO()
      mtz_object.show_summary(out=sio, prefix="")
      lines = flex.split_lines(str(sio.getvalue()))
      # need to convert to standard setting
      outxt = []
      for line in lines:
        outxt.append(line)
      show_summary_where = enum_out_stream.verbose
      phaser_out.logBlank(show_summary_where)
      phaser_out.logTab(show_summary_where, "Mtz file summary")
      phaser_out.logTabArray(show_summary_where, outxt)

    phaser_out.logTrailer(enum_out_stream.concise)
    return nodes_to_return

    print('SHERLOCK check')
    print(1 / 0)

  except Exception as e:
      traceback.print_exc(file=sys.stdout)

  return basetng


if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=True)
  arguments = parser.parse_args()
  user_phil = arguments.phil.read()
  isostructure_code(user_phil)

