from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from future.standard_library import install_aliases
install_aliases()
from libtbx.utils import Sorry

def run():
  # Set up to import voyager functions
  import os,sys
  module_path = os.path.dirname(os.path.abspath(__file__))
  src = os.path.join(module_path, "../../../src/")
  sys.path.append(src)

  import argparse
  parser = argparse.ArgumentParser(
          description='Dock model into cryo-EM map with simple interface')
  parser.add_argument('--map1',help='Map file for half-map 1')
  parser.add_argument('--map2', help='Map file for half-map 2')
  parser.add_argument('--d_min', help='d_min for maps', type=float)
  parser.add_argument('--model_file', help='Search model')
  parser.add_argument('--model_vrms', help='vrms for model', type=float, default=1.5)
  parser.add_argument('--fixed_model_file', help='optional fixed model')
  parser.add_argument('--sequence_composition',
                      help='Sequence file with composition for full reconstruction')
  parser.add_argument('--output_folder', help='folder to put output PDB and mtz files')
  parser.add_argument('--top_files', help='Maximum number of top files to save',
                      type=int, default = None)
  parser.add_argument('--level', help='Output level from process to testing',
                      default='process')

  args = parser.parse_args()
  map1 = args.map1
  map2 = args.map2
  if map is None and (map1 is None or map2 is None):
    raise Sorry("Either full map or half-maps must be given")
  d_min = args.d_min
  model_file = args.model_file
  model_vrms = args.model_vrms
  sequence_composition = args.sequence_composition
  output_folder = args.output_folder
  top_files = args.top_files
  level = args.level
  if sequence_composition is None:
    raise Sorry("Sequence for composition must be provided")

  from New_Voyager.scripts.em_placement_script import emplace_simple
  results = emplace_simple(
      map1, map2, d_min,
      sequence_composition, model_file,
      model_vrms = model_vrms,
      output_folder = output_folder,
      top_files = top_files,
      level = level)
  print("\nNumber of solutions found: ",results.nsolfound)
  nkept = len(results.top_models)
  if results.nsolfound > 1:
    print("   of which ", nkept, " are kept")
  for i in range(nkept):
    modelnum = i+1
    print("For docking solution ", modelnum, ":")
    print("   mapLLG = ",results.top_models_mapLLG[i])
    print("   mapCC  = ",results.top_models_mapCC[i])

if __name__ == "__main__":
  run()
