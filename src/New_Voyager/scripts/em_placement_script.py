###################################################################################################
#                                          LICENSE                                                #
# PhaserVoyager is distributed under three different licences                                     #
#                                                                                                 #
# [Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)                 #
# [CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)                         #
# [Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)                     #
#                                                                                                 #
# By accessing the source code in this repository you agree to be bound by one of these licences. #
#                   (c) 2000-2024 Cambridge University Technical Services Ltd                     #
#                                                                                                 #
###################################################################################################

from __future__ import division
from __future__ import print_function
import argparse
import traceback
import math
import sys,os,shutil

try:
  from libtbx import group_args
  from libtbx.phil import parse
  from libtbx.utils import Sorry
  from phasertng.phil.master_phil_file import build_phil_str
  from phasertng.phil.converter_registry import converter_registry
  from phasertng.phil.phil_to_cards import phil_to_cards
  from phasertng.phil.master_node_file import *
  from phasertng.phil.converter_registry import *
  from phasertng import Phasertng
  from phasertng import PhaserError
  from phasertng import Output
  from phasertng import enum_out_stream, enum_err_code, enum_entry_data
  from libtbx.utils import multi_out
  import iotbx
  from iotbx.data_manager import DataManager
  from iotbx.map_model_manager import map_model_manager
  from cctbx import crystal
  from cctbx.array_family import flex
  from cctbx.maptbx import prepare_map_for_docking
  from cctbx.maptbx.prepare_map_for_docking import local_mean_density
  from cctbx.maptbx.prepare_map_for_docking import get_ordered_volume_exact
  from New_Voyager._prepare_docking_sphere import prepare_docking_sphere
  # NOTE CM: if something in the structure of the repo changes these paths manipulation lines should be adapted
  module_path = os.path.dirname(os.path.abspath(__file__))
  sys.path.append(os.path.join(module_path, "../"))
  from New_Voyager import simple_tools
  from New_Voyager import system_utility
except Exception as e:
  traceback.print_exc(file=sys.stdout)
  raise Sorry('There was an error with the cctbx/phasertng/phaser_voyager imports')

@system_utility.timing

def access_nodes(basetng):
  # NOTE CM: potentially better to move it somewhere else, either in the phasertng code or in the voyagerbase
  # Access the content of the dag database
  phil_dagdatabase = basetng.DAGDATABASE.phil_str()
  master_phil = parse(build_node_str(), converter_registry=converter_registry)
  all_working_params = []
  # the splitter is the single line brace at the end of each phaserdag block
  # the end of the file is "\n}"
  splitter = "\n}\n"
  f_nodes = phil_dagdatabase.split(splitter)
  for f_node in f_nodes:
    if len(f_node) > 10:  # hack removes the splitter and final splitter
      f_node = f_node + splitter  # need to put the splitter back for parsing
      user_phil = parse(f_node, converter_registry=converter_registry)
      working_phil = master_phil.fetch(source=user_phil).extract()  # NOT diff
      all_working_params.append(working_phil)
  return all_working_params

def get_mw_and_volume_from_sequences(sequences):
  # Must be a list or tuple to enumerate
  if type(sequences) not in [type((1,2,)), type([1,2,3])]:
    sequences = [sequences]
  from iotbx.bioinformatics import chain_type_and_residues
  protein_mw = 0
  nucleic_mw = 0
  total_volume = 0
  # get chain type and residues: currently just use given chain type and count residues
  # update protein_mw and/or nucleic_mw with information from composition
  for ind,seque in enumerate(sequences):
    chain_type, n_residues = chain_type_and_residues(text=seque)
    if n_residues is None or n_residues <= 0:
      continue
    if chain_type == 'PROTEIN':
      mw_residue = 110.0  # from $CDOC/matthews.doc
    else:
      mw_residue = 330.0  # guess for DNA/RNA
    mw_this_chain = mw_residue*n_residues
    if chain_type == 'PROTEIN':
      protein_mw = protein_mw+mw_this_chain
    else:
      nucleic_mw = nucleic_mw+mw_this_chain
  # Provide float value or leave as None if not present in composition
  if protein_mw == 0:
      protein_mw = None
  else:
    total_volume += protein_mw*1.229
  if nucleic_mw == 0:
    nucleic_mw == None
  else:
    total_volume += nucleic_mw*0.945
  return protein_mw, nucleic_mw, total_volume

def generate_hexagonal_grid(box, model_radius, sphere_radius, mask_radius):
  # In orthogonal space, the hexagonal close-packed steps are defined in terms
  # of the nearest-neighbour distance, dist. The largest dist that ensures the
  # model will be enclosed in at least one of the sampling spheres is worked out
  # from the sphere_radius and model_radius.
  # Overfill box, change origin so that a tetrahedral void will be at the center,
  # then keep the grid points that are within the ordered radius.

  # Note that when model_radius is nearly as large as sphere_radius, the hcp
  # distance becomes very small so many points are generated. Inflating the
  # sphere_radius by 10% helps considerably without decreasing worst case overlap
  # by more than a percent or two in the range we use (sphere_radius >= 1.15*model_radius).

  assert(sphere_radius > model_radius) # Fails otherwise!
  dist = math.sqrt(8./3)*(1.1*sphere_radius - model_radius)

  steps = dist * flex.double((1.,math.sqrt(3./4),math.sqrt(2./3)))
  center = flex.double(box)/2
  xyzmin = flex.double((-dist,-dist,-dist))
  xyzmax = flex.double(box) + flex.double((dist,dist,dist))
  off1 = 5./6
  tmp_grid_points = []
  nraw = 0
  import numpy as np
  # The offset changes from line to line and plane to plane.
  for z in np.arange(xyzmin[2], xyzmax[2],steps[2]):
    off1 = 1. - off1
    off0 = 3./4
    for y in np.arange(xyzmin[1] + off1*steps[1], xyzmax[1], steps[1]):
      off0 = 1. - off0
      for x in np.arange(xyzmin[0] + off0*steps[0], xyzmax[0], steps[0]):
        tmp_grid_points.append([x, y, z])
        nraw += 1
  # Find lattice point closest to center of box
  dsqr_min = flex.sum(flex.pow2(flex.double(box)))
  ibest = -1
  for i in range(nraw):
    test_point = flex.double(tmp_grid_points[i])
    dsqr_to_center = flex.sum(flex.pow2(test_point-center))
    if dsqr_to_center < dsqr_min:
      ibest = i
      dsqr_min = dsqr_to_center

  grid_points = []
  # Translate to put a tetrahedral void in the center of the box
  tetvoid = (dist/2) * flex.double((1.,1./math.sqrt(3.),1/math.sqrt(6.)))
  tvec = center - flex.double(tmp_grid_points[ibest]) - tetvoid
  npts = 0
  for i in range(nraw):
    test_point = flex.double(tmp_grid_points[i]) + tvec
    if (flex.min(test_point) <= 0.
        or test_point[0] >= box[0] or test_point[1] >= box[1] or test_point[2] >= box[2]):
      continue
    d_to_center = math.sqrt(flex.sum(flex.pow2(test_point-center)))
    if d_to_center >= mask_radius:
      continue
    grid_points.append(list(test_point))
    npts += 1
  return grid_points

def setup_search_subvolumes(mm_ordered_mask, mask_radius, sphere_radius, model_radius, target_volume):
  """ Make a list of overlapping search volumes inside ordered density

  Generate a set of overlapping spheres on a hexagonal grid, and list the ones
  containing a required fraction of ordered volume.

  Arguments:
  mm_ordered_mask: map_manager containing ordered mask
  mask_radius: radius that encloses almost all ordered density
  sphere_radius: target radius for subvolume spheres
  model_radius: radius containing model coordinates
  target_volume: volume of ordered density that should be in sphere to contain entire model
  """

  mask_spherical_average = local_mean_density(mm_ordered_mask, sphere_radius)
  sphere_volume = (4./3) * math.pi * sphere_radius**3
  uc = mm_ordered_mask.unit_cell()
  ucpars = uc.parameters()
  box = [ucpars[0],ucpars[1],ucpars[2]]
  # Spherical average method can be deceived if box is cut too tightly around
  # reconstruction so that sphere can include ordered volume in neighbouring cells
  mincell = min(box)
  check_exact_volume = (mincell/2 < (mask_radius + sphere_radius))
  grid_points = generate_hexagonal_grid(box,model_radius,sphere_radius,mask_radius)
  box = flex.double(box)

  nkept = 0
  regions_dict = {}
  for i in range(len(grid_points)):
    sphere_center_orth = grid_points[i]
    sphere_center_frac = uc.fractionalize(sphere_center_orth)
    sphere_center_grid = [int(round(n * f)) for n,f in zip(mm_ordered_mask.map_data().all(), sphere_center_frac)]
    ordered_volume = mask_spherical_average.map_data()[sphere_center_grid] * sphere_volume

    if ordered_volume >= target_volume:
      if check_exact_volume:
        ordered_volume = get_ordered_volume_exact(mm_ordered_mask,sphere_center_orth,sphere_radius)
      if (ordered_volume >= target_volume):
        nkept += 1
        region_name = "sphere" + str(nkept)
        regions_dict[region_name] = {}
        regions_dict[region_name]['center'] = sphere_center_orth
        regions_dict[region_name]['radius'] = sphere_radius

  # Fallback to ensure at least one region: should never happen
  if nkept == 0:
    region_name = "sphere0"
    regions_dict[region_name] = {}
    regions_dict[region_name]['center'] = [box[0]/2,box[1]/2,box[2]/2]
    regions_dict[region_name]['radius'] = sphere_radius
    nkept = 1

  return regions_dict

def prepare_model_free_data(working_params_global,phasertng_dir,hklfile,basetng):
  working_params_global.em_placement.phasertng.mode = ['imtz', 'data', 'aniso', 'feff']
  working_params_global.em_placement.phasertng.suite.database = phasertng_dir
  working_params_global.em_placement.phasertng.reflections.read_map = ['True']
  working_params_global.em_placement.phasertng.hklin.filename = hklfile
  working_params_global.em_placement.phasertng.macaniso.macrocycle1 = ['scale','bins']
  working_params_global.em_placement.phasertng.tncs.order = 1
  cards = phil_to_cards(user_phil=working_params_global.em_placement, program_name="InputCard")
  basetng.run(cards)
  if not basetng.success():
    raise Sorry("Failure in model-independent data preparation")
  return basetng

def prepare_model_dependent_data(working_params_global,master_phil_tng,modified_phil,
    scatfrac,mol_search,basetng,d_min,ellg_target=None,boxscale=None):
  output_prev_phil = parse(basetng.phil_str(),converter_registry=converter_registry)
  working_params_tng = master_phil_tng.fetch(sources=[modified_phil,output_prev_phil]).extract()
  if d_min is None or d_min <= 0.:
    raise Sorry("d_min must be positive for prepare_model_dependent_data")
  fsol1 = 0.9 # High-resolution limit of fsol
  fsol2 = 0.2 # Low-resolution limit of fsol, to pay more attention to low-res data
  d_mid = 12. # Switchover point for fsol in sigmoid function
  k_slope = 1./3. # How fast is switchover
  fsol = fsol2 - (fsol2-fsol1)/(1.+math.exp(-k_slope*(d_mid-d_min)))
  working_params_tng.phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol = fsol
  if ellg_target is not None:
    working_params_tng.phasertng.expected.ellg.target = ellg_target
  working_params_tng.phasertng.mode = ['cca','ccs']
  # NOTE CM: now setting the composition as the fraction of the full map
  if working_params_global.em_placement.map_model.fixed_scattering_ratio is None:
    working_params_tng.phasertng.biological_unit.sequence.filename = os.path.abspath(working_params_global.em_placement.map_model.sequence_composition)
  else:
    working_params_tng.phasertng.biological_unit.model.filename = os.path.abspath(mol_search.model_file)
  if boxscale is not None:
    working_params_tng.phasertng.molecular_transform.boxscale = boxscale
  working_params_tng.phasertng.composition.number_in_asymmetric_unit = scatfrac
  working_params_tng.phasertng.composition.map = True
  cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
  basetng.run(cards)
  if not basetng.success():
    raise Sorry("Failure in model-dependent data preparation at cca+ccs steps")

  output_prev_phil = parse(basetng.phil_str(),converter_registry=converter_registry)
  working_params_tng = master_phil_tng.fetch(sources=[modified_phil,output_prev_phil]).extract()
  # Preparation and ensembling of the model up to the point of eLLG calc
  working_params_tng.phasertng.mode = ['esm','ellg']
  working_params_tng.phasertng.model.filename = mol_search.model_file
  working_params_tng.phasertng.model.vrms_estimate = mol_search.starting_model_vrms
  cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
  basetng.run(cards)
  if not basetng.success():
    raise Sorry("Failure in model-dependent data preparation in eca to ellg steps")
  return basetng

def em_placement_code(user_phil_str=""):

  try:
    sys.stdout.flush() # Flush prints from interpreting phil before calling this code
    # Initialise variables that are returned now, in case of crash before end
    nsolfound = 0
    top_models_focus = []
    top_models_map = []
    top_models_mapLLG = []
    top_models_mapCC = []
    top_models_RT = []
    top_models_cell_scale = []

    ######################################################################
    # Preparing the basetng and em_placement objects, and the phil input #
    ######################################################################
    out_wrapper = multi_out()
    basetng = Phasertng("VOYAGER")
    basetng.set_file_object(out_wrapper)
    phasertng_dir = os.path.abspath(simple_tools.create_directory_timestamp("phasertng_"))

    # Get the masterphil with all the defaults from phasertng
    initparams = build_phil_str(program_name="InputCard")
    assert (len(initparams))
    master_phil_tng = parse(initparams, converter_registry=converter_registry)
    # Merge em_placement and phasertng phil scopes
    from New_Voyager.programs.em_placement_phil import em_placement_scope
    merged_phil_str = 'em_placement { ' + em_placement_scope + initparams + '}'
    master_phil_global = parse(merged_phil_str,converter_registry=converter_registry)
    # And get those values which have been set by the user
    user_phil = iotbx.phil.parse(user_phil_str)
    working_params_global = master_phil_global.fetch(sources=[user_phil]).extract()
    modified_phil = master_phil_tng.format(python_object=working_params_global)
    output_prev_phil = parse(basetng.phil_str(),converter_registry=converter_registry)
    # Save original boxscale to use for focused refinement
    working_params_tng = master_phil_tng.fetch(sources=[modified_phil,output_prev_phil]).extract()
    original_boxscale = working_params_tng.phasertng.molecular_transform.boxscale
    if working_params_global.em_placement.docking.save_output_folder:

      #######################
      # Prepare the output  #
      #######################
      # Prepare the output folder, with a unique name related to phasertng_dir, and the output file
      # There is probably a more elegant way of doing this...
      if working_params_global.em_placement.docking.output_folder is not None:
        folder_output_files = working_params_global.em_placement.docking.output_folder
      else:
        tngdir_parts = os.path.split(phasertng_dir)
        folder_output_files = os.path.join(tngdir_parts[0], 'emplaced_files_' + tngdir_parts[1].split('_')[-1])
      from pathlib import Path
      Path(folder_output_files).mkdir(parents=True, exist_ok=True)

      output_table_path = os.path.join(folder_output_files, 'output_table_docking.csv')
      if not os.path.exists(output_table_path):
        fichi_table = open(output_table_path, 'w')
        fichi_table.write('Solution_name,Model,Docking_region,mapLLG,Z-score,Map-ModelSol-CC,Mtz-ModelSol-CC,Annotation,VRMS\n')
      else:
        fichi_table = open(output_table_path, 'a')
      del fichi_table

    ####################################################
    # Prepare the logging and formatting of the output #
    ####################################################
    primary_stream = sys.stdout # This is where a pointer to an output stream could be set
    out_wrapper.register("Primary output stream", primary_stream)
    level = working_params_global.em_placement.level
    if level == "process":
      working_params_global.em_placement.phasertng.suite.level = "silence" # Not an option yet in em_placement
    else:
      working_params_global.em_placement.phasertng.suite.level = level
    # Translate output level into verbosity number for prepare_map_for_docking
    # RJR note: These should be rationalised later!
    verbosity = 1 # default if level keyword not recognised
    if level == "silence" or level == "process" or level == "concise" or level == "summary":
      verbosity = 0
    elif level == "logfile":
      verbosity = 1
    elif level == "verbose":
      verbosity = 2
    elif level == "testing":
      verbosity = 4
    phaser_out = Output("VOYAGER")
    phaser_out.set_file_object(out_wrapper)
    phaser_out.SetLevel(level)  # set both
    phaser_out.SetStore(level)
    phaser_out.logHeader(enum_out_stream.concise, "EM-PLACEMENT data preparation")
    # at the end close file log_file.close()
    phaser_out.logUnderLine(enum_out_stream.logfile, "Data reading")

    #######################################################################
    # Getting the input information from the configuration parsed phil    #
    #######################################################################
    full_map = working_params_global.em_placement.map_model.full_map
    if full_map is not None:
      full_map_file_path = os.path.abspath(working_params_global.em_placement.map_model.full_map)
      phaser_out.logTab(enum_out_stream.logfile,
          "Full map location:   " + os.path.basename(full_map_file_path))

    if len(working_params_global.em_placement.map_model.half_map) == 2:
      half_map_1_file_path = os.path.abspath(working_params_global.em_placement.map_model.half_map[0])
      half_map_2_file_path = os.path.abspath(working_params_global.em_placement.map_model.half_map[1])
      phaser_out.logTab(enum_out_stream.logfile,
          "Half maps locations: " + os.path.basename(half_map_1_file_path))
      phaser_out.logTab(enum_out_stream.logfile,
          "                     " + os.path.basename(half_map_2_file_path))
      have_half_maps = True
    else:
      have_half_maps = False

    # symmetry, best resolution and top_files information
    best_res = working_params_global.em_placement.map_model.best_resolution
    point_sym = working_params_global.em_placement.map_model.point_group_symmetry
    top_files = working_params_global.em_placement.docking.top_files
    if best_res is None or best_res <= 0.:
      phaser_out.logTab(enum_out_stream.logfile, "Map resolution will be estimated")
      best_res = None
      guess_d_min = True
    else:
      guess_d_min = False
      phaser_out.logTab(enum_out_stream.logfile, "Best provided resolution is:     " + str(best_res))
    search_d_min = best_res
    phaser_out.logTab(enum_out_stream.logfile,'Provided symmetry of the reconstruction is '+str(point_sym))
    if point_sym == 'helical':
      include_helical_symmetry = True
    else:
      include_helical_symmetry = False

    ########################################################################################
    # Getting the available number of cores to use more in certain map model manager calls #
    ########################################################################################
    phaser_out.logBlank(enum_out_stream.logfile)
    phaser_out.logUnderLine(enum_out_stream.logfile, 'System checking')
    cpu_available = system_utility.get_number_of_cpus()
    nproc_to_use = round(cpu_available / 2)  # just in case for now
    phaser_out.logTab(enum_out_stream.logfile, 'Number of cpus available is ' +str(cpu_available))
    phaser_out.logTab(enum_out_stream.logfile, 'Maximum cores used '+str(nproc_to_use))

    #######################################################################
    # Reading into the map model manager and interpreting the symmetry    #
    #######################################################################
    dm = DataManager()  # Initialize the DataManager and call it dm
    dm.set_overwrite(True)  # tell the DataManager to overwrite files with the same name?
    # Reading in all the maps, the full (if provided) and the two half maps
    if full_map is not None:
      dm.process_real_map_file(full_map_file_path)
      mm = dm.get_real_map(full_map_file_path)
    else:
      mm = None
    if len(working_params_global.em_placement.map_model.half_map) == 2:
      dm.process_real_map_file(half_map_1_file_path)
      mmh1 = dm.get_real_map(half_map_1_file_path)
      dm.process_real_map_file(half_map_2_file_path)
      mmh2 = dm.get_real_map(half_map_2_file_path)
    else:
      mmh1 = None
      mmh2 = None
    mmm = map_model_manager(map_manager=mm, map_manager_1=mmh1, map_manager_2=mmh2)
    # Keep track of origin shift, if any
    origin_shift_grid = mmm.map_manager().origin_shift_grid_units
    if flex.sum(flex.abs(flex.double(origin_shift_grid))) > 0:
      origin_shift = []
      unit_cell = mmm.map_manager().unit_cell().parameters()[:3]
      unit_cell_grid = mmm.map_manager().unit_cell_grid
      for a,n,o in zip(unit_cell, unit_cell_grid, origin_shift_grid):
        origin_shift.append(a*o/n)
      phaser_out.logTab(enum_out_stream.logfile,"Origin shift in input maps: "+ str(origin_shift))
    else:
      origin_shift = [0.,0.,0.]

    if ((working_params_global.em_placement.map_model.fixed_scattering_ratio is not None)
        and ((working_params_global.em_placement.map_model.region_approach == "manual")
          or (working_params_global.em_placement.map_model.region_approach == "rigid_body"))
        or (not have_half_maps)):
      determine_ordered_volume = False
      ordered_mask_id = None
    else:
      determine_ordered_volume = True
      #####################################################################################
      # Add map_model_manager containing mask indicating ordered volume of reconstruction #
      #####################################################################################
      # Need to get the number of chains that are protein and the ones that are nucleic
      # NOTE CM: just a workaround to get some multifastas for the composition at the moment
      # possibly should go to a script
      from iotbx.bioinformatics import get_sequences
      sequence = open(working_params_global.em_placement.map_model.sequence_composition).read()
      sequences = get_sequences(text=sequence)

      phaser_out.logUnderLine(enum_out_stream.logfile, "Establish ordered volume of reconstruction")
      protein_mw, nucleic_mw, total_volume = get_mw_and_volume_from_sequences(sequences)
      ordered_mask_id = 'ordered_volume_mask'
      # add_ordered_volume_mask was taking too much time, now a bit better
      # import line_profiler
      # lp = line_profiler.LineProfiler(prepare_map_for_docking.add_ordered_volume_mask)
      # lp.enable()
      prepare_map_for_docking.add_ordered_volume_mask(mmm, d_min=best_res, protein_mw=protein_mw, nucleic_mw=nucleic_mw, ordered_mask_id=ordered_mask_id)
      # lp.disable()
      # lp.print_stats()
      mm_ordered_mask = mmm.get_map_manager_by_id(ordered_mask_id)
      cell_volume = mm_ordered_mask.unit_cell().volume()
      masked_volume = flex.mean(mm_ordered_mask.map_data()) * cell_volume
      target_completeness = 0.98
      mask_radius = prepare_map_for_docking.get_mask_radius(mmm.get_map_manager_by_id(ordered_mask_id),target_completeness)
      phaser_out.logTab(enum_out_stream.logfile,"Radius of sphere containing "
          +  str(100*target_completeness) +  " percent of ordered volume: "+str(mask_radius))

    mmm.set_multiprocessing(nproc=nproc_to_use, multiprocessing='multiprocessing')

    # Find the symmetry or set it up
    # you can limit the search if you have info about the symmetry
    if point_sym not in ['C1', 'c1']:  # maybe change default from the argument to something else, no symmetry might
      # still be a case in which it was not applied to the reconstruction and you want to find??
      # constraining to a symmetry
      if include_helical_symmetry:
        mmm.get_ncs_from_map(symmetry='helical', include_helical_symmetry=include_helical_symmetry)
      else:
        mmm.get_ncs_from_map(symmetry=point_sym)
      # without constraining it (slower, as it tries all of the possible ones)
      # mmm.get_ncs_from_map()
      phaser_out.logUnderLine(enum_out_stream.logfile,'Symmetry interpretation')
      phaser_out.logTab(enum_out_stream.logfile, str(mmm.ncs_object()))
      phaser_out.logBlank(enum_out_stream.logfile)

    ###################################################################################
    # Specifying the input regions of the density where the docking will be performed #
    ###################################################################################
    # Handle multiple molecule search later...
    if working_params_global.em_placement.biological_unit.molecule is None:
      raise Sorry("No search molecule specified")
    mol_search = working_params_global.em_placement.biological_unit.molecule
    search_model = dm.get_model(mol_search.model_file)
    # Check for fixed model
    mol_fixed = working_params_global.em_placement.biological_unit.molecule_fixed
    fixed_model_present = mol_fixed.fixed_model_file is not None
    if fixed_model_present:
      fixed_model = dm.get_model(mol_fixed.fixed_model_file)

    # Kludge for handling mmCIF format model files
    if search_model.input_model_format_cif():
      # Write out PDB-format file needed by phasertng in a temporary directory
      import tempfile
      tmpPDBdirname = tempfile.mkdtemp()
      mol_search.model_file = os.path.join(tmpPDBdirname, "search_model.pdb")
      dm.write_model_file(search_model, mol_search.model_file, format = "pdb")
    if fixed_model_present and fixed_model.input_model_format_cif():
      # Write out PDB-format file needed by phasertng in a temporary directory
      import tempfile
      tmpPDBdirname = tempfile.mkdtemp()
      mol_fixed.fixed_model_file = os.path.join(tmpPDBdirname, "fixed_model.pdb")
      dm.write_model_file(fixed_model, mol_fixed.fixed_model_file, format = "pdb")

    model_sequences = []
    for m in search_model.as_model_manager_each_chain():
      chain_sequence = ''.join(m.as_sequence())
      if chain_sequence:
        model_sequences.append(chain_sequence)
    pmw, nmw, model_volume = get_mw_and_volume_from_sequences(model_sequences)
    if fixed_model_present:
      model_sequences = []
      for m in fixed_model.as_model_manager_each_chain():
        chain_sequence = ''.join(m.as_sequence())
        if chain_sequence:
          model_sequences.append(chain_sequence)
      pmw, nmw, fixed_model_volume = get_mw_and_volume_from_sequences(model_sequences)
    else:
      fixed_model_volume = 0.
    if (working_params_global.em_placement.map_model.fixed_scattering_ratio is None):
      if model_volume + fixed_model_volume > total_volume:
        raise Sorry("Model(s) exceed total specified composition of reconstruction")
    model_center, model_radius = prepare_map_for_docking.sphere_enclosing_model(search_model)
    phaser_out.logUnderLine(enum_out_stream.logfile, str('Model to place is '+mol_search.molecule_name))
    if fixed_model_present:
      phaser_out.logUnderLine(enum_out_stream.logfile, str('Fixed model is '+mol_fixed.fixed_molecule_name))
      mmm.add_model_by_id(model=fixed_model, model_id='fixed_model')
      mmm.generate_map(model=fixed_model, d_min=best_res, map_id='fixed_atom_map')
      prepare_map_for_docking.mask_fixed_model_region(mmm, best_res,
                                fixed_model=fixed_model,
                                ordered_mask_id=ordered_mask_id,
                                fixed_mask_id='mask_around_atoms')

    regions_dict = {}
    cards = phil_to_cards(user_phil=working_params_global.em_placement, program_name="InputCard")
    basetng.run(cards) # Initialise basetng parameters
    if not basetng.success():
      raise Sorry("Failure in basetng initialisation")

    if working_params_global.em_placement.map_model.region_approach == 'manual':
      for num,region in enumerate(working_params_global.em_placement.map_model.region_for_docking):
        if region.name == None:
          message = "Please provide a name for each docking region"
          phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fatal, message))
          phaser_out.logTrailer(enum_out_stream.concise)
          raise Exception("FATAL ERROR: " + message)
        else:
          phaser_out.logUnderLine(enum_out_stream.logfile, 'Checking docking region ' + str(num+1))
        regions_dict[region.name] = {}

        regions_dict[region.name]['center'] = [region.center.x, region.center.y, region.center.z]
        regions_dict[region.name]['radius'] = region.radius
        phaser_out.logTab(enum_out_stream.logfile,"Name of the region : "+region.name)
        phaser_out.logTab(enum_out_stream.logfile, "Center of the region:  " +
                  str([region.center.x, region.center.y, region.center.z]))
        phaser_out.logTab(enum_out_stream.logfile,
                  "Radius of the region:  " + str(region.radius))

    elif working_params_global.em_placement.map_model.region_approach == 'rigid_body':
      phaser_out.logUnderLine(enum_out_stream.logfile, 'Set up refinement region around current position')
      region_name = "modelsphere"
      regions_dict[region_name] = {}
      regions_dict[region_name]['center'] = model_center
      regions_dict[region_name]['radius'] = model_radius + best_res/2.

    elif working_params_global.em_placement.map_model.region_approach == 'automatic':
      # Allow 5% slop in minimal ordered volume for search sphere
      target_volume = 1.05*(model_volume / total_volume) * masked_volume
      # Use eLLG for rotation search to decide on divide-and-conquer strategy
      phaser_out.logUnderLine(enum_out_stream.logfile, 'Set up automated docking region(s)')
      # Check whether eLLG will allow search over entire map
      center = list(flex.double(mmh1.unit_cell().parameters()[:3])/2 + flex.double(origin_shift))
      region_name = "bigsphere"
      regions_dict[region_name] = {}
      regions_dict[region_name]['center'] = center
      regions_dict[region_name]['radius'] = mask_radius
      results = prepare_docking_sphere(
                        map_model_manager = mmm,
                        d_min = best_res,
                        sphere_cent = regions_dict[region_name]['center'],
                        radius = regions_dict[region_name]['radius'],
                        phasertng_dir = phasertng_dir,
                        region_name = region_name,
                        verbosity = verbosity,
                        log = sys.stdout)
      regions_dict[region_name]['results_correction'] = results
      hklfile = regions_dict[region_name]['results_correction']['path_mtz']
      if best_res is  None or best_res <= 0.:
        this_d_min = regions_dict[region_name]['results_correction']['mtz_resolution']
      else:
        this_d_min = best_res
      basetng = prepare_model_free_data(working_params_global,phasertng_dir,hklfile,basetng)
      # Set scatfrac to 1 instead of leaving at 0.98, in case we're placing entire assembly
      scatfrac = 1.
      # Set an ellg target suitable for rotation search only. Aim for a generous number
      # so that orientations can be as clear as possible.
      # Set boxscale to 4 to save time and memory, then reset for focused docking
      basetng = prepare_model_dependent_data(working_params_global,master_phil_tng,
          modified_phil,scatfrac,mol_search,basetng,this_d_min,
          ellg_target=working_params_global.em_placement.docking.frf_target_ellg,
          boxscale=4.)
      output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)
      working_params_tng = master_phil_tng.fetch(sources=[modified_phil,output_prev_phil]).extract()
      all_working_params = access_nodes(basetng)
      ellg_result = all_working_params[0].phaserdag.node.seek_components_ellg
      if guess_d_min:
        # Save overall resolution estimate in case we dock in sub-volumes
        search_d_min = all_working_params[0].phaserdag.node.signal_resolution_phased
      else:
        search_d_min = best_res
      if working_params_global.em_placement.docking.brute_approach == 'search':
        ellg_min = working_params_global.em_placement.docking.minimum_frf_ellg
        # If number of subvolumes not too large, better to go for more signal in FRF
        try_ellg_min = 4*ellg_min
        if ellg_result < try_ellg_min:
          # We can't achieve even this signal searching over whole ordered density.
          # How large can subvolume be to achieve this minimal signal for the best subvolume?
          # The goal is to keep strong signal in the rotation search for the best subvolume,
          # without having too many subvolumes.
          region_radius = regions_dict[region_name]['radius']
          sphere_radius = region_radius * math.pow(ellg_result/try_ellg_min,1./3)
          # With sphere_radius < 1.15*model_radius, definitely too many spheres
          if (sphere_radius < 1.15*model_radius) and (try_ellg_min > ellg_min):
            try_ellg_min = ellg_min
            sphere_radius = region_radius * math.pow(ellg_result/try_ellg_min,1./3)
          if sphere_radius < 1.15*model_radius:
            phaser_out.logTab(enum_out_stream.logfile, "Use brute-force rotation: eLLG too low for FRF")
            working_params_global.em_placement.docking.brute_approach = 'six_dimensional_full'
          else:
            mm_ordered_mask = mmm.get_map_manager_by_id(ordered_mask_id)
            regions_dict = setup_search_subvolumes(mm_ordered_mask, mask_radius,
                sphere_radius, model_radius, target_volume)
            if (len(regions_dict) > 100) and (try_ellg_min > ellg_min):
              # Better off using fewer subvolumes with lower signal
              sphere_radius = region_radius * math.pow(ellg_result/ellg_min,1./3)
              regions_dict = setup_search_subvolumes(mm_ordered_mask, mask_radius,
                  sphere_radius, model_radius, target_volume)
        #else carry on with the full volume and previous definition of regions_dict

    else:
      message = "Disallowed option for region_approach"
      phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fatal, message))
      phaser_out.logTrailer(enum_out_stream.concise)
      raise Exception("FATAL ERROR: " + message)

    ##########################################################################
    # Calling the refinement protocol from Randy to prepare map coefficients #
    ##########################################################################
    # NOTE verbosity in Randy's function: 0/1/2/3/4 for mute/log/verbose/debug/testing (5 options)
    # NOTE verbosity phasertng process concise summary logfile verbose testing (6 options)
    # NOTE pending to adapt verbosity

    nregions = len(regions_dict)
    if nregions > 1:
      phaser_out.logUnderLine(enum_out_stream.summary,
          'Prepare map regions for ' + str(nregions) + ' spheres')
    for keyre in regions_dict.keys():
      phaser_out.logUnderLine(enum_out_stream.summary,
          'Prepare map region for ' + keyre)
      sphere_centre = regions_dict[keyre]['center']
      sphere_radius = regions_dict[keyre]['radius']
      phaser_out.logTab(enum_out_stream.logfile,
          'Centre: ' + str(tuple(sphere_centre)))
      phaser_out.logTab(enum_out_stream.logfile,
          'Radius: ' + str(sphere_radius))
      if 'results_correction' not in regions_dict[keyre]:
        results = prepare_docking_sphere(
                          map_model_manager = mmm,
                          d_min = search_d_min,
                          sphere_cent = sphere_centre,
                          radius = sphere_radius,
                          phasertng_dir = phasertng_dir,
                          region_name = keyre,
                          determine_ordered_volume = determine_ordered_volume,
                          verbosity = verbosity,
                          log = sys.stdout)
        regions_dict[keyre]['results_correction'] = results

    #####################################################
    # MAIN LOOP over the regions for the current model  #
    #####################################################
    phaser_out.logHeader(enum_out_stream.concise, "EM-PLACEMENT search ")
    iregion = 0
    highest_d_min = 1000. # Find highest resolution needed for any subvolume
    highest_d_min_phased = 1000.
    highest_signal_resolution = 1000. # Individual values per volume would be better
    highest_signal_resolution_phased = 1000.
    #frf_cards is a container for results
    frf_cards = ""

    for keyre in regions_dict.keys():
      iregion = iregion + 1
      phaser_out.logUnderLine(enum_out_stream.logfile,
          'Preparing to fit model to '+ keyre + ' (' + str(iregion) + ' of ' + str(nregions) + ')')
      # If enough signal in bigsphere, data have already been prepared
      if keyre != "bigsphere": # Haven't prepared the data yet
        hklfile = regions_dict[keyre]['results_correction']['path_mtz']
        if best_res is  None or best_res <= 0.:
          this_d_min = regions_dict[keyre]['results_correction']['mtz_resolution']
        else:
          this_d_min = best_res
        basetng.DAGDATABASE.clear()
        basetng = prepare_model_free_data(working_params_global,phasertng_dir,hklfile,basetng)
        if working_params_global.em_placement.map_model.fixed_scattering_ratio is None:
          scatfrac = regions_dict[keyre]['results_correction']['scattering_fraction']
        else:
          scatfrac = working_params_global.em_placement.map_model.fixed_scattering_ratio
        basetng = prepare_model_dependent_data(working_params_global,
            master_phil_tng,modified_phil,scatfrac,mol_search,basetng,this_d_min,
            ellg_target=working_params_global.em_placement.docking.frf_target_ellg,
            boxscale=4.)

      reflid = basetng.REFLECTIONS.REFLID
      all_working_params = access_nodes(basetng)
      ellg_result = all_working_params[0].phaserdag.node.seek_components_ellg
      seek_resolution = all_working_params[0].phaserdag.node.seek_resolution # Zero if ellg target not reached
      seek_resolution_phased = all_working_params[0].phaserdag.node.seek_resolution_phased
      signal_resolution = all_working_params[0].phaserdag.node.signal_resolution
      signal_resolution_phased = all_working_params[0].phaserdag.node.signal_resolution_phased
      highest_signal_resolution = min(highest_signal_resolution, signal_resolution)
      highest_signal_resolution_phased = min(highest_signal_resolution_phased,signal_resolution_phased)
      this_d_min = max(seek_resolution, signal_resolution)
      reflid_card = ("phasertng fast_rotation_function maps id "
          + str(reflid.identifier()) + " tag \"" + reflid.tag() + "\"\n")
      highest_d_min = min(highest_d_min, this_d_min)
      this_d_min_phased = max(seek_resolution_phased, signal_resolution_phased)
      highest_d_min_phased = min(highest_d_min_phased, this_d_min_phased)

      output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)
      working_params_tng = master_phil_tng.fetch(sources=[modified_phil,output_prev_phil]).extract()

      if working_params_global.em_placement.map_model.region_approach == "rigid_body":
        # Implement this by fixing orientation through brf before ptf and refinement
        # If starting position is close, ptf should give basically zero translation.
        # Default starting orientation is identity for brf "around", and set
        # range small enough that no orientation sampling
        working_params_tng.phasertng.mode = ['brf']
        working_params_tng.phasertng.brute_rotation_function.maximum_stored = 0
        working_params_tng.phasertng.brute_rotation_function.signal_resolution = highest_d_min_phased
        working_params_tng.phasertng.resolution = highest_d_min_phased
        working_params_tng.phasertng.brute_rotation_function.generate_only = True
        working_params_global.em_placement.docking.frf_rescoring = False
        working_params_tng.phasertng.brute_rotation_function.volume = 'around'
        working_params_tng.phasertng.brute_rotation_function.range = 1.

      elif working_params_global.em_placement.docking.brute_approach == 'search':
        working_params_tng.phasertng.mode = ['frf']
        # Set signal_resolution for downstream modes, resolution for the frf itself
        working_params_tng.phasertng.fast_rotation_function.signal_resolution = this_d_min
        working_params_tng.phasertng.resolution = this_d_min
        # Override default percent value, which has been changed from 65 to 50
        working_params_tng.phasertng.fast_rotation_function.percent = 65.
        if working_params_global.em_placement.docking.frf_rescoring:
          # if rescoring afterwards it may not make sense to cluster first
          # NB: this should be tested on a large variety of cases later!
          working_params_tng.phasertng.fast_rotation_function.cluster = False
        elif working_params_global.em_placement.docking.frf_clustering:
          working_params_tng.phasertng.fast_rotation_function.cluster = True

      elif working_params_global.em_placement.docking.brute_approach.startswith('six_dimensional'):
        working_params_tng.phasertng.mode = ['brf']
        working_params_tng.phasertng.brute_rotation_function.maximum_stored = 0
        # Use highest_d_min_phased because all real calculations are phased after
        # using brf to generate angles
        working_params_tng.phasertng.brute_rotation_function.signal_resolution = highest_d_min_phased
        working_params_tng.phasertng.resolution = highest_d_min_phased
        working_params_tng.phasertng.brute_rotation_function.generate_only = True
        working_params_global.em_placement.docking.frf_rescoring = False
        if working_params_global.em_placement.docking.brute_approach == 'six_dimensional_local':
          working_params_tng.phasertng.brute_rotation_function.volume = 'around'
          range_brute = working_params_global.em_placement.docking.angle_around.range
          working_params_tng.phasertng.brute_rotation_function.range = range_brute
        else:
          working_params_tng.phasertng.brute_rotation_function.volume = 'unique'
      else:
        message = "Illegal value of brute_approach"
        phaser_out.logError(enum_out_stream.summary, PhaserError(enum_err_code.fatal, message))
        phaser_out.logTrailer(enum_out_stream.concise)
        raise Exception("FATAL ERROR: " + message)

      # Now run frf or brf
      cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
      cards = cards + reflid_card
      basetng.run(cards)
      if not basetng.success():
        raise Sorry("Failure in frf or brf step")

      if (working_params_global.em_placement.docking.brute_approach == 'search'):
        frf_cards = frf_cards + basetng.DAGDATABASE.cards()
    # End of loop running FRF over search subvolumes or BRF over whole volume
    basetng.DAGDATABASE.parse_cards(frf_cards)

    # If there is rescoring there needs to be a call to the frfr mode
    if working_params_global.em_placement.docking.frf_rescoring:
      output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)
      working_params_tng = master_phil_tng.fetch(sources=[modified_phil, output_prev_phil]).extract()
      working_params_tng.phasertng.resolution = 0.
      working_params_tng.phasertng.mode = ['frfr']
      if working_params_global.em_placement.docking.frf_clustering:
        working_params_tng.phasertng.rescore_rotation_function.cluster = True
      else:
        working_params_tng.phasertng.rescore_rotation_function.cluster = False
      cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
      basetng.run(cards)
      if not basetng.success():
        raise Sorry("Failure in frfr step")
      output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)
    else:
      output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)

    # Run gyre mode with refinement turned off
    # RJR note: check for some difficult cases later whether gyre would have helped
    working_params_tng = master_phil_tng.fetch(sources=[modified_phil, output_prev_phil]).extract()
    working_params_tng.phasertng.mode = ['gyre']
    working_params_tng.phasertng.macgyre.protocol = 'off'
    cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")

    basetng.run(cards)
    if not basetng.success():
      raise Sorry("Failure in gyre step")

    # Now ptf, pose and packing (chained now, but can be separated if needed)
    output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)
    working_params_tng = master_phil_tng.fetch(sources=[modified_phil, output_prev_phil]).extract()
    if working_params_tng.phasertng.resolution < highest_d_min_phased:
      working_params_tng.phasertng.resolution = highest_d_min_phased
    working_params_tng.phasertng.mode = ['ptf','pose','pak']
    working_params_tng.phasertng.packing.move_to_compact = False
    working_params_tng.phasertng.phased_translation_function.maximum_stored = max(50,5*top_files)
    cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
    basetng.run(cards)
    if not basetng.success():
      raise Sorry("Failure in ptf+pose+pak steps")

    # NOTE: Run rmr mode first at resolution for rotation and ptf searches
    output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)
    working_params_tng = master_phil_tng.fetch(sources=[modified_phil, output_prev_phil]).extract()
    working_params_tng.phasertng.mode = ['rmr']
    # Only refine cell scale if requested
    if working_params_global.em_placement.docking.refine_cell_scale:
      working_params_tng.phasertng.macrm.macrocycle2 = ['rotn','tran','vrms','cell']
      working_params_tng.phasertng.cell_scale = [0.8,1.25]
    # We only want a maximum of top_files solutions after the final refinement
    if working_params_tng.phasertng.resolution > highest_signal_resolution_phased:
      working_params_tng.phasertng.macrm.maximum_stored = max(10,top_files)
    else:
      working_params_tng.phasertng.macrm.maximum_stored = top_files
    cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
    basetng.run(cards)
    if not basetng.success():
      raise Sorry("Failure in initial rmr step")

    output_prev_phil = parse(basetng.phil_str(), converter_registry=converter_registry)
    working_params_tng = master_phil_tng.fetch(sources=[modified_phil, output_prev_phil]).extract()
    working_params_tng.phasertng.rigid_body_maps.maximum_stored = top_files
    if working_params_tng.phasertng.resolution > highest_signal_resolution_phased:
      # Re-refine at highest useful resolution for best subvolume
      working_params_tng.phasertng.resolution = highest_signal_resolution_phased
      working_params_tng.phasertng.mode = ['rmr']
      if working_params_global.em_placement.docking.refine_cell_scale:
        working_params_tng.phasertng.macrm.macrocycle2 = ['rotn','tran','vrms','cell']
        working_params_tng.phasertng.cell_scale = [0.8,1.25]
      working_params_tng.phasertng.macrm.maximum_stored = top_files
      cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
      basetng.run(cards)
      if not basetng.success():
        raise Sorry("Failure in rmr step at full resolution")

    model_used = os.path.basename(working_params_tng.phasertng.model.filename)
    phaser_out.logHeader(enum_out_stream.logfile, "Evaluation of the docking solutions")
    all_working_params = access_nodes(basetng)

    refine_cell_scale = False
    if working_params_global.em_placement.docking.refine_cell_scale:
      refine_cell_scale = True
    DAGDB = basetng.DAGDATABASE
    focus_regions_dict = {}
    for ind, solution in enumerate(DAGDB.NODES.LIST):
      if ind >= top_files:
        DAGDB.NODES.resize(ind)
        break
      # Only pursue potential solutions with significantly positive mapllg
      # Approximate sigma(mapllg) as sqrt(mapllg), and only keep solutions
      # either within sigma_cut*sigma of top mapllg with positive mapllg,
      # or above 120, which is strongly significant.
      mapllg = all_working_params[ind].phaserdag.node.mapllg
      sigma_cut = working_params_global.em_placement.docking.sigma_cut
      if ind == 0:
        if mapllg > sigma_cut**2:
          min_mapllg = max(10.,mapllg - sigma_cut*math.sqrt(mapllg))
          min_mapllg = min(120.,min_mapllg)
        else:
          min_mapllg = min(mapllg,10.) # Try at least one
      if mapllg < min_mapllg:
        DAGDB.NODES.resize(ind)
        break
      # Find the center of this docked solution
      subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
      entfile = solution.TRACKER.ULTIMATE.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
      file_solution = os.path.join(phasertng_dir,subdir,entfile)

      # Prepare data from map focused on this solution
      region_name = "focus" + str(ind+1)
      focus_regions_dict[region_name] = {}
      sol_model = dm.get_model(file_solution)
      this_center, model_radius = prepare_map_for_docking.sphere_enclosing_model(sol_model)
      this_center = list(this_center)
      phaser_out.logUnderLine(enum_out_stream.logfile,
          'Prepare map region for ' + region_name)
      # Report actual centre/radius of molecule even if cell_scale was refined
      phaser_out.logTab(enum_out_stream.logfile,
          'Centre: ' + str(this_center))
      phaser_out.logTab(enum_out_stream.logfile,
          'Radius: ' + str(model_radius))
      if refine_cell_scale:
        # Correct centre to where it would be before scaling cell
        cell_scale = all_working_params[ind].phaserdag.node.data_scale
        this_center = list(flex.double(this_center)/cell_scale)
        # Correct radius too
        model_radius = model_radius/cell_scale
      if best_res is None: # Keep focused docking comparable in resolution limits
        this_d_min = highest_signal_resolution_phased
      else:
        this_d_min = best_res
      results = prepare_docking_sphere(
                        map_model_manager = mmm,
                        d_min = this_d_min,
                        sphere_cent = this_center,
                        radius = model_radius,
                        phasertng_dir = phasertng_dir,
                        region_name = region_name,
                        determine_ordered_volume = determine_ordered_volume,
                        verbosity = verbosity,
                        log = sys.stdout)
      focus_regions_dict[region_name]['mmm_focus'] = results['mmm_focus']
      focus_regions_dict[region_name]['path_mtz'] = results['path_mtz']
      basetng2 = Phasertng("VOYAGER")
      basetng2.set_file_object(out_wrapper)
      hklfile = results['path_mtz']
      oversampling = results['oversampling']
      if best_res is  None or best_res <= 0.:
        this_d_min = results['mtz_resolution']
      else:
        this_d_min = best_res
      basetng2 = prepare_model_free_data(working_params_global,phasertng_dir,hklfile,basetng2)
      if working_params_global.em_placement.map_model.fixed_scattering_ratio is None:
        target_fraction = (model_volume / total_volume)
        scatfrac = results['scattering_fraction']
        # Allow for error in determination of ordered volume, within limits
        min_scattering_ratio = 1.4
        min_scatfrac = min(min_scattering_ratio * target_fraction, 1.)
        if scatfrac < min_scatfrac:
          phaser_out.logTab(enum_out_stream.logfile,
              '\nNB: Allowing for uncertainty in ordered volume: scatfrac increased from ' +
              str(scatfrac) + ' to ' + str(min_scatfrac) + '\n')
          scatfrac = min_scatfrac
      else:
        scatfrac = working_params_global.em_placement.map_model.fixed_scattering_ratio
      # Use default boxscale to get most accurate results at focused docking stage
      basetng2 = prepare_model_dependent_data(working_params_global,master_phil_tng,
        modified_phil,scatfrac,mol_search,basetng2,this_d_min,
        boxscale=original_boxscale)
      all_working_params_focus = access_nodes(basetng2)
      signal_resolution_phased = all_working_params_focus[0].phaserdag.node.signal_resolution_phased
      solution.SIGNAL_RESOLUTION = signal_resolution_phased
      solution.REFLID = basetng2.REFLECTIONS.REFLID # Associate solution with new focused map data
      solution.Z = basetng2.REFLECTIONS.Z
      solution.set_cell(basetng2.REFLECTIONS.get_cell())
      solution.TOTAL_SCATTERING = basetng2.REFLECTIONS.TOTAL_SCATTERING
      DAGDB.NODES.set_node(ind,solution)
      DAGDB.parse_dogtags(basetng2.DAGDATABASE.unparse_dogtags())
    # End of loop setting up focused docking regions

    output_prev_phil = parse(basetng2.phil_str(), converter_registry=converter_registry)
    working_params_tng = master_phil_tng.fetch(sources=[modified_phil,output_prev_phil]).extract()
    working_params_tng.phasertng.resolution = best_res
    working_params_tng.phasertng.mode = ['rmr']
    # working_params_tng.phasertng.macrm.study_parameters = True
    if refine_cell_scale:
      working_params_tng.phasertng.macrm.macrocycle2 = ['rotn','tran','vrms','cell']
      working_params_tng.phasertng.cell_scale = [0.8,1.25]
    cards = phil_to_cards(user_phil=working_params_tng, program_name="InputCard")
    basetng.run(cards)
    if not basetng.success():
      raise Sorry("Failure in rmr step for focused docking")

    # Now compute mapCC values for top focused docking solution(s) and write data to a csv
    DAGDB = basetng.DAGDATABASE
    all_working_params = access_nodes(basetng)

    # Keep track of number of final solutions found
    nsolfound = 0
    for ind, solution in enumerate(DAGDB.NODES.LIST):
      # Approximate sigma(mapllg) as sqrt(mapllg), and only keep solutions
      # either within sigma_cut*sigma of top mapllg with positive mapllg,
      # or above 120, which is strongly significant.
      mapllg = all_working_params[ind].phaserdag.node.mapllg
      if ind == 0:
        if mapllg > sigma_cut**2:
          min_mapllg = mapllg - sigma_cut*math.sqrt(mapllg)
          min_mapllg = min(120.,min_mapllg)
        else:
          min_mapllg = min(mapllg,0.)
      if mapllg < min_mapllg:
        break

      # Get tag to retrieve appropriate prepared map
      this_tag = all_working_params[ind].phaserdag.node.hklin.tag
      focus_id = this_tag.split('_')[0]
      mmm_focus = focus_regions_dict[focus_id]['mmm_focus']
      subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
      entfile = solution.TRACKER.ULTIMATE.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
      file_solution_focus = os.path.join(phasertng_dir,subdir,entfile)
      model_focus = dm.get_model(file_solution_focus)
      mm_focus  = mmm_focus.get_map_manager_by_id('map_manager_lwtd')
      hklfile = focus_regions_dict[focus_id]['path_mtz']

      # Apply cell scale to map if it was refined
      if refine_cell_scale:
        cell_scale = all_working_params[ind].phaserdag.node.data_scale
        unit_cell_crystal_symmetry = mm_focus.unit_cell_crystal_symmetry()
        unit_cell_parameters = list(unit_cell_crystal_symmetry.unit_cell().parameters())
        for icell in range(3):
          unit_cell_parameters[icell] = unit_cell_parameters[icell]*cell_scale
        new_cs = crystal.symmetry(tuple(unit_cell_parameters), 1)
        mm_focus.set_unit_cell_crystal_symmetry(new_cs)

      mmm_focus_scaled = map_model_manager(map_manager=mm_focus, model=model_focus)
      correlation_to_focus_map = mmm_focus_scaled.map_model_cc()
      drms = all_working_params[ind].phaserdag.node.variance[0].drms
      vrms_start = working_params_tng.phasertng.model.vrms_estimate
      vrms_final = math.sqrt(vrms_start**2 + drms)

      # Extract rotation/translation applied to input model from DAG, convert
      # to 3x4 rotation/translation matrix that can be used (e.g. by ChimeraX)
      # to transform the initial model
      pose_rot = all_working_params[ind].phaserdag.node.pose[0].rmat
      pose_tra = all_working_params[ind].phaserdag.node.pose[0].shift_orthogonal
      line1 = pose_rot[0:3]
      line1.append(pose_tra[0])
      line2 = pose_rot[3:6]
      line2.append(pose_tra[1])
      line3 = pose_rot[6:9]
      line3.append(pose_tra[2])
      RT = list((line1,line2,line3))

      output_pathname = os.path.join(phasertng_dir,
                                      (os.path.basename(file_solution_focus)).split('.')[0])
      phaser_out.logTab(enum_out_stream.summary,
                        '\n' + 'Results for focused docking solution ' + str(ind+1))
      phaser_out.logTab(enum_out_stream.summary,
                        'Refined map LLG: '
                        + str(mapllg))
      phaser_out.logTab(enum_out_stream.summary,
                        'Map-model cc between solution and focused map: '
                        + str(correlation_to_focus_map))

      # Compute and report map-model FSC if level >= logfile
      if verbosity > 0:
        phaser_out.logTab(enum_out_stream.summary,
              '\n       d       1/d     map_model_FSC')
        mc = mmm_focus_scaled.map_manager().map_as_fourier_coefficients(d_min=best_res)
        mmm_focus_scaled.generate_map(d_min=best_res,map_id='model_map')
        fc = mmm_focus_scaled.map_as_fourier_coefficients(d_min=best_res,map_id='model_map')
        fsc_results = fc.fsc(other=mc)
        fsc_bins = fsc_results.fsc
        d = fsc_results.d
        d_inv = fsc_results.d_inv
        for i_bin in range(fsc_bins.size()):
          phaser_out.logTab(enum_out_stream.logfile,
              "%7.3f %8.5f %8.4f" % (d[i_bin], d_inv[i_bin], fsc_bins[i_bin]))

        # XXX test code to calculate and display sigmaA curve
        if False:
          dm.get_reflection_file_server(filenames=[hklfile])
          miller_arrays = dm.get_miller_arrays()
          Emean = miller_arrays[0]
          Dobs = miller_arrays[1]
          fc = fc.select(fc.d_star_sq().data() > 0.) # Omit F000
          # We're comparing to hklfile map where origin shift has been applied
          shift_cart = mmm_focus_scaled.shift_cart()
          uc = fc.crystal_symmetry().unit_cell()
          shift_frac = uc.fractionalize(shift_cart)
          shift_frac = tuple(-flex.double(shift_frac))
          fc = fc.translational_shift(shift_frac)
          fc.setup_binner_d_star_sq_bin_size()
          Emean.use_binner_of(fc)
          Dobs.use_binner_of(fc)
          xdat = []
          ydat = []
          wdat = []
          for i_bin in fc.binner().range_used():
            sel = fc.binner().selection(i_bin)
            fcsel = fc.select(sel)
            absEc = fcsel.amplitudes().data()
            p1 = fcsel.phases().data()
            sigmaP = flex.mean_default(flex.pow2(absEc),0)
            absEc /= math.sqrt(sigmaP)
            Emeansel = Emean.select(sel)
            absEmean = Emeansel.amplitudes().data()
            p2 = Emeansel.phases().data()
            cosdphi = flex.cos(p2 - p1)
            Dobsdata = Dobs.select(sel).data()
            # meanDobs = flex.mean_default(Dobsdata,0)
            # Solve for sigmaA yielding 1st derivative of LLG approximately equal to zero (assuming only numerator matters)
            sum0 = flex.sum(2.*Dobsdata*absEmean*absEc*cosdphi)
            sum1 = flex.sum(2*flex.pow2(Dobsdata) * (1. - flex.pow2(absEmean) - flex.pow2(absEc)))
            sum2 = flex.sum(2*flex.pow(Dobsdata,3)*absEmean*absEc*cosdphi)
            sum3 = flex.sum(- 2*flex.pow(Dobsdata,4))
            u1 = -2*sum2**3 + 9*sum1*sum2*sum3 - 27*sum0*sum3**2
            sqrt_arg = u1**2 - 4*(sum2**2 - 3*sum1*sum3)**3
            if sqrt_arg < 0: # Paranoia: haven't run into this in a wide variety of calculations
              raise Sorry("Argument of square root in sigmaA calculation is negative")
            third = 1./3
            x1 = (u1 + math.sqrt(sqrt_arg))**third
            sigmaA = ( (2 * 2.**third * sum2**2 - 6 * 2.**third*sum1*sum3
                        - 2*sum2*x1 + 2.**(2*third) * x1**2) /
                      (6 * sum3 * x1) )
            sigmaA = max(min(sigmaA,0.999),0.01)
            # Now work out approximate e.s.d. of sigmaA estimate from 2nd derivative of LLG at optimal sigmaA
            denom = flex.pow((1-flex.pow2(Dobsdata)*sigmaA**2),3)
            sum0 = flex.sum((2*flex.pow2(Dobsdata) * (1. - flex.pow2(absEmean) - flex.pow2(absEc)))/denom)
            sum1 = flex.sum((12*flex.pow(Dobsdata,3)*absEmean*absEc*cosdphi)/denom)
            sum2 = flex.sum((-6*flex.pow(Dobsdata,4)*(flex.pow2(absEmean) + flex.pow2(absEc)))/denom)
            sum3 = flex.sum((4*flex.pow(Dobsdata,5)*absEmean*absEc*cosdphi)/denom)
            sum4 = flex.sum((-2*flex.pow(Dobsdata,6))/denom)
            d2LLGbydsigmaA = sum0 + sum1*sigmaA + sum2*sigmaA**2 + sum3*sigmaA**3 + sum4*sigmaA**4
            d2LLGbydsigmaA /= oversampling
            # sigma_sigmaA = 1./math.sqrt(abs(d2LLGbydsigmaA))
            ssqr = flex.mean_default(fcsel.d_star_sq().data(),0)
            # ndat = Dobsdata.size()
            # print(ndat,meanDobs,ssqr,sigmaA,sigma_sigmaA)
            xdat.append(ssqr)
            ydat.append(math.log(sigmaA))
            wdat.append(abs(d2LLGbydsigmaA))

          xdat = flex.double(xdat)
          ydat = flex.double(ydat)
          wdat = flex.double(wdat)
          W = flex.sum(wdat)
          Wx = flex.sum(wdat * xdat)
          Wy = flex.sum(wdat * ydat)
          Wxx = flex.sum(wdat * xdat * xdat)
          Wxy = flex.sum(wdat * xdat * ydat)
          slope = (W * Wxy - Wx * Wy) / (W * Wxx - Wx * Wx)
          intercept = (Wy * Wxx - Wx * Wxy) / (W * Wxx - Wx * Wx)
          print("Slope and intercept of sigmaA plot: ",slope,intercept)
          frac_complete = math.exp(2.*intercept)
          print("Approximate fraction of scattering: ",frac_complete)
          if slope <= 0:
            rmsd = math.sqrt(-3*slope/(8*math.pi**2))
            print("Approximate rms error: ",rmsd)
          linlogsiga = flex.double()
          logsiga_combined = flex.double()
          sigma_linlog = math.log(1.5) # Allow 50% error in linear fit
          for i_bin in range(xdat.size()):
            sigmaA = math.exp(ydat[i_bin])
            linlog = intercept + slope*xdat[i_bin]
            sigmaA_from_line = math.exp(linlog)
            linlogsiga.append(linlog)
            sigma_sigmaA = 1./math.sqrt(wdat[i_bin])
            sigma_lnsigmaA = math.exp(-ydat[i_bin])*sigma_sigmaA
            logsiga_combined.append(
                          (ydat[i_bin]/sigma_lnsigmaA**2 + linlog/sigma_linlog**2) /
                          (1./sigma_lnsigmaA**2 + 1./sigma_linlog**2) )
            # Z_score = (sigmaA - sigmaA_from_line)/sigma_sigmaA
            # print(xdat[i_bin],sigmaA,sigmaA_from_line,sigma_sigmaA,math.log(sigmaA))
          import matplotlib.pyplot as plt
          fig, ax = plt.subplots(2,1)
          ax[0].plot(xdat,ydat,label="ln(sigmaA)")
          ax[0].plot(xdat,linlogsiga,label="line fit")
          ax[0].plot(xdat,logsiga_combined,label="combined")
          ax[0].legend(loc="upper right")
          ax[1].plot(xdat,flex.exp(ydat),label="sigmaA")
          ax[1].plot(xdat,flex.exp(linlogsiga),label="line fit")
          ax[1].plot(xdat,flex.exp(logsiga_combined),label="combined")
          ax[1].legend(loc="lower left")
          plt.show()
        # XXX

      if (working_params_global.em_placement.docking.check_full_map):
        mmm_check = mmm.deep_copy()
        mmm_check.set_model(model_focus)
        if point_sym not in ['C1', 'c1']:
            mmm_check.get_ncs_from_map(symmetry=point_sym)
        # These three lines are generating the file with the symmetry applied (not sure I need that)
        model_with_ncs = mmm_check.model_building(nproc=1).apply_ncs()
        # Now we load the expanded pdb file back into the map model manager
        mmm_check.set_model(model_with_ncs)  # Using same id (model), to substitute the current one
        output_filename = output_pathname + '_ncs_applied.pdb'
        dm.write_model_file(model_with_ncs, output_filename)
        returned = mmm_check.map_model_cc()
        phaser_out.logTab(enum_out_stream.summary,
                          'Map-model cc between solution and original map after symmetry is applied: '
                          + str(returned))
      else:
        returned = " ND "
      # Save best results
      top_models_focus.append(model_focus)
      top_models_map.append(mm_focus)
      top_models_mapLLG.append(mapllg)
      top_models_mapCC.append(correlation_to_focus_map)
      top_models_RT.append(RT)
      if refine_cell_scale:
        top_models_cell_scale.append(cell_scale)
      else:
        top_models_cell_scale.append(1.0)
      nsolfound += 1
      if working_params_global.em_placement.docking.save_output_folder:
        # Now dump the results to a table and save the top models
        fichi_table = open(output_table_path, 'a')
        fichi_table.write(
          os.path.basename(file_solution_focus) + ',' + model_used + ',' +
            all_working_params[ind].phaserdag.node.hklin.tag + ',' + str(mapllg) + ',' +
            str(all_working_params[ind].phaserdag.node.zscore) + ',' +str(returned) + ',' +
            str(correlation_to_focus_map) + ',' +
            all_working_params[ind].phaserdag.node.annotation + ',' + str(vrms_final) +'\n')
        del fichi_table
        output_filename = os.path.join(folder_output_files,'docked_model' +
                                       str(ind+1) + '.pdb')
        dm.write_model_file(model_focus, filename = output_filename)
        output_filename = os.path.join(folder_output_files,'docked_model' +
                                       str(ind+1) + '.map')
        mm_focus.write_map(output_filename)
        output_filename = os.path.join(folder_output_files,'weighted_map_data' +
                                       str(ind+1) + '.mtz')
        shutil.copy2(hklfile,output_filename)

    phaser_out.logTab(enum_out_stream.logfile, "\nNumber of solutions found :  "+str(nsolfound))

    # Clean up if mmCIF input had to be converted to temporary PDB file
    try:
      tmpPDBdirname
    except NameError:
      pass
    else:
      shutil.rmtree(tmpPDBdirname)

  except Exception as e:
    traceback.print_exc(file=sys.stdout)
    raise Sorry("Search failed")

  # Clean up behind us
  simple_tools.clean_directories()
  if working_params_global.em_placement.remove_phasertng_folder and os.path.exists(phasertng_dir):
    shutil.rmtree(phasertng_dir)

  # return basetng? For now, best models and scores instead
  return group_args(
      top_models = top_models_focus,
      top_models_map = top_models_map,
      top_models_mapLLG = top_models_mapLLG,
      top_models_mapCC = top_models_mapCC,
      top_models_RT = top_models_RT,
      top_models_cell_scale = top_models_cell_scale,
      nsolfound = nsolfound)

def emplace_simple(map1_fname, map2_fname, d_min, sequence_composition,
                   model_file, model_vrms = None, fixed_model_file = None,
                   output_folder = None, top_files = None,
                   level = "process"):
  """ Simplified interface to EM-placement global search

  Required arguments:
  map1_fname: path to half map 1
  map2_fname: path to half map 2
  d_min: nominal (best) resolution of map
  sequence_composition: path to FASTA format sequence file for full reconstruction
  model_file: path to model to dock

  Optional arguments:
  model_vrms: RMSD to assign to docking model, falls back to EM-placement default
  fixed_model_file: path to any pre-placed component(s) of reconstruction
  output_folder: path for folder for output model, map and mtz files
                 defaults to folder generated within EM-placement
  top_files: number of solutions within default cutoffs of top, default 5
  level: verbosity of output, defaults to "process" (essentially mute)
  """

  dm = DataManager()
  search_model = dm.get_model(model_file)
  center_temp, model_radius = prepare_map_for_docking.sphere_enclosing_model(search_model)

  half_maps_string = ''
  if map1_fname is not None and map2_fname is not None:
    half_maps_string = '    half_map = ' + map1_fname + '\n' + '    half_map = ' + map2_fname + '\n'
  else:
    if d_min is None or d_min <= 0:
      raise Sorry("d_min must be specified if no half-maps")

  if d_min is None or d_min <= 0:
    resolution_string = ''
  else:
    resolution_string = '    best_resolution = ' + str(d_min) + '\n'

  assert (sequence_composition is not None)
  composition_string = '    sequence_composition = ' + sequence_composition + '\n'

  output_folder_string = ''
  if output_folder is not None:
    output_folder_string = '    output_folder = ' + str(output_folder) + '\n'

  top_files_string = ''
  if top_files is not None:
    top_files_string = '    top_files = ' + str(top_files) + '\n'

  if level == "process" or level == "silence":
    remove_phasertng_folder = "True"
  else:
    remove_phasertng_folder = "False"

  vrms_string = ''
  if model_vrms is not None:
    vrms_string = '      starting_model_vrms = ' + str(model_vrms) + '\n'

  fixed_model_string = ''
  if fixed_model_file is not None:
    fixed_model_string = '      fixed_model_file = ' + fixed_model_file + '\n'

  phil_string = (
    'em_placement\n' +
    '{\n' +
    '  remove_phasertng_folder = ' + remove_phasertng_folder + '\n' +
    '  level = ' + level + '\n' +
    '  docking\n' +
    '  {\n' +
    '    check_full_map = False\n' +
    '    save_output_folder = True\n' +
         output_folder_string +
         top_files_string +
    '  }\n' +
    '  map_model\n' +
    '  {\n' +
         half_maps_string +
         resolution_string +
         composition_string +
    '    region_approach = automatic \n' +
    '  }\n' +
    '  biological_unit \n' +
    '  {\n' +
    '    molecule \n' +
    '    {\n' +
    '      molecule_name = model\n' +
    '      model_file = ' + model_file + '\n' +
          vrms_string +
    '    }\n' +
    '    molecule_fixed \n' +
    '    {\n' +
           fixed_model_string +
    '    }\n' +
    '  }\n' +
    '}\n' )

  # print('XXX Check phil_string:\n', phil_string)

  results = em_placement_code(phil_string)

  return results

def emplace_local(map_fname, map1_fname, map2_fname, d_min, model_file, model_vrms,
                  fixed_model_file = None, sphere_center = None, top_files = None,
                  sequence_composition = None, fixed_scattering_ratio = None,
                  brute_rotation = False, refine_scale = False, level = "process"):
  """ Simplified interface to EM-placement local search

  Either a full map or two half maps must be specified
  Either sequence_composition or fixed_scattering_ratio must be provided

  Required arguments:
  map_fname:  path to full map (or optionally None if half maps given)
  map1_fname: path to half map 1 (optionally None)
  map2_fname: path to half map 2 (optionally None)
  d_min: nominal (best) resolution of map
  model_file: path to model to dock
  model_vrms: RMSD to assign to docking model

  Optional arguments:
  fixed_model_file: path to any pre-placed component(s) of reconstruction
  sphere_center: Cartesian coordinates of center of search sphere
                 defaults to center of input model
  top_files: number of solutions within default cutoffs of top, default 5
  sequence_composition: path to FASTA format sequence file for full reconstruction
  fixed_scattering_ratio: ratio between total scattering in sphere and in target
                          A value of 1.5 could be used
  brute_rotation: should brute-force full rotation search be used? Default False
  refine_scale: should voxel scale be refined? Default False
  level: verbosity of output, defaults to essentially mute
  """

  # Composition of sphere has to be set either by giving full map composition or
  # the scattering ratio by which to multiply the model scattering

  dm = DataManager()
  search_model = dm.get_model(model_file)
  center_temp, model_radius = prepare_map_for_docking.sphere_enclosing_model(search_model)

  map_string = ''
  if map_fname is not None:
    map_string = '    full_map = ' + str(map_fname) + '\n'

  half_maps_string = ''
  if map1_fname is not None and map2_fname is not None:
    half_maps_string = '    half_map = ' + map1_fname + '\n' + '    half_map = ' + map2_fname + '\n'
  else:
    if d_min is None or d_min <= 0:
      raise Sorry("d_min must be specified if no half-maps")

  if d_min is None or d_min <= 0:
    resolution_string = ''
  else:
    resolution_string = '    best_resolution = ' + str(d_min) + '\n'
  sphere_radius = model_radius + 3. # Expand slightly to allow for error in input search center

  # Sphere center can be taken as center of input model or specified explicitly
  if sphere_center is None:
    sphere_center = tuple(center_temp)

  assert (sequence_composition is not None) or (fixed_scattering_ratio is not None)
  if fixed_scattering_ratio is not None:
    composition_string = '    fixed_scattering_ratio = ' + str(fixed_scattering_ratio) + '\n'
  else:
    composition_string = '    sequence_composition = ' + sequence_composition + '\n'

  top_files_string = ''
  if top_files is not None:
    top_files_string = '    top_files = ' + str(top_files) + '\n'
  if level == "process" or level == "silence":
    remove_phasertng_folder = "True"
  else:
    remove_phasertng_folder = "False"

  brute_rotation_string = ''
  if brute_rotation:
    brute_rotation_string = '    brute_approach = six_dimensional_full\n'

  refine_scale_string = ''
  if refine_scale:
    refine_scale_string = '    refine_cell_scale = True\n'

  fixed_model_string = ''
  if fixed_model_file is not None:
    fixed_model_string = '      fixed_model_file = ' + fixed_model_file + '\n'

  phil_string = (
    'em_placement\n' +
    '{\n' +
    '  remove_phasertng_folder = ' + remove_phasertng_folder + '\n' +
    '  level = ' + level + '\n' +
    '  docking\n' +
    '  {\n' +
    '    check_full_map = False\n' +
    '    save_output_folder = False\n' +
         top_files_string +
         brute_rotation_string +
         refine_scale_string +
    '  }\n' +
    '  map_model\n' +
    '  {\n' +
         map_string +
         half_maps_string +
         resolution_string +
         composition_string +
    '    region_approach = manual \n' +
    '    region_for_docking \n' +
    '    {\n' +
    '      name = sphere\n' +
    '      center\n' +
    '      {\n' +
    '        x = ' + str(sphere_center[0]) + '\n' +
    '        y = ' + str(sphere_center[1]) + '\n' +
    '        z = ' + str(sphere_center[2]) + '\n' +
    '      }\n' +
    '      radius = ' + str(sphere_radius) + '\n' +
    '    }\n' +
    '  }\n' +
    '  biological_unit \n' +
    '  {\n' +
    '    molecule \n' +
    '    {\n' +
    '      molecule_name = model\n' +
    '      model_file = ' + model_file + '\n' +
    '      starting_model_vrms = ' + str(model_vrms) + '\n' +
    '    }\n' +
    '    molecule_fixed \n' +
    '    {\n' +
           fixed_model_string +
    '    }\n' +
    '  }\n' +
    '}\n' )

  # print('XXX Check phil_string:\n', phil_string)

  results = em_placement_code(phil_string)

  return results


if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=True)
  arguments = parser.parse_args()
  user_phil = arguments.phil.read()
  em_placement_code(user_phil)

