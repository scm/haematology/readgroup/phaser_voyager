from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from future.standard_library import install_aliases
install_aliases()
from libtbx.utils import Sorry

def run():
  # Set up to import voyager functions
  import os,sys
  module_path = os.path.dirname(os.path.abspath(__file__))
  src = os.path.join(module_path, "../../../src/")
  sys.path.append(src)

  import argparse
  parser = argparse.ArgumentParser(
          description='Dock model into sphere from cryo-EM map')
  parser.add_argument('--map',help='Map file for full map')
  parser.add_argument('--map1',help='Map file for half-map 1')
  parser.add_argument('--map2', help='Map file for half-map 2')
  parser.add_argument('--d_min', help='d_min for maps', type=float)
  parser.add_argument('--model_file', help='Search model')
  parser.add_argument('--sphere_center',help='Centre of sphere for docking',
                      nargs=3, type=float)
  parser.add_argument('--model_vrms', help='vrms for model', type=float, default=1.5)
  parser.add_argument('--sequence_composition',
                      help='Sequence file with composition for full reconstruction, overrides fixed_scattering_ratio')
  parser.add_argument('--fixed_scattering_ratio',
                      help='Estimated scattering ratio between sphere and model',
                      type=float, default = 1.5)
  parser.add_argument('--brute_rotation', dest='brute_rotation', default=False,
                      action='store_true', help='Test orientations systematically')
  parser.add_argument('--refine_scale', dest='refine_scale', default=False,
                      action='store_true', help='Refine cell_scale')
  parser.add_argument('--output_root', help='Root name for output model(s)',
                      default='top_sol')
  parser.add_argument('--top_files', help='Maximum number of top files to save',
                      type=int, default = None)
  parser.add_argument('--level', help='Output level from process to testing',
                      default='process')

  args = parser.parse_args()
  map = args.map
  map1 = args.map1
  map2 = args.map2
  if map is None and (map1 is None or map2 is None):
    raise Sorry("Either full map or half-maps must be given")
  d_min = args.d_min
  model_file = args.model_file
  if args.sphere_center is None:
    # Sphere center will be taken as center of input model
    sphere_center = None
  else:
    sphere_center = tuple(args.sphere_center)
  model_vrms = args.model_vrms
  sequence_composition = args.sequence_composition
  fixed_scattering_ratio = args.fixed_scattering_ratio
  output_root = args.output_root
  brute_rotation = args.brute_rotation
  refine_scale = args.refine_scale
  top_files = args.top_files
  level = args.level
  assert (sequence_composition is not None) or (fixed_scattering_ratio is not None)
  if sequence_composition is not None:
    fixed_scattering_ratio = None

  from New_Voyager.scripts.em_placement_script import emplace_local
  results = emplace_local(
      map, map1, map2,
      d_min, model_file, model_vrms, sphere_center,
      sequence_composition = sequence_composition,
      fixed_scattering_ratio = fixed_scattering_ratio,
      brute_rotation = brute_rotation,
      refine_scale = refine_scale,
      top_files = top_files,
      level = level)
  from iotbx.data_manager import DataManager
  dm = DataManager()
  dm.set_overwrite(True)
  print("\nNumber of solutions found: ",results.nsolfound)
  nkept = len(results.top_models)
  if results.nsolfound > 1:
    print("   of which ", nkept, " are kept")
  for i in range(nkept):
    modelnum = i+1
    model_filename = output_root + '_' + str(modelnum) + '.pdb'
    dm.write_model_file(results.top_models[i],filename=model_filename)
    print("\nModel ", modelnum, " written to: ",model_filename)
    map_filename = output_root + '_' + str(modelnum) + '.map'
    results.top_models_map[i].write_map(map_filename)
    print("Corresponding map written to: ",map_filename)
    print("   mapLLG = ",results.top_models_mapLLG[i])
    print("   mapCC  = ",results.top_models_mapCC[i])

if __name__ == "__main__":
  run()
