###################################################################################################
#                                          LICENSE                                                #
# PhaserVoyager is distributed under three different licences                                     #
#                                                                                                 #
# [Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)                 #
# [CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)                         #
# [Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)                     #
#                                                                                                 #
# By accessing the source code in this repository you agree to be bound by one of these licences. #
#                   (c) 2000-2024 Cambridge University Technical Services Ltd                     #
#                                                                                                 #
###################################################################################################

try:
  import sys,os,shutil,copy
  from libtbx.phil import parse
  from phasertng.phil.master_phil_file import build_phil_str
  from phasertng.phil.converter_registry import converter_registry
  from phasertng.phil.phil_to_cards import phil_to_cards
  from phasertng.phil.phil_to_cards import phil_to_cards_change_mode
  from phasertng import Phasertng

  import argparse
  import iotbx
  import traceback
  import pandas as pd

  # NOTE CM temporary till I figure the structure of the repo and how to do the imports
  module_path = os.path.dirname(os.path.abspath(__file__))
  sys.path.append(os.path.join(module_path, "../"))
  from New_Voyager import voyager_base
  from New_Voyager import simple_tools
  from New_Voyager import pdb_structure
except:
  import traceback
  print('There was an error with the cctbx/phasertng/phaser_voyager imports')
  traceback.print_exc(file=sys.stdout)


def casp_rel_ellg_code(user_phil_str=""):
  # Summary of status error codes:
  # -10 = unspecified failure in running phasertng for reLLG calculation
  #  -7 = no point running as_lddt_coarser because pLDDT only given per residue, not per atom
  #  -6 = no point evaluating because all occupancies are zero
  #  -5 = no point running as_lddt because all pLDDT values are identical
  #  -4 = no point running as_lddt or as_lddt_coarser because all pLDDT values < minimum allowed
  #  -3 = some failure in process_predicted_model, probably too few residues above min pLDDT
  #  -1 = model is too extended or has format errors
  try:

    # Instantiate the basetng object and create the directory
    basetng = Phasertng("PHENIX")
    phasertng_dir = os.path.abspath(simple_tools.create_directory_timestamp("phasertng_"))
    # Get the masterphil with all the defaults from phasertng
    initparams = build_phil_str(program_name="InputCard")
    assert (len(initparams))
    master_phil_tng = parse(initparams, converter_registry=converter_registry)
    # Now instantiate the basevoyager class.One of its attributes is the master phil with all the voyager scope keywords except the tng
    basevoyager = voyager_base.VoyagerBase()
    # Now we merge both of them
    merged_phil_str = 'voyager { ' + basevoyager.voyager_phil_str + initparams + '}'
    master_phil_global = parse(merged_phil_str,converter_registry=converter_registry)
    # And get those values which have been set by the user
    user_phil = iotbx.phil.parse(user_phil_str)
    working_params_global = master_phil_global.fetch(sources=[user_phil]).extract()
    modified_phil = master_phil_global.format(python_object=working_params_global)
    working_parameters_tng = master_phil_tng.fetch(sources=[modified_phil]).extract()


    # setting the modes for generating the data
    working_parameters_tng.phasertng.mode = ['ipdb', 'data', 'aniso', 'tncso', 'tncs', 'feff', 'cca', 'ccs']

    # setting the rest of the parameters
    working_parameters_tng.phasertng.threads.number = 1
    working_parameters_tng.phasertng.molecular_transform.boxscale = 5 # Good compromise between speed and precision here
    working_parameters_tng.phasertng.suite.database = phasertng_dir
    if (working_params_global.voyager.level != "verbose" and
        working_params_global.voyager.level != "testing"):
      # Keep size of phasertng directory under control, then remove at end
      working_parameters_tng.phasertng.suite.write_files = True
      working_parameters_tng.phasertng.suite.write_log = False
      working_parameters_tng.phasertng.suite.write_cards = False
      working_parameters_tng.phasertng.suite.write_phil = False
      working_params_global.voyager.remove_phasertng_folder = True

    working_parameters_tng.phasertng.suite.level = working_params_global.voyager.level
    working_parameters_tng.phasertng.reflections.read_map = True
    working_parameters_tng.phasertng.reflections.wavelength = 1.0
    working_parameters_tng.phasertng.resolution = float((working_params_global.voyager.generate_dataset.resolution_dataset))
    working_parameters_tng.phasertng.model.filename = str(os.path.abspath(working_params_global.voyager.generate_dataset.target_model))
    working_parameters_tng.phasertng.pdbin.filename = str(os.path.abspath(working_params_global.voyager.generate_dataset.target_model))
    # Define composition in terms of sequence of target_model, not just the
    # atoms in that model because side chains can be truncated in experimental structures
    # This makes target a bit bigger than models, because models typically lack
    # H atoms, but treats all models equally
    target_pdb = pdb_structure.PDB(working_parameters_tng.phasertng.model.filename)
    target_sequence = target_pdb.sequence
    target_sequence_path = os.path.join(phasertng_dir, 'target_sequence.seq')
    dm = iotbx.data_manager.DataManager()
    dm.write_sequence_file(target_sequence,target_sequence_path,overwrite=True)
    working_parameters_tng.phasertng.macaniso.macrocycle1 = ['scale','bins']
    working_parameters_tng.phasertng.tncs.order = 1
    working_parameters_tng.phasertng.biological_unit.sequence.filename = target_sequence_path
    working_parameters_tng.phasertng.biological_unit.water_percent = 0
    working_parameters_tng.phasertng.composition.number_in_asymmetric_unit = 1
    working_parameters_tng.phasertng.composition.maximum_z = 1

    # Convert to cards to call tng
    cards = phil_to_cards(user_phil=working_parameters_tng, program_name="InputCard")
    basetng.run(cards)
    if basetng.success():
      saved_cards = basetng.DAGDATABASE.cards()
      # Now we can prepare the models
      list_errors, path_modified_models = basevoyager.prepare_casp_models_for_evaluation(
          directory_models=working_params_global.voyager.prepare_casp_models_for_evaluation.models,
          phasertng_dir=phasertng_dir,
          target=str(os.path.abspath(working_params_global.voyager.generate_dataset.target_model)),
          superpose_models=working_params_global.voyager.prepare_casp_models_for_evaluation.superpose_models)


    #print('\n\nSHERLOCK list_errors, path_modified_models', list_errors, path_modified_models)
    #print(1/0)
    # In this particular method we have not modified the basetng object so we do not return it or evaluate
    # success on it.
    # need also a metric of 'success' for the functions that do not call tng (some attribute of the voyager base class?
    if basevoyager.success:
        # Treatment choices
        choice_treatment = working_params_global.voyager.models_evaluation_against_target.bfactor_treatment
        #print('SHERLOCK choice_treatment',choice_treatment)
        eval_conditions = {"as_bfactors": 0, "as_lddt": 0, "as_lddt_coarser": 0, "as_rmsd": 0, "bfactor_constant": 0}
        if choice_treatment == 'both':
          eval_conditions = {"as_lddt": 1, "bfactor_constant": 1}
        elif choice_treatment == 'all':
          eval_conditions = {"as_lddt": 1, "as_lddt_coarser": 1, "bfactor_constant": 1}
        else:
          eval_conditions[choice_treatment] = 1


        # need to get the files that were prepared in the previous step
        pdblist = [os.path.join(path_modified_models, pdbfi) for pdbfi in os.listdir(path_modified_models) if pdbfi.endswith('.pdb')]
        pdblist = sorted(pdblist)
        #print('SHERLOCK pdblist',pdblist)
        # we need to fill a list with the different arguments we will be using to call
        values_flag = [eval_conditions[key] for key in eval_conditions.keys()]
        total = len(pdblist)*sum(values_flag)
        counter = 0

        # now compute the reLLG from tng
        # copy the state of the reflections object
        original_refle = copy.deepcopy(basetng.REFLECTIONS)
        dict_table = {}
        for treatment,flag in eval_conditions.items():
            if flag == 1:
                pdbnum = -1
                for pdb in pdblist:
                    pdbnum += 1
                    if pdbnum not in dict_table.keys():
                      dict_table[pdbnum] = {}
                    this_model = os.path.basename(pdb)
                    dict_table[pdbnum]['Model'] = this_model

                    # First check this is not a model in the error list
                    if os.path.basename(pdb) in list_errors: # those gave already problems in the first step
                        print("Model "+pdb+" produced a PDB format error or was too extended, skipping it")
                        dict_table[pdbnum]['reLLG_'+treatment] = float(0)
                        dict_table[pdbnum]['status_'+treatment] = int(-1)
                        continue
                    counter = counter +1
                    print('\n Processing '+pdb+' with treatment '+treatment+" which is "+str(counter)+'/'+str(total))
                    # Now I will be calling the function that really does call phasertng after
                    # reinitialising basetng
                    basetng = Phasertng("PHENIX")
                    tuple_result = basevoyager.process_casp_model(model_pdb=pdb,
                                                                  choice_treatment=treatment,
                                                                  basetng=basetng,
                                                                  vrms_estimated=working_params_global.voyager.models_evaluation_against_target.estimated_vrms,
                                                                  original_reflections=original_refle,
                                                                  saved_cards=saved_cards,
                                                                  working_params=working_parameters_tng
                                                                  )
                    #print('sherlock tuple_result',tuple_result)
                    #print(1/0)
                    if tuple_result[0] == 'ERROR':
                        print("Model "+pdb+" produced an error, skipping it")
                        dict_table[pdbnum]['reLLG_'+treatment] = tuple_result[1]
                        dict_table[pdbnum]['status_'+treatment] = int(tuple_result[2])
                        continue
                    else:
                        updated_basetng = tuple_result[1]
                        results_phil_str = copy.deepcopy(updated_basetng.phil_str())
                        object = parse(results_phil_str, converter_registry=converter_registry).extract()
                        # Report reLLG as percent score for ease of interpretation
                        dict_table[pdbnum]['reLLG_'+treatment] = 100*float(object.phasertng.expected.llg.rellg[0])
                        dict_table[pdbnum]['status_'+treatment] = int(0)
        # Convert dict to DataFrame so pandas can be used to write out .csv file
        df = pd.DataFrame.from_dict(dict_table,orient='index')

        # Use or create an appropriate file name for the .csv file
        def normalizefilename(fn):
            validchars = "-_.() "
            out = ""
            for c in fn:
                if str.isalpha(c) or str.isdigit(c) or (c in validchars):
                    out += c
                else:
                    out += "_"
            return out

        cwd = os.getcwd()
        if working_params_global.voyager.analysis_casp_evaluation.output_csv_filename != None:
            # print("using the output name")
            if (working_params_global.voyager.analysis_casp_evaluation.output_csv_filename).endswith('csv'):
                path_to_output_csv = os.path.join(cwd,
                                      working_params_global.voyager.analysis_casp_evaluation.output_csv_filename)
            else:
                path_to_output_csv = os.path.join(cwd,
                                      working_params_global.voyager.analysis_casp_evaluation.output_csv_filename + '.csv')
        else:
            # print("using default value")
            reference_name = os.path.basename(working_params_global.voyager.generate_dataset.target_model)[:-4]
            model_part = os.path.abspath(working_params_global.voyager.prepare_casp_models_for_evaluation.models).split('/')[-1]
            # print('model part before',model_part)
            model_part = normalizefilename(model_part)
            # print('model part after', model_part)
            if model_part.endswith('.pdb'):  # would only happen if using a single model but just in case
                model_part = model_part[:-4]
            if model_part.endswith('pdb'):  # would only happen if using a single model but just in case
                model_part = model_part[:-3]
            path_to_output_csv = os.path.join(cwd, reference_name + '_' + model_part + '_table_rellg.csv')

        # Now actually write the CSV file
        df.to_csv(path_to_output_csv, index=False)

        # Clean up behind us
        simple_tools.clean_directories()
        if working_params_global.voyager.remove_phasertng_folder and os.path.exists(phasertng_dir):
            shutil.rmtree(phasertng_dir)

  except Exception as e:
      traceback.print_exc(file=sys.stdout)

  return basetng

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=True)
  arguments = parser.parse_args()
  user_phil = arguments.phil.read()
  casp_rel_ellg_code(user_phil)

