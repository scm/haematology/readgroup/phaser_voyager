import os,sys
import math
import traceback

try:
    from iotbx.data_manager import DataManager
    from iotbx.map_model_manager import map_model_manager
    from libtbx.utils import Sorry
    from cctbx.array_family import flex
    from cctbx.maptbx import prepare_map_for_docking as prepare_map_for_docking
except:
    print('There was an error with the cctbx/phasertng/phaser_voyager imports')
    traceback.print_exc(file=sys.stdout)

def write_mtz_from_prepared_map(results_prep,phasertng_dir,region_name):
    dm = DataManager()
    ssqmax = results_prep.resultsdict['ssqmax']
    dmin = 1./math.sqrt(ssqmax)
    oversampling_factor = results_prep.over_sampling_factor
    scattering_fraction = results_prep.fraction_scattering
    id_tng_dir = os.path.basename(phasertng_dir).split('_')[1]
    expectE = results_prep.expectE
    mtz_dataset = expectE.as_mtz_dataset(column_root_label='Emean')
    dobs = results_prep.dobs
    mtz_dataset.add_miller_array(dobs, column_root_label='Dobs', column_types='W')
    mtz_object = mtz_dataset.mtz_object()
    # Writing the oversampling and map origin in the history of the mtz
    os_string = "%8.7g" % oversampling_factor
    shift_cart = results_prep.shift_cart
    mapori = -flex.double(shift_cart)
    mapori_string = "%8.2f " % mapori[0] + "%8.2f " % mapori[1] + "%8.2f" % mapori[2]
    history_string = "PHASER OSAMP " + os_string + " MAPORI " + mapori_string
    mtz_object.add_history(lines=flex.std_string([history_string]))
    mtzout_file_name = os.path.join(phasertng_dir, region_name + '_' + id_tng_dir + "_weighted_map_data.mtz")
    dm.write_miller_array_file(mtz_object, filename=mtzout_file_name)
    return {'path_mtz':mtzout_file_name,
            'mtz_resolution':dmin,'oversampling':oversampling_factor,
            'scattering_fraction':scattering_fraction,'shift_cart':shift_cart,
            'mmm_focus':results_prep.new_mmm}

def write_mtz_from_full_map(mmm,d_min,sphere_cent,radius,phasertng_dir,region_name,log):
    dm = DataManager()
    unit_cell = mmm.map_manager().unit_cell()
    unit_cell_grid = mmm.map_manager().unit_cell_grid
    if d_min is None:
       raise Sorry("d_min must be specified")
    spacings = prepare_map_for_docking.get_grid_spacings(unit_cell,unit_cell_grid)
    ucpars = unit_cell.parameters()
    # Keep track of shifted origin
    origin_shift = mmm.map_manager().origin_shift_grid_units
    if flex.sum(flex.abs(flex.double(origin_shift))) > 0:
      shifted_origin = True
    else:
      shifted_origin = False
    # Force sphere center to be on a map grid point
    # Account for origin shift if present in input map
    sphere_cent_frac = unit_cell.fractionalize(sphere_cent)
    sphere_cent_grid = [round(n * f) for n,f in zip(unit_cell_grid, sphere_cent_frac)]
    if shifted_origin:
      sphere_cent_grid = [(c - o) for c,o in zip(sphere_cent_grid, origin_shift)]
    sphere_cent_map = [(g * s) for g,s in zip(sphere_cent_grid, spacings)]
    sphere_cent_map = flex.double(sphere_cent_map)

    # Get map coefficients for map after spherical masking
    # Define box big enough to hold sphere plus soft masking
    boundary_to_smoothing_ratio = 2
    soft_mask_radius = d_min
    padding = soft_mask_radius * boundary_to_smoothing_ratio
    cushion = flex.double(3,radius+padding)
    cart_min = flex.double(sphere_cent_map) - cushion
    cart_max = flex.double(sphere_cent_map) + cushion
    max_outside = 0.
    for i in range(3):
      if cart_min[i] < 0:
        max_outside = max(max_outside, -cart_min[i])
      if cart_max[i] > ucpars[i]-spacings[i]:
        max_outside = max(max_outside, cart_max[i]-(ucpars[i]-spacings[i]))
    if max_outside > padding + radius/2:
      print("\nWARNING: substantial fraction of sphere is outside map volume", file=log)
    elif max_outside > padding:
      print("\nWARNING: sphere is partially outside map volume", file=log)
    elif max_outside > 0:
      print("\nWARNING: sphere too near map edge to allow full extent of smooth masking", file=log)

    cs = mmm.crystal_symmetry()
    uc = cs.unit_cell()

    # Box the map within xyz bounds, converted to map grid units
    lower_frac = uc.fractionalize(tuple(cart_min))
    upper_frac = uc.fractionalize(tuple(cart_max))
    all_orig = mmm.map_data().all()
    lower_bounds = [int(math.floor(f * n)) for f, n in zip(lower_frac, all_orig)]
    upper_bounds = [int(math.ceil( f * n)) for f, n in zip(upper_frac, all_orig)]
    working_mmm = mmm.extract_all_maps_with_bounds(
        lower_bounds=lower_bounds, upper_bounds=upper_bounds)

    # Make and apply spherical mask
    working_mmm.create_spherical_mask(
      mask_radius=radius, soft_mask_radius=soft_mask_radius,
      boundary_to_smoothing_ratio=boundary_to_smoothing_ratio)
    working_mmm.apply_mask_to_maps()

    # Compute normalised map coefficients and Dobs values that drop to 0.1 at stated resolution limit
    d_max = 2.1 * cushion[0] # Bigger than largest box dimension to avoid 0,0,0 term
    map_coeffs = working_mmm.map_as_fourier_coefficients(d_min=d_min, d_max=d_max)
    e_array = map_coeffs.amplitudes()
    e_array.setup_binner(auto_binning = True)
    e_array = e_array.quasi_normalize_structure_factors()
    expectE = e_array.phase_transfer(phase_source = map_coeffs.phases(deg=False), deg = False)
    ones_array = flex.double(expectE.size(), 1.)
    dobs = expectE.customized_copy(data=ones_array)
    B_falloff = -4*d_min**2 * math.log(0.1)
    dobs = dobs.apply_debye_waller_factors(b_iso=B_falloff)

    # Compute coefficients for map used for mapCC calculations
    sigmaA = 0.9 # Default for likelihood-weighted map
    dosa = sigmaA*dobs.data()
    lweight = dobs.customized_copy(data=(2*dosa)/(1.-flex.pow2(dosa)))
    wEmean2 = lweight*expectE
    working_mmm.add_map_from_fourier_coefficients(map_coeffs=wEmean2, map_id='map_manager_lwtd')

    # Apply phase shift needed for map coefficients outside of map_model_manager framework
    shift_cart = working_mmm.shift_cart()
    ucwork = expectE.crystal_symmetry().unit_cell()
    shift_frac = ucwork.fractionalize(shift_cart)
    shift_frac = tuple(-flex.double(shift_frac))
    expectE = expectE.translational_shift(shift_frac)
    mtz_dataset = expectE.as_mtz_dataset(column_root_label='Emean')
    mtz_dataset.add_miller_array(dobs, column_root_label='Dobs', column_types='W')
    mtz_object = mtz_dataset.mtz_object()
    # Write the oversampling and map origin in the history of the mtz
    oversampling_factor = 4. # Guess in this context when ordered volume not known
    os_string = "%8.7g" % oversampling_factor
    mapori = -flex.double(shift_cart)
    mapori_string = "%8.2f " % mapori[0] + "%8.2f " % mapori[1] + "%8.2f" % mapori[2]
    history_string = "PHASER OSAMP " + os_string + " MAPORI " + mapori_string
    mtz_object.add_history(lines=flex.std_string([history_string]))
    id_tng_dir = os.path.basename(phasertng_dir).split('_')[1]
    mtzout_file_name = os.path.join(phasertng_dir, region_name + '_' + id_tng_dir + "_weighted_map_data.mtz")
    dm.write_miller_array_file(mtz_object, filename=mtzout_file_name)
    scattering_fraction = 1./1.5

    return{'path_mtz':mtzout_file_name,'oversampling':oversampling_factor,
           'scattering_fraction':scattering_fraction,'shift_cart':shift_cart,
           'mmm_focus':working_mmm}

def prepare_docking_sphere(map_model_manager,d_min,sphere_cent,radius,
                           phasertng_dir,region_name,determine_ordered_volume=True,
                           verbosity=1,log=sys.stdout):
    if (map_model_manager.map_manager_1() and map_model_manager.map_manager_2()):
      if determine_ordered_volume:
        ordered_mask_id = 'ordered_volume_mask'
      else:
        ordered_mask_id = None
      results = prepare_map_for_docking.assess_cryoem_errors(
                            mmm=map_model_manager,
                            d_min=d_min,
                            determine_ordered_volume=determine_ordered_volume,
                            ordered_mask_id=ordered_mask_id,
                            fixed_mask_id='mask_around_atoms',
                            sphere_cent=sphere_cent,
                            radius=radius,
                            verbosity=verbosity,
                            log=log)
      dict_info_map = write_mtz_from_prepared_map(results_prep=results,
                            phasertng_dir=phasertng_dir,region_name=region_name)
    else:
      if (not map_model_manager.map_manager()):
        raise Sorry("Full map is required")
      if determine_ordered_volume:
         raise Sorry("Ordered volume cannot be determined without half-maps")
      dict_info_map = write_mtz_from_full_map(
                            mmm=map_model_manager,
                            d_min=d_min,
                            sphere_cent=sphere_cent,
                            radius=radius,
                            phasertng_dir=phasertng_dir,
                            region_name=region_name,log=log)
    return dict_info_map
