#! /usr/bin/env python
# -*- coding: utf-8 -*-

#####################################################################
#  Copyright and authorship belong to:                               #
#  @author: Massimo Sammito                                         #
#  @email: msacri@ibmb.csic.es / massimo.sammito@gmail.com          #
#####################################################################
#####################################################################
#                              LICENCE                              #
#                                                                   #
#                                                                   #
#####################################################################

# System imports
# System imports
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

from future.standard_library import install_aliases

install_aliases()
import sys
import math
import itertools
import os
import time
import math
import networkx
import copy
import logging
import pickle
import multiprocessing.queues
from operator import itemgetter
from functools import reduce  # forward compatibility for Python 3
import operator
import shutil
import collections
import numpy as np
import mmtbx.utils
import mmtbx.programs.fmodel
import mmtbx.model
import iotbx.pdb.mmcif
from cctbx.crystal import reindex


try:
    from queue import Queue, Empty
except:
    from Queue import Empty

try:
    import cctbx
    import cctbx.uctbx
    import cctbx.sgtbx
    import scitbx
    from cctbx.array_family import flex
    from cctbx.uctbx.determine_unit_cell import NCDist
    from iotbx.file_reader import any_file
    from libtbx.phil import parse
    from phasertng.phil.master_node_file import *
    from phasertng.phil.master_phil_file import *
    from phasertng.phil.converter_registry import converter_registry
    from phasertng.phil.phil_to_cards import phil_to_cards
    from phasertng.phil.cards_to_phil import cards_to_phil
    import phasertng as ptng
    from mmtbx.scaling import pair_analyses
except:
    pass


SET_OF_DIR_CREATED = set([])

#####################################################################################################
#                               SUPPORT MATH FUNCTIONS                                              #
#####################################################################################################


def split_pairwise_matrix_in_groups(matrix_input, test, boolean_condition=False, getlist=False):
    # NOTE: this function is returning the indeces starting from 1 instead of the classical
    # notation starting from 0. This is because is actually returning what models can be put
    # in the same cluster and not the indeces.

    # NOTE: This first line ensure that all the diagonal, and thus all the models paired with themselves, are considered as satisfying to belong in the same group
    if boolean_condition:
        matrix = matrix_input.astype(int)
    else:
        matrix = matrix_input.copy()

    # matrix[np.diag_indices(matrix.shape[0], ndim=2)] = test
    # print("MATRIX AFTER DIAGONAL CORRECTION")
    # print([list(ll) for ll in list(matrix)])
    # print()

    if (not boolean_condition and matrix[np.where(matrix < test)].flatten().shape[0]) > 0 or (
            boolean_condition and test and matrix[np.where(matrix)].flatten().shape[0] > 0) or (
            boolean_condition and not test and matrix[np.where(~matrix)].flatten().shape[0] > 0):
        # NOTE the following copy here is fundamental because then we need to inspect this matrix without modifying it
        # otherwise the second test of each if will convert every element into 0
        mm = matrix.copy()
        if not boolean_condition:
            matrix[np.where(mm >= test)] = 1
            matrix[np.where(mm < test)] = 0
        elif boolean_condition and test:
            matrix[np.where(mm)] = 1
            matrix[np.where(~mm)] = 0
        elif boolean_condition and not test:
            matrix[np.where(~mm)] = 1
            matrix[np.where(mm)] = 0

        # print("MATRIX TRANSFORMED IN ADJACENCY MATRIX")
        # print([list(ll) for ll in list(matrix)])
        # print()
        # print(list(matrix))

        G = networkx.convert_matrix.from_numpy_matrix(matrix, parallel_edges=False)
        cliques = networkx.algorithms.find_cliques(G)

        if not getlist:
            return cliques, matrix
        else:
            return list(cliques), matrix
    else:
        return []

    #     dicorr = {}
    #     for i in range(matrix.shape[0]):
    #         dicorr[i + 1] = []
    #         for j in range(matrix.shape[1]):
    #             if not boolean_condition and matrix[i][j] >= test:
    #                 dicorr[i + 1].append(j + 1)
    #             elif boolean_condition and test == matrix[i][j]:
    #                 dicorr[i + 1].append(j + 1)
    #
    #
    #     #NOTE: Checking the compatibility for each row and its intersections with the involved rows
    #     listcompat = []
    #     for key, item in dicorr.items():
    #         #print(key, item)
    #         listcompat.append(tuple(key))
    #
    #         s = set(item)
    #         for
    #         for l1 in item:
    #             s = s & set(dicorr[l1])
    #             if len(s) > 0:
    #                 #print("Intersect:", s)
    #                 listcompat.append(tuple(s))
    #
    #     #NOTE: Finding the subset already contained in other clusters
    #     newset = set([])
    #     listcompat = list(set(listcompat))
    #     for iii, s1 in enumerate(listcompat):
    #         found = False
    #         for jjj, s2 in enumerate(listcompat):
    #             if jjj == iii: continue
    #             #print("s1",s1,"s2",s2,"s1<=s2",set(s1) <= set(s2))
    #             if set(s1) <= set(s2):
    #                 found = True
    #                 break
    #         if not found:
    #             newset.add(s1)
    #
    #     dicorr = list(newset)
    #     return dicorr
    # else:
    #     return [range(1,matrix.shape[0]+1)]

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def cantor_pairing(listall):
    if len(listall) == 0:
        return None
    if len(listall) == 1:
        return listall[0]
    lastElement = listall.pop(0)
    return (0.5 * (cantor_pairing(listall) + lastElement) * (cantor_pairing(listall) + lastElement + 1) + lastElement)

def string_is_numeric(input):
    try:
        float(input)
        return True
    except:
        return False

def hatti(chain_lenght, alignment_ref, alignment_b, molprobity_score, targetRes):
    # print("CHAIN_LENGHT",chain_lenght,"MOLSCORE",molprobity_score,"TARGET_RES",targetRes)
    return RMScalculation(chain_lenght, alignment_ref, alignment_b, molprobity_score, targetRes)

def pairs_with_non_standard_amino_acids(special_non_standard_amino_acid, standard_amino_acid):
    '''The following dictonary gives average gonnet score values for special
      amino acid cases.  For example, dictonary_for_X defines the average values
      for each amino acid substituteion if substituted by any amino acid indicated
      by 'X' alphabet.
      '''
    if special_non_standard_amino_acid == "X":
        dictonary_for_X={'C': -0.8,
                    'S': -0.355,
                    'T': -0.35,
                    'P': -1.11,
                    'A': -0.395,
                    'G': -1.545,
                    'N': -0.48,
                    'D': -0.935,
                    'E': -0.68,
                    'Q': -0.32,
                    'H': -0.04,
                    'R': -0.505,
                    'K': -0.52,
                    'M': -0.525,
                    'I': -0.92,
                    'L': -0.885,
                    'V': -0.695,
                    'F': -0.87,
                    'Y': -0.445,
                    'W': -1.315,
                    }
        return(dictonary_for_X[standard_amino_acid])
    elif special_non_standard_amino_acid == "B":
        ## "B" indicates either Aspartic acid (D) or Asparagine (N) the following
        ###dictonary gives average values for B
        dictonary_for_B={'C': -2.5,
                    'S': 0.7,
                    'T': 0.25,
                    'P': -0.8,
                    'A': -0.3,
                    'G': 0.25,
                    'N': 3.0,
                    'D': 3.45,
                    'E': 1.8,
                    'Q': 0.8,
                    'H': 0.8,
                    'R': 0.0,
                    'K': 0.65,
                    'M': -2.6,
                    'I': -3.3,
                    'L': -3.5,
                    'V': -2.55,
                    'F': -3.8,
                    'Y': -2.1,
                    'W': -4.4,
                    }
        return(dictonary_for_B[standard_amino_acid])
    elif special_non_standard_amino_acid == "Z":
        ##"Z" indicates either Glutamic acid (E) or Glutamine (Q) the following
        ###dictonary gives average values for Z
        dictonary_for_Z={'C': -2.7,
                        'S': 0.2,
                        'T': -0.05,
                        'P': -0.35,
                        'A': -0.1,
                        'G': -0.9,
                        'N': 0.8,
                        'D': 1.8,
                        'E': 2.65,
                        'Q': 2.2,
                        'H': 0.8,
                        'R': 0.95,
                        'K': 1.35,
                        'M': -1.5,
                        'I': -2.3,
                        'L': -2.2,
                        'V': -1.7,
                        'F': -3.25,
                        'Y': -2.2,
                        'W': -3.5,
                        }
        return(dictonary_for_Z[standard_amino_acid])
    elif special_non_standard_amino_acid == "J":
        ##"J" indicates either Leucine (L) or Isoleucine (I), used in mass-spec (NMR)
        ###the following dictonary gives average values for J
        dictonary_for_J={'C': -1.3,
                        'S': -1.95,
                        'T': -0.95,
                        'P': -2.45,
                        'A': -1.0,
                        'G': -4.45,
                        'N': -2.9,
                        'D': -3.9,
                        'E': -2.75,
                        'Q': -1.75,
                        'H': -2.05,
                        'R': -2.3,
                        'K': -2.1,
                        'M': 2.65,
                        'I': 3.4,
                        'L': 3.4,
                        'V': 2.45,
                        'F': 1.5,
                        'Y': -0.35,
                        'W': -1.25,
                        }
        return(dictonary_for_J[standard_amino_acid])
    else:
        print("Special non-standard amino acid not defined")
        return()

def score_match(pair, matrix):
    non_standard_amino_acids = ['X', 'B', 'Z', 'J']
    if (pair[0] not in non_standard_amino_acids) and (pair[1] not in non_standard_amino_acids):
        if pair not in matrix:
            return matrix[(tuple(reversed(pair)))]
        else:
            return matrix[pair]
    elif (pair[0] in non_standard_amino_acids) and (pair[1] not in non_standard_amino_acids):
        return pairs_with_non_standard_amino_acids(pair[0], pair[1])

    elif (pair[0] not in non_standard_amino_acids) and (pair[1] in non_standard_amino_acids):
        return pairs_with_non_standard_amino_acids(pair[1], pair[0])
    else:
        print("Special non-standard amino acid not defined in the pair ", pair)

def RMScalculation_NMR(phasingModelPDBID_sculpted_merged_chainLength,gonne_score,molprobity,targetResolution,model_RMSDmedian):
    '''
    This function calculates ePrimeVRMS
    '''
    Ak=0.434668
    Bk=-1.257682
    Ck=0.087817
    Fk=0.220692
    Ek=0.458175
    Dk=0.151477
    eVRMS=((Ak*(phasingModelPDBID_sculpted_merged_chainLength)**(1/3)) + (Bk * (math.exp(Ck* (gonne_score)))) + (Dk * (molprobity)) + (Ek * (model_RMSDmedian)) + (Fk * (targetResolution)))**(1/2)
    return (eVRMS)

def score_pairwise_generic(alignment_ref, alignment_b, matrixType):
    '''
    Calculates BLOSUM 45 or 62 matrix score. It also needs score_match function
    which is internally called.  This function can be called as below:
    $ score_pairwise(seqAlignFile, 62, -11, -1)
    '''

    from Bio.SubsMat import MatrixInfo
    # print("REFERENCE")
    # print(alignment_ref)
    # print("OTHER")
    # print(alignment_b)

    gap_s = 0
    gap_e = 0
    matrixType = matrixType
    # matrix = MatrixInfo.blosum45
    matrix = getattr(MatrixInfo, matrixType)
    # matrix=matrixType
    seq1 = alignment_ref
    seq2 = alignment_b
    matrixScore = []
    score = 0
    gap = False
    numOfTotalGaps = 0
    numberOf_alignedResidues = 0
    seq1_length = len(str(seq1).replace('-', ''))
    seq2_length = len(str(seq2).replace('-', ''))
    for i in range(len(seq1)):
        pair = (seq1[i], seq2[i])
        if not gap:
            if '-' in pair:
                gap = True
                score += gap_s
                numOfTotalGaps += 1
            else:
                score += score_match(pair, matrix)
        else:
            if '-' not in pair:
                gap = False
                score += score_match(pair, matrix)
            else:
                score += gap_e
                numOfTotalGaps += 1
    numberOf_alignedResidues = float(((seq1_length + seq2_length) - numOfTotalGaps) / 2)
    #print("SHERLOCK numberOf..",numberOf_alignedResidues)
    #print("SHERLOCK check seq1",seq1)
    #print("SHERLOCK check seq2", seq2)
    #print("SHERLOCK score", score)
    try:
        Nscore = float(score / numberOf_alignedResidues)
    except ZeroDivisionError:
        Nscore = 0
    # print("normalised gonnet score is", Nscore)
    # print(Nscore)
    # print()

    return Nscore

def RMScalculation(phasingModelChainLength, alignment_ref, alignment_b, MolProbityScore, targetResolution):
    '''
    This function calculates ePrimeVRMS
    '''
    # calling gonnet scoring function

    gonnetscore = score_pairwise_generic(alignment_ref, alignment_b, "gonnet")
    # print("GONNET",sequenceIdentity)
    # NOTE: Negative number will generate an error in the calculation of the eVRMS
    if gonnetscore < 0: gonnetscore = 0

    Ak = 0.001457
    Bk = 1.684
    Ck = -0.2543
    Dk = 0.09712
    Fk = 0.01533
    eVRMS = ((Ak * (phasingModelChainLength) ** (1)) + (Bk * (math.exp(Ck * (gonnetscore) ** (2.5)))) + (
                Dk * (MolProbityScore) ** (1)) + (Fk * (targetResolution) ** (3))) ** (1 / 2)
    # print ("eVRMS of this file is ", eVRMS)
    return eVRMS

def sort_multi_ensembles_pdblist_by_sol(enseout, topsol):
    solu = [{"euler": x.getEuler(),
                           "frac": x.TRA,
                           "bfac": x.BFAC,
                           "mult": x.MULT,
                           "ofac": x.OFAC,
                           "modlid": x.MODLID} for x in topsol.KNOWN]

    newlist= []
    for sol in solu:
        print("Solu",sol["modlid"])
        for ense in enseout:
            if sol["modlid"] in open(ense, "r").read():
                print("  found in",ense)
                newlist.append(ense)
                break
    assert len(newlist) == len(enseout)

    return newlist

def create_directory_timestamp(pathbase):
    direct = None
    global SET_OF_DIR_CREATED
    while 1:
        try:
            direct = pathbase +str(np.random.randint(low=1, high=10000))+str(int(time.time()))
            os.makedirs(direct)
            break
        except:
            continue

    if not os.path.basename(direct).startswith("phasertng_"):
        SET_OF_DIR_CREATED.add(direct)

    return direct

def clean_directories():
    global SET_OF_DIR_CREATED
    for ele in SET_OF_DIR_CREATED:
        if os.path.exists(ele):
            print("Removing: ",ele)
            shutil.rmtree(ele)
    SET_OF_DIR_CREATED = set([])

def add_paths_to_directory_set(path_or_set_of_paths):
    global SET_OF_DIR_CREATED

    if isinstance(path_or_set_of_paths, set):
        SET_OF_DIR_CREATED = SET_OF_DIR_CREATED.union(path_or_set_of_paths)
    elif islistortuple(path_or_set_of_paths):
        SET_OF_DIR_CREATED = SET_OF_DIR_CREATED.union(set(path_or_set_of_paths))
    else:
        SET_OF_DIR_CREATED.add(path_or_set_of_paths)


def cantor_pairing(listall):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com

    :param listall:
    :return:
    """
    listall = list(listall)
    if len(listall) == 0:
        return None
    if len(listall) == 1:
        return listall[0]
    lastElement = listall.pop(0)
    return (0.5 * (cantor_pairing(listall) + lastElement) * (cantor_pairing(listall) + lastElement + 1) + lastElement)

def get_from_dict_through_list(dataDict, mapList):
    try:
        return reduce(operator.getitem, mapList, dataDict)
    except:
        return None

def chunks(lst, n, i=0, f=1):
    """Yield successive n-sized chunks from lst."""
    final = []
    while 1:
        t = lst[i:i + (f * n)]
        i += len(t)
        f += 1
        if len(t) > 0:
            final.append(t)
        else:
            break
    return final

def alternate_and_mix_pos(pos):
    z = list(range(pos.shape[0]))
    inter = int(round(((len(z) / 2) - 1) / 2))
    i_0 = 0
    i_180 = int(round(len(z) / 2))
    i_90 = i_0 + inter
    i_270 = i_180 + inter
    # print(inter,i_0,i_90,i_180,i_270)

    z_1 = list(reversed(z[i_90:i_270 + 1]))  # reversed
    z_2 = z[i_270 + 1:] + z[i_0:i_90]
    q = np.array(list(zip(z_1, z_2))).flatten()
    a = np.array([x for x in z_1 if x not in q])
    # print(q)
    # print(a)
    if a.shape[0] > 0:
        q = np.append(q, a)
    b = np.array([x for x in z_2 if x not in q])
    # print(b)
    if b.shape[0] > 0:
        q = np.append(q, b)

    # print(q)

    q = np.array(q)
    # print(pos)
    print(q)
    pos = pos[q]
    # print(pos)
    return pos

def dict_merge(dct, merge_dct):
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]

def pickle_trick(obj, max_depth=30):
        import pickle
        output = {}

        if max_depth <= 0:
            return output

        try:
            pickle.dumps(obj)
        except (pickle.PicklingError, TypeError) as e:
            failing_children = []

            if hasattr(obj, "__dict__"):
                for k, v in obj.__dict__.items():
                    result = pickle_trick(v, max_depth=max_depth - 1)
                    if result:
                        failing_children.append(result)

            output = {
                "fail": obj,
                "err": e,
                "depth": max_depth,
                "failing_children": failing_children
            }

        return output

class MinMaxScaler(object):
    def __init__(self, OldMin, OldMax, NewMin, NewMax):
        self.OldMin = OldMin
        self.OldMax = OldMax
        self.NewMin = NewMin
        self.NewMax = NewMax

    def scale(self, value, integer=False):
        q = (((value - self.OldMin) * (self.NewMax - self.NewMin)) / (self.OldMax - self.OldMin)) + self.NewMin
        if integer:
            return int(round(q))
        else:
            return q

class MultiProcessingPriorityQueue(object):
    def __init__(self):
        try:
            ctx = multiprocessing.get_context()
            self.queues = {"high":multiprocessing.queues.Queue(ctx=ctx), "normal":multiprocessing.queues.Queue(ctx=ctx), "low":multiprocessing.queues.Queue(ctx=ctx)}
        except:
            self.queues = {"high":multiprocessing.queues.Queue(), "normal":multiprocessing.queues.Queue(), "low":multiprocessing.queues.Queue()}

        self.registered_commands = {}
        self.in_memory_db = {}

    #How to constrain the state_id_index and the waiting_node_index without extracting checking and then reinserting?
    def register_command(self, command, state_id_index=None, waiting_node_index=None, priority_level=None):
        from New_Voyager import system_utility
        if system_utility.is_str_or_unicode(priority_level):
            if priority_level == "high":
                priority_level = 0
            elif priority_level == "normal":
                priority_level = 0.5
            elif priority_level == "low":
                priority_level = -1

        self.registered_commands[command] = (priority_level, state_id_index, waiting_node_index)

    #How to get a copy of all entries without extracting everything and then reinserting?
    def get_a_copy_of_all_entries(self):
        return [x for x in self.in_memory_db.values()]

    def put(self, obj, **kwargs):
        #ee = pickle.dumps(obj)
        if islistortuple(obj):
            obj,where = obj
        else:
            where = None
        ii = int(id(obj)+time.time()) + np.random.randint(0, 1000)

        if "block" not in kwargs: kwargs["block"] = True
        if obj["command"] in self.registered_commands:
            where = self.registered_commands[obj["command"]][0]

        if (where is not None and where==0) or (where is None and "priority" in obj and (obj["priority"] is None or obj["priority"] == 0)):
            self.queues["high"].put((ii,obj), **kwargs)
            where = "high"
        elif (where is not None and 0<where<=1) or (where is None and "priority" in obj and 0 < obj["priority"] <= 1):
            self.queues["normal"].put((ii,obj), **kwargs)
            where = "normal"
        else:
            self.queues["low"].put((ii,obj), **kwargs)
            where = "low"

        self.in_memory_db[ii] = (obj,where)

    def get(self, *args, **kwargs):
        states = None
        if "states" in kwargs:
            states = kwargs["states"]
            del kwargs["states"]

        ii,ee,where = None,None,None
        temporary = []
        all_empty = False
        while 1:
            try:
                #print("Getting from the high priority queue...")
                ii,ee = self.queues["high"].get(*args, **kwargs)
                where = "high"
            except:
                # import traceback
                # print(sys.exc_info())
                try:
                    #print("Getting from the normal priority queue")
                    ii,ee = self.queues["normal"].get(*args, **kwargs)
                    where = "normal"
                except:
                    # import traceback
                    # print(sys.exc_info())
                    try:
                        #print("Getting from the low priority queue")
                        ii,ee = self.queues["low"].get(*args, **kwargs)
                        where = "low"
                    except:
                        #import traceback
                        #print(sys.exc_info())
                        #print("Everything is empty")
                        all_empty = True
                        break

            if states is not None and \
                    ee["command"] in self.registered_commands and \
                    self.registered_commands[ee["command"]][2] is not None and \
                    ee[self.registered_commands[ee["command"]][2]] is not None and \
                    ee[self.registered_commands[ee["command"]][2]] not in \
                    [no for path in states[ee[self.registered_commands[ee["command"]][1]]].ended_branches for no in path]:
                #print("...The extracted schedule is not ready to be used...Skip...")
                temporary.append((where, ii, ee))
                continue
            else:
                break

        for (w,i,e) in temporary:
            #print("Adding back to",w,"the schedule",i)
            self.queues[w].put((i, e), block=True)

        if all_empty or ee is None:
            #print("Everything is empty return empty exeception")
            raise Empty()
        else:
            #print("Removing",ii,"from memory")
            if ii in self.in_memory_db: del self.in_memory_db[ii]
            #print("returning",ee)
            return ee


class NotRemovableList(list):
    """
    A List where all the operation that require the deletion of one or more elements are forbidden.
    It raises an exeception if del, list.remove(), list.pop() are invoked.
    """

    def __init__(self, list_a):
        """
        Constructor for NotRemovableLits

        :param list_a: List of elements that constitute the NotRemovableLits
        :type list_a: list
        """
        super(list, self).__init__()
        for x in list_a: self.append(x)

    def __delattr__(self, item):
        raise Exception("NotRemovableList does not allow any attribute removal")

    def __delitem__(self, key):
        raise Exception("NotRemovableList does not allow any item removal")

    def __delslice__(self, i, j):
        raise Exception("NotRemovableList does not allow any slice removal")

    def pop(self, *args, **kwargs):
        """
        Raise an exception as pop is not allowed

        :param index: Index to be removed from the list
        :type index: int
        :raise Exception: NotRemovableList does not allow any pop item
        """
        raise Exception("NotRemovableList does not allow any pop item")

    def remove(self, obj):
        """
        Raise an exception as remove is not allowed

        :param obj: Object to be removed from the list
        :type obj: object
        :raise Exception: NotRemovableList does not allow any item removal
        """
        raise Exception("NotRemovableList does not allow any item removal")

#####################################################################################################
#                               SUPPORT ITERABLE FUNCTIONS                                              #
#####################################################################################################

def isiterable(obj):
    try:
        iter(obj)
    except Exception:
        return False
    else:
        return True

def islistortuple(obj):
    if isinstance(obj, list):
        return True
    elif isinstance(obj, tuple):
        return True
    else:
        return False

def consecutive_groups(iterable, ordering=lambda x: x):
    """Yield groups of consecutive items using :func:`itertools.groupby`.
    The *ordering* function determines whether two items are adjacent by
    returning their position.
    By default, the ordering function is the identity function. This is
    suitable for finding runs of numbers:
        >>> iterable = [1, 10, 11, 12, 20, 30, 31, 32, 33, 40]
        >>> for group in consecutive_groups(iterable):
        ...     print(list(group))
        [1]
        [10, 11, 12]
        [20]
        [30, 31, 32, 33]
        [40]
    For finding runs of adjacent letters, try using the :meth:`index` method
    of a string of letters:
        >>> from string import ascii_lowercase
        >>> iterable = 'abcdfgilmnop'
        >>> ordering = ascii_lowercase.index
        >>> for group in consecutive_groups(iterable, ordering):
        ...     print(list(group))
        ['a', 'b', 'c', 'd']
        ['f', 'g']
        ['i']
        ['l', 'm', 'n', 'o', 'p']
    """

    #NOTE: It looks like it gives problems in python3 ??. Super Double Check!!!
    for k, g in itertools.groupby(enumerate(iterable), key=lambda x: x[0] - ordering(x[1])):
        yield map(itemgetter(1), g)


def g6_form(v):
    g6 = np.zeros(6)

    # NOTE: G6 form defined as: [a^2, b^2, c^2, 2bccos(alpha), 2accos(beta), 2abcos(gamma)]
    g6[0] = v[0] ** 2
    g6[1] = v[1] ** 2
    g6[2] = v[2] ** 2
    g6[3] = 2 * v[1] * v[2] * np.cos(v[3])
    g6[4] = 2 * v[0] * v[2] * np.cos(v[4])
    g6[5] = 2 * v[0] * v[1] * np.cos(v[5])

    return g6

try :
    class V7(object):
        def __init__(self, cell1):
            """

            :param cell1: [a,b,c,alpha,beta,gamma] angles in degree
            """
            self.Scaledist = np.sqrt(6.0 / 7.0)

            cell1 = np.array(cell1)

            ####self.r16 = self.get_reciprocal_cell(self.angles_to_radians(self.cell1))
            p16 = cctbx.uctbx.unit_cell(list(cell1)).niggli_cell().reciprocal_parameters()
            ####self.t16 = cctbx.uctbx.unit_cell(list(self.angles_to_degrees(self.r16))).niggli_cell().parameters()
            q16 = cctbx.uctbx.unit_cell(p16).niggli_cell().parameters()

            self.reduced_cell = self.angles_to_radians(cell1)
            self.reduced_reciprocal = self.angles_to_radians(q16)

            # self.reduced_cell_v7 = self.v7_form(self.reduced_cell, self.reduced_reciprocal)
            self.reduced_cell_v7 = self.v7_form_native(self.reduced_cell)

        def angles_to_radians(self, v):
            # NOTE: Transforming angles in radians
            q = np.array(v).copy()
            q[3] = v[3] * (np.pi / 180.0)
            q[4] = v[4] * (np.pi / 180.0)
            q[5] = v[5] * (np.pi / 180.0)
            return q

        def angles_to_degrees(self, v):
            q = np.array(v).copy()
            q[3] = v[3] * (180.0 / np.pi)
            q[4] = v[4] * (180.0 / np.pi)
            q[5] = v[5] * (180.0 / np.pi)
            return q

        def g6_form(self, v):
            g6 = np.zeros(6)

            # NOTE: G6 form defined as: [a^2, b^2, c^2, 2bccos(alpha), 2accos(beta), 2abcos(gamma)]
            g6[0] = v[0] ** 2
            g6[1] = v[1] ** 2
            g6[2] = v[2] ** 2
            g6[3] = 2 * v[1] * v[2] * np.cos(v[3])
            g6[4] = 2 * v[0] * v[2] * np.cos(v[4])
            g6[5] = 2 * v[0] * v[1] * np.cos(v[5])

            return g6

        def v7_form(self, v, vr):
            v7 = np.zeros(7)

            v7[0] = v[0]
            v7[1] = v[1]
            v7[2] = v[2]
            v7[3] = 1.0 / vr[0]
            v7[4] = 1.0 / vr[1]
            v7[5] = 1.0 / vr[2]
            v7[6] = self.get_cell_volume(v) ** (1.0 / 3.0)

            # print("Cell Volume method V7",v7[6])
            # print("Cell Volume method cctbx", cctbx.uctbx.unit_cell(list(self.angles_to_degrees(v))).volume())

            return v7

        def v7_form_native(self, v):
            g6 = self.g6_form(v)
            v7 = np.zeros(7)
            v7[0] = np.sqrt(g6[0])
            v7[1] = np.sqrt(g6[1])
            v7[2] = np.sqrt(g6[2])

            nc = self.cell_from_g6(g6)
            vi = self.get_reciprocal_cell(nc)

            # vig6 = self.g6_form(vi)

            return self.v7_form(v, vi)

            # ri = self.reduce_cell(vig6)

        # def reduce(self, vig6, delta=0.0):
        #     m1 = np.zeros(6)

        def cell_from_g6(self, v):
            m_cell = np.zeros(6)
            m_cell[0] = np.sqrt(v[0])
            m_cell[1] = np.sqrt(v[1])
            m_cell[2] = np.sqrt(v[2])

            cosalpha = (0.5 * v[3] / (m_cell[1] * m_cell[2]))
            cosbeta = (0.5 * v[4] / (m_cell[0] * m_cell[2]))
            cosgamma = (0.5 * v[5] / (m_cell[0] * m_cell[1]))

            m_cell[3] = np.arctan2(np.sqrt(1.0 - (cosalpha ** 2)), cosalpha)
            m_cell[4] = np.arctan2(np.sqrt(1.0 - (cosbeta ** 2)), cosbeta)
            m_cell[5] = np.arctan2(np.sqrt(1.0 - (cosgamma ** 2)), cosgamma)

            return m_cell

        def get_reciprocal_cell(self, v):
            cell = np.zeros(6)
            volume = self.get_cell_volume(v)

            cosAlphaStar = (np.cos(v[4]) * np.cos(v[5]) - np.cos(v[3])) / np.abs(np.sin(v[4]) * np.sin(v[5]))
            cosBetaStar = (np.cos(v[3]) * np.cos(v[5]) - np.cos(v[4])) / np.abs(np.sin(v[3]) * np.sin(v[5]))
            cosGammaStar = (np.cos(v[3]) * np.cos(v[4]) - np.cos(v[5])) / np.abs(np.sin(v[3]) * np.sin(v[4]))

            cell[0] = v[1] * v[2] * np.sin(v[3]) / volume
            cell[1] = v[0] * v[2] * np.sin(v[4]) / volume
            cell[2] = v[0] * v[1] * np.sin(v[5]) / volume
            cell[3] = np.arctan2(np.sqrt(1.0 - (cosAlphaStar) ** 2), cosAlphaStar)
            cell[4] = np.arctan2(np.sqrt(1.0 - (cosBetaStar) ** 2), cosBetaStar)
            cell[5] = np.arctan2(np.sqrt(1.0 - (cosGammaStar) ** 2), cosGammaStar)

            return cell

        def get_cell_volume(self, v):
            return v[0] * v[1] * v[2] * np.sqrt(
                1.0 - np.cos(v[3]) ** 2 - np.cos(v[4]) ** 2 - np.cos(v[5]) ** 2 + 2.0 * np.cos(v[3]) * np.cos(
                    v[4]) * np.cos(v[5]))

        def V7_distance(self, cell2):
            cell2 = np.array(cell2)
            p26 = cctbx.uctbx.unit_cell(list(cell2)).niggli_cell().reciprocal_parameters()
            q26 = cctbx.uctbx.unit_cell(p26).niggli_cell().parameters()

            reduced_cell2 = self.angles_to_radians(cell2)
            reduced_reciprocal2 = self.angles_to_radians(q26)
            # reduced_cell2_v7 = self.v7_form(reduced_cell2, reduced_reciprocal2)
            reduced_cell2_v7 = self.v7_form_native(reduced_cell2)

            # print(self.reduced_cell_v7 - reduced_cell2_v7)
            # print((self.reduced_cell_v7 - reduced_cell2_v7)**2)
            # print(np.sum((self.reduced_cell_v7 - reduced_cell2_v7)**2))
            # print(np.sqrt(np.sum((self.reduced_cell_v7 - reduced_cell2_v7)**2)))
            return np.linalg.norm(self.reduced_cell_v7 - reduced_cell2_v7, ord=2) * self.Scaledist

    class SAUC_search(object):
        def __init__(self, uc, lattice):

            self.CS6M_MAT_P = np.array([
                [1., 0., 0., 0., 0., 0.],
                [0., 1., 0., 0., 0., 0.],
                [0., 0., 1., 0., 0., 0.],
                [0., 0., 0., 1., 0., 0.],
                [0., 0., 0., 0., 1., 0.],
                [0., 0., 0., 0., 0., 1.]])

            self.CS6M_MAT_I = np.array([
                [1., 0., 0., 0., 0., 0.],
                [0., 1., 0., 0., 0., 0.],
                [.25, .25, .25, .25, .25, .25],
                [0., 1., 0., .5, 0., .5],
                [1., 0., 0., 0., .5, .5],
                [0., 0., 0., 0., 0., 1.]])

            self.CS6M_MAT_A = np.array([
                [1., 0., 0., 0., 0., 0.],
                [0., 1., 0., 0., 0., 0.],
                [0., .25, .25, .25, 0., 0.],
                [0., 1., 0., .5, 0., 0.],
                [0., 0., 0., 0., .5, .5],
                [0., 0., 0., 0., 0., .1]])

            self.CS6M_MAT_B = np.array([
                [1., 0., 0., 0., 0., 0.],
                [0., 1., 0., 0., 0., 0.],
                [.25, 0., .25, 0., .25, 0.],
                [0., 0., 0., .5, 0., .5],
                [1., 0., 0., 0., .5, 0.],
                [0., 0., 0., 0., 0., 1.]])

            self.CS6M_MAT_C = np.array([
                [1., 0., 0., 0., 0., 0.],
                [.25, .25, 0., 0., 0., .25],
                [0., 0., 1., 0., 0., 0.],
                [0., 0., 0., .5, .5, 0.],
                [0., 0., 0., 0., 1., 0.],
                [1., 0., 0., 0., 0., .5]])

            self.CS6M_MAT_F = np.array([
                [.25, .25, 0., 0., 0., .25],
                [.25, 0., .25, 0., .25, 0.],
                [0., .25, .25, .25, 0., 0.],
                [0., 0., .5, .25, .25, .25],
                [0., .5, 0., .25, .25, .25],
                [.5, 0., 0., .25, .25, .25]])

            self.CS6M_MAT_H = np.array([
                [1., 1., 1., 1., -1., -1.],
                [4., 1., 1., 1., 2., 2.],
                [1., 4., 1., -2., -1., 2.],
                [-4., -4., 2., -1., 1., -5.],
                [2., -4., 2., -1., -2., 1.],
                [-4., 2., 2., 2., 1., 1]])

            self.CS6M_MAT_R[36] = np.array([
                [1., 0., 0., 0., 0., 0.],
                [0., 1., 0., 0., 0., 0.],
                [0., 0., 1., 0., 0., 0.],
                [0., 0., 0., 1., 0., 0.],
                [0., 0., 0., 0., 1., 0.],
                [0., 0., 0., 0., 0., 1.]])

            self.CS6M_HEXPERP = np.array([
                [.6666666666667, -.3333333333333, 0., 0., 0., .3333333333333],
                [-.3333333333333, .6666666666667, 0., 0., 0., .3333333333333],
                [0., 0., 0., 0., 0., 0.],
                [0., 0., 0., -1., 0., 0.],
                [0., 0., 0., 0., -1., 0.],
                [.3333333333333, .333333333333, 0., 0., 0., .6666666666667]])

            self.CS6M_RHMPERP = np.array([
                [.6666666666667, -.3333333333333, -.3333333333333, 0., 0., 0.],
                [-.3333333333333, .6666666666667, -.3333333333333, 0., 0., 0.],
                [-.3333333333333, -.3333333333333, .6666666666667, 0., 0., 0.],
                [0., 0., 0., .6666666666667, -.3333333333333, -.3333333333333],
                [0., 0., 0., -.3333333333333, .6666666666667, -.3333333333333],
                [0., 0., 0., -.3333333333333, -.3333333333333, .6666666666667]])

            self.probelattice = lattice
            self.probeArray = uc

            edgemax = np.max(self.probeArray[:3])
            if self.probelattice.lower() == "v" and (
                    np.min(self.probeArray[:3]) <= 1 or np.max(self.probeArray[3:]) > edgemax * 1.01):
                print("Invalid G6 cell ", self.probeArray)
            elif np.min(self.probeArray[:3]) <= 1 or np.max(self.probeArray[3:]) - 90.0 > 75.0:
                print("Invalid ", self.probelattice, "cell")

            self.makeprimredprobe()

        def makeprimredprobe(self):
            self.rawcell = self.probeArray[3:] / 57.2957795130823
            latsym = self.probelattice[0] if len(self.probelattice) > 0 else "P"
            xrawcell = g6_form(self.rawcell)
            print("xrawcell", xrawcell)
            if latsym.upper() in ["P", "A", "B", "C", "I", "F", "R", "H"]:
                primcell = self.CS6M_LatSymMat66(xrawcell, latsym)
            elif latsym.upper() == "V":
                primcell = self.probeArray
            else:
                print("Unrecognized lattice symbol ", latsym, " treated as P")
                primcell = self.CS6M_LatSymMat66(xrawcell, "P")

            self.CS6M_G6Reduce(primcell, redprimcell, reduced)

        def CS6M_G6Reduce(self, primcell):
            pass

        def CS6M_LatSymMat66(self, g6vec, latsym):
            mat66 = None
            if latsym.upper() == "P":
                mat66 = self.CS6M_MAT_P
            elif latsym.upper() == "I":
                mat66 = self.CS6M_MAT_I
            elif latsym.upper() == "A":
                mat66 = self.CS6M_MAT_A
            elif latsym.upper() == "A":
                mat66 = self.CS6M_MAT_B
            elif latsym.upper() == "C":
                mat66 = self.CS6M_MAT_C
            elif latsym.upper() == "F":
                mat66 = self.CS6M_MAT_F
            elif latsym.upper() in ["H", "R"]:
                hexperp = np.dot(self.CS6M_HEXPERP, g6vec.T)
                rhmperp = np.dot(self.CS6M_RHMPERP, g6vec.T)
                hexperpnormsq = np.dot(hexperp, hexperp)
                rhmperpnormsq = np.dot(rhmperp, rhmperp)

                if (hexperpnormsq < rhmperpnormsq):
                    mat66 = self.CS6M_MAT_H / 9.0
                else:
                    mat66 = self.CS6M_MAT_P
            else:
                mat66 = self.CS6M_MAT_P

            return np.dot(mat66, g6vec.T)

    class NCDist_metric(object):
        """
        Code and comments are from H.J.B:
        This is a python translated version of NCDist.h

        //
        //  CNCDist.c
        //
        //
        //  Created by Herbert J. Bernstein on 3/26/13.
        //
        //

        /* The projectors for the 15 base types (5-D boundaries
         in G6), plus a few extra for internal boundaries
         Note that the array indices are swapped from the
         Fortan versions */
        """

        def __init__(self, gvec1):
            self.Scaledist = 1.0 / np.sqrt(6.0)
            self.gvec1 = np.array(gvec1)
            self.ttt = [self.gvec1[0], self.gvec1[1], self.gvec1[2]]
            #####self.gvec1 = self.angles_to_radians(self.gvec1)
            self.gvec1 = self.g6_form(self.gvec1)
            self.PASS = 0
            self.NREFL_OUTER_MIN = 1
            self.NBND = 15
            self.DCUT = 0.9995
            self.P_1 = 0
            self.P_2 = 1
            self.P_3 = 2
            self.P_4 = 3
            self.P_5 = 4
            self.P_6 = 5
            self.P_7 = 6
            self.P_8 = 7
            self.P_9 = 8
            self.P_A = 9
            self.P_B = 10
            self.P_C = 11
            self.P_D = 12
            self.P_E = 13
            self.P_F = 14
            self.P_6C = 15
            self.P_67 = 16
            self.P_9A = 17
            self.P_CD = 18
            self.P_12 = 19
            self.P_8B = 20
            self.P_8E = 21
            self.P_8F = 22
            self.P_BF = 23
            self.P_EF = 24
            self.P_28F = 25
            self.P_2BF = 26
            self.P_2EF = 27
            self.P_269 = 28
            self.P_26C = 29
            self.P_2F = 30
            self.P_27 = 31
            self.P_2A = 32
            self.P_2D = 33
            self.P_28E = 34
            self.P_28B = 35

            # /* Boundary projectors */
            self.prj = np.zeros((36, 36))
            self.prj[self.P_1] = np.array([
                0.5, 0.5, 0.0, 0.0, 0.0, 0.0,
                0.5, 0.5, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_2] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.5, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.5, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_3] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_4] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_5] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            self.prj[self.P_6] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_7] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_8] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, -0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, -0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_9] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_A] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1])
            self.prj[self.P_B] = np.array([
                0.5, 0.0, 0.0, 0.0, -0.5, 0.0,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                -0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_C] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_D] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_E] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, -0.5,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                -0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_F] = np.array([
                0.8, -0.2, 0.0, -0.2, -0.2, -0.2,
                -0.2, 0.8, 0.0, -0.2, -0.2, -0.2,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                -0.2, -0.2, 0.0, 0.8, -0.2, -0.2,
                -0.2, -0.2, 0.0, -0.2, 0.8, -0.2,
                -0.2, -0.2, 0.0, -0.2, -0.2, 0.8])
            self.prj[self.P_6C] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_67] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.5, 0.5,
                0.0, 0.0, 0.0, 0.0, 0.5, 0.5])
            self.prj[self.P_9A] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.5, 0.0, 0.5,
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.5, 0.0, 0.5])
            self.prj[self.P_CD] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5,
                0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.5, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.5, 0.5, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_12] = np.array([
                .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0, 0.0,
                .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0, 0.0,
                .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_8B] = np.array([
                0.5, 0.0, 0.0, 0.0, -0.5, 0.0, 0.0, 0.5, 0.0, -0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -0.5, 0.0, 0.5, 0.0, 0.0,
                -0.5, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_8E] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, -0.5, 0.0, 0.5, 0.0, -0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_8F] = np.array([
                .6666666666666667, 0.0, 0.0, 0.0, -.3333333333333333, -.3333333333333333,
                0.0, 0.5, 0.0, -0.5, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, -0.5, 0.0, 0.5, 0.0, 0.0,
                -.3333333333333333, 0.0, 0.0, 0.0, .6666666666666667, -.3333333333333333,
                -.3333333333333333, 0.0, 0.0, 0.0, -.3333333333333333, .6666666666666667])
            self.prj[self.P_BF] = np.array([
                0.5, 0.0, 0.0, 0.0, -0.5, 0.0,
                0.0, .6666666666666667, 0.0, -.3333333333333333, 0.0, -.3333333333333333,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, -.3333333333333333, 0.0, .6666666666666667, 0.0, -.3333333333333333,
                -0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, -.3333333333333333, 0.0, -.3333333333333333, 0.0, .6666666666666667])
            self.prj[self.P_EF] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, -0.5,
                0.0, .6666666666666667, 0.0, -.3333333333333333, -.3333333333333333, 0.0,
                0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                0.0, -.3333333333333333, 0.0, .6666666666666667, -.3333333333333333, 0.0,
                0.0, -.3333333333333333, 0.0, -.3333333333333333, .6666666666666667, 0.0,
                -0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_28F] = np.array([
                .6666666666666667, 0.0, 0.0, 0.0, -.3333333333333333, -.3333333333333333,
                0.0, .3333333333333333, .3333333333333333, -.3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, -.3333333333333333, 0.0, 0.0,
                0.0, -.3333333333333333, -.3333333333333333, .3333333333333333, 0.0, 0.0,
                -.3333333333333333, 0.0, 0.0, 0.0, .6666666666666666, -.3333333333333333,
                -.3333333333333333, 0.0, 0.0, 0.0, -.3333333333333333, .6666666666666667])
            self.prj[self.P_2BF] = np.array([
                0.5, 0.0, 0.0, 0.0, -0.5, 0.0,
                0.0, 0.4, 0.4, -0.2, 0.0, -0.2,
                0.0, 0.4, 0.4, -0.2, 0.0, -0.2,
                0.0, -0.2, -0.2, 0.6, 0.0, -0.4,
                -0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, -0.2, -0.2, -0.4, 0.0, 0.6])
            self.prj[self.P_2EF] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, -0.5,
                0.0, 0.4, 0.4, -0.2, -0.2, 0.0,
                0.0, 0.4, 0.4, -0.2, -0.2, 0.0,
                0.0, -0.2, -0.2, 0.6, -0.4, 0.0,
                0.0, -0.2, -0.2, -0.4, 0.6, 0.0,
                -0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_269] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_26C] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_2F] = np.array([
                .7777777777777778, -.1111111111111111, -.1111111111111111,
                -.2222222222222222, -.2222222222222222, -.2222222222222222,
                -.1111111111111111, .4444444444444444, .4444444444444444,
                -.1111111111111111, -.1111111111111111, -.1111111111111111,
                -.1111111111111111, .4444444444444444, .4444444444444444,
                -.1111111111111111, -.1111111111111111, -.1111111111111111,
                -.2222222222222222, -.1111111111111111, -.1111111111111111,
                .7777777777777778, -.2222222222222222, -.2222222222222222,
                -.2222222222222222, -.1111111111111111, -.1111111111111111,
                -.2222222222222222, .7777777777777778, -.2222222222222222,
                -.2222222222222222, -.1111111111111111, -.1111111111111111,
                -.2222222222222222, -.2222222222222222, .7777777777777778])
            self.prj[self.P_27] = np.array([
                1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_2A] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.5, 0.5, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.5, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
            self.prj[self.P_2D] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5,
                0.0, 0.5, 0.5, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.5, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_28E] = np.array([
                0.5, 0.0, 0.0, 0.0, 0.0, -0.5,
                0.0, .3333333333333333, .3333333333333333, -.3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, -.3333333333333333, 0.0, 0.0,
                0.0, -.3333333333333333, -.3333333333333333, .3333333333333333, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
                -0.5, 0.0, 0.0, 0.0, 0.0, 0.5])
            self.prj[self.P_28B] = np.array([
                0.5, 0.0, 0.0, 0.0, -0.5, 0.0,
                0.0, .3333333333333333, .3333333333333333, -.3333333333333333, 0.0, 0.0,
                0.0, .3333333333333333, .3333333333333333, -.3333333333333333, 0.0, 0.0,
                0.0, -.3333333333333333, -.3333333333333333, .3333333333333333, 0.0, 0.0,
                -0.5, 0.0, 0.0, 0.0, 0.5, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 1.0])

            # /* The following matrices are the transformation
            #    matrices that may be applied at the associated
            #    boundaries  */

            self.MS = np.zeros((15, 36))
            self.MS[0] = np.array([
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 1])
            self.MS[1] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, 0, 1, 0])
            self.MS[2] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 0, 0, -1])
            self.MS[3] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, -1])
            self.MS[4] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 0, 0, 1])
            self.MS[5] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 1, 1, -1, 0, 0,
                0, -2, 0, 1, 0, 0,
                0, 0, 0, 0, -1, 1,
                0, 0, 0, 0, 0, -1])
            self.MS[6] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 1, 1, -1, 0, 0,
                0, 2, 0, -1, 0, 0,
                0, 0, 0, 0, -1, 1,
                0, 0, 0, 0, 0, 1])
            self.MS[7] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 1, 1, 1, 0, 0,
                0, 2, 0, 1, 0, 0,
                0, 0, 0, 0, -1, -1,
                0, 0, 0, 0, 0, -1])
            self.MS[8] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 0, 1, 0, -1, 0,
                0, 0, 0, -1, 0, 1,
                -2, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, -1])
            self.MS[9] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 0, 1, 0, -1, 0,
                0, 0, 0, -1, 0, 1,
                2, 0, 0, 0, -1, 0,
                0, 0, 0, 0, 0, 1])
            self.MS[10] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 0, 1, 0, 1, 0,
                0, 0, 0, -1, 0, -1,
                2, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, -1])
            self.MS[11] = np.array([
                1, 0, 0, 0, 0, 0,
                1, 1, 0, 0, 0, -1,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, -1, 1, 0,
                0, 0, 0, 0, -1, 0,
                -2, 0, 0, 0, 0, 1])
            self.MS[12] = np.array([
                1, 0, 0, 0, 0, 0,
                1, 1, 0, 0, 0, -1,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, -1, 1, 0,
                0, 0, 0, 0, 1, 0,
                2, 0, 0, 0, 0, -1])
            self.MS[13] = np.array([
                1, 0, 0, 0, 0, 0,
                1, 1, 0, 0, 0, 1,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, -1, -1, 0,
                0, 0, 0, 0, -1, 0,
                2, 0, 0, 0, 0, 1])
            self.MS[14] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1,
                0, -2, 0, -1, 0, -1,
                -2, 0, 0, 0, -1, -1,
                0, 0, 0, 0, 0, 1])

            self.NBDPRJ = 36
            self.NREFL_OUTER_FULL = 24
            # /* The 24 elements of the group of reflections generated by M_1, M_2,M_3, M_4 and M_5 */
            self.NREFL = 24
            self.RS = np.zeros((self.NREFL, 36))
            self.RS[0] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1])
            self.RS[1] = np.array([
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 1])
            self.RS[2] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, 0, 1, 0])
            self.RS[3] = np.array([
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, 1, 0, 0])
            self.RS[4] = np.array([
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0])
            self.RS[5] = np.array([
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 1, 0, 0])
            self.RS[6] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 0, 0, -1])
            self.RS[7] = np.array([
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, 0, -1])
            self.RS[8] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, 0, -1, 0])
            self.RS[9] = np.array([
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, -1, 0, 0])
            self.RS[10] = np.array([
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, -1, 0])
            self.RS[11] = np.array([
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, -1, 0, 0])
            self.RS[12] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, -1])
            self.RS[13] = np.array([
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, -1])
            self.RS[14] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, 0, -1, 0])
            self.RS[15] = np.array([
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 0, 0, 1,
                0, 0, 0, -1, 0, 0
            ])
            self.RS[16] = np.array([
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, -1, 0
            ])
            self.RS[17] = np.array([
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, -1, 0, 0
            ])
            self.RS[18] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 0, 0, 1
            ])
            self.RS[19] = np.array([
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, 0, 1
            ])
            self.RS[20] = np.array([
                1, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, 0, 1, 0
            ])
            self.RS[21] = np.array([
                0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, 1, 0, 0
            ])
            self.RS[22] = np.array([
                0, 0, 1, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, -1, 0, 0,
                0, 0, 0, 0, 1, 0
            ])
            self.RS[23] = np.array([
                0, 0, 1, 0, 0, 0,
                0, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, -1,
                0, 0, 0, 0, -1, 0,
                0, 0, 0, 1, 0, 0
            ])
            # /*                                                          . . . . . . . . . . . . . . . . . . . . .*/
            # /*                              1 2 3 4 5 6 7 8 9 A B C D E F C 7 A D 2 B E F F F F F F 9 C F 7 A D E B*/

            self.ispbd = np.array(
                [1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0,
                 0])
            self.ismbd = np.array(
                [1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1,
                 1])
            self.ispmappedbd = np.array([1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0])
            self.ismmappedbd = np.array([1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1])
            self.pmmappedbd = np.array([0, 1, 2, 3, 4, 7, -1, -1, 10, -1, -1, 13, -1, -1, -1])
            self.mpmappedbd = np.array([0, 1, 2, 3, 4, -1, -1, 5, -1, -1, 8, -1, -1, 11, -1])
            self.baseord = np.array([0, 1, 2, 3, 4, 5, 5, 7, 8, 8, 10, 11, 11, 13, 14])
            self.rord = np.array([0, 1, 2, 3, 4, 5, 9, 12, 14, 19, 15, 13, 20, 6, 8, 10, 11, 16, 17, 18, 21, 22, 23, 7])

        def angles_to_radians(self, v):
            # NOTE: Transforming angles in radians
            q = np.array(v).copy()
            q[3] = v[3] * (np.pi / 180.0)
            q[4] = v[4] * (np.pi / 180.0)
            q[5] = v[5] * (np.pi / 180.0)
            return q

        def angles_to_degrees(self, v):
            q = np.array(v).copy()
            q[3] = v[3] * (180.0 / np.pi)
            q[4] = v[4] * (180.0 / np.pi)
            q[5] = v[5] * (180.0 / np.pi)
            return q

        def g6_form(self, v):
            g6 = np.zeros(6)

            # NOTE: G6 form defined as: [a^2, b^2, c^2, 2bccos(alpha), 2accos(beta), 2abcos(gamma)]
            g6[0] = v[0] ** 2
            g6[1] = v[1] ** 2
            g6[2] = v[2] ** 2
            g6[3] = 2 * v[1] * v[2] * np.cos(v[3])
            g6[4] = 2 * v[0] * v[2] * np.cos(v[4])
            g6[5] = 2 * v[0] * v[1] * np.cos(v[5])

            return g6

        def CNCM_norm(self, v):
            return np.sqrt(self.CNCM_normsq(v))

        def CNCM_normsq(self, v):
            return v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3] + v[4] * v[4] + v[5] * v[5]

        def NCDist(self, gvec2):
            gvec1 = self.gvec1
            gvec2 = np.array(gvec2)
            self.ttt += [gvec2[0], gvec2[1], gvec2[2]]
            ####gvec2 = self.angles_to_radians(gvec2)
            gvec2 = self.g6_form(gvec2)
            rgvec1 = np.zeros((24, 6))
            rgvec2 = np.zeros((24, 6))
            ndist1 = np.zeros(24)
            ndist2 = np.zeros(24)
            signgvec1p = self.CNCM_vtoppp(gvec1)
            signgvec1m = self.CNCM_vtommm(gvec1)
            signgvec2p = self.CNCM_vtoppp(gvec2)
            signgvec2m = self.CNCM_vtommm(gvec2)
            opass = self.PASS
            dist1 = self.minbddist(gvec1)
            dist2 = self.minbddist(gvec2)
            distmin = min(dist1, dist2)
            rpasses = self.NREFL_OUTER_MIN
            dist = self.guncpmdist(gvec1, gvec2)
            # print("Entered NCDist gdist = ", dist, ","," pass = ", self.PASS)
            dist = self.NCDist_pass(gvec1, gvec2, dist)

            lgv1 = self.CNCM_norm(gvec1)
            lgv2 = self.CNCM_norm(gvec2)
            # print("dist1 = ", dist1, ",","dist2 = ", dist2)
            if (dist1 + dist2 < dist * .995):
                rpasses = self.NREFL_OUTER_FULL
            # print("rpasses = ",rpasses)

            for irt in range(rpasses):
                ir = self.rord[irt]
                if (ir == 0): continue
                rgvec1[ir] = self.imv6(gvec1, self.RS[ir])
                ndist1[ir] = self.NCDist_pass(rgvec1[ir], gvec2, dist)
                if (ndist1[ir] < dist): dist = ndist1[ir]
                rgvec2[ir] = self.imv6(gvec2, self.RS[ir])
                ndist2[ir] = self.NCDist_pass(gvec1, rgvec2[ir], dist)
                if (ndist2[ir] < dist): dist = ndist2[ir]

            self.PASS = opass + 100

            return np.sqrt(dist) * (np.sqrt(6) / np.mean(self.ttt))

        def NCDist_pass(self, gvec1, gvec2, dist):
            signgvec1p = self.CNCM_vtoppp(gvec1)
            signgvec1m = self.CNCM_vtommm(gvec1)
            signgvec2p = self.CNCM_vtoppp(gvec2)
            signgvec2m = self.CNCM_vtommm(gvec2)

            maxdist = self.fudge(dist)
            gvec1, dists1, iord1, pgs1, rgs1, mpgs1, mvecs1, maxdist, ngood1 = self.bdmaps(gvec1, maxdist)
            gvec2, dists2, iord2, pgs2, rgs2, mpgs2, mvecs2, maxdist, ngood2 = self.bdmaps(gvec2, maxdist)

            mindists1 = self.minbddist(gvec1)
            mindists2 = self.minbddist(gvec2)

            if (mindists1 + mindists2 > dist):
                # print("I do not need to go further the distance is already ok", mindists1 + mindists2, dist)
                return dist

            if (mindists1 + mindists2 < maxdist):
                # print("The sum of mindist is smaller than maxdist",mindists1 + mindists2,maxdist)
                for jx1 in range(ngood1):
                    j1 = iord1[jx1]
                    d1 = dists1[j1]
                    signpg1p = self.CNCM_vtoppp(pgs1[j1])
                    signpg1m = self.CNCM_vtommm(pgs1[j1])
                    signmpg1p = self.CNCM_vtoppp(mpgs1[j1])
                    signmpg1m = self.CNCM_vtommm(mpgs1[j1])

                    if (d1 < maxdist and ((self.ispbd[j1] and signgvec1p > 0) or (self.ismbd[j1] and signgvec1m < 0))
                            and ((self.ispbd[j1] and signpg1p > 0) or (self.ismbd[j1] and signpg1m < 0))
                            and ((self.ispmappedbd[j1] and signmpg1p > 0) or (self.ismmappedbd[j1] and signmpg1m < 0))):
                        dist = min(dist, self.guncpmdist(gvec2, mpgs1[j1]) + d1)

                for jx2 in range(ngood2):
                    j2 = iord2[jx2]
                    d2 = dists2[j2]
                    signpg2p = self.CNCM_vtoppp(pgs2[j2])
                    signpg2m = self.CNCM_vtommm(pgs2[j2])
                    signmpg2p = self.CNCM_vtoppp(mpgs2[j2])
                    signmpg2m = self.CNCM_vtommm(mpgs2[j2])

                    if (d2 < maxdist and ((self.ispbd[j2] and signgvec2p > 0) or (self.ismbd[j2] and signgvec2m < 0))
                            and ((self.ispbd[j2] and signpg2p > 0) or (self.ismbd[j2] and signpg2m < 0))
                            and ((self.ispmappedbd[j2] and signmpg2p > 0) or (self.ismmappedbd[j2] and signmpg2m < 0))):
                        dist = min(dist, (self.guncpmdist(gvec1, mpgs2[j2]) + d2))

                maxdist = self.fudge(dist)
                for jx1 in range(self.NBND):
                    j1 = iord1[jx1]
                    d1 = dists1[j1]
                    if (d1 < maxdist and ((self.ispbd[j1] and signgvec1p > 0) or (self.ismbd[j1] and signgvec1m < 0))):
                        for jx2 in range(self.NBND):
                            j2 = iord2[jx2]
                            d2 = dists2[j2]
                            if (d2 < maxdist and (
                                    (self.ispbd[j2] and signgvec2p > 0) or (self.ismbd[j2] and signgvec2m < 0))):
                                # savechanged = changed
                                dist = self.NCDist_2bds(gvec1, rgs1[j1], pgs1[j1], mpgs1[j1], j1,
                                                        gvec2, rgs2[j2], pgs2[j2], mpgs2[j2], j2, dist)
                                # changed = savechanged

            # print("Final value of dist",dist,"mindist1+mindist2 and maxdist",mindists1 + mindists2,maxdist)
            return dist

        def NCDist_2bds(self, gvec1, rgvec1,
                        pg1, mpg1, bd1,
                        gvec2, rgvec2,
                        pg2, mpg2, bd2, dist):
            """
            /* Compute the minimal distance between gvec1 and gvec2 going
            through bd1 and bd2, assuming rgvec1 and rgevc2 are their
            reflections in those boundaries
            */
            :return:
            """
            d11 = np.zeros(4)
            d12 = np.zeros(4)
            d21 = np.zeros(4)
            d22 = np.zeros(4)
            rgv1 = [None] * 4
            rgv2 = [None] * 4
            dg1g2 = np.zeros(4)
            bdint1 = np.zeros(6)
            bdint2 = np.zeros(6)
            signgvec1p = self.CNCM_vtoppp(gvec1)
            signgvec1m = self.CNCM_vtommm(gvec1)
            signgvec2p = self.CNCM_vtoppp(gvec2)
            signgvec2m = self.CNCM_vtommm(gvec2)

            if (not self.isunc(gvec1) or not self.isunc(gvec2) or not self.isunc(pg1) or not self.isunc(
                pg2) or not self.isunc(mpg1) or not self.isunc(mpg2)): return dist

            d11[0] = self.bddist(gvec1, bd1)
            d22[0] = self.bddist(gvec2, bd2)

            dist2 = np.abs(d11[0]) + np.abs(d22[0]) + \
                    min(min(min(
                        self.guncpmdist(pg1, pg2),
                        self.guncpmdist(pg1, mpg2)),
                        self.guncpmdist(mpg1, pg2)),
                        self.guncpmdist(mpg1, mpg2)
                    )

            dist = min(dist, dist2)

            """
            /* This is the general case, in which we must cross
           boundary 1 and boundary 2

           The possibilities are that we may go from gvec1 to gvec2 and
               do that, go from gvec1 to rgvec2 and do that, go from
               rgvec1 to gvec2 and do that, or go from rgvec1 to rgvec2
               and do that, or none of the above.

               rgv1   rgv2     d11        d12        d21        d22
           0  gvec1  gvec2  gvec1-bd1  gvec1-bd2  gvec2-bd1  gvec2-bd2
           1  gvec1 rgvec2  gvec1-bd1  gvec1-bd2 rgvec2-bd1 -gvec2+bd2
           2 rgvec1 rgvec2 -gvec1+bd1 rgvec1-bd2 rgvec2-bd1 -gvec2+bd2
           3 rgvec1  gvec2 -gvec1+bd1 rgvec1-bd2  gvec2-bd1  gvec2-bd2

         */
            """

            # print(type(rgv1[0]),type(rgv1[1]),type(gvec1))
            # print(rgv1[0],rgv1[1],gvec1)
            rgv1[0] = rgv1[1] = gvec1
            rgv1[2] = rgv1[3] = rgvec1
            rgv2[0] = rgv2[3] = gvec2
            rgv2[1] = rgv2[2] = rgvec2

            d12[0] = d11[0]
            d21[0] = d22[0]

            if (self.baseord[bd1] != self.baseord[bd2]):
                d12[0] = self.bddist(gvec1, bd2)
                d21[0] = self.bddist(gvec2, bd1)

            d11[1] = d11[0]
            d12[1] = d12[0]
            d21[1] = self.bddist(rgvec2, bd1)
            d22[1] = -d22[0]

            d11[2] = -d11[0]
            d12[2] = self.bddist(rgvec1, bd2)
            d21[2] = d21[1]
            d22[2] = d22[1]

            d11[3] = d11[2]
            d12[3] = d12[2]
            d21[3] = d21[0]
            d22[3] = d22[0]

            dg1g2[0] = self.CNCM_gdist(gvec1, gvec2)
            dg1g2[1] = self.CNCM_gdist(gvec1, rgvec2)
            dg1g2[2] = self.CNCM_gdist(rgvec1, rgvec2)
            dg1g2[3] = self.CNCM_gdist(rgvec1, gvec2)

            for jj in range(4):
                if (d11[jj] * d21[jj] <= 1.e-38 and d22[jj] * d12[jj] <= 1.e-38):
                    # /* (r)gvec1 and (r)gvec2 are on opposite sides of both  boundaries*/
                    if (np.abs(d11[jj]) < 1.e-38):
                        alpha1 = 0.
                    else:
                        alpha1 = min(1., np.abs(d11[jj]) / (1.e-38 + np.abs(d11[jj]) + np.abs(d21[jj])))

                    if (np.abs(d22[jj]) < 1.e-38):
                        alpha2 = 0.
                    else:
                        alpha2 = min(1., np.abs(d22[jj]) / (1.e-38 + np.abs(d22[jj]) + np.abs(d12[jj])))

                    s1 = alpha1 * dg1g2[jj]
                    s2 = alpha2 * dg1g2[jj]
                    for ii in range(6):
                        bdint1[ii] = rgv1[jj][ii] + alpha1 * (rgv2[jj][ii] - rgv1[jj][ii])
                        bdint2[ii] = rgv2[jj][ii] + alpha2 * (rgv1[jj][ii] - rgv2[jj][ii])

                    mbdint1 = self.imv6(bdint1, self.MS[bd1])
                    mbdint2 = self.imv6(bdint2, self.MS[bd2])
                    s1 = min(s1, self.CNCM_gdist(rgv1[jj], mbdint1))
                    s1 = min(s1, np.abs(d11[jj]) + self.guncpmdist(mpg1, bdint1))
                    s1 = min(s1, np.abs(d11[jj]) + self.guncpmdist(mpg1, mbdint1))
                    if (s1 > dist): return dist
                    s2 = min(s2, self.CNCM_gdist(rgv2[jj], mbdint2))
                    s2 = min(s2, np.abs(d22[jj]) + self.guncpmdist(mpg2, bdint2))
                    s2 = min(s2, np.abs(d22[jj]) + self.guncpmdist(mpg2, mbdint2))
                    if (s1 + s2 > dist): return dist

                    dbdi1bdi2 = min(min(min(
                        self.guncpmdist(bdint1, bdint2),
                        self.guncpmdist(bdint1, mbdint2)),
                        self.guncpmdist(mbdint1, bdint2)),
                        self.guncpmdist(mbdint1, mbdint2))

                    dist = min(dist, s1 + s2 + dbdi1bdi2)

            return dist

        def CNCM_gdist(self, v1, v2):
            return np.sqrt(self.CNCM_gdistsq(v1, v2))

        def bddist(self, gvec, bdnum):
            bdin = 1.0

            if (bdnum < self.NBND):
                if bdnum == self.P_1:
                    if (gvec[0] > gvec[1]): bdin = -1
                elif bdnum == self.P_2:
                    if (gvec[1] > gvec[2]): bdin = -1
                elif bdnum == self.P_3:
                    if (gvec[3] > 0.): bdin = -1
                elif bdnum == self.P_4:
                    if (gvec[4] > 0.): bdin = -1
                elif bdnum == self.P_5:
                    if (gvec[5] > 0.): bdin = -1
                elif bdnum == self.P_6:
                    if (gvec[3] > gvec[1]): bdin = -1
                elif bdnum == self.P_7:
                    if (gvec[3] > gvec[1]): bdin = -1
                elif bdnum == self.P_8:
                    if (-gvec[3] > gvec[1]): bdin = -1
                elif bdnum == self.P_9:
                    if (gvec[4] > gvec[0]): bdin = -1
                elif bdnum == self.P_A:
                    if (gvec[4] > gvec[0]): bdin = -1
                elif bdnum == self.P_B:
                    if (-gvec[4] > gvec[0]): bdin = -1
                elif bdnum == self.P_C:
                    if (gvec[5] > gvec[0]): bdin = -1
                elif bdnum == self.P_D:
                    if (gvec[5] > gvec[0]): bdin = -1
                elif bdnum == self.P_E:
                    if (-gvec[5] > gvec[0]): bdin = -1
                elif bdnum == self.P_F:
                    if (gvec[0] + gvec[1] + gvec[3] + gvec[4] + gvec[5] < 0.): bdin = -1
                else:
                    bdin = 1.0

                if bdnum == self.P_1:
                    return bdin * np.abs(gvec[1] - gvec[0]) / np.sqrt(2.)
                elif bdnum == self.P_2:
                    return bdin * np.abs(gvec[2] - gvec[1]) / np.sqrt(2.)
                elif bdnum == self.P_3:
                    return bdin * np.abs(gvec[3])
                elif bdnum == self.P_4:
                    return bdin * np.abs(gvec[4])
                elif bdnum == self.P_5:
                    return bdin * np.abs(gvec[5])
                elif bdnum == self.P_6:
                    return bdin * np.abs(gvec[1] - gvec[3]) / np.sqrt(2.)
                elif bdnum == self.P_7:
                    return bdin * np.abs(gvec[1] - gvec[3]) / np.sqrt(2.)
                elif bdnum == self.P_8:
                    return bdin * np.abs(gvec[1] + gvec[3]) / np.sqrt(2.)
                elif bdnum == self.P_9:
                    return bdin * np.abs(gvec[0] - gvec[4]) / np.sqrt(2.)
                elif bdnum == self.P_A:
                    return bdin * np.abs(gvec[0] - gvec[4]) / np.sqrt(2.)
                elif bdnum == self.P_B:
                    return bdin * np.abs(gvec[0] + gvec[4]) / np.sqrt(2.)
                elif bdnum == self.P_C:
                    return bdin * np.abs(gvec[0] - gvec[5]) / np.sqrt(2.)
                elif bdnum == self.P_D:
                    return bdin * np.abs(gvec[0] - gvec[5]) / np.sqrt(2.)
                elif bdnum == self.P_E:
                    return bdin * np.abs(gvec[0] + gvec[5]) / np.sqrt(2.)
                elif bdnum == self.P_F:
                    return bdin * np.abs(gvec[0] + gvec[1] + gvec[3] + gvec[4] + gvec[5]) / np.sqrt(5.)
                else:
                    return None

            return None

        def bdmaps(self, gvec, maxdist):
            iord = np.arange(self.NBND)
            dists = np.zeros(self.NBND)
            dists[self.P_1] = np.abs(gvec[1] - gvec[0]) / np.sqrt(2.)
            dists[self.P_2] = np.abs(gvec[2] - gvec[1]) / np.sqrt(2.)
            dists[self.P_3] = np.abs(gvec[3])
            dists[self.P_4] = np.abs(gvec[4])
            dists[self.P_5] = np.abs(gvec[5])
            dists[self.P_6] = np.abs(gvec[1] - gvec[3]) / np.sqrt(2.)
            dists[self.P_7] = np.abs(gvec[1] - gvec[3]) / np.sqrt(2.)
            dists[self.P_8] = np.abs(gvec[1] + gvec[3]) / np.sqrt(2.)
            dists[self.P_9] = np.abs(gvec[0] - gvec[4]) / np.sqrt(2.)
            dists[self.P_A] = np.abs(gvec[0] - gvec[4]) / np.sqrt(2.)
            dists[self.P_B] = np.abs(gvec[0] + gvec[4]) / np.sqrt(2.)
            dists[self.P_C] = np.abs(gvec[0] - gvec[5]) / np.sqrt(2.)
            dists[self.P_D] = np.abs(gvec[0] - gvec[5]) / np.sqrt(2.)
            dists[self.P_E] = np.abs(gvec[0] + gvec[5]) / np.sqrt(2.)
            dists[self.P_F] = np.abs(gvec[0] + gvec[1] + gvec[3] + gvec[4] + gvec[5]) / np.sqrt(5.)

            igap = self.NBND
            # print("Check if this is sorting")
            # print(dists)
            # print(iord)
            ####MERGE_SORT
            while (igap > 1):
                igap = int(igap / 2.0)
                idone = 0
                while (not idone):
                    idone = 1
                    jj = 0
                    while (jj < self.NBND - igap):
                        if (dists[iord[jj]] > dists[iord[jj + igap]]):
                            idone = 0
                            itemp = iord[jj]
                            iord[jj] = iord[jj + igap]
                            iord[jj + igap] = itemp
                        jj += igap

            # print("After procedure")
            # print(iord)
            # print([dists[t] for t in iord])

            ngood = self.NBND
            pgs = np.zeros((self.NBND, 6))
            rgs = np.zeros((self.NBND, 6))
            mpgs = np.zeros((self.NBND, 6))
            mvecs = np.zeros((self.NBND, 6))
            for jj in range(self.NBND):
                pgs[jj] = self.rmv6(gvec, self.prj[jj])
                rgs[jj] = self.twoPminusI(pgs[jj], gvec)
                mpgs[jj] = self.imv6(pgs[jj], self.MS[jj])
                mvecs[jj] = self.imv6(gvec, self.MS[jj])
                if (dists[jj] > maxdist): ngood -= 1

            return (gvec, dists, iord, pgs, rgs, mpgs, mvecs, maxdist, ngood)

        def twoPminusI(self, pg, g):
            gout = np.zeros(6)
            for i in range(6):
                gout[i] = 2. * pg[i] - g[i]
            return gout

        def imv6(self, v1, m):
            v2 = np.zeros(6)
            sum = 0.0
            for i in range(6):
                sum = 0.0
                for j in range(6):
                    sum = sum + (m[6 * i + j]) * v1[j]

                v2[i] = sum
            return v2

        def rmv6(self, v1, m):
            v2 = np.zeros(6)
            sum = 0.0
            for i in range(6):
                sum = 0.0
                for j in range(6):
                    sum = sum + m[6 * i + j] * v1[j]

                v2[i] = sum
            return v2

        def fudge(self, d):
            return self.DCUT * d

        def CNCM_gdistsq(self, v1, v2):
            return ((v1[0] - v2[0]) * (v1[0] - v2[0]) +
                    (v1[1] - v2[1]) * (v1[1] - v2[1]) +
                    (v1[2] - v2[2]) * (v1[2] - v2[2]) +
                    (v1[3] - v2[3]) * (v1[3] - v2[3]) +
                    (v1[4] - v2[4]) * (v1[4] - v2[4]) +
                    (v1[5] - v2[5]) * (v1[5] - v2[5]))

        def isunc(self, v):
            """
            Test for being in the unrolled Niggli cone

            :param v:
            :return:
            """
            fz = .0000001

            if (v[0] < 0. or v[1] < 0. or v[2] < 0.): return 0
            maxedge = v[1] if v[0] < v[1] else v[0]
            maxedge = maxedge if v[2] < maxedge else v[2]
            if (
                    v[3] > v[1] + maxedge * fz
                    or v[3] > v[2] + maxedge * fz
                    or v[4] > v[0] + maxedge * fz
                    or v[4] > v[1] + maxedge * fz
                    or v[5] > v[0] + maxedge * fz
                    or v[5] > v[1] + maxedge * fz
                    or v[3] < -v[1] - maxedge * fz
                    or v[3] < -v[2] - maxedge * fz
                    or v[4] < -v[0] - maxedge * fz
                    or v[4] < -v[1] - maxedge * fz
                    or v[5] < -v[0] - maxedge * fz
                    or v[5] < -v[1] - maxedge * fz
            ): return 0

            if (v[0] + v[1] + v[2] + v[3] + v[4] + v[5] < maxedge * (1. - fz)): return 0
            if (v[0] + v[1] + v[2] + v[3] - v[4] - v[5] < maxedge * (1. - fz)): return 0
            if (v[0] + v[1] + v[2] - v[3] + v[4] - v[5] < maxedge * (1. - fz)): return 0
            if (v[0] + v[1] + v[2] - v[3] - v[4] + v[5] < maxedge * (1. - fz)): return 0

            return 1

        def guncpmdist(self, v1, v2):
            return np.sqrt(self.guncpmdistsq(v1, v2))

        def CNCM_gtestdist(self, v1, v2):
            return self.CNCM_g123pmdist(v1, v2)

        def CNCM_g123pmdistsq(self, v1, v2):
            if (self.CNCM_vtoppp(v1) * self.CNCM_vtoppp(v2) > 0 and self.CNCM_vtommm(v1) * self.CNCM_vtommm(v2) > 0):
                return (self.CNCM_gdistsq(v1, v2))
            else:
                return (min((min([self.CNC_g456distsq_byelem(v1[0], v1[1], v1[2], v1[3], v1[4], v1[5],
                                                             v2[0], v2[1], v2[2], v2[3], v2[4], v2[5]),
                                  self.CNC_g456distsq_byelem(v1[0], v1[1], v1[2], v1[3], v1[4], v1[5],
                                                             v2[0], v2[2], v2[1], v2[3], v2[5], v2[4]),
                                  self.CNC_g456distsq_byelem(v1[0], v1[1], v1[2], v1[3], v1[4], v1[5],
                                                             v2[1], v2[0], v2[2], v2[4], v2[3], v2[5])])),
                            (min([self.CNC_g456distsq_byelem(v1[0], v1[1], v1[2], v1[3], v1[4], v1[5],
                                                             v2[1], v2[2], v2[0], v2[4], v2[5], v2[3]),
                                  self.CNC_g456distsq_byelem(v1[0], v1[1], v1[2], v1[3], v1[4], v1[5],
                                                             v2[2], v2[0], v2[1], v2[5], v2[3], v2[4]),
                                  self.CNC_g456distsq_byelem(v1[0], v1[1], v1[2], v1[3], v1[4], v1[5],
                                                             v2[2], v2[1], v2[0], v2[5], v2[4], v2[3])]))))

        def CNC_g456distsq_byelem(self, v11, v12, v13, v14, v15, v16, v21, v22, v23, v24, v25, v26):
            """
            /*     Compute the best distance between 2 G6 vectors
            allowing for permulations of g1, g2, g3 as
            well as sign changes
            */
            """
            return np.abs((v11 - v21) * (v11 - v21) + (v12 - v22) * (v12 - v22) + (v13 - v23) * (v13 - v23) +
                          (v14 - v24) * (v14 - v24) + (v15 - v25) * (v15 - v25) + (v16 - v26) * (v16 - v26) +
                          4. * min(min(min(0., v14 * v24 + v15 * v25), v14 * v24 + v16 * v26), v15 * v25 + v16 * v26))

        def CNCM_g123pmdist(self, v1, v2):
            return np.sqrt(self.CNCM_g123pmdistsq(v1, v2))

        def guncpmdistsq(self, v1, v2):
            if (not self.isunc(v1) or not self.isunc(v2)): return self.CNCM_gdistsq(v1, v2)

            if (self.CNCM_vtoppp(v1) * self.CNCM_vtoppp(v2) > 0 and self.CNCM_vtommm(v1) * self.CNCM_vtommm(
                v2) > 0): return self.CNCM_gtestdistsq(v1, v2)

            # /* We have one end in +++, one in ---
            #    Call them plusvec and minusvec */

            if (self.CNCM_vtoppp(v1) < 0 or self.CNCM_vtommm(v1) < 0):
                plusvec = v2
                minusvec = v1
            else:
                plusvec = v1
                minusvec = v2

            # /* Get the minimum distance through 3, 4 and 5 */
            distsq = self.CNCM_gtestdistsq(plusvec, minusvec)

            # /* now try for tunnels: 6-8, 9-B, C-E */
            d6 = np.abs(plusvec[3] - plusvec[1]) / np.sqrt(2.)
            d8 = np.abs(minusvec[3] + minusvec[1]) / np.sqrt(2.)
            d9 = np.abs(plusvec[4] - plusvec[0]) / np.sqrt(2.)
            dB = np.abs(minusvec[4] + minusvec[0]) / np.sqrt(2.)
            dC = np.abs(plusvec[5] - plusvec[0]) / np.sqrt(2.)
            dE = np.abs(minusvec[5] + minusvec[0]) / np.sqrt(2.)

            # /* p6 in +++ tunnels to mp6 in --- */
            p6 = np.zeros(6)
            p6[0] = plusvec[0]
            p6[1] = (plusvec[3] + plusvec[1]) / 2.
            p6[2] = plusvec[2]
            p6[3] = (plusvec[3] + plusvec[1]) / 2.
            p6[4] = plusvec[4]
            p6[5] = plusvec[5]

            mp6 = np.zeros(6)
            mp6[0] = plusvec[0]
            mp6[1] = (plusvec[3] + plusvec[1]) / 2.
            mp6[2] = plusvec[2]
            mp6[3] = -(plusvec[3] + plusvec[1]) / 2.
            mp6[4] = plusvec[5] - plusvec[4]
            mp6[5] = -plusvec[5]

            # /* p8 in --- tunnels to mp8 in +++ */
            p8 = np.zeros(6)
            p8[0] = minusvec[0]
            p8[1] = -(minusvec[3] - minusvec[1]) / 2.
            p8[2] = minusvec[2]
            p8[3] = (minusvec[3] - minusvec[1]) / 2.
            p8[4] = minusvec[4]
            p8[5] = minusvec[5]

            mp8 = np.zeros(6)
            mp8[0] = minusvec[0]
            mp8[1] = -(minusvec[3] - minusvec[1]) / 2.
            mp8[2] = minusvec[2]
            mp8[3] = -(minusvec[3] - minusvec[1]) / 2.
            mp8[4] = -minusvec[5] - minusvec[4]
            mp8[5] = -minusvec[5]

            # /* p9 in +++ tunnels to mp9 in --- */
            p9 = np.zeros(6)
            p9[0] = (plusvec[4] + plusvec[0]) / 2.
            p9[1] = plusvec[1]
            p9[2] = plusvec[2]
            p9[3] = plusvec[3]
            p9[4] = (plusvec[4] + plusvec[0]) / 2.
            p9[5] = plusvec[5]

            mp9 = np.zeros(6)
            mp9[0] = (plusvec[4] + plusvec[0]) / 2.
            mp9[1] = plusvec[1]
            mp9[2] = plusvec[2]
            mp9[3] = plusvec[5] - plusvec[3]
            mp9[4] = -(plusvec[4] + plusvec[0]) / 2.
            mp9[5] = -plusvec[5]

            # /* pB in --- tunnels to mpB in +++ */
            pB = np.zeros(6)
            pB[0] = -(minusvec[4] - minusvec[0]) / 2.
            pB[1] = minusvec[1]
            pB[2] = minusvec[2]
            pB[3] = minusvec[3]
            pB[4] = (minusvec[4] - minusvec[0]) / 2.
            pB[5] = minusvec[5]

            mpB = np.zeros(6)
            mpB[0] = -(minusvec[4] - minusvec[0]) / 2
            mpB[1] = minusvec[1]
            mpB[2] = minusvec[2]
            mpB[3] = -minusvec[5] - minusvec[3]
            mpB[4] = -(minusvec[4] - minusvec[0]) / 2.
            mpB[5] = -minusvec[5]

            # /* pC in +++ tunnels to mpC in --- */
            pC = np.zeros(6)
            pC[0] = (plusvec[5] + plusvec[0]) / 2.
            pC[1] = plusvec[1]
            pC[2] = plusvec[2]
            pC[3] = plusvec[3]
            pC[4] = plusvec[4]
            pC[5] = (plusvec[5] + plusvec[0]) / 2.

            mpC = np.zeros(6)
            mpC[0] = (plusvec[5] + plusvec[0]) / 2.
            mpC[1] = plusvec[1]
            mpC[2] = plusvec[2]
            mpC[3] = plusvec[4] - plusvec[3]
            mpC[4] = -plusvec[4]
            mpC[5] = -(plusvec[5] + plusvec[0]) / 2.

            # /* pE in --- tunnels to mpE in +++ */
            pE = np.zeros(6)
            pE[0] = -(minusvec[5] - minusvec[0]) / 2.
            pE[1] = minusvec[1]
            pE[2] = minusvec[2]
            pE[3] = minusvec[3]
            pE[4] = minusvec[4]
            pE[5] = (minusvec[5] - minusvec[0]) / 2.

            mpE = np.zeros(6)
            mpE[0] = -(minusvec[5] - minusvec[0]) / 2.
            mpE[1] = minusvec[1]
            mpE[2] = minusvec[2]
            mpE[3] = -minusvec[4] - minusvec[3]
            mpE[4] = -minusvec[4]
            mpE[5] = -(minusvec[5] - minusvec[0]) / 2

            # /* There are 2 types of routes to try
            #   from plusvec to 6, 9 or C and from
            #   minusvec to 8, B or E respectively
            #   completing the path via the shortest
            #   connection between the ends of the
            #   tunnels, or just go plusvec to 6, 9 or C
            #   and directly from the other end of the
            #   tunnel to minusvec, etc. */

            dist = d6 + self.CNCM_gtestdist(mp6, minusvec)
            dist = min(dist, d9 + self.CNCM_gtestdist(mp9, minusvec))
            dist = min(dist, dC + self.CNCM_gtestdist(mpC, minusvec))
            dist = min(dist, d8 + self.CNCM_gtestdist(mp8, plusvec))
            dist = min(dist, dB + self.CNCM_gtestdist(mpB, plusvec))
            dist = min(dist, dE + self.CNCM_gtestdist(mpE, plusvec))

            distsq = min(distsq, dist * dist)

            dist68sq = self.CNCM_gtestdistsq(p6, p8)
            dist68sq = min(dist68sq, self.CNCM_gtestdistsq(p6, mp8))
            dist68sq = min(dist68sq, self.CNCM_gtestdistsq(mp6, p8))
            dist68sq = min(dist68sq, self.CNCM_gtestdistsq(mp6, mp8))
            dist68sq += (d6 + d8) * (d6 + d8)
            if (dist68sq < distsq): distsq = dist68sq

            dist9Bsq = self.CNCM_gtestdistsq(p9, pB)
            dist9Bsq = min(dist9Bsq, self.CNCM_gtestdistsq(p9, mpB))
            dist9Bsq = min(dist9Bsq, self.CNCM_gtestdistsq(mp9, pB))
            dist9Bsq = min(dist9Bsq, self.CNCM_gtestdistsq(mp9, mpB))
            dist9Bsq += (d9 + dB) * (d9 + dB)
            if (dist9Bsq < distsq): distsq = dist9Bsq

            distCEsq = self.CNCM_gtestdistsq(pC, pE)
            distCEsq = min(distCEsq, self.CNCM_gtestdistsq(pC, mpE))
            distCEsq = min(distCEsq, self.CNCM_gtestdistsq(mpC, pE))
            distCEsq = min(distCEsq, self.CNCM_gtestdistsq(mpC, mpE))
            distCEsq += (dC + dE) * (dC + dE)
            if (distCEsq < distsq): distsq = distCEsq

            return distsq

        def CNCM_gtestdistsq(self, v1, v2):
            return self.CNCM_gpmdistsq(v1, v2)

        def CNCM_gpmdistsq(self, v1, v2):
            """
            /* Compute the gdist distance when moving between +++ and --- */
            """
            if (self.CNCM_vtoppp(v1) * self.CNCM_vtoppp(v2) > 0 and self.CNCM_vtommm(v1) * self.CNCM_vtommm(v2) > 0):
                return (self.CNCM_gdistsq(v1, v2))
            else:
                return np.abs((v1[0] - v2[0]) * (v1[0] - v2[0]) +
                              (v1[1] - v2[1]) * (v1[1] - v2[1]) +
                              (v1[2] - v2[2]) * (v1[2] - v2[2]) +
                              (v1[3] * v1[3] + v2[3] * v2[3] + v1[4] * v1[4] + v2[4] * v2[4] + v1[5] * v1[5] + v2[5] * v2[
                                  5]) +
                              2. * (min([np.abs(v1[3] * v2[3]) - (v1[4] * v2[4]) - (v1[5] * v2[5]),
                                         - (v1[3] * v2[3]) + np.abs(v1[4] * v2[4]) - (v1[5] * v2[5]),
                                         - (v1[3] * v2[3]) - (v1[4] * v2[4]) + np.abs(v1[5] * v2[5])])))

        def CNCM_vtoppp(self, v):
            return ((self.CNCM_ctop(v[3])) * (self.CNCM_ctop(v[4])) * (self.CNCM_ctop(v[5])))

        def CNCM_ctop(self, comp):
            return 1 if (comp) > (-1.e-38) else -1

        def CNCM_vtommm(self, v):
            return ((self.CNCM_ctom(v[3])) * (self.CNCM_ctom(v[4])) * (self.CNCM_ctom(v[5])))

        def CNCM_ctom(self, comp):
            return -1 if (comp) < (1.e-38) else 1

        def minbddist(self, gvec):
            dists = np.zeros(4)
            dists[0] = min(min(np.abs(gvec[1] - gvec[0]), np.abs(gvec[2] - gvec[1])), np.abs(gvec[2] - gvec[0])) / np.sqrt(
                2.0)
            dists[1] = min(min(np.abs(gvec[3]), np.abs(gvec[4])), np.abs(gvec[5]))
            dists[2] = min(min(np.abs(gvec[1] - np.abs(gvec[3])), np.abs(gvec[0] - np.abs(gvec[4]))),
                           np.abs(gvec[0] - np.abs(gvec[5])))
            dists[2] = min(dists[2], np.abs(gvec[1] - np.abs(gvec[5])))
            dists[2] = min(min(dists[2], np.abs(gvec[2] - np.abs(gvec[4]))), np.abs(gvec[2] - np.abs(gvec[3]))) / np.sqrt(
                2.0)
            dists[3] = np.abs(gvec[0] + gvec[1] + gvec[3] + gvec[4] + gvec[5])
            dists[3] = min(dists[3], np.abs(gvec[0] + gvec[1] - gvec[3] - gvec[4] + gvec[5]))
            dists[3] = min(dists[3], np.abs(gvec[0] + gvec[1] - gvec[3] + gvec[4] - gvec[5]))
            dists[3] = min(dists[3], np.abs(gvec[0] + gvec[1] + gvec[3] - gvec[4] - gvec[5])) / np.sqrt(5.0)

            minbd = dists[0]
            for ii in range(1, 4):
                # print("minbddist",ii,minbd,dists[ii],min(minbd, dists[ii]))
                minbd = min(minbd, dists[ii])

            return minbd

    class PIC_utilities(object):

        @staticmethod
        def ChangeBasesList(CBlist, cb_op):
            # Apply change-of-basis operator to all operators in list
            # to allow for change of frame
            # Post-multiply operators by cb_op
            # Also reorder operators if necessary to put identity operator
            # first in sublist

            CB_newlist = [None] * (len(CBlist))
            if (len(CBlist) > 0):
                for i in range(len(CBlist)):
                    Nops = CBlist[i].Nop()
                    CbOps = []
                    for j in range(Nops):
                        # Operator relative to constructor frame
                        cb_ij = CBlist[i].Op(j).new_denominators(cb_op)

                        CB = cb_ij * cb_op
                        if (CB.is_identity_op()):
                            CbOps = [CB] + CbOps
                        else:
                            CbOps.append(CB)

                    CB_newlist[i] = AlternativeBases(CbOps, CBlist[i].CellDiff())

            return CB_newlist

        @staticmethod
        def clipper_cell_equals_at_res(cell1, cell2, tol=1.0):
            if (cell1 is None or all([x == 0 for x in cell1.parameters()]) or cell2 is None or all(
                    [x == 0 for x in cell2.parameters()])):
                return False

            s = 0.0
            fracmat1 = np.array(list(cell1.fractionalization_matrix())).reshape((3, 3))
            fracmat2 = np.array(list(cell2.fractionalization_matrix())).reshape((3, 3))
            for i in range(3):
                for j in range(3):
                    s += (fracmat1[i][j] - fracmat2[i][j]) ** 2.0
            return s < ((tol ** 2.0) / (cell1.volume() ** 1.333))

        @staticmethod
        def SetReindexOp(ChB, rotgroup=None):
            # Convert change-of-basis operator to ReindexOp
            op = ChB.c_inv().r().as_double()

            vop = np.array(list(op)).reshape((3, 3))
            vp = ReindexOp(rot=vop, as_hkl=ChB.as_hkl(), rotgroup=rotgroup)
            return vp

        @staticmethod
        def Scell_change_basis(cell1, reindexop):
            """
            # H reindex operator (ignores translations)
            # B current orthogonalisation matrix
            # new (B)' = B H(T)^-1
            :param cell1:
            :param reindexop:
            :return:
            """

            degtorad = np.pi / 180.0  # equivalent to: math.atan(1.0) / 45.0
            Bmat_ = PIC_utilities.Bmat(cell1)

            Bp = np.dot(Bmat_, np.linalg.inv(reindexop.transpose()))

            # Reciprocal metric tensor = (B)'(T) (B)'
            # construct new cell object from reciprocal metric tensor
            newrot = np.linalg.inv(np.dot(Bp.T, Bp))
            newcell = np.zeros(6)

            newcell[0] = np.sqrt(newrot[0][0])  # 00
            newcell[1] = np.sqrt(newrot[1][1])  # 11
            newcell[2] = np.sqrt(newrot[2][2])  # 22
            newcell[3] = np.arccos(0.5 * (newrot[1][2] + newrot[2][1]) / (newcell[1] * newcell[2])) / degtorad
            newcell[4] = np.arccos(0.5 * (newrot[0][2] + newrot[2][0]) / (newcell[2] * newcell[0])) / degtorad
            newcell[5] = np.arccos(0.5 * (newrot[0][1] + newrot[1][0]) / (newcell[0] * newcell[1])) / degtorad
            newcell = list(newcell)

            return cctbx.uctbx.unit_cell(newcell)

        @staticmethod
        def Bmat(cell1):
            degtorad = np.pi / 180.0  # equivalent to: math.atan(1.0) / 45.0
            rec_cell_ = cell1.reciprocal().parameters()
            cell_ = cell1.parameters()
            asi = rec_cell_[0]  # a *
            bs = rec_cell_[1]  # b *
            cs = rec_cell_[2]  # c *
            # double cas = cos(rec_cell_[3] * degtorad); // cos(alpha *)
            cbs = np.cos(rec_cell_[4] * degtorad)  # cos(beta *)
            cgs = np.cos(rec_cell_[5] * degtorad)  # cos(gamma *)
            # double sas = sin(rec_cell_[3] * degtorad); # sin(alpha *)
            sbs = np.sin(rec_cell_[4] * degtorad)  # sin(beta *)
            sgs = np.sin(rec_cell_[5] * degtorad)  # sin(gamma *)

            cc = cell_[2]
            ca = np.cos(cell_[3] * degtorad)

            Bmat_ = np.array([[asi, bs * cgs, cs * cbs],
                              [0., bs * sgs, -cs * sbs * ca],
                              [0., 0., 1.0 / cc]])

            return Bmat_

        @staticmethod
        def Difference(cell1, cell2):
            # "difference" between two cells in A, maximum allowed distance
            # algorithm from clipper::Cell::equals
            # Even Kevin doesn't understand this, though presumably V^1/3 is an
            # average cell length

            s = 0.0
            Bmat_1 = PIC_utilities.Bmat(cell1)
            Bmat_2 = PIC_utilities.Bmat(cell2)

            for j in range(3):
                for i in range(3):
                    s += (Bmat_1[i][j] - Bmat_2[i][j]) * (Bmat_1[i][j] - Bmat_2[i][j])
                    # sum of squares of orthogonalisation matrix differences
            # Average cell volume
            vav = (cell1.volume() + cell2.volume()) * 0.5
            v43 = vav ** 1.333333  # V^4/3

            # print("Difference",np.sqrt(v43 * s))

            return np.sqrt(v43 * s)

        @staticmethod
        def get_sgtbx_rt_mx(rot, tra=[0, 0, 0], r_den=1, t_den=cctbx.sgtbx.cb_t_den):
            R = rot.flatten()
            # if r_den:
            #     DM = cctbx.sgtbx.rot_mx(tuple([int(x) for x in R]), r_den)
            # else:
            #     DM = cctbx.sgtbx.rot_mx(tuple([int(x) for x in R]))

            # print("PPP",[x for x in R],r_den,t_den)
            # cctbx.sgtbx.rt_mx([x for x in R],tra, r_den=r_den, t_den=t_den)
            # if t_den:
            #     TM = cctbx.sgtbx.tr_vec(tuple([int(x) for x in tra]), t_den)
            # else:
            #     TM = cctbx.sgtbx.tr_vec(tuple([int(x) for x in tra]))

            # return cctbx.sgtbx.rt_mx(DM, TM)

            # if [1.0, 0.0, -0.5, 0.0, 0.0, 0.5, -0.0, -1.0, -0.0] == [x for x in R]:
            #     print("PPP1", [x for x in R], r_den, t_den)
            #     print("ZZZ2", [math.ceil(x) if x>0 else math.ceil(x) for x in R], 12, t_den)
            #     return cctbx.sgtbx.rt_mx([x for x in R], tra, r_den=12, t_den=t_den)

            return cctbx.sgtbx.rt_mx([x for x in R], tra, r_den=r_den, t_den=t_den)

    class PointLess_Intensity_Correlation(object):
        """
        This class is a porting in Python and cctbx to some functions
        in pointless from Dr. Phil Evans

        :param mtz1: mtz path
        :type mtz1: str
        :param mtz1obj: the mtz object
        :type mtz1obj: class 'iotbx.file_reader.any_file_input'
        :param crystal1: crystal symmetry (e.g. format Unit cell: (137.02, 300.93, 145.76, 90, 113.126, 90)
                                           Space group: P 1 21 1 (No. 4))
        :type crystal1: class 'cctbx.crystal.symmetry'
        :param unit_cell1: unit cell dimensions (e.g. format (184.9720001220703, 116.66000366210938, 116.66000366210938, 90.0, 90.0, 90.0)
        :type unit_cell1: tuple of floats
        :param spg1: space group symbol (e.g. format  P 1 1 2)
        :type spg1: str
        :param num1: space group number
        :type num1: int
        """

        def __init__(self, mtz1, logger=None):
            if logger is None:
                logging.basicConfig(format='%(message)s',
                                    datefmt='%H:%M:%S',
                                    level=logging.DEBUG)
                self.logger = logging.getLogger("PIC")
            else:
                #logger level set by call from run
                self.logger = logger
            self.mtz1 = mtz1
            self.mtz1obj = any_file(self.mtz1)
            self.crystal1 = self.mtz1obj.crystal_symmetry()
            self.unit_cell1 = self.crystal1.unit_cell().parameters()
            self.spg1 = self.crystal1.space_group_info().symbol_and_number()
            self.num1 = self.crystal1.space_group_number()
            # self.space_group1 = self.crystal1.space_group()

        def findIndexingAlternatives(self, cell, symm, num, max_delta, allowI2, resmax, print_all=True, pad=""):
            """

            :param cell:
            :param symm:
            :param num:
            :param max_delta:
            :param allowI2:
            :param resmax:
            :param print_all:
            :param pad:
            :return:
            """
            # accept reindex operators only if they preserve
            # the point-group symmetry, ie assume that symm is the correct point group

            reds = []
            checksymmetry = True
            cell_ = cctbx.uctbx.unit_cell(cell)
            reindexing = self.AlternativeReindexList(cell, cell, symm, symm, num, num, max_delta, allowI2,
                                                     checksymmetry=checksymmetry)

            outxt = []
            if (len(reindexing) == 0):
              reindexing = [ReindexOp()]
            self.has_reindexing = (len(reindexing) > 1)
            self.logger.setLevel(logging.ERROR) # turn off output
           #self.logger.setLevel(logging.INFO) # turn on output
            if len(reindexing) > 1:
                self.logger.info(pad + "Final point group choice has alternative indexing possibilities")

            assert (reindexing[0].IsIdentity())  # 1st one should be h,k,l

            # Get cell deviations for alternatives, in A
            delta = [0.0] * len(reindexing)
            samecell = [True] * len(reindexing)
            cells = [None] * len(reindexing)
            if (len(cells) > 0):
                cells[0] = cell_.parameters()

            for k in range(1, len(reindexing)):
                rcell = PIC_utilities.Scell_change_basis(cell_, reindexing[k])
                delta[k] = PIC_utilities.Difference(cell_, rcell)

                """
                // true if cells are not different at the maximum resolution,
                //  according to Clipper criterion
                // Two cells disagree if the difference in their orthogonalisation
                // matrices is sufficient to map a reflection from one cell onto
                // a different reflection in the other cell at the given tolerance,
                // which is the resolution of the reflection in Angstroms.
                """
                samecell[k] = PIC_utilities.clipper_cell_equals_at_res(cell_, rcell, resmax)
                cells[k] = rcell.parameters()

            self.logger.info(
                pad + "Alternative indexing possibilities are marked '*' if the cells are too different at the maximum resolution " + str(
                    resmax) + "\n")
            self.logger.info(pad + "CellDifference(A) ReindexOperator")
            for k in range(len(reindexing)):
                diff = ' '
                if not samecell[k]: diff = '*'
              # if not print_all and diff == '*': continue
                self.logger.info(pad+str("%3d  %7.1f %c        %s" % (k + 1, delta[k], diff, reindexing[k].as_hkl()))+" "+str(cells[k])+" "+str(reindexing[k].as_hkl_string)+" "+str(reindexing[k].rotgroup))
    #self.outxt.append("CDM Cell Distance Metric in Angstroms")
    #self.outxt.append("*   Cells different with respect to maximum resolution " + str("{:.3}".format(maxres)))
                outxt.append("%-17s %7.5f %1s %-37s %s" % (
                           #does not include Labels, this is prepended externally
                            str(reindexing[k].rotgroup),
                            delta[k],
                            diff,
                            cctbx.uctbx.unit_cell(cells[k]),
                            str(k+1) + ":" + str(reindexing[k].as_hkl_string)))
              # if diff != "*":
                if True:
                  reds.append(
                    {"k": k + 1, "ops": reindexing[k], "cell": cells[k], "hkl": reindexing[k].as_hkl_string,
                     "rotgroup": reindexing[k].rotgroup})

            return reds,outxt

        def AlternativeReindexList(self, ref_cell, test_cell, RefSym, TestSym, Num1, Num2, LatticeTolerance, AllowI2,
                                   checksymmetry=True):
            """
            Returns list of reindex operators to match Test on to Ref
            checksymmetry  if true, accept reindex operators only if they preserve
            the point-group symmetry, ie assume that RefSym is the correct
            point group
            :param ref_cell:
            :type ref_cell:
            :param test_cell:
            :type test_cell:
            :param RefSym:
            :type RefSym:
            :param TestSym:
            :type TestSym:
            :param LatticeTolerance:
            :type LatticeTolerance:
            :param AllowI2:
            :type AllowI2:
            :param checksymmetry:
            :type checksymmetry:
            :return:
            :rtype:
            """

            """
            Allowed cell discrepancy for alternative indexing
            Very rough: convert angular tolerance to "length" using
            average cell edge
            """

            edge = 0.333 * (test_cell[0] + test_cell[1] + test_cell[2])
            max_diff = self.d2rad(LatticeTolerance) * edge

            # Laue group, identity reindex op
            RefLG = PointGroup(name=RefSym, num=Num1)
            TestLG = PointGroup(name=TestSym, num=Num2)

            # No change of C2 -> I2 here
            RefLG.SetCell(ref_cell, ReindexOp(), False)
            TestLG.SetCell(test_cell, ReindexOp(), False)
            strict = False
            reindexing =  self.AlternativeIndexing(RefLG, TestLG, strict, ref_cell, max_diff, AllowI2, checksymmetry)
            return  reindexing

        def get_reindex_ops(self, reindexing, input_cell, resmax, print_all=True, pad="", verbose=True):
            reds = []
            checksymmetry = True
            cell_ = cctbx.uctbx.unit_cell(input_cell)
            self.logger.setLevel(logging.ERROR) # turn off output
          # self.logger.setLevel(logging.INFO) # turn on output

            if len(reindexing) > 1:
                self.logger.info(pad + "Final point group choice has alternative indexing possibilities")

            # Get cell deviations for alternatives, in A
            delta = [0.0] * len(reindexing)
            samecell = [True] * len(reindexing)
            cells = [None] * len(reindexing)
            cells[0] = cell_

            for k in range(1, len(reindexing)):
                rcell = PIC_utilities.Scell_change_basis(cell_, reindexing[k])
                delta[k] = PIC_utilities.Difference(cell_, rcell)

                """
                // true if cells are not different at the maximum resolution,
                //  according to Clipper criterion
                // Two cells disagree if the difference in their orthogonalisation
                // matrices is sufficient to map a reflection from one cell onto
                // a different reflection in the other cell at the given tolerance,
                // which is the resolution of the reflection in Angstroms.
                """
                samecell[k] = PIC_utilities.clipper_cell_equals_at_res(cell_, rcell, resmax)
                cells[k] = rcell

            if verbose: self.logger.info(
                pad + "Alternative indexing possibilities are marked '*' if the cells are too different at the maximum resolution " + str(resmax) + "\n")
            if verbose: self.logger.info(pad + "CellDifference(A) ReindexOperator")
            for k in range(len(reindexing)):
                diff = ' '
                if not samecell[k]: diff = '*'
                if not print_all and diff == '*': continue
                if verbose: self.logger.info(
                    pad + str("%3d  %7.1f %c        %s" % (k + 1, delta[k], diff, reindexing[k].as_hkl())) + " " + str(
                        cells[k]) + " " + str(reindexing[k].as_hkl_string) + " " + str(reindexing[k].rotgroup))
                if diff != "*": reds.append(
                    {"k": k + 1, "ops": reindexing[k], "cell": cells[k], "hkl": reindexing[k].as_hkl_string,
                     "rotgroup": reindexing[k].rotgroup})

            return reds

        def d2rad(self, degree):
            return degree * (np.pi / 180.0)

        def AlternativeIndexing(self, PG, TG, strict, target_cell, max_delta, AllowI2, checksymmetry):
            """
              #PG is reference group, with a unit cell
              #TG is test group, with a unit cell
              # Return list of possible alternative indexing schemes
              # compatible with cell or with lattice symmetry
              #
              # (1) If strict = true
              #   return symmetry operators from symmetry elements
              #   present in lattice group but not in pointgroup
              #   (max_delta is ignored)
              #   This can only happen for symmetries > orthorhombic
              # else (2) strict = false
              #   find accidental alternatives arising from special
              #   cell dimension relationships
              #   max_delta is angular tolerance for cell similarity
              #
              # if checksymmetry = true, accept reindex operators only if they preserve
              #  the point-group symmetry, ie assume that PG is the correct point group
              #
              #  Returns:-
              #   List of reindex operators, including:-
              #     strict flag: true if "strict"
              #     deviations - cell deviations from initial cell
              #                  (maximum angle in degrees)
              #                  = 0.0 for strict settings
              #
              # Friend of class PointGroup

            :param PG:
            :param TG:/
            :param strict:
            :param target_cell:
            :param max_delta:
            :param AllowI2:
            :param chek:
            :return:
            """

            # ^
            # std::cout << "AlternativeIndexing\n* * * PG\n";
            # PG.dump();
            # std::cout << "\n* * * TG\n";
            # TG.dump();
            # std::cout << "\n* * * END\n";
            # ^ -

            self.ReindexList = []
            self.CrystalSystem = {"NOSYSTEM": 0, "TRICLINIC": 1, "MONOCLINIC": 2, "ORTHORHOMBIC": 3, "TETRAGONAL": 4,
                                  "TRIGONAL": 5, "HEXAGONAL": 6, "CUBIC": 7}

            if strict:
                if (LatticeGroup(PG.LaueGrp_ref, PG.CentringSymbol(PG.LaueGrp_ref)).crystal_system() <= self.CrystalSystem[
                    "ORTHORHOMBIC"]):
                    # strict option only valid for high symmetry
                    return self.ReindexList

                self.ExcludeIdentity = False
                self.AnyCell = False
                self.BestCell = False
                self.CbOp_list = PG.GetAlternativeBases(PG.LaueGrp_ref, self.ExcludeIdentity, self.AnyCell, self.BestCell,
                                                        PG.uccell_ref, PG.uccell_ref, 3.0, max_delta, AllowI2)
                # change-of-basis operators are relative to reference cell
                if (len(self.CbOp_list) > 0):
                    # Change basis of all operators to cell frame
                    self.CBlist = PIC_utilities.ChangeBasesList(self.CbOp_list, PG.ChBasis)

                    for k in range(len(self.CBlist)):
                        self.ReindexList.append(PIC_utilities.SetReindexOp(self.CBlist[k].SimplestOp(),
                                                                           rotgroup=PG.RotGrp.type().lookup_symbol()))
                        self.ReindexList[-1].SetStrict(True)
            else:
                # not strict
                # Lattice group of pointgroup
                self.LG = LatticeGroup(PG.LaueGrp_ref, PG.CentringSymbol(PG.LaueGrp_ref))
                # List of possible "strict" bases (high symmetry only)
                self.StrictOps = []
                if (self.LG.crystal_system() > self.CrystalSystem["ORTHORHOMBIC"]):
                    self.StrictCB = PG.PossibleChBOp(self.LG, PG.LaueGrp_ref, AllowI2)
                    for i in range(len(self.StrictCB)):
                        self.StrictOps.append(
                            PIC_utilities.SetReindexOp(self.StrictCB[i], rotgroup=PG.RotGrp.type().lookup_symbol()))
                # Maximum lattice symmetry

                self.lat = LatticeSymmetry(TG, AllowI2, max_delta)

                #      CCtbxSym::LatticeSymmetry lat(TG.TransformedCell(), TG.LatType, AllowI2, max_delta);
                #      CCtbxSym::LatticeSymmetry lat(PG.input_cell, PG.LatType, AllowI2, max_delta);
                # Reindexing operator for "best" spacegroup
                #  from original -> lattice (best), inverse operator for hkl

                #print("SHERLOCKA",self.lat.best_spacegroup_symbol())
                self.reindex_op = self.lat.best_sg_reindex_op(rotgroup=self.lat.best_spacegroup_symbol())
                # ^
                #      std::cout << "reindex_op " << reindex_op.as_hkl() <<"\n";
                #      std::cout <<"AllowI2 " << AllowI2 <<"\n";
                #      std::cout << "\nTest Input Cell: ";
                #      for (int i=0;i<6;i++) std:: cout << " " << TG.input_cell[i];
                #      std::cout << "\n";
                # ^-
                # All subgroups
                self.subgroups = self.GetSubGroups(hkl_symmetry(self.lat.best_spacegroup_symbol()))

                if len(self.subgroups) > 0:
                    # Loop subgroups to find any which are the same as this
                    for k in range(len(self.subgroups)):
                        # print(self.subgroups[k].LatType)
                        # print(self.subgroups[k].LatticeType)
                        # print(self.subgroups[k].LaueGrp_type.lookup_symbol())
                        # print(self.subgroups[k].LaueGrp_ref_type.lookup_symbol())
                        # print(self.subgroups[k].Pgroup.type().lookup_symbol())
                        # print(self.subgroups[k].PntGrp_ref.type().lookup_symbol())
                        # print(self.subgroups[k].RotGrp.type().lookup_symbol())
                        # print(self.subgroups[k].RotGrp_ref.type().lookup_symbol())
                        # ^
                        # std::cout << "\n>> AlternativeIndexing "
                        # << " subgroups[k].LaueGrp_ref " <<
                        # subgroups[k].LaueGrp_ref.type().hall_symbol() <<
                        # " RotGrp (cntr) " << subgroups[k].RotGrp.type().hall_symbol()
                        # << " PG.LaueGrp_ref " <<
                        # PG.LaueGrp_ref.type().hall_symbol() << "\n";
                        # ^ -1
                        # print(self.subgroups[k].LaueGrp_ref.info().type().lookup_symbol(),PG.LaueGrp_ref.info().type().lookup_symbol())
                        if self.subgroups[
                            k].LaueGrp_ref.info().type().lookup_symbol() == PG.LaueGrp_ref.info().type().lookup_symbol():
                            # Yes it is
                            # double celldiff = subgroups[k].SetCell(TG.TransformedCell(), reindex_op, AllowI2);
                            # print(self.subgroups[k].RotGrp.type().lookup_symbol())

                            celldiff = self.subgroups[k].SetCell(TG.input_cell, self.reindex_op, AllowI2)

                            # ^ debug
                            #      std::cout << "\nAlternative found: reindex "
                            #                << subgroups[k].RefSGreindexFormat() << "\n";
                            #      std::cout << "Cell: ";
                            #      std::vector<double> tcell =
                            #        subgroups[k].TransformedCell();
                            #      for (int i=0;i<6;i++) std:: cout << " " << tcell[i];
                            #      std::cout << "\nRef Input Cell: ";
                            #      for (int i=0;i<6;i++) std:: cout << " " << PG.input_cell[i];
                            #      std::cout << "\n";
                            #      std::cout << "Subgroup Input Cell: ";
                            #      for (int i=0;i<6;i++) std:: cout << " " << subgroups[k].input_cell[i];
                            #      std::cout << "\n";
                            #
                            cell_diffs = []
                            possible_reindex, cell_diffs = self.subgroups[k].GetCloseCell(target_cell, max_delta, False,
                                                                                          AllowI2)

                            for i in range(len(possible_reindex)):
                                # Is this a "strict" operator?
                                possible_reindex[i].SetStrict(possible_reindex[i] in self.StrictOps)
                                possible_reindex[i].SetDeviation(cell_diffs[i])
                                # Is this an allowed operator in this pointgroup?
                                OK = True
                                if checksymmetry:  # test for compatible symmetry
                                    OK = PG.allowedReindex(possible_reindex[i])
                                    # print("OK is",OK,self.subgroups[k].RotGrp,possible_reindex[i].as_hkl())
                                if OK:
                                    self.ReindexList.append(possible_reindex[i])

            self.ReindexList = sorted(self.ReindexList)

            return self.ReindexList

        def GetSubGroups(self, symm):
            # Make list of subgroups
            Nel = symm.Nelement()
            LatticeType = symm.lattice_type()
            subgroups = []

            # Always put P-1 (or C1 etc) into list
            subgroups.append(PointGroup(LatticeType=LatticeType))
            # Loop pairs of symmetry elements, constructing pointgroup from each pair
            for i in range(Nel):
                assert (symm.NopInElement(i) >= 0)
                # 1st Symop from symmetry element i
                Ri = symm.SymopInElement(0, i)
                for j in range(Nel):
                    assert (symm.NopInElement(j) >= 0)
                    # 1st Symop from symmetry element j
                    Rj = symm.SymopInElement(0, j)
                    # Generate point-group from symops i & j
                    PG = PointGroup(Kelement1=i, Rmatrix1=Ri, Kelement2=j, Rmatrix2=Rj, LatticeType=LatticeType)

                    # Store if we don't have it already
                    if PG not in subgroups:
                        subgroups.append(PG)
                        # ^
                        #      std::cout << "GetSubGroups subgroup added "
                        #                <<  subgroups.back().RefPGname() << " reindex to standard "
                        #                << subgroups.back().RefSGreindex().as_hkl() << "\n";
                        # ^-
                    else:
                        pass
                        # ^
                        #      std::cout << "GetSubGroups subgroup NOT added (same) "
                        #                <<  subgroups.back().RefPGname() << " reindex to standard "
                        #                << CCtbxSym::PointGroup(PG).RefSGreindex().as_hkl() << "\n";
                        # ^-

            # ^
            #    std::cout << "Number of subgroups = " << subgroups.size() << "\n\n";
            # ^-

            # . . . . . . . . . . . . . . . . . . . . . . . . . . . .
            # We have a list of "subgroups" constructed from
            # symmetry elements - loop round subgroups adding any additional
            # elements which belong

            for k in range(len(subgroups)):
                # Test all elements to see if they are present
                for j in range(Nel):
                    if not subgroups[k].HasElementInt(j):
                        # Element not yet there, test if it should be added
                        Rj = symm.SymopInElement(0, j)
                        added = subgroups[k].AddElement(j, Rj)

            return subgroups

    class LatticeSymmetry(object):

        def __init__(self, pg, AllowI2, max_delta=3.0):
            """
            #^
            #    std::cout <<"LatticeSymmetry: Ltype "<<lattice_type<<" AllowI2 "<< AllowI2;
            #    for (int i=0;i<6;++i) {std::cout<<" "<< unit_cell_dimensions[i];}
            #    std::cout <<"\n";
            #^-
            # Symmetry object combines unit_cell object and
            # space-group (in this P1, C1, I1, F1 or R1)
            :param unit_cell_dimensions:
            :param lattice_type:
            :param AllowI2:
            :param max_delta:
            """
            self.pg = pg
            unit_cell_dimensions = self.pg.input_cell
            lattice_type = self.pg.OriginalLatType()

            self.CrystalSystem = {"NOSYSTEM": 0, "TRICLINIC": 1, "MONOCLINIC": 2, "ORTHORHOMBIC": 3, "TETRAGONAL": 4,
                                  "TRIGONAL": 5, "HEXAGONAL": 6, "CUBIC": 7}

            self.lattice_type_ = lattice_type
            self.max_delta_ = max_delta
            self.dcell = list(unit_cell_dimensions)
            # CCtbx routines need lattice type H as R

            if (self.lattice_type_ == 'H'): self.lattice_type_ = 'R'
            # but if the rhombohedral setting is really rhombohedral,
            # then it needs to be treated as P here, otherwise the z2p_op is wrong
            if (self.lattice_type_ == 'R'):
                # Hexagonal setting has angles 90,90,120
                # Rhombohedral has alpha=beta=gamma
                if (self.RhombohedralAxes(unit_cell_dimensions)): self.lattice_type_ = 'P'

            self.input_symmetry_ = cctbx.crystal.symmetry(unit_cell=cctbx.uctbx.unit_cell(self.dcell),
                                                          space_group=cctbx.sgtbx.space_group(self.lattice_type_ + "1"))
            # get an operator to convert centred cell to primitive
            # == I if P already
            z2p_op = self.input_symmetry_.space_group().z2p_op()
            # Reduce primitive cell
            p_cell = self.input_symmetry_.unit_cell().change_basis(z2p_op.c_inv().r().as_double())
            red_op = self.ReduceCell(p_cell, z2p_op)
            z2p_op = red_op * z2p_op
            # print(str(z2p_op),str(red_op),str(z2p_op))
            primitive_symmetry = self.input_symmetry_.change_basis(z2p_op)

            # ^
            #    std::cout <<"lattype "  << lattice_type << " max_delta " << max_delta<<"\n";
            #    std::cout <<"Input cell ";
            #    for (int i=0;i<6;++i){std::cout <<" "<<dcell[i];}
            #    std::cout <<"\n";
            #    std::cout << "primitive_symmetry.unit_cell() "<<
            #      UcellFormat(primitive_symmetry.unit_cell()) <<"\n";
            # ^-
            # Get highest symmetry compatible with lattice
            # (note the group_search overloads operator()
            #   to create spacegroup)
            # computes potential axes
            # **  call changed in cctbx version 2
            try:
                # ** for new cctbx **
                lattice_group = cctbx.sgtbx.lattice_symmetry.group(primitive_symmetry.unit_cell(), max_delta)
            except:
                # ** old version 1
                lattice_group = cctbx.sgtbx.lattice_symmetry.group_search(primitive_symmetry.unit_cell(), max_delta)

            lattice_group.make_tidy()

            # print(primitive_symmetry.unit_cell().parameters(), lattice_group.type().lookup_symbol())
            # Adjust unit cell to fit lattice symmetry
            # (eg force angles = 90 etc)
            adjust_sym = cctbx.crystal.symmetry(unit_cell=lattice_group.average_unit_cell(primitive_symmetry.unit_cell()),
                                                space_group=lattice_group)
            # Convert adjusted cell to reference setting
            adjust_sym_type = cctbx.sgtbx.space_group_type(adjust_sym.space_group())
            cb_op_ref = adjust_sym_type.cb_op()
            self.best_symmetry_ = adjust_sym.change_basis(cb_op_ref)

            # ^
            #    std::cout << "\nLatticeSymmetry::BestSymmetry "
            #        << best_symmetry_.space_group().type().hall_symbol() << "\n";
            #    std::cout << "Cell: " <<  UcellFormat(best_symmetry_.unit_cell()) << "\n";
            # ^-
            #  Select "best" orthorhombic or monoclinic cell

            cb_op_opt = cctbx.sgtbx.change_of_basis_op()

            LatG = LatticeGroup(self.best_symmetry_.space_group(),
                                self.pg.CentringSymbol(self.best_symmetry_.space_group()))
            if (LatG.crystal_system() == self.CrystalSystem["ORTHORHOMBIC"] or LatG.crystal_system() == self.CrystalSystem[
                "MONOCLINIC"]):
                cb_op_opt = self.pg.GetBestCell(self.best_symmetry_.space_group(), self.best_symmetry_.unit_cell(), AllowI2)
                self.best_symmetry_ = self.best_symmetry_.change_basis(cb_op_opt)

            # Total basis transformation
            self.cb_op_inp_best = cb_op_opt * cb_op_ref * z2p_op
            # Use identity change-of-basis operator if possible
            if (self.best_symmetry_.unit_cell().is_similar_to(self.input_symmetry_.unit_cell())):
                cb_op_corr = self.cb_op_inp_best.inverse()
                if (self.best_symmetry_.change_basis(cb_op_corr).space_group() == self.best_symmetry_.space_group()):
                    self.cb_op_inp_best = cb_op_corr * self.cb_op_inp_best

            # Maximum deviation from original cell
            self.delta = cctbx.sgtbx.lattice_symmetry.find_max_delta(
                self.cb_op_inp_best.apply(self.input_symmetry_.unit_cell()),
                self.best_symmetry_.space_group().build_derived_point_group())

            # self.lattice_symmetry_ = cctbx.crystal.symmetry(unit_cell=primitive_symmetry.unit_cell(), space_group=lattice_group)

        def RhombohedralAxes(self, unit_cell_dimensions):
            # True if not hexagonal setting(angles 90, 90, 120)
            tol = 0.2  # tolerance on angles
            if (abs(unit_cell_dimensions.at(3) - 90.0) <= tol):
                if (abs(unit_cell_dimensions.at(4) - 90.0) <= tol):
                    if (abs(unit_cell_dimensions.at(5) - 120.0) <= tol):
                        return False
            return True

        def ReduceCell(self, cell, RefOp=cctbx.sgtbx.change_of_basis_op()):
            red = cctbx.uctbx.fast_minimum_reduction(cell)
            return cctbx.sgtbx.change_of_basis_op(
                cctbx.sgtbx.rt_mx(cctbx.sgtbx.rot_mx(red.r_inv(), 1)).inverse()).new_denominators(RefOp)

        def best_sg_reindex_op(self, rotgroup=None):
            op = self.cb_op_inp_best.c_inv().r().as_double()
            vop = np.array(list(op)).reshape((3, 3))

            return ReindexOp(vop, as_hkl=self.cb_op_inp_best.as_hkl(), rotgroup=rotgroup)

        def best_spacegroup_symbol(self):
            return self.SpaceGroupName(self.best_symmetry_.space_group(), Rlattice='H')

        def SpaceGroupName(self, SG, Rlattice='R', ReferenceSetting=True):
            # Format spacegroup name in "standard" convention
            # Second argument is symbol to use for rhombohedral lattice (R or H)
            SGtype = SG.type()
            symbol = SGtype.lookup_symbol()
            sgs = cctbx.sgtbx.space_group_symbols(symbol)
            ###sgname = sgs.hermann_mauguin()
            sgname = sgs.hall()
            # std::cout << "SpaceGroupName " << sgname << "\n"; // ^
            sgn = SGtype.number()
            # if ReferenceSetting true, then use the reference setting rather than to actual setting
            if (ReferenceSetting):
                ###sgname = cctbx.sgtbx.space_group_symbols(sgn).hermann_mauguin()
                sgname = cctbx.sgtbx.space_group_symbols(sgn).hall()

            # Check for monoclinic centred lattice
            if (self.centred_monoclinic(sgn)):
                ###sgname = cctbx.sgtbx.space_group_symbols(SGtype.lookup_symbol()).hermann_mauguin()
                sgname = cctbx.sgtbx.space_group_symbols(SGtype.lookup_symbol()).hall()

            # Look for "R" or "H"
            if "R" in sgname:
                i = sgname.index("R")
                sgname = sgname[:i] + Rlattice + sgname[i + 1:]
            elif "H" in sgname:
                i = sgname.index("H")
                sgname = sgname[:i] + Rlattice + sgname[i + 1:]

            return sgname

        def centred_monoclinic(self, sgn):
            # true if space group number sgn is C/I-centred monoclinic
            if (sgn == 5 or sgn == 8 or sgn == 9 or sgn == 12 or sgn == 15): return True
            return False

    class ReindexOp(object):
        # Reindex operator H  (plus optional translation, usually 0,0,0)
        # Applies to index h such that h'T = hT H thus H matrices concatenate in order H' = H1 H2
        def __init__(self, rot=None, as_hkl=None, rotgroup=None):
            self.RTop = np.eye(3, dtype=float) if rot is None else rot
            self.strict = False
            self.deviation = 0.0
            self.as_hkl_string = as_hkl
            self.rotgroup = rotgroup

        def SetStrict(self, Strict):
            self.strict = Strict

        def Strict(self):
            return self.strict

        def SetDeviation(self, Deviation):
            self.deviation = Deviation

        def Deviation(self):
            return self.deviation

        def transpose(self):
            return self.RTop.T

        def inverse(self):
            return np.linalg.inv(self.RTop)

        def rot(self):
            return self.RTop

        def __eq__(self, b):
            tol = 1.0e-6
            return np.sum(np.abs(self.RTop - b.RTop)) < tol

        def __lt__(self, b):
            r = self.cmp(b)
            if r:
                return True
            else:
                return False

        def __le__(self, b):
            r = self.cmp(b)
            if r or self.__eq__(b):
                return True
            else:
                return False

        def __ne__(self, b):
            r = self.__eq__(b)
            if not self.__eq__(b):
                return True
            else:
                return False

        def __gt__(self, b):
            r = self.cmp(b)
            if not r:
                return True
            else:
                return False

        def __ge__(self, b):
            r = self.cmp(b)
            if not r or self.__eq__(b):
                return True
            else:
                return False

        def cmp(self, b):
            # Sort on deviation, putting identity before anything else
            if self.is_rtop_ident(): return True
            if b.is_rtop_ident(): return False
            return self.deviation < b.deviation

        def is_rtop_ident(self):
            return np.allclose(self.RTop, np.eye(3))

        def IsIdentity(self):
            return self.is_rtop_ident()

        def as_hkl(self):

            R = PIC_utilities.get_sgtbx_rt_mx(self.transpose(), r_den=cctbx.sgtbx.cb_r_den, t_den=cctbx.sgtbx.cb_t_den)

            chb = R.as_xyz(False, False, "hkl")
            # print(chb)

            chb = chb.strip('*')
            return "[" + chb + "]"

    class PointGroup(object):

        def __init__(self, **kwargs):
            """
            All Constructs
            """

            self.ElementNums = []
            self.LatticeType = None
            self.Cell_Diff = 0.0

            self.CrystalSystem = {"NOSYSTEM": 0, "TRICLINIC": 1, "MONOCLINIC": 2, "ORTHORHOMBIC": 3, "TETRAGONAL": 4,
                                  "TRIGONAL": 5, "HEXAGONAL": 6, "CUBIC": 7}

            if len(kwargs.keys()) == 0:
                self.init(cctbx.sgtbx.space_group(), 'P')
            elif len(set(["LatticeType"]) & set(kwargs.keys())) == len(kwargs.keys()):
                self.LatticeType = kwargs["LatticeType"]
                Pgroup = cctbx.sgtbx.space_group()
                self.init(Pgroup, kwargs["LatticeType"])
            elif len(set(["name", "num"]) & set(kwargs.keys())) == len(kwargs.keys()):
                self.name = kwargs["name"]
                self.num = kwargs["num"]

                if self.is_name_centred_triclinic(self.name):
                    # centred triclinic C 1 etc
                    self.LatticeType = self.name[0]
                    Pgroup = cctbx.sgtbx.space_group()
                    self.init(Pgroup, self.LatticeType)
                else:
                    self.sgsymbol = cctbx.sgtbx.space_group_symbols(self.num)
                    self.Pgroup = cctbx.sgtbx.space_group(self.sgsymbol.hall()).build_derived_reflection_intensity_group(
                        False)
                    self.LatticeType = self.CentringSymbol(self.Pgroup)
                    self.init(self.Pgroup, self.LatticeType)
            elif len(set(["Kelement1", "Rmatrix1", "Kelement2", "Rmatrix2", "LatticeType"]) & set(kwargs.keys())) == len(
                    kwargs.keys()):
                assert (kwargs["Rmatrix1"].shape == (3, 3))
                assert (kwargs["Rmatrix2"].shape == (3, 3))

                # record unique element numbers
                # Symmetry element list
                self.ElementNums.append(kwargs["Kelement1"])

                # Make rt_mx matrices
                # input operators are reciprocal space operators
                # invert to get real-space
                R1 = PIC_utilities.get_sgtbx_rt_mx(np.linalg.inv(kwargs["Rmatrix1"]), r_den=1, t_den=cctbx.sgtbx.sg_t_den)
                if (kwargs["Kelement2"] != kwargs["Kelement1"]):
                    self.ElementNums.append(kwargs["Kelement2"])
                    R2 = PIC_utilities.get_sgtbx_rt_mx(np.linalg.inv(kwargs["Rmatrix2"]), r_den=1,
                                                       t_den=cctbx.sgtbx.sg_t_den)
                # initialise space-group, add  1st & 2nd operators
                Pgroup = cctbx.sgtbx.space_group()
                # Pgroup.reset(t_den=cctbx.sgtbx.cb_t_den)
                # print(Pgroup.r_den())
                # print(Pgroup.t_den())
                Pgroup.expand_smx(R1)
                if (kwargs["Kelement2"] != kwargs["Kelement1"]): Pgroup.expand_smx(R2)
                Pgroup.make_tidy()
                self.init(Pgroup, kwargs["LatticeType"])

        def init(self, Pgroup, LatticeType):
            self.Pgroup = Pgroup
            self.LatticeType = LatticeType
            # Force rotation group acentric (remove any centre)
            self.RotGrp = self.Pgroup.build_derived_acentric_group()
            self.LatType = self.LatticeType
            if (self.LatType == 'H'): self.LatType = 'R'
            # Laue group in constructor frame
            # Add inversion & centering
            self.LaueGrp = self.LaueGroup(self.RotGrp, self.LatType)

            # Reference setting
            self.LaueGrp_type = self.LaueGrp.type()
            # change of basis from current setting to reference setting
            self.ChBasis_ref = self.LaueGrp_type.cb_op()

            # change of basis from current setting to reference setting
            self.ChBasis = self.ChBasis_ref
            self.input_cell = np.zeros(6)
            # Don't initialise cell (takes too long to call SetCell!)

            # Rotation group in reference frame
            self.RotGrp_ref = self.RotGrp.change_basis(self.ChBasis_ref).build_derived_point_group()
            self.LaueGrp_ref = self.LaueGrp.change_basis(self.ChBasis_ref)
            self.LaueGrp_ref_type = cctbx.sgtbx.space_group_type(self.LaueGrp_ref)
            self.LatType = self.CentringSymbol(self.LaueGrp_ref)
            # Point group, including lattice centering
            self.PntGrp_ref = self.RotGrp_ref
            self.PntGrp_ref.expand_conventional_centring_type(self.LatType)

        def is_name_centred_triclinic(self, name):
            if len(name) != 3: return False
            if name[1:3] != " 1": return False  # not triclinic

            lattices = ['P', 'A', 'B', 'C', 'I', 'F', 'R', 'H']
            if name[0] in lattices:
                return True
            else:
                return False

        def SGnameHtoR(self, sgname, HorR):
            """
              Change H to R or vv in space group name
              If 1st character of name is H or R, change to :
              1. character after ":" if H or R
              2. HorR if 'H' or 'R' [default 'H']
              3. else leave
              eg  R 3 2 -> H 3 2
                  R 3 :H -> H 3
            :param sgname:
            :param HorR:
            :return:
            """

            AllowedTypes = ['P', 'A', 'B', 'C', 'I', 'F', 'R', 'H']

            # remove leading and trailing spaces
            sname = sgname.strip()
            # First character should be a lattice type
            lattype = sname[0].upper()
            if (lattype == 'H' or lattype == 'R'):  # only do anything if H or R
                parts = sname.split(":")
                if len(parts) > 1:
                    ext = parts[1].strip()
                    if len(ext) == 1: ext = str(ext[0])
                    if (ext == "H" or ext == "R"):
                        lattype = ext[0]
                        sname = parts[0].strip()  # strip off ":X"
                elif (HorR == 'H' or HorR == 'R'):
                    lattype = HorR

                if lattype not in AllowedTypes: raise ("Illegal lattice type " + lattype)
                sname = sname[1:].strip()
                # if spaces in original name, add a space after lattice type
                if " " in sgname:
                    sname = lattype + " " + sname
                else:
                    sname = lattype + sname
            return sname

        def CentringSymbol(self, Group):
            # // Extract conventional lattice symbol from spacegroup
            # // For rhombohedral lattices, set it to 'R' even in rhombohedral
            # // axis system (cctbx returns 'P' in this case)

            LT = 'P'

            if (Group.crystal_system().lower() == "trigonal"):
                # // Extract code from Hermann-Mauguin symbol, first non-blank
                HMcode = cctbx.sgtbx.space_group_symbols(Group.type().number()).hermann_mauguin()
                for i in range(len(HMcode)):
                    if HMcode[i] != ' ':
                        LT = HMcode[i]
                        break

            else:
                LT = Group.conventional_centring_type_symbol()

            return LT

        def LaueGroup(self, RotGrp, LatType):
            # Make Laue group from rotation group by adding inversion centre
            # and lattice centring

            Pgroup = cctbx.sgtbx.space_group(RotGrp)
            Pgroup.expand_inv(cctbx.sgtbx.tr_vec([0, 0, 0]))  # add inversion centre
            if LatType != ' ': Pgroup.expand_conventional_centring_type(LatType)
            Pgroup.make_tidy()
            return Pgroup

        def GetBestCell(self, space_group, uccell, AllowI2):
            # Returns change of basis to "best" cell
            # only affects triclinic, monoclinic & orthorhombic cells

            ExcludeIdentity = False
            AnyCell = True
            BestCell = True

            CbOp_list = self.GetAlternativeBases(space_group, ExcludeIdentity, AnyCell, BestCell, uccell, uccell, 3.0,
                                                 0.0001, AllowI2)
            # should return only one value

            return CbOp_list[0].FirstOp()

        def ReduceCell(self, cell, RefOp=cctbx.sgtbx.change_of_basis_op()):
            red = cctbx.uctbx.fast_minimum_reduction(cell)
            return cctbx.sgtbx.change_of_basis_op(
                cctbx.sgtbx.rt_mx(cctbx.sgtbx.rot_mx(red.r_inv(), 1)).inverse()).new_denominators(RefOp)

        def PossibleChBOp(self, LatGroup, Group, AllowI2):
            """
              // Get list of possible change of basis operators, depending
              // on symmetry group
              //
              // On entry:
              //  LatGroup     lattice group
              //  Group        symmetry group
              //  AllowI2      +1 to add I2 settings to C2 (mC or mI)
              //
              // 1) Symmetry > orthorhombic
              //    find operators in LatGroup which are not present in Group
              // 2) Symmetry == orthorhombic
              //    axis permutation operators (independent of Group)
              // 3) Symmetry == monoclinic or triclinic
              //    find all affine transformations which preserve point group
              //    (plus I2 alternative to C2 if AllowI2 > 0)
              //
            :param LatGroup:
            :param Group:
            :param AllowI2:
            :return:
            """

            cb_ops = []
            cb_op_def = cctbx.sgtbx.change_of_basis_op()  # to define correct denominators

            CrysSys = LatGroup.crystal_system()

            if (CrysSys > self.CrystalSystem["ORTHORHOMBIC"]):
                # Higher symmetry than orthorhombic
                cb_ops.append(cctbx.sgtbx.change_of_basis_op())  # add in identity
                # Test if there any symmetry operators which are in
                # the lattice group but not the Laue group
                # These are potential alternative bases
                LatGrp = LatGroup.lattice_group()
                for i in range(1, LatGrp.n_smx()):
                    # Is this operator in Laue group?
                    found = False
                    for j in range(1, Group.n_smx()):
                        if (LatGrp.smx(i) == Group.smx(j)):
                            found = True
                            break
                    if not found:
                        # Add operator into vector as change of basis
                        cb_ops.append(cctbx.sgtbx.change_of_basis_op(list(LatGrp.smx())[i]))
            elif (CrysSys == self.CrystalSystem["ORTHORHOMBIC"]):
                # Orthorhombic
                cb_ops.append(cctbx.sgtbx.change_of_basis_op())  # add in identity
                # Use cubic symmetry to generate permutation operators
                affine_group = cctbx.sgtbx.space_group("P 4 3*")
                for i_smx in range(1, affine_group.n_smx()):
                    cb_ops.append(cctbx.sgtbx.change_of_basis_op(affine_group(i_smx)).new_denominators(cb_op_def))
            elif (CrysSys == self.CrystalSystem["MONOCLINIC"] or CrysSys == self.CrystalSystem["TRICLINIC"]):
                ChooseI2 = 0
                # Is this centred monoclinic?
                if (CrysSys == self.CrystalSystem["MONOCLINIC"] and (self.CentringSymbol(Group) == 'C')):
                    if (AllowI2 > 0):
                        ChooseI2 = +1  # allow I2
                    elif (AllowI2 < 0):
                        ChooseI2 = -1  # force I2
                # List of change-of-basis operators which leave
                # spacegroup invariant and leave cell the same within tolerance

                affine_range = 4  # default = 2
                if (ChooseI2 >= 0):
                    affine = cctbx.sgtbx.find_affine(Group, affine_range)
                    # print(affine.cb_mx())
                    affine_cb_mx = list(affine.cb_mx())  # affine.cb_mx().const_ref()

                    # Loop and store potential change - of - basis operators
                    for i_cb_mx in range(len(affine_cb_mx)):
                        cb_ops.append(cctbx.sgtbx.change_of_basis_op(affine_cb_mx[i_cb_mx]).new_denominators(cb_op_def))

                if (ChooseI2 != 0):
                    # Change of basis from C2 to I2
                    SG_C2 = cctbx.sgtbx.space_group(cctbx.sgtbx.space_group_symbols("C2/m"))
                    SG_I2 = cctbx.sgtbx.space_group(cctbx.sgtbx.space_group_symbols("I2/m"))
                    I2toC2 = SG_I2.type().cb_op().new_denominators(cb_op_def)
                    C2toI2 = I2toC2.inverse().new_denominators(cb_op_def)

                    affineI2 = cctbx.sgtbx.find_affine(Group.change_basis(C2toI2), affine_range)
                    affine_cb_mxI2 = list(affineI2.cb_mx().const_ref())

                    # Loop and store potential change - of - basis operators
                    for i_cb_mx in range(len(affine_cb_mxI2)):
                        new_cbop = cctbx.sgtbx.change_of_basis_op(affine_cb_mxI2[i_cb_mx]).new_denominators(cb_op_def)
                        # Post - multiply each operator in I2 frame by I2 to C2 transformation
                        cb_ops.append(new_cbop * I2toC2)

            return cb_ops

        def UpdateBestCell(self, sameGroup, CrysSys, test_cell, UniqueAxis, cb_op, angular_tolerance, best_cb_op,
                           best_cell):
            # If test_cell is closer to the "standard" cell than
            # best_cell, update best_cell := test_cell and best_cbop := cb_op
            # Monoclinic & orthorhombic (not C-centred, detected by sameGroup == false)
            # only, no change otherwise
            # Return true if accepted

            compare = -1
            if (CrysSys == self.CrystalSystem["ORTHORHOMBIC"] and sameGroup):
                compare = best_cell.compare_orthorhombic(test_cell)
            elif CrysSys == self.CrystalSystem["MONOCLINIC"]:
                compare = best_cell.compare_monoclinic(test_cell, UniqueAxis, angular_tolerance)
                # Accept new one anyway if beta >= 90 and old best had beta < 90
                if (compare < 1):
                    if (best_cell.parameters()[UniqueAxis + 3] < 90.0 and test_cell.parameters()[UniqueAxis + 3] >= 90.0):
                        compare = +1

            if (compare > 0):
                """
                  //^
                  //      std::cout << "Updating cell: " << CrysSys << " "
                  //                << sameGroup << " " << angular_tolerance << "\n"
                  //                << "Old best cell: " << UcellFormat(best_cell) <<"\n"
                  //                << "New best cell: " << UcellFormat(test_cell) <<"\n";
                  //      for (int i=0;i<6;++i) {std::cout <<" "<<best_cell.parameters()[i];}
                  //      std::cout <<"\n";
                  //-!
                """
                best_cb_op = cb_op
                best_cell = test_cell
            return (compare > 0)

        def SymInGroup(self, Group, cb_op_mx):
            """
            # Returns true if ChBoperator is in group (including identity)
            #^^
            #    std::cout << "SymInGroup " << cb_op_mx.as_xyz() <<"\n";
            :param Group:
            :param cb_op_mx:
            :return:
            """
            for j in range(Group.n_smx()):
                """
                #^
                #        std::cout << "   Symop   " << Group.smx(j).as_xyz() <<"\n";
                #        if (cb_op_mx == Group.smx(j)) {
                """
                if (cb_op_mx.r() == list(Group.smx())[j].r()): return True
            return False

        def GetAlternativeBases(self, Group, ExcludeIdentity, AnyCell, BestCell, uccell, targetcell, tolerance,
                                angular_tolerance, AllowI2):
            """
            // Get list of alternative basis sets for Laue group
            // subject to criteria set by flags
            //
            // On entry:
            //  Group       symmetry group
            //  ExcludeIdentity if true, exclude settings identical or
            //                   symmetry-related to initial setting
            //  AnyCell     if true, accept any cell (no test against targetcell)
            //              if false, accept only cells within tolerance of
            //              given targetcell, ranked by difference
            //  BestCell    if true, only accept "best" conventional cell
            //              starting from uccell
            //              Not relevent for symmetry > Orthorhombic
            //              definition of "best" depends on AnyCell
            //                AnyCell true  - best according to standard
            //                AnyCell false - closest to targetcell
            //
            //  uccell      initial unit cell
            //  targetcell  target unit cell for comparison
            //  tolerance   tolerance on mean square base vector difference
            //  angular_tolerance  similarity allowed for
            //                     monoclinic & triclinic cells
            //  AllowI2      > 0 to add I2 settings to C2 (mC or mI)
            //               < 0 force I2 setting
            //
            // On exit:
            //  returns vector of vectors of change-of-basis operators
            //  grouped if they are related by the Group symmetry

            // Best Cell definitions:
            //   1) orthorhombic
            //      test all permutations of axes, standard has a<b<c
            //   3) monoclinic
            //      test affine transformations around unique axis
            //      best has minimum beta - 90
            //      If AllowI2 > 0, test I2 setting as well as C2 for centred monoclinic
            //   3) triclinic
            //      get reduced cell
            //
            //
            :param Group:
            :param ExcludeIdentity:
            :param AnyCell:
            :param BestCell:
            :param uccell:
            :param targetcell:
            :param tolerance:
            :param angular_tolerance:
            :param AllowI2:
            :return:
            """

            """
            //^
            //    std::cout << "GetAlternativeBases " << Group.type().hall_symbol() << "\n";
            //    std::cout <<"ExcludeIdentity " << ExcludeIdentity
            //        << " AnyCell " << AnyCell <<" BestCell " << BestCell
            //        << " AllowI2 "<<AllowI2<<"\n";
            //    std::cout << "Cell: " << UcellFormat(uccell) << "\n";
            //    std::cout << "Target Cell: " << UcellFormat(targetcell) << "\n";
            //-!
            """

            AltGroup = Group
            LatGroup = LatticeGroup(Group, self.CentringSymbol(Group))
            CrysSys = LatGroup.crystal_system()
            best_cb_op = cctbx.sgtbx.change_of_basis_op()
            ChBops = None

            best_cell = uccell
            test_cell = uccell

            ChBasisVec = []

            UniqueAxis = -1

            if CrysSys == self.CrystalSystem["MONOCLINIC"]:
                # // Unique axis = 0,1,2 for a,b,c, = -1 if triclinic
                UniqueAxis = self.MonoclinicUniqueAxis(Group)

            GotPossibleChBOpList = False

            # First get standard "best" cell in all cases where it is needed
            if (AnyCell):
                if (CrysSys == self.CrystalSystem["TRICLINIC"]):
                    # triclinic, just get reduced cell
                    best_cb_op = self.ReduceCell(test_cell)
                elif (CrysSys == self.CrystalSystem["MONOCLINIC"] or CrysSys == self.CrystalSystem["ORTHORHOMBIC"]):
                    # Get list of possible change of basis operators to test,
                    # depending on crystal system
                    # This is independent of cell, but will run over a range
                    # (+-affine_range) of cell offsets
                    ChBops = self.PossibleChBOp(LatGroup, Group, AllowI2)
                    GotPossibleChBOpList = True
                    op1 = 0
                    if (AllowI2 < 0):
                        # // Force I2 option - apply first operator
                        best_cb_op = ChBops[0]
                        best_cell = best_cb_op.apply(uccell)
                        op1 = 1  # skip 1st op

                    for i in range(op1, len(ChBops)):
                        cb_op = ChBops[i]
                        cb_op_mx = cb_op.c().new_denominators(list(Group.smx())[0])

                        AltGroup = Group.change_basis(cb_op)
                        sameGroup = (AltGroup == Group)
                        #              if (AltGroup == Group) {
                        # Exclude operators which change group
                        #  (possibly never happens)
                        test_cell = cb_op.apply(uccell)
                        # Best standard cell
                        updated = self.UpdateBestCell(sameGroup, CrysSys, test_cell, UniqueAxis, cb_op, angular_tolerance,
                                                      best_cb_op, best_cell)
                        # updated = updated
                        # //^
                        # //              if (updated) {
                        # //                std::cout <<"\nUpdated\n";
                        # //                std::cout << "Alternative " << i
                        # //                          << " group "
                        # //                          << AltGroup.type().universal_hermann_mauguin_symbol()
                        # //                          << " lattice "<< AltGroup.conventional_centring_type_symbol()
                        # //                          << " cell " << UcellFormat(cb_op.apply(uccell)) << "\n";
                        # //                PrintChBOp(cb_op);
                        # //              } else {
                        # //                std::cout << "Not accepted, groups "
                        # //                          << Group.type().hall_symbol() << " : "
                        # //                          << AltGroup.type().hall_symbol() << "\n";
                        # //                              }
                        # //-!
                    # End loop potential change-of-basis operators

                # We now have a "best" cell
                if (BestCell):
                    # Store best operator as sole solution
                    # that is all we need
                    ChBasisVec = []
                    # Exclude identity option
                    cb_op_mx = best_cb_op.c().new_denominators(list(Group.smx())[0])
                    if (not (ExcludeIdentity and self.SymInGroup(Group, cb_op_mx))):
                        ChBasisVec.append(AlternativeBases(best_cb_op, 0.0))
                    # // ^
                    # // std::
                    #     cout << "**** Best cell "
                    #     // << UcellFormat(best_cb_op.apply(uccell)) << "\n";
                    # // PrintChBOp(best_cb_op);
                    # // ^ -
                    return ChBasisVec
                best_cell = best_cb_op.apply(uccell)
            else:
                best_cell = targetcell

            # ^
            #    std::cout << "* * Input cell        " << UcellFormat(uccell) << "\n";
            #    std::cout << "* * Target(best) cell " << UcellFormat(best_cell) << "\n";
            # Get differences against either "best" cell or target cell
            # only get here if !BestCell

            # Get list of possible change of basis operators to test,
            # depending on crystal system, if we haven't got them already
            # This is independent of cell, but will run over a range
            # (+-affine_range) of cell offsets

            if (not GotPossibleChBOpList):
                ChBops = self.PossibleChBOp(LatGroup, Group, AllowI2)

            # Loop potential change-of-basis operators
            for i in range(len(ChBops)):
                cb_op = ChBops[i]
                cb_op_mx = cb_op.c().new_denominators(list(Group.smx())[0])
                if (not (ExcludeIdentity and self.SymInGroup(Group, cb_op_mx))):
                    # Not identity or symmetry-related, if excluded
                    AltGroup = Group.change_basis(cb_op)
                    if (AltGroup.info().type().lookup_symbol() == Group.info().type().lookup_symbol()):
                        # Exclude operators which change group
                        test_cell = cb_op.apply(uccell)
                        # Measure of difference between cells
                        # root mean square difference of bases
                        celldiff = np.sqrt(best_cell.bases_mean_square_difference(test_cell))
                        # ^
                        # std::cout << "* * Test cell " << UcellFormat(test_cell) << "\n";

                        if (AnyCell or celldiff < tolerance):
                            #              targetcell.is_similar_to
                            #              (test_cell,length_tolerance,angular_tolerance))
                            # ^
                            #              std::cout << "**** Got it! " << UcellFormat(test_cell) << "\n";
                            #              PrintChBOp(cb_op);
                            # ^-
                            # Cell acceptable on AnyCell or similar
                            # or all acceptable cells (not BestCell)
                            ChBasisVec = self.AppendUniqueChBasis(cb_op, cb_op_mx, celldiff, Group, ChBasisVec)
            # Sort solutions of deviation from best or target cell
            ChBasisVec = sorted(ChBasisVec, key=lambda x: x.CellDiff())
            # for "BestCell" option, just return first example
            if (len(ChBasisVec) > 0 and BestCell): ChBasisVec = ChBasisVec[:1]

            return ChBasisVec

        def AppendUniqueChBasis(self, cb_op, cb_op_mx, celldiff, Group, ChBasisVec):
            """
            # Add cb_op into ChBasisVec (vector of AlternativeBases)
            # note that the AlternativeBases class is itself a vector of ops
            # If it is equivalent by symmetry in Group (rotational part), add
            #  to equivalent vector, else start new vector
            # Don't store if identical to existing op
            # Special for P1: no check on existing operators, just append
            #
            # On entry:
            #  cb_op      change of basis operator
            #  cb_op_mx   equivalent rt_mx operator
            #  Group      symmetry group
            #  ChBasisVec vector to update
            :param cb_op:
            :param cb_op_mx:
            :param celldiff:
            :param Group:
            :return:
            """

            kop = 0
            equiv_op = False
            ident = False
            if (len(ChBasisVec) > 0 and Group.n_smx() > 1):
                # do we have a symmetry-related version of this one already?
                for j in range(Group.n_smx()):
                    S = list(Group.smx())[j].multiply(cb_op_mx).new_denominators(cb_op_mx)
                    for k in range(len(ChBasisVec)):
                        if (S == ChBasisVec[k].FirstOp().c().new_denominators(S)):
                            if (j == 0):
                                # Identical
                                ident = True
                            equiv_op = True
                            kop = k
                            break

            if (not ident):
                if (equiv_op):
                    # Equivalent operator, accumulate in vector
                    ChBasisVec[kop].AddOp(cb_op)
                else:
                    # New operator
                    ChBasisVec.append(AlternativeBases(cb_op, celldiff))

            return ChBasisVec

        def MonoclinicUniqueAxis(self, Group):
            """
            // Returns unique axis for monoclinic spacegroup
            // = 0,1,2 for a,b,c
            // = -1 if not monoclinic
            // from Ralf Grosse-Kunstleve's lattice_symmetry.cpp
            :param Group:
            :return:
            """

            if (Group.n_smx() != 2): return -1
            # // Second symmetry operator (1st is identity)
            two_fold_info = cctbx.sgtbx.rot_mx_info(Group(1).r())
            # // must be dyad
            assert (abs(int(two_fold_info.type())) == 2)
            # // Axis direction (eigenvector), the element we want = +1
            # // Assumed to be some permutation of (0,0,1)
            ev = two_fold_info.ev()
            # // Axis = 0,1,2
            return list(ev).index(1)

        def SetCell(self, cellin, reindex_op, AllowI2):
            # Store cell, returns maximum angular deviation from imposing
            # symmetry constraints, and store change-of-basis from
            # cell frame to symmetry frame used to construct this object
            # Note the change of basis operator is inverse of reindex operator
            #
            # reindex_op is reindexing from cell frame to
            # "constructor" (lattice) frame

            assert (len(cellin) == 6)
            self.input_cell = cellin
            dcell = [x for x in cellin]
            uccell = cctbx.uctbx.unit_cell(dcell)

            # Reindex Op from original cell frame to this constructor frame
            CellReindexOp = reindex_op
            ChBasis_cell = self.MakeChangeOfBasisOp(CellReindexOp)
            # print(self.ChBasis_ref.c().t().den())
            # print(ChBasis_cell.c().t().den())
            self.ChBasis = self.ChBasis_ref * ChBasis_cell

            # print("cellin",cellin)
            # print("uccell",uccell.parameters())
            uccell_chb = self.ChBasis.apply(uccell)
            # print("uccell_chb",uccell_chb.parameters())
            # ^
            #    std::cout << "\n=== SetCell " << RefLGname() << " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";
            # ^-
            # Try to get "best" setting of cell and new reference
            #  cb_op_best transforms reference to "best"
            cb_op_best = self.GetBestCell(self.LaueGrp_ref, uccell_chb, AllowI2)

            self.uccell_ref = cb_op_best.apply(uccell_chb)

            # ^
            #    std::cout << "\nPointGroup::SetCell\nReindex original->constructor ChBasis_cell): "
            #                <<  ChangeBasisFormat_as_Reindex(ChBasis_cell) << "\n"
            #                << "Reindex original->reference ChBasis): "
            #                <<  ChangeBasisFormat_as_Reindex(ChBasis) << "\n"
            #                << "Reindex constructor->reference ChBasis_ref): "
            #                <<  ChangeBasisFormat_as_Reindex(ChBasis_ref) << "\n"
            #                << "CellIn:  " << UcellFormat(uccell) << "\n"
            #                << "CellChB: " << UcellFormat(uccell_chb) << "\n"
            #                << "CellRef: " << UcellFormat(uccell_ref) << "\n";
            # -!

            # Test for change of symmetry, C2 to I2
            # Does change of basis operator change the Laue group?
            LGnew = self.LaueGrp_ref.change_basis(cb_op_best)
            if (LGnew != self.LaueGrp_ref):
                # Update stored groups, those in new reference frame
                self.RotGrp_ref = self.RotGrp_ref.change_basis(cb_op_best)
                self.PntGrp_ref = self.PntGrp_ref.change_basis(cb_op_best)
                self.LaueGrp_ref = LGnew
                self.LaueGrp_ref_type = cctbx.sgtbx.space_group_type(self.LaueGrp_ref)
                self.LatType = self.CentringSymbol(self.LaueGrp_ref)
                # ^
                #      std::cout << "\n==== SetCell: updated groups " << LatType << " "
                #                << LaueGrp_ref_type.hall_symbol() << "\n";
                #      std::cout << "Cell_ref: " << UcellFormat(uccell_ref) << "\n";
                #      std::cout << "ChBasis:\n";
                #      PrintChBOp(ChBasis);
                #      std::cout << "ChBasis_ref:\n";
                #      PrintChBOp(ChBasis_ref);
                #      std::cout << "cb_op_best:\n";
                #      PrintChBOp(cb_op_best);
                # ^-

            # ChBasis is operator for Original(cell) -> reference
            self.ChBasis = cb_op_best * self.ChBasis
            self.ChBasis_ref = cb_op_best * self.ChBasis_ref
            #    } else {
            #       // ChBasis is operator for Original(cell) -> reference
            #      ChBasis = cb_op_best * ChBasis;
            #     }
            #  If unit cell and symmetry haven't changed, reset
            #  change-of-basis to identity
            if (self.uccell_ref.is_similar_to(uccell)):
                #      if (OperatorInGroup(ChBasis, RotGrp_ref))
                LatGroup = LatticeGroup(self.RotGrp_ref, self.CentringSymbol(self.RotGrp_ref))
                if (self.OperatorInGroup(self.ChBasis, LatGroup.lattice_group())):
                    self.ChBasis = cctbx.sgtbx.change_of_basis_op()  # identity

            # ^
            #     std::cout << "End of SetCell:\n"
            #         << "  Reindex original->reference ChBasis): "
            #         <<  ChangeBasisFormat_as_Reindex(ChBasis) << "\n";
            # ^-
            #  Maximum angular deviation of cell from that
            #  required by rotation group
            delta = cctbx.sgtbx.lattice_symmetry.find_max_delta(self.uccell_ref, LatticeGroup(self.RotGrp_ref,
                                                                                              self.CentringSymbol(
                                                                                                  self.RotGrp_ref)).lattice_group().build_derived_acentric_group())

            return delta

        def OperatorInGroup(self, ChBasis, SG):
            # Return true if change-of-basis operator is a member of the spacegroup
            smx_list = list(SG.smx())
            # Denominator of rotation part of change-of-basis operator
            den = ChBasis.c().r().den()

            for i in range(len(smx_list)):
                if (smx_list[i].r().new_denominator(den) == ChBasis.c().r()): return True
            return False

        def MakeChangeOfBasisOp(self, reindex_op):
            # Return change_of_basis_op based on reindex operator (its inverse)

            # Reindex Op
            R = reindex_op.inverse()
            # NOTE: The following commented line is what is in Phil code but it does not work in cctbx does not recognize the signature
            # C = cctbx.sgtbx.change_of_basis_op(cctbx.sgtbx.rt_mx(CM, flex.vec3_double(1, (0.0,0.0,0.0)), cctbx.sgtbx.cb_r_den, cctbx.sgtbx.cb_t_den))
            C = cctbx.sgtbx.change_of_basis_op(PIC_utilities.get_sgtbx_rt_mx(R, [0, 0, 0], r_den=cctbx.sgtbx.cb_r_den,
                                                                             t_den=cctbx.sgtbx.cb_t_den))  # r_den=cctbx.sgtbx.cb_r_den
            return C

        def AddElement(self, Kelement, Rmatrix):
            # Test if this symmetry operator is present in pointgroup,
            # if it is return true & add element number to list
            if (self.HasElement(Rmatrix)):
                self.ElementNums.append(Kelement)
                return True

            return False

        def HasElement(self, Rmatrix):
            # Test if this symmetry operator is present in pointgroup,
            # if it is return true
            R = PIC_utilities.get_sgtbx_rt_mx(np.linalg.inv(Rmatrix), r_den=1, t_den=cctbx.sgtbx.sg_t_den)
            # Compare this with all the primitive operators of the
            # point group
            found = False
            for k in range(self.RotGrp.n_smx()):
                if (R == self.RotGrp(0, 0, k)):
                    found = True
                    break

            return found

        def HasElementInt(self, Kelement):
            # // Returns true if element is present in element list
            if Kelement in self.ElementNums:
                return True
            return False

        def GetCloseCell(self, cell_target, diff_tolerance, ExcludeIdentity, AllowI2):
            """
            // Return list of reindex operators & cell differences for
            // alternative indexing schemes which preserve Laue group
            // and have cell close to target.
            //
            // Reindex operators are in target cell frame
            //
            // On entry:
            //   cell_target      target cell in "original(cell)" frame
            //   diff_tolerance   tolerance
            //                     (root mean square difference of base vectors)
            //   ExcludeIdentity  true to exclude identity operator
            //   celldiff         returns cell differences
            //                    (root mean square difference of base vectors)
            //                     (matching reindex operators)
            :param cell_target:
            :param diff_tolerance:
            :param ExcludeIdentity:
            :param celldiff:
            :param AllowI2:
            :return:
            """

            diff_max = 10.0
            dcell = cell_target
            # Target in original frame
            uccell_target = cctbx.uctbx.unit_cell(dcell)

            # Get list of alternative bases in "reference" frame
            AnyCell = False
            BestCell = False
            max_delta = 180.0  # angle tolerance

            CbOp_list = self.GetAlternativeBases(self.LaueGrp_ref, ExcludeIdentity, AnyCell, BestCell, self.uccell_ref,
                                                 uccell_target, diff_tolerance, max_delta, AllowI2)

            diff0 = -1.0
            diff = -1.0

            CloseBases = []

            # Things to return
            CloseReindex = []

            celldiff = []

            if len(CbOp_list) >= 1:
                # Change all operators to be relative original cell frame
                CBlist = PIC_utilities.ChangeBasesList(CbOp_list, self.ChBasis)

                # Best cell difference
                diff0 = CBlist[0].CellDiff()

                for k in range(len(CBlist)):
                    # Deviation from target
                    diff = CBlist[k].CellDiff()
                    if (diff < diff_tolerance and (k == 0 or (diff - diff0) < diff_max)):
                        # Reindex operator + diff, save for sorting
                        CloseBases.append(AlternativeBases(CBlist[k].SimplestOp(), diff))

                # Sort list & transfer for output
                CloseBases = sorted(CloseBases)
                for k in range(len(CBlist)):
                    CloseReindex.append(
                        PIC_utilities.SetReindexOp(CloseBases[k].FirstOp(), rotgroup=self.RotGrp.type().lookup_symbol()))
                    celldiff.append(CloseBases[k].CellDiff())

            return CloseReindex, celldiff

        def OriginalLatType(self):
            return self.CentringSymbol(self.RotGrp)

        def allowedReindex(self, reindex_op):
            # return true if reindex_op is allowed in this point-group, ie it doesn't change the group
            cbop = self.MakeChangeOfBasisOp(reindex_op)
            newgroup = None
            try:
                newgroup = self.RotGrp.change_basis(cbop)
            except:
                return False

            OK = self.sameRotationOps(newgroup)

            return OK

        def sameRotationOps(self, otherSG):
            symops = list(self.RotGrp.all_ops())
            for k in range(len(symops)):
                if not (self.SymInGroup(otherSG, symops[k])):
                    return False
            return True

        def __eq__(self, b):
            return self.RotGrp == b.RotGrp

    class LatticeGroup(object):

        def __init__(self, Group, LatType):
            """
            // Determines lattice group corresponding to spacegroup
            // (from lookup)
            // and crystal system CrysSys (eg MONOCLINIC)
            // International Tables spacegroup number
            //   195-230 cubic
            //   168-194 hexagonal
            //   143-167 trigonal
            //   75-142  tetragonal
            //   16-74   orthorhombic
            //   3-15    monoclinic
            //   1-2     triclinic
            :param Group:
            :param Lat_type:
            """

            self.CrystalSystem = {"NOSYSTEM": 0, "TRICLINIC": 1, "MONOCLINIC": 2, "ORTHORHOMBIC": 3, "TETRAGONAL": 4,
                                  "TRIGONAL": 5, "HEXAGONAL": 6, "CUBIC": 7}

            valid = False
            self.CrysSys = 0
            SpaceGroup = Group
            SpaceGroupNumber = SpaceGroup.type().number()
            if not self.AllowedLatticeType(LatType): print("LatticeGroup: Illegal lattice-type " + str(LatType))

            LatGrpSymbol = ""

            if (SpaceGroupNumber > 194):
                # // Cubic
                self.CrysSys = self.CrystalSystem["CUBIC"]
                if (LatType == 'P'):
                    LatGrpSymbol = "P m -3 m"
                elif (LatType == 'I'):
                    LatGrpSymbol = "I m -3 m"
                elif (LatType == 'F'):
                    LatGrpSymbol = "F m -3 m"
            elif (SpaceGroupNumber > 142):
                # // Hexagonal or trigonal
                if (SpaceGroupNumber > 167):
                    self.CrysSys = self.CrystalSystem["HEXAGONAL"]
                else:
                    self.CrysSys = self.CrystalSystem["TRIGONAL"]
                if (LatType == 'P'):
                    LatGrpSymbol = "P 6/m m m"
                elif (LatType == 'R' or LatType == 'H'):
                    LatGrpSymbol = "R -3 m :H"
            elif (SpaceGroupNumber > 74):
                # // Tetragonal
                self.CrysSys = self.CrystalSystem["TETRAGONAL"]
                if (LatType == 'P'):
                    LatGrpSymbol = "P 4/m m m"
                elif (LatType == 'I'):
                    LatGrpSymbol = "I 4/m m m"
            elif (SpaceGroupNumber > 15):
                # // Orthorhombic
                self.CrysSys = self.CrystalSystem["ORTHORHOMBIC"]
                if (LatType == 'P'):
                    LatGrpSymbol = "P m m m"
                elif (LatType == 'I'):
                    LatGrpSymbol = "I m m m"
                elif (LatType == 'C'):
                    LatGrpSymbol = "C m m m"
                elif (LatType == 'F'):
                    LatGrpSymbol = "F m m m"
            elif (SpaceGroupNumber > 2):
                self.CrysSys = self.CrystalSystem["MONOCLINIC"]
                if (LatType == 'P'):
                    LatGrpSymbol = "P 1 2/m 1"
                elif (LatType == 'C'):
                    LatGrpSymbol = "C 1 2/m 1"
                elif (LatType == 'I'):
                    LatGrpSymbol = "I 1 2/m 1"
            else:
                # // Triclinic
                self.CrysSys = self.CrystalSystem["TRICLINIC"]
                LatGrpSymbol = "P -1"

            if (LatGrpSymbol != ""):
                self.LatGroup = cctbx.sgtbx.space_group(cctbx.sgtbx.space_group_symbols(LatGrpSymbol).hall())
                valid = True

        def AllowedLatticeType(self, LatType):
            # //!< Returns true if LatType is allowed

            AllowedTypes = ['P', 'A', 'B', 'C', 'I', 'F', 'R', 'H']
            if LatType in AllowedTypes:
                return True
            else:
                return False

        def crystal_system(self):
            return self.CrysSys

        def lattice_group(self):
            return self.LatGroup

    class AlternativeBases(object):
        # Vector of symmetry-related change of basis operators +
        # "difference" in cell dimensions from some reference cell

        def __init__(self, cbops=None, celldiff=0.0):
            self.CbOps = [cbops] if not isinstance(cbops, list) else cbops
            self.CellDiff_ = celldiff

        def AddOp(self, cb_op):
            # Special for identity operator, make sure it is the first one
            if (cb_op.is_identity_op()):
                self.CbOps = [cb_op] + self.CbOps
            else:
                self.CbOps.append(cb_op)

        def CellDiff(self):
            return self.CellDiff_

        def Nop(self):
            return len(self.CbOps)

        def FirstOp(self):
            return self.CbOps[0]

        def Op(self, i):
            return self.CbOps[i]

        def SimplestOp(self):
            # Return "simplest" operation:
            # identity or one with smallest non-zero elements
            if (self.CbOps[0].is_identity_op()): return self.CbOps[0]
            besttot = 1000000.
            minneg = 100000
            kbest = 0
            tol = 0.01  # tolerance
            for k in range(len(self.CbOps)):
                total = 0.0
                nneg = 0
                op = np.array(list(self.CbOps[k].c_inv().r().as_double())).reshape((3, 3))
                # Criteria for "simplest"
                # 1) smallest sum of absolute value of all elements
                # 2) minimum number of negatives
                for i in range(3):
                    for j in range(3):
                        total += abs(op[i][j])
                        if op[i][j] < 0.0: nneg += 1
                if (total < besttot - tol):
                    besttot = total
                    kbest = k
                    minneg = nneg
                elif abs(total - besttot) <= tol:
                    if (nneg < minneg):
                        besttot = total
                        kbest = k
                        minneg = nneg

            return self.CbOps[kbest]

        def clear(self):
            self.CbOps = []

        def __lt__(self, b):
            # for sorting by rank on celldiff
            return (self.CellDiff_ < b.CellDiff_)

        def __gt__(self, b):
            r = self.cmp(b)
            if not r:
                return True
            else:
                return False

        def cmp(self, b):
            # for sorting by rank on celldiff
            return (self.CellDiff_ < b.CellDiff_)

    class hkl_symmetry(object):
        """
        #
        #! All required reciprocal-space symmetry stuff
        #
        #  Constructor
        #    hkl_symmetry(const scala::SpaceGroup& SG)
        #    hkl_symmetry(const std::string SpgName);
        #    hkl_symmetry(const int& SpgNumber);
        #    hkl_symmetry(const clipper::Spacegroup& ClpSG);
        #
        #  Reset symmetry
        #    void set_symmetry()
        #
        #  Constructor & set_symmetry setup tables to identify symmetry
        #  elements (N-fold rotations) and list corresponding symmetry
        #  operators
        #  Also sets up the operation corresponding to pairs of ISYM codes
        #  ( ISYM code = SymopNumber*2 + 1(hkl) or +2(-h-k-l))
        #
        #  An n-fold rotation axis corresponds to n operators including I
        #  I is a 1-fold rotation
        #
        #  int get_symelmt(isym1, isym2) returns symmetry element number
        #                               (from 0) of primitive operator
        #                                relating a pair ofobservations
        #                                with isym1 & isym2
        #
        """

        def __init__(self, SpgName):
            self.Nsymp = 0
            self.spgname = SpgName  # name from constructor or xHM name
            self.spaceGroup = cctbx.sgtbx.space_group(self.spgname)  # the actual space group
            self.all_ops = list(self.spaceGroup.all_ops())
            self.SetLatType()
            self.set_symmetry()

        def SetLatType(self):
            #  xHM symbol, colons removed (ie R 3 :H  converted to H 3)
            #  but allow for spacegroup names with ":1" etc in them
            # Take either the first character or the one after a colon
            lattype = 'P'
            aftercolon = ' '
            first = True
            for i in range(len(self.spgname)):
                if first:
                    if self.spgname[i] != ' ':
                        lattype = self.spgname[i]
                        first = False
                if (self.spgname[i] == ':'):
                    aftercolon = self.spgname[i + 1]

            if (self.AllowedLatticeType(aftercolon)):
                lattype = aftercolon

            if (lattype == 'R' or lattype == 'H'):
                # Rhombohedral, check symmetry operators
                zxy = cctbx.sgtbx.rt_mx("z,x,y")
                found = False
                for op in self.all_ops:
                    if op == zxy:
                        found = True
                        break
                if (found):
                    lattype = 'R'
                else:
                    lattype = 'H'

            self.LatType = lattype

        def AllowedLatticeType(self, LatType):
            # //!< Returns true if LatType is allowed

            AllowedTypes = ['P', 'A', 'B', 'C', 'I', 'F', 'R', 'H']
            if LatType in AllowedTypes:
                return True
            else:
                return False

        def Nelement(self):
            return self.Nelement_

        def lattice_type(self):
            return self.LatType

        def NopInElement(self, kelement):
            if (kelement < 0 or kelement > self.Nelement_):
                return -1

            return len(self.elements[kelement].symops)

        def SymopInElement(self, lsym, kelement):
            rot = np.array(list(self.all_ops[self.elements[kelement].symops[lsym]].r().as_double())).reshape((3, 3))
            return self.InvRotSymop(rot)

        def InvRotSymop(self, rot):
            return np.linalg.inv(rot)

        def set_symmetry(self):
            Nsymp = self.num_primops()
            symelmt = []
            self.elements = []

            # Extract lattice centre symbol
            LatType = self.LatType
            cryssys = str(self.spaceGroup.crystal_system()).upper()

            # TODO: should I also check for the self.spaceGroup.is_origin_centric(
            centro = self.spaceGroup.is_centric()
            chiral = self.spaceGroup.is_chiral()

            # Generate list of symmetry elements and associated symops
            # Generate list of elements by repeatedly applying each operator to itself
            for i in range(Nsymp):
                rot1 = self.InvRotSymop(np.array(list(self.all_ops[i].r().as_double())).reshape((3, 3)))
                rot = rot1.copy()
                if i == 0:
                    if not np.allclose(rot1, np.eye(3)):
                        # First symmetry operator is not identity, fatal
                        raise ("hkl_symmetry: Illegal spacegroup, identity not first")
                # Start new group
                k = 1
                found = False
                elmt = SymElement()
                # First symop of element, store
                elmt.symops.append(i)

                if i == 0:
                    # 1st operator = I
                    found = True
                else:
                    while 1:
                        k += 1
                        # NOTE: #In the original code is the multiplications of
                        # two clipper::Symops although these objects contain also
                        # a translation vector, this is always [0,0,0] as defined in
                        # the init() method of the class Spacegroup in hkl_symmetry.cpp
                        # Thus everything is equivalent to the matrix multiplication below
                        rot = np.dot(rot, rot1)
                        if np.allclose(rot, np.eye(3)):
                            # Found k-fold rotation
                            found = True
                            break
                    # find which symmetry operator this corresponds to
                    found = False
                    for j in range(Nsymp):
                        rot3 = self.InvRotSymop(np.array(list(self.all_ops[j].r().as_double())).reshape((3, 3)))
                        if j != i and np.allclose(rot, rot3):
                            elmt.symops.append(j)
                            found = True
                            break
                    if not found:
                        # Incomplete symmetry element, already got this one
                        print("Incomplete symmetry element, already got this one")
                        break

                if found:
                    # rot**k is identity, ie k-fold rotation axis
                    # Store symmetry element incuding identity (k=1)
                    elmt.Nfold = k
                    # Have we already go this one?
                    known = False
                    if len(self.elements) > 0:
                        for j in range(len(self.elements)):
                            if self.ElementEqual(elmt, self.elements[j]):
                                known = True
                                break
                    if not known:
                        elmt.iaxis = self.AxisDirection(elmt)
                        self.elements.append(elmt)

            # Sort symmetry elements by rotation order
            # NOTE: This automatically trigger the __cmp__ method of
            # to get the SymElement class
            self.elements = sorted(self.elements, key=lambda x: x)

            self.Nelement_ = len(self.elements)

            """
            // Make list of which symmetry element each symop belongs to
            // Accept only the first occurance, so that operators which
            // belong to more than one element belong only to the lower
            // rotation order
            // ie from these indices eg a 4-fold element will not contain
            // the 2-fold operator
            // Delete occurances after first from element list
            """

            element_index = [-1] * Nsymp
            # Loop elements
            for j in range(len(self.elements)):
                ops = []  # for copying symops with some perhaps deleted
                # Loop symops for element j
                for i in range(len(self.elements[j].symops)):
                    k = self.elements[j].symops[i]  # symop number
                    if element_index[k] < 0:
                        element_index[k] = j
                        # save this symop
                        ops.append(self.elements[j].symops[i])
                self.elements[j].symops = ops

            # Check that all symops are assigned to an element
            for i in range(Nsymp):
                if element_index[i] < 0:
                    raise ("hkl_symmetry: symmetry operator not in element")

        def ElementEqual(self, a, b):
            if (a.Nfold != b.Nfold): return False
            if (len(a.symops) != len(b.symops)): return False
            for i in range(len(a.symops)):
                found = False
                for j in range(len(b.symops)):
                    if (a.symops[i] == b.symops[j]):
                        found = True
                        break

                if (not found): return False

            return True

        def AxisDirection(self, elmt):
            # Axis direction for kelement'th symmetry element
            rot = np.array(list(self.all_ops[elmt.symops[0]].r().as_double())).reshape((3, 3))
            RecSymOpM = self.InvRotSymop(rot)
            axis = self.Axis(RecSymOpM)
            # Make integral version of vector
            return [int(x) for x in axis]

        def num_primops(self):
            return len(self.all_ops)

        def Axis(self, R):

            # // Get axis direction for rotation matrix R
            # // Returns normalised axis direction
            # //
            # // Algorithm (from Kevin Cowtan)
            # // If we rotate three non-colinear points (eg axis vector
            # // 100, 010, 001) then the axis is perpendicular to the two
            # // largest difference vectors,
            # // ie to the cross-product with largest magnitude
            # //
            # // For matrix R, get cross-product of all columns of (R - I)
            # // choose the one with the largest magnitude, & normalise
            # //
            # // This works even for rotations in non-orthogonal space, as long
            # // as they are crystallographic (ie non-orthogonality is
            # // preserved by this operator)
            #
            # // The identity matrix
            Imat = np.eye(3)

            # R - I
            RmI = np.zeros((3, 3))
            RmI = R - Imat

            # Extract columns
            cols = RmI.T

            # Cross products for all 3 pairs
            maxv = -1.0
            maxc = 0
            cp = np.zeros((3, 3))
            mag = np.zeros(3)

            for i in range(3):
                j = (i + 1) % 3
                k = (j + 1) % 3

                cp[i] = np.cross(cols[j], cols[k])  # clipper::Vec3<double>::cross(cols[j], cols[k]);
                mag[i] = np.linalg.norm(cp[i])
                if (mag[i] > maxv):
                    # largest cross-product
                    maxv = mag[i]
                    maxc = i

            vec = cp[maxc]
            # TODO: The first time this function is called the element contains
            # the identity operation so the RmI rotation is full 0 and here
            # we have a serious problem in the normalization because we divide by 0
            if np.linalg.norm(vec) > 0:
                dd = 1.0 / np.sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2])
            else:
                dd = 1
            return [x * dd for x in vec]

    class SymElement(object):
        # //! Symmetry element: an N-fold rotation
        def __init__(self):
            self.Nfold = 0  # // order of rotation
            self.iaxis = []  # // rotation axis direction vector
            self.symops = []  # // list of symop numbers representing this element

        def __lt__(self, b):
            r = self.cmp(b)
            if r:
                return True
            else:
                return False

        # def __le__(self, b):
        #     r = self.cmp(b)
        #     if r:
        #         return True
        #     else:
        #         return False

        # def __eq__(self, b):
        #     r = self.cmp(b)
        #     if r:
        #         return True
        #     else:
        #         return False

        # def __ne__(self, b):
        #     r = self.cmp(b)
        #     if r:
        #         return True
        #     else:
        #         return False

        def __gt__(self, b):
            r = self.cmp(b)
            if not r:
                return True
            else:
                return False

        # def __ge__(self, b):
        #     r = self.cmp(b)
        #     if not r:
        #         return True
        #     else:
        #         return False

        def cmp(self, b):
            # Sort on rotation order and axis direction (a,b,c)
            # 1. rotation order
            # 2. principle axis, c < b < a
            # 3. Sum(a+b+c) = 0
            #
            # This has the effect of sorting together trigonal dyads belonging to either
            # 321 or 312 point groups
            # ( this isn't very clever)

            if (self.Nfold != b.Nfold): return self.Nfold < b.Nfold  # different rotation order
            # Along principle axis?
            asum = abs(self.iaxis[0]) + abs(self.iaxis[1]) + abs(self.iaxis[2])
            bsum = abs(b.iaxis[0]) + abs(b.iaxis[1]) + abs(b.iaxis[2])
            ap = (asum == 1)
            bp = (bsum == 1)
            if (ap and not bp): return True  # a principle, b not
            if (not ap and bp): return False  # b principle, a not
            if (ap and bp):
                # // both principle axes, c < b < a
                adx = np.argmax(self.iaxis)
                bdx = np.argmax(b.iaxis)
                return adx > bdx

            # neither
            asum = self.iaxis[0] + self.iaxis[1] + self.iaxis[2]
            bsum = b.iaxis[0] + b.iaxis[1] + b.iaxis[2]
            return asum < bsum

    class ReindexByReference(object):
        """

        """

        def __init__(self, target, logger=None):
            self.data1 = target
            if logger is None:
                logging.basicConfig(format='%(message)s',
                                    datefmt='%H:%M:%S',
                                    level=logging.DEBUG)
                self.logger = logging.getLogger("IC")
            else:
                self.logger = logger

            self.mtz1obj = any_file(self.data1)
            self.millers1 = []
            for ma in self.mtz1obj.file_object.as_miller_arrays():
                if ma.is_xray_data_array():
                    r = ma.d_max_min()[1]
                    self.millers1.append((ma, r, ma.info().labels, ma.info().label_string()))

            self.crystal1 = self.mtz1obj.crystal_symmetry()
            self.unit_cell1 = self.crystal1.unit_cell().parameters()
            self.spg1 = self.crystal1.space_group_info().symbol_and_number()
            self.num1 = self.crystal1.space_group_info().type().number()
            self.laue1 = self.crystal1.space_group().build_derived_laue_group().info().type().lookup_symbol()
            self.num_laue1 = self.crystal1.space_group().build_derived_laue_group().info().type().number()

            self.pic = PointLess_Intensity_Correlation(self.data1, logger=logger)

            self.alt_list = []

        def run(self, reference, thresh=0.80):
            self.data2 = reference
            self.mtz2obj = any_file(self.data2)
            self.millers2 = []
            for ma in self.mtz2obj.file_object.as_miller_arrays():
                if ma.is_xray_data_array():
                    try:
                        r = ma.d_max_min()[1]
                        self.millers2.append((ma, r, ma.info().labels))
                    except:
                        continue
            self.crystal2 = self.mtz2obj.crystal_symmetry()
            self.unit_cell2 = self.crystal2.unit_cell().parameters()
            self.spg2 = self.crystal2.space_group_info().type().lookup_symbol()
            self.num2 = self.crystal2.space_group_info().type().number()
            self.num_laue2 = self.crystal2.space_group().build_derived_laue_group().info().type().number()
            self.laue2 = self.crystal2.space_group().build_derived_laue_group().info().type().lookup_symbol()

         #  self.print_info_data()

            reindexing = self.pic.AlternativeReindexList(self.unit_cell1, self.unit_cell2, self.laue1, self.laue2, self.num1, self.num2, 3.0, True,
                                                         checksymmetry=True)

            # Single alternative is only allowed if it is not the identity
            self.maxcc = -1.0
            if len(reindexing) > 1 or (len(reindexing)==1 and not reindexing[0].IsIdentity()):
                # Yes we have some possible alternative indexing schemes
                ic = Intensity_Correlation(self.data2, list_ops=reindexing, logger=self.logger)
                goodcc,result = ic.compare(self.data1, thresh=thresh)
                if goodcc or self.maxcc == -1.0:
                    self.maxcc = ic.cc
                    self.ope = ic.operation
                    self.logger.info("The deposited data associated to the structure shows a CC of " + str(ic.cc) + " against the given data")
                if goodcc:
                    return True,reindexing
            elif len(reindexing) == 0:
                self.logger.info("No alternative indexing possible")
            else:
                self.logger.info("**** Incompatible cells ****")

            return False,reindexing

        def print_info_data(self):
            self.logger.info("Spacegroup: "+str(self.spg1))
            self.logger.info("Unit cell: "+str(cctbx.uctbx.unit_cell(self.unit_cell1)))
            self.logger.info("Laue: "+str(self.laue1)+" Num: "+str(self.num_laue1))

            if hasattr(self, "data2"):
                self.logger.info("Spacegroup: "+str(self.spg2))
                self.logger.info("Unit cell: "+str(cctbx.uctbx.unit_cell(self.unit_cell2)))
                self.logger.info("Laue: "+str(self.laue2)+" Num: "+str(self.num_laue2))


    class Intensity_Correlation(object):
        """
        An Intensity correlation is a task that takes as input a dataset and

        Attributes:
            :param data1:
            :type data1:
            :param mtz1obj:
            :type mtz1obj:
            :param millers1:
            :type millers1:
            :param crystal1:

            :param unit_cell1:

            :param spg1:

            :param num1:

            :param laue1:

            :param num_laue1:


        """
        def __init__(self, data1, list_ops=None, logger=None):
            self.high_resolution = 3.0
            self.data1 = data1
            if logger is None:
                logging.basicConfig(format='%(message)s',
                                    datefmt='%H:%M:%S',
                                    level=logging.DEBUG)
                self.logger = logging.getLogger("IC")
            else:
                self.logger = logger

            self.mtz1obj = any_file(self.data1)
            self.crystal1 = self.mtz1obj.crystal_symmetry()


            #AJM TODO
            #if the reference is a pdb file then there is NO REINDEXING
            self.niggli = None
            if self.mtz1obj.file_type == 'pdb':
                phil2 = mmtbx.programs.fmodel.master_phil
                xray_structure = self.mtz1obj.file_object.xray_structure_simple()
                self.crystal1 = self.mtz1obj.file_object.crystal_symmetry()
                params2 = phil2.extract()
                params2.high_resolution = self.high_resolution
                params2.low_resolution = 100
                params2.output.type = "real"
                f_model = mmtbx.utils.fmodel_from_xray_structure(
                    xray_structure = xray_structure,
                    f_obs          = None,
                    add_sigmas     = False,
                    params         = params2)
                mtz_dataset = f_model.f_model.as_mtz_dataset(column_root_label="FMODEL")
             #  array = f_model.f_model.as_amplitude_array()
                miller_arrays = mtz_dataset.mtz_object().as_miller_arrays()
            else:
                #have to move reference to niggli cell for doing the cc check
                miller_arrays = self.mtz1obj.file_object.as_miller_arrays()
             #  ncop = reindex.reindexing_operators(self.crystal1,self.crystal1.niggli_cell())
                mtz_dataset = None
                for ma in miller_arrays:
                  if ma.is_xray_data_array():
                    try:
                      cb_op = ma.change_of_basis_op_to_niggli_cell()
                      label = str(ma.info().labels[0])
                      if len(label) > 3:
                        if label[-3:] == "(+)":
                          label = label[:-3]
                      maxlabwidth = 20
                      if len(label) > maxlabwidth:
                        label = label[:maxlabwidth] # maximum is 30, will throw error on add_miller_array
                      ma=ma.average_bijvoet_mates()
                      if not mtz_dataset:
                        mtz_dataset = ma.as_mtz_dataset(column_root_label=label)
                      else:
                        #2evr fails passing just ma to add_miller_array
                        mtz_dataset.add_miller_array(miller_array=ma, column_root_label=label)
                    except Exception as e:
                      message = "Cannot add miller array to reference " + str(label)
                      self.logger.info(message)
                      self.logger.error(e)
                    # raise Exception(message)
                      continue
                mtz_object = mtz_dataset.mtz_object()
            ##  mtz_object.change_basis_in_place(cb_op=cb_op) ##
                miller_arrays = mtz_object.as_miller_arrays()
                self.crystal1 = self.crystal1.niggli_cell()
            #   mtz_object.show_summary()
            #   print(cb_op.as_hkl())

            self.millers1 = []
            for ma in miller_arrays:
                if ma.is_xray_data_array():
                    r = ma.d_max_min()[1]
                    self.millers1.append((ma, r, ma.info().labels, ma.info().label_string()))

            self.crystal1 = self.mtz1obj.crystal_symmetry()#2qcw requires
            self.unit_cell1 = self.crystal1.unit_cell().parameters()
            self.spg1 = self.crystal1.space_group_info().symbol_and_number()
            self.num1 = self.crystal1.space_group_number()
            self.laue1 = self.crystal1.space_group().build_derived_laue_group().info().type().lookup_symbol()
            self.num_laue1 = self.crystal1.space_group().build_derived_laue_group().info().type().number()

            self.pic = PointLess_Intensity_Correlation(self.data1, logger=logger)
            self.alt_list = []
            self.outxt = []
            maxres = self.high_resolution # mi1[1]
            maxdelta = 3.0
            if list_ops is None or len(list_ops) == 0:
                for mi1 in self.millers1:
                    self.pic.findIndexingAlternatives(self.unit_cell1, self.laue1, self.num_laue1, maxdelta, True,
                                                                    maxres, print_all=True, pad="  ")
                    if (self.pic.has_reindexing):
                      self.outxt.append("Space Group has alternative indexing possibilities")
                    else:
                      self.outxt.append("Space Group does not have alternative indexing possibilities")
                    break

            #reference set is a loop over datasets and a loop over settings
            self.outxt.append("CDM Cell Distance Metric in Angstroms")
            self.outxt.append("*   Cells different with respect to maximum resolution " + str("{:.3}".format(maxres)))
            if list_ops is None or len(list_ops) == 0:
                k = 1
                for mi1 in self.millers1:
                   self.outxt.append("Labels: #%-3d=%s Resolution: %5.2f" % (k,mi1[2],mi1[1]))
                   k = k+1
            self.outxt.append("%6s %-17s %7s %1s %-37s %s" % (
                            "Labels",
                            "RotationGroup",
                            "CDM(A)",
                            "*",
                            "Cell",
                            "Reindex"))

            if list_ops is None or len(list_ops) == 0:
                k = 1
                for mi1 in self.millers1:
                    reindex_ops,toutxt = self.pic.findIndexingAlternatives(self.unit_cell1, self.laue1, self.num_laue1, maxdelta, True,
                                                                    maxres, print_all=True, pad="  ")
    #self.outxt.append("CDM Cell Distance Metric in Angstroms")
                    for txt in toutxt: #else append nested array
                      self.outxt.append("#%-5d %s" % (k,txt)) #prepend to rest of line
                    self.alt_list.append(reindex_ops)
                    k=k+1
            else:
                for i, mi1 in enumerate(self.millers1):
                    reindex_ops = self.pic.get_reindex_ops(list_ops, self.unit_cell1, maxres, verbose=True if i==0 else False)
                    self.alt_list.append(reindex_ops)

        def print_info_data(self):
            self.logger.info("1 Spacegroup: "+str(self.spg1))
            self.logger.info("1 Unit cell: "+str(cctbx.uctbx.unit_cell(self.unit_cell1)))
            self.logger.info("1 Laue: "+str(self.laue1)+" Num: "+str(self.num_laue1))

            if hasattr(self, "data2"):
                self.logger.info("2 Spacegroup: "+str(self.spg2))
                self.logger.info("2 Unit cell: "+str(cctbx.uctbx.unit_cell(self.unit_cell2)))
                self.logger.info("2 Laue: "+str(self.laue2)+" Num: "+str(self.num_laue2))

        def compare(self, data2, thresh,verbose=False):
            self.logger.setLevel(logging.ERROR) # turn off output
          # self.logger.setLevel(logging.INFO) # turn on output
            self.data2 = data2
            self.mtz2obj = any_file(str(self.data2))
            self.millers2 = []
            self.niggli2 = None
            self.logger.info("input file type  "+self.mtz2obj.file_type)
            if self.mtz2obj.file_type != 'hkl':
                try:
                   #str around self.data2 is essential or else 144d.cif is read as ncs type (!)
                   self.mtz2obj = any_file(str(self.data2),raise_sorry_if_errors=True)
                except Exception as e:
                   message = "Cannot read coordinate file"
                   self.logger.info(message)
                   self.logger.error(e)
                   raise Exception(message)
                phil2 = mmtbx.programs.fmodel.master_phil
                xray_structure = self.mtz2obj.file_object.xray_structure_simple()
                self.crystal2 = self.mtz2obj.file_object.crystal_symmetry()
                params2 = phil2.extract()
                params2.high_resolution = self.high_resolution
                params2.low_resolution = 100
                params2.output.type = "real"
                f_model = mmtbx.utils.fmodel_from_xray_structure(
                    xray_structure = xray_structure,
                    f_obs          = None,
                    add_sigmas     = False,
                    params         = params2)
                mtz_dataset = f_model.f_model.as_mtz_dataset(column_root_label="FMODEL")
            #   array = f_model.f_model.as_amplitude_array()
                miller_arrays = mtz_dataset.mtz_object().as_miller_arrays()
                for ma in miller_arrays:
                  self.niggli2 = ma.change_of_basis_op_to_niggli_cell()
            else:
                self.crystal2 = self.mtz2obj.crystal_symmetry()
                #may be problem with space group
                miller_arrays = self.mtz2obj.file_object.as_miller_arrays(crystal_symmetry=self.crystal2)
             #  ncop = reindex.reindexing_operators(self.crystal2,self.crystal2.niggli_cell())
                mtz_dataset = None
                try:
                  k = 1
                  for ma in miller_arrays:
                    if ma.is_xray_data_array():
                      try:
                        labels = ma.info().labels
                        label = labels[0]
                        if len(labels) > 1 and  labels[1].startswith("_refln."):
                          label = labels[1]
                        maxlabwidth = 20
                        if len(label) > maxlabwidth:
                          label = label[:maxlabwidth] # maximum is 30, will throw error on add_miller_array
                        if len(labels) > 1 and  labels[1].startswith("_refln."):
                          label = label+str(k)
                          k=k+1
                        self.logger.info("labels: "+str(labels))
                        ma=ma.average_bijvoet_mates()
                        cb_op = ma.change_of_basis_op_to_niggli_cell()
                        if not mtz_dataset:
                          mtz_dataset = ma.as_mtz_dataset(column_root_label=label)
                        else:
                          mtz_dataset.add_miller_array(miller_array=ma,column_root_label=label)
                      except Exception as e:
                        message = "Testing miller array skipped: " + str(labels)
                        self.logger.info(message)
                        self.logger.error(e)
                        raise Exception(message)
                        continue
                except Exception as e:
                   print(e)

                mtz_object = mtz_dataset.mtz_object()
            ##  mtz_object.change_basis_in_place(cb_op=cb_op) ##
                miller_arrays = mtz_object.as_miller_arrays()
                self.crystal2 = self.crystal2.niggli_cell()
             #  mtz_object.show_summary()
                self.niggli2 = cb_op

            for ma in miller_arrays:
                if ma.is_xray_data_array():
                    try:
                        r = ma.d_max_min()[1]
                        self.millers2.append((ma, r, ma.info().labels, ma.info().label_string()))
                    except:
                        continue
            self.unit_cell2 = self.crystal2.unit_cell().parameters()
            self.cell2 = self.crystal2.unit_cell()
            self.spg2 = self.crystal2.space_group_info().symbol_and_number()
            self.num2 = self.crystal2.space_group_number()
            self.num_laue2 = self.crystal2.space_group().build_derived_laue_group().info().type().number()
            self.laue2 = self.crystal2.space_group().build_derived_laue_group().info().type().lookup_symbol()

            self.print_info_data()

            # IS = []
            # IP = []
         #  self.miller_indexed = None
            self.cc = None
            self.operation = None
            self.crystal_match = None
            self.niggli_match = None
            maxcc = 0
         #  self.all_millers_indexed = []
            result = []
            for i, mi1 in enumerate(self.millers1):
                # if i>0: break
                self.reso = mi1[1]
                self.logger.info("  --------------")
                self.logger.info("  : "+str(mi1[2]))
                self.logger.info("  Resolution: "+str(round(mi1[1],3)))
                m1d = mi1[0].map_to_asu()
                reindex_ops = [ self.alt_list[i][0] ] # just hkl, total hack from previous code
                for aa, op in enumerate(reindex_ops):
                  for t, mi2 in enumerate(self.millers2):
                    try:
                      from phasertng.scripts.jiffy import TextIO
                      outstr = TextIO()
                      self.reference_analyses = pair_analyses.reindexing(
                               mi1[0],
                               mi2[0],
                               file_name=self.data2,
                               out=outstr)
                      lines = outstr.getvalue().split("\n")
                      for line in lines:
                        self.logger.info(str(line.lstrip()))
                      nops = len(self.reference_analyses.nice_cb_ops)
                      self.logger.info("+++++++++")
                      self.logger.info(": "+str(mi2[2]))
                      self.logger.info("Resolution: "+str(round(mi2[1],3)))
                      if len(mi2[2]) == 1:
                        label = str(mi2[2]) #[0] # special case for model
                      else:
                        label = str(mi2[2]) #[1] #[7:] # cif with first column sf
                      self.logger.info("Number of ops " + str(nops))
                      for o in range(nops):
                            # if t>0: break
                            cc = self.reference_analyses.cc_values[o]
                            self.logger.info("k"+str(op["k"]))
                            self.logger.info(str(label))
                            self.logger.info(str(cc))
                         #  self.logger.info(str(selection1.size()))
                            self.logger.info(str(mi2[1]))
                            self.logger.info(str(self.spg2))
                            self.logger.info(str(self.niggli2))
                            self.logger.info(str(op["hkl"]))
                            result.append(
                                {
                                 "k":op["k"],
                                 "data":label, #erase _refl.
                                 "cc":cc,
                                 "nref": self.reference_analyses.set_a.size(), # selection1.size(),
                                 "reso":mi2[1],
                                 "sg":self.spg2,
                                 "niggli":self.niggli2,
                                 "reindex":self.reference_analyses.nice_cb_ops[o].as_hkl()}) #op["hkl"]})
                            self.logger.info("    +++++++++")
                            if cc >= maxcc:
                         #      self.all_millers_indexed.append([ma1_, mi1[1], mi1[2], mi1[3]])
                                self.cc = cc
                          #     self.miller_indexed = ma1_
                                self.operation = self.reference_analyses.nice_cb_ops[o].as_hkl() # op["hkl"]
                                self.crystal_match = self.crystal2
                                self.niggli_match = self.niggli2
                                maxcc = cc
                    except:
                            # import traceback
                            # print(sys.exc_info())
                            # traceback.print_exc(file=sys.stdout)
                            continue
                self.logger.info("  --------------")

            # print(len(set(list(IS[0][0]))-set(list(IS[1][0]))))
            # print(len(set(list(IS[0][1]))-set(list(IS[1][1]))))
            # print(len(IS[0][0]),len(IS[1][0]),len(IS[0][1]),len(IS[1][1]))
            # print("---------------------------")
            # print(len(set(list(IP[0][0])) - set(list(IP[1][0]))))
            # print(len(set(list(IP[0][1])) - set(list(IP[1][1]))))
            # print(len(IP[0][0]), len(IP[1][0]), len(IP[0][1]), len(IP[1][1]))
            # print("((((",all(list(IP[0][0]==IP[1][0])),all(list(IP[0][1]==IP[1][1])),"))))")
            if maxcc < thresh:
                return False, result
            else:
                return True, result

        def match_reflections(self, ma1, ma2):
            I1 = ma1.data()
            H1 = ma1.indices()
            I2 = ma2.data()
            H2 = ma2.indices()

            lookup = {}
            for i, h in enumerate(H1):
                lookup[h] = i
            selection1 = flex.size_t()
            selection2 = flex.size_t()

            for i, h in enumerate(H2):
                if h in lookup:
                    selection1.append(lookup[h])
                    selection2.append(i)

            return selection1, I1, selection2, I2
except:
    pass

if __name__ == '__main__':
    if len(sys.argv) == 3:
        print("TEST FOR THE INTENSITY CORRELATION")
        mtz1 = sys.argv[1]
        mtz2 = sys.argv[2]

        ic = Intensity_Correlation(mtz1)
        ic.compare(mtz2)
    elif len(sys.argv) == 10:
        a = float(sys.argv[1])
        b = float(sys.argv[2])
        c = float(sys.argv[3])
        alpha = float(sys.argv[4])
        beta = float(sys.argv[5])
        gamma = float(sys.argv[6])
        step = float(sys.argv[7])
        trials = int(sys.argv[8])
        name = sys.argv[9]

        UC1 = [a, b, c, alpha, beta, gamma]
        ara = np.zeros((6, 2 * trials))  # celldim x trials
        for i in range(6):
            for j in range(trials):
                UC2 = copy.deepcopy(UC1)
                if i < 3:
                    UC2[i] += step * j
                elif UC2[i] + step * j <= 360.0:
                    UC2[i] += step * j
                else:
                    break
                print(UC1)
                print(UC2)
                print()
                ara[i][j] = np.sqrt(NCDist(g6_form(UC1), g6_form(UC2))) * (np.sqrt(6.0) / np.mean(UC1[:3] + UC2[:3]))

            for j in range(trials):
                UC2 = copy.deepcopy(UC1)
                if UC2[i] - step * j >= 0:
                    UC2[i] -= step * j
                else:
                    break
                print(UC1)
                print(UC2)
                print()
                ara[i][trials + j] = np.sqrt(NCDist(g6_form(UC1), g6_form(UC2))) * (
                            np.sqrt(6.0) / np.mean(UC1[:3] + UC2[:3]))

        np.savetxt(name, ara, delimiter=",")
    elif len(sys.argv) == 4:
        if sys.argv[1] == "all":
            a = {"P 1": "86.167   86.199   86.567  83.87  83.93  60.55",
                 "P 2": "263.100  203.700  100.000  90.00  90.00 107.10",
                 "P 21": "39.100   24.350   67.110  90.00 102.15  90.00", "C 2": "",
                 "P 2 2 2": "72.395  127.071  170.384  90.00  90.00  90.00",
                 "P 2 2 21": "72.628   81.152   35.317  90.00  90.00  90.00",
                 "P 21 21 2": "50.900   67.300   49.600  90.00  90.00  90.00",
                 "P 21 21 21": "72.710   79.760   81.320  90.00  90.00  90.00",
                 "C 2 2 21": "56.300   75.400  181.700  90.00  90.00  90.00",
                 "C 2 2 2": "22.520   59.370   24.350  90.00  90.00  90.00",
                 "F 2 2 2": "69.900   61.410   54.250  90.00  90.00  90.00",
                 "I 2 2 2": "65.200   76.000  148.200  90.00  90.00  90.00",
                 "I 21 21 21": "87.200   93.200  221.700  90.00  90.00  90.00",
                 "P 4": "83.700   83.700   33.100  90.00  90.00  90.00",
                 "P 41": "72.230   73.230   49.790  90.00  90.00  90.00",
                 "P 42": " 126.000  126.000  144.700  90.00  90.00  90.00",
                 "P 43": "73.900   73.900   42.900  90.00  90.00  90.00",
                 "I 4": " 88.513   88.513   79.947  90.00  90.00  90.00",
                 "I 41": "86.109   86.109  153.825  90.00  90.00  90.00",
                 "P 4 2 2": "80.510   80.510   46.770  90.00  90.00  90.00",
                 "P 4 21 2": " 150.400  150.400   43.000  90.00  90.00  90.00",
                 "P 41 2 2": " 93.400   93.400  362.040  90.00  90.00  90.00",
                 "P 41 21 2": "84.040   84.040   58.480  90.00  90.00  90.00",
                 "P 42 2 2": "109.359  109.359  101.499  90.00  90.00  90.00",
                 "P 42 21 2": " 88.730   88.730  102.900  90.00  90.00  90.00",
                 "P 43 2 2": "88.730   88.730  102.900  90.00  90.00  90.00",
                 "P 43 21 2": "30.000   30.000  219.000  90.00  90.00  90.00",
                 "I 4 2 2": "125.000  125.000  105.600  90.00  90.00  90.00",
                 "I 41 2 2": "138.578  138.578  231.389  90.00  90.00  90.00",
                 "P 3": "90.766   90.766   69.501  90.00  90.00 120.00",
                 "P 31": "70.590   70.590  110.090  90.00  90.00 120.00",
                 "P 32": "84.016   84.016   94.476  90.00  90.00 120.00",
                 "R 3": "291.460  291.460  291.460  61.95  61.95  61.95",
                 "P 3 1 2": "98.802   98.802   90.107  90.00  90.00 120.00",
                 "P 3 2 1": "120.294  120.294  142.555  90.00  90.00 120.00",
                 "P 31 1 2": "146.630  146.630  297.220  90.00  90.00 120.00",
                 "P 31 2 1": "86.100   86.100  144.300  90.00  90.00 120.00",
                 "P 32 1 2": "66.300   66.300  101.600  90.00  90.00 120.00",
                 "P 32 2 1": "83.900   83.900  108.100  90.00  90.00 120.00",
                 "R 3 2": " 100.600  100.600  100.600  81.40  81.40  81.40",
                 "P 6": "91.200   91.200   45.870  90.00  90.00 120.00",
                 "P 61": "44.710   44.710   42.400  90.00  90.00 120.00",
                 "P 65": "125.900  125.900  104.100  90.00  90.00 120.00",
                 "P 62": "63.995   63.995  190.474  90.00  90.00 120.00",
                 "P 64": "113.859  113.859  197.540  90.00  90.00 120.00",
                 "P 63": "61.060   61.060  110.570  90.00  90.00 120.00",
                 "P 6 2 2": "208.648  208.648  252.031  90.00  90.00 120.00",
                 "P 61 2 2": "55.660   55.660  194.950  90.00  90.00 120.00",
                 "P 65 2 2": "74.826   74.826  142.747  90.00  90.00 120.00",
                 "P 62 2 2": "44.122   44.122   93.036  90.00  90.00 120.00",
                 "P 64 2 2": "119.401  119.401  114.115  90.00  90.00 120.00",
                 "P 63 2 2": "123.800  123.800   81.560  90.00  90.00 120.00",
                 "P 2 3": "119.600  119.600  119.600  90.00  90.00  90.00",
                 "F 2 3": "126.270  126.270  126.270  90.00  90.00  90.00",
                 "I 2 3": "135.331  135.331  135.331  90.00  90.00  90.00",
                 "P 21 3": "98.400   98.400   98.400  90.00  90.00  90.00",
                 "I 21 3": "144.810  144.810  144.810  90.00  90.00  90.00",
                 "P 4 3 2": "74.910   74.910   74.910  90.00  90.00  90.00",
                 "P 42 3 2": "47.504   47.504   47.504  90.00  90.00  90.00",
                 "F 4 3 2": "183.765  183.765  183.765  90.00  90.00  90.00",
                 "F 41 3 2": "306.160  306.160  306.160  90.00  90.00  90.00",
                 "I 4 3 2": "180.831  180.831  180.831  90.00  90.00  90.00",
                 "P 43 3 2": "259.550  259.550  259.550  90.00  90.00  90.00",
                 "P 41 3 2": "98.281   98.281   98.281  90.00  90.00  90.00",
                 "I 41 3 2": "175.600  175.600  175.600  90.00  90.00  90.00"}

            step = float(sys.argv[2])
            trials = int(sys.argv[3])
            for p, c in a.items():
                if not c: continue
                print("Spacegroup", p, "cell", c)
                UC1 = [float(x) for x in c.split()]
                ara = np.zeros((6, 2 * trials))  # celldim x trials
                for i in range(6):
                    for j in range(trials):
                        UC2 = copy.deepcopy(UC1)
                        if i < 3:
                            UC2[i] += step * j
                        elif UC2[i] + step * j <= 360.0:
                            UC2[i] += step * j
                        else:
                            break
                        # print(UC1)
                        # print(UC2)
                        # print()
                        ara[i][j] = np.sqrt(NCDist(g6_form(UC1), g6_form(UC2))) * (
                                np.sqrt(6.0) / np.mean(UC1[:3] + UC2[:3]))

                    for j in range(trials):
                        UC2 = copy.deepcopy(UC1)
                        if UC2[i] - step * j >= 0:
                            UC2[i] -= step * j
                        else:
                            break
                        # print(UC1)
                        # print(UC2)
                        # print()
                        ara[i][trials + j] = np.sqrt(NCDist(g6_form(UC1), g6_form(UC2))) * (
                                    np.sqrt(6.0) / np.mean(UC1[:3] + UC2[:3]))
                np.savetxt(p.replace(" ", "_") + ".csv", ara, delimiter=",")

# coding: utf-8
# Copyright (c) 2008-2011 Volvox Development Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Author: Konstantin Lepa <konstantin.lepa@gmail.com>

__ALL__ = [ 'colored', 'cprint' ]

VERSION = (1, 1, 0)

ATTRIBUTES = dict(
        list(zip([
            'bold',
            'dark',
            '',
            'underline',
            'blink',
            '',
            'reverse',
            'concealed'
            ],
            list(range(1, 9))
            ))
        )
del ATTRIBUTES['']


HIGHLIGHTS = dict(
        list(zip([
            'on_grey',
            'on_red',
            'on_green',
            'on_yellow',
            'on_blue',
            'on_magenta',
            'on_cyan',
            'on_white'
            ],
            list(range(40, 48))
            ))
        )


COLORS = dict(
        list(zip([
            'grey',
            'red',
            'green',
            'yellow',
            'blue',
            'magenta',
            'cyan',
            'white',
            ],
            list(range(30, 38))
            ))
        )


RESET = '\033[0m'

def colored(text, color=None, on_color=None, attrs=None):
    """Colorize text.

    Available text colors:
        red, green, yellow, blue, magenta, cyan, white.

    Available text highlights:
        on_red, on_green, on_yellow, on_blue, on_magenta, on_cyan, on_white.

    Available attributes:
        bold, dark, underline, blink, reverse, concealed.

    Example:
        colored('Hello, World!', 'red', 'on_grey', ['blue', 'blink'])
        colored('Hello, World!', 'green')
    """
    if os.getenv('ANSI_COLORS_DISABLED') is None:
        fmt_str = '\033[%dm%s'
        if color is not None:
            text = fmt_str % (COLORS[color], text)

        if on_color is not None:
            text = fmt_str % (HIGHLIGHTS[on_color], text)

        if attrs is not None:
            for attr in attrs:
                text = fmt_str % (ATTRIBUTES[attr], text)

        text += RESET
    return text

def cprint(text, color=None, on_color=None, attrs=None, **kwargs):
    """Print colorize text.

    It accepts arguments of print function.
    """

    print((colored(text, color, on_color, attrs)), **kwargs)


def output_tng_phil_to_working_params(basetng):
    output_philstr = basetng.phil_str()
    parsed_philstr = parse(output_philstr, converter_registry=converter_registry)
    working_parameters_output = phasertng.master_phil.fetch(source=parsed_philstr).extract()
    return working_parameters_output
