from __future__ import division, print_function

from libtbx.program_template import ProgramTemplate
from libtbx.utils import Sorry
import os

# =============================================================================
# Program class (location should be src/New_Voyager/programs)
class Program(ProgramTemplate):
  # Class variables are used to define basic program properties

  # Datatypes expected by program
  datatypes = ['sequence','phil', 'model', 'real_map',]
  program_name = 'phenix.voyager.em_placement'

  description = ''' EM Placement:  Dock a model in a cryo EM map with a likelihood target'''

  from New_Voyager.programs.em_placement_phil import em_placement_scope
  master_phil_str = 'em_placement { ' + em_placement_scope + '}'

  def run(self):

    user_phil_str = self.master_phil.format(python_object=self.params).as_str()
    from New_Voyager.scripts.em_placement_script import em_placement_code
    self.result_info = em_placement_code(user_phil_str=user_phil_str)

  def validate(self):

    # Make sure we are at least on the way there

    if len(self.params.em_placement.map_model.half_map) != 2:
      raise Sorry("Please supply two half-maps (total of %s found)" %(
        len(self.params.em_placement.map_model.half_map)))

    if self.params.em_placement.biological_unit.molecule.model_file is None:
      raise Sorry("Please supply a search model")

    if not os.path.isfile(
      self.params.em_placement.biological_unit.molecule.model_file):
      raise Sorry("The search model %s is missing" %(
         self.params.em_placement.biological_unit.molecule.model_file))

    if self.params.em_placement.biological_unit.molecule_fixed.fixed_model_file is not None:
      if not os.path.isfile(
        self.params.em_placement.biological_unit.molecule_fixed.fixed_model_file):
        raise Sorry("The fixed model file %s is missing" %(
          self.params.em_placement.biological_unit.molecule_fixed.fixed_model_file))

    # Make sure half maps have same origin
    origin = None
    ok_file_name = None
    real_maps = []
    real_map_file_names = []
    for real_map_file in self.data_manager.get_real_map_names():
      real_map = self.data_manager.get_real_map(filename = real_map_file)
      if origin is None:
        origin = tuple(real_map.data.origin())
        ok_file_name = real_map_file
        real_maps = [real_map]
        real_map_file_names = [real_map_file]
      elif origin != tuple(real_map.data.origin()):
        raise Sorry("Please supply maps all with the same origin (%s has %s and %s has %s)" %(
         ok_file_name, str(origin), real_map_file, str(tuple(real_map.data.origin()))))
      else:
        real_maps.append(real_map)
        real_map_file_names.append(real_map_file)

    self.mmm = self.data_manager.get_map_model_manager(
         map_files = real_map_file_names,
         from_phil=False,
         guess_files = False,
         ignore_symmetry_conflicts = False,
         wrapping = False)

    # Now check the map_model_manager
    if not self.mmm or \
        (not self.mmm.map_manager_1()) or (not self.mmm.map_manager_2()) :
      raise Sorry("Please supply two half-maps")

    if self.mmm.map_manager_1() and self.mmm.map_manager_2() and (
        list(self.mmm.map_manager_1().map_data().as_1d()) ==
         list(self.mmm.map_manager_2().map_data().as_1d())):
          raise Sorry("Please supply half-maps that are not identical")

    # Make sure we have full-size half maps
    if (not self.mmm.map_manager_1().is_full_size()) or  (
        not self.mmm.map_manager_2().is_full_size()):
      raise Sorry("Please supply half-maps that are full-size, not cut out from a larger box")

    if (self.params.em_placement.map_model.sequence_composition is None) and \
       (self.params.em_placement.map_model.fixed_scattering_ratio is None):
      raise Sorry("Please supply a sequence file")

    if self.params.em_placement.biological_unit.molecule.molecule_name is None:
      raise Sorry("Please supply a name for the search model")


    if self.params.em_placement.biological_unit.molecule.starting_model_vrms is None:
      raise Sorry("Please supply a starting model VRMS")

  def get_results(self):
    # The GUI will use this for displaying any final results that are important
    return self.result_info

# =============================================================================
# Command-line tool (location should be phaser/command_line)
from iotbx.cli_parser import run_program

if (__name__ == '__main__'):
  run_program(Program)

# Some example command-line commands
#   libtbx.python example.py
#   libtbx.python example.py --show-defaults
#   libtbx.python example.py --show-defaults --show-attributes-level=1
#   libtbx.python example.py --citations
#   libtbx.python example.py model_filename=<model file>
#   libtbx.python example.py sequence_filename=<sequence file>

# Arguments should be grouped. The "--" flags should be together.

# The CCTBXParser (iotbx.cli_parser) will automatically show the program name
# based on the dispatcher name and construct appropriate filenames for PHIL
# files. Some default filenames for the output is planned.

# For multiple ensembles in this example, a PHIL file will need to be defined
# to keep models and sequences together. Files (.type=path) in a PHIL file
# will be automatically tracked by the DataManager.

# The CCTBXParse will read and parse PHIL files

# =============================================================================
# for reference documentation keywords
master_phil_str = Program.master_phil_str
