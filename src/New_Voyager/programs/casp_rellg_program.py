from __future__ import division, print_function

from libtbx.program_template import ProgramTemplate
from libtbx.utils import Sorry
from phasertng import Phasertng
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.master_phil_file import *
from phasertng.phil.converter_registry import *
from New_Voyager.scripts.casp_rel_ellg_script import casp_rel_ellg_code
from voyager_phil_keywords import voyager_scope



# =============================================================================
# Program class (location should be src/New_Voyager/programs)
class Program(ProgramTemplate):

  # Class variables are used to define basic program properties
  description = ''' '''

  # Define PHIL scope (this is the voyager scope)
  # Get the masterphil with all the defaults from phasertng
  initparams = build_phil_str(program_name="InputCard")
  assert (len(initparams))
  master_phil_tng = parse(initparams, converter_registry=converter_registry)
  # Now we merge both of them
  merged_phil_str = 'voyager { ' + voyager_scope + initparams + '}'
  master_phil_str = merged_phil_str

  phil_converters = additional_converters

  # Datatypes expected by program
  # Datatypes not listed will not be recognized and show up as unused files
  # The list of available datatypes is in iotbx/data_manager
  # Each datatype is in its own file with some basic functions defined
  datatypes = ['phil']

  # Citations
  # The list of known citations is in libtbx/citations.params
  # Custom citations can be added via the "citations" class variable
  known_article_ids = ['phasertng']

  # The constructor is not needed since all it does is store the DataManager and
  # PHIL scope extract as instance variables, self.data_manager and self.params,
  # respectively. Also, a multi_out instance is created for logging (self.logger)

  def validate(self):
    # The DataManager can do some basic checks
    # More basic checks are planned and they will exist in a central location
    # Also, do checks on the parameters

    # Require at least 1 model (see iotbx/data_manager/model.py for more
    # arguments). The raise_sorry will cause a Sorry to be raised if False
    # Other datatypes have a similar function (has_sequences)
    phil_scope = self.master_phil.format(python_object=self.params)  # converts extract to scope
    phil_str = phil_scope.as_str(expert_level=3, attributes_level=0)  # converts scope to str

  def run(self):
    # Actual code for scientific stuff
    # The Program template is basically a wrapper for handling I/O with the user
    print('Calling the script')
    phil_scope = self.master_phil.format(python_object=self.params)  # converts extract to scope
    phil_str = phil_scope.as_str(expert_level=3, attributes_level=0)  # converts scope to str
    returned_from_script = casp_rel_ellg_code(user_phil_str=phil_str)
    print('\n\n\n I successfully ran the casp_rellg_program and script')


  def get_results(self):
    # The GUI will use this for displaying any final results that are important
    pass

# =============================================================================
# Command-line tool (location should be phaser/command_line)
from iotbx.cli_parser import run_program

if (__name__ == '__main__'):
  run_program(Program)

# Some example command-line commands
#   libtbx.python example.py
#   libtbx.python example.py --show-defaults
#   libtbx.python example.py --show-defaults --show-attributes-level=1
#   libtbx.python example.py --citations
#   libtbx.python example.py model_filename=<model file>
#   libtbx.python example.py sequence_filename=<sequence file>

# Arguments should be grouped. The "--" flags should be together.

# The CCTBXParser (iotbx.cli_parser) will automatically show the program name
# based on the dispatcher name and construct appropriate filenames for PHIL
# files. Some default filenames for the output is planned.

# For multiple ensembles in this example, a PHIL file will need to be defined
# to keep models and sequences together. Files (.type=path) in a PHIL file
# will be automatically tracked by the DataManager.

# The CCTBXParse will read and parse PHIL files
