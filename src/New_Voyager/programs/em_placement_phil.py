em_placement_scope = '''

  remove_phasertng_folder = True
    .type = bool
    .expert_level = 0
    .optional = True
    .short_caption = Removes the phasertng folder with the databases at the end of the run
    .help = Removes the phasertng folder with the databases at the end of the run
  level = process concise *summary logfile verbose testing
    .type = choice(multi=False)
    .expert_level = 0
    .short_caption = verbosity level for output
    .help = Set the verbosity level
    .caption = Configure the verbosity level for all the output

  map_model
  {
    full_map = None
      .type = path
      .help = Optional input full map file for map correlation calculation
      .short_caption = Optional full map
      .style = file_type:ccp4_map input_file
    half_map = None
      .type = path
      .multiple = True
      .help = Input half map files
      .short_caption = Half map
      .style = file_type:ccp4_map input_file
    point_group_symmetry = C1
      .type = str
      .expert_level = 0
      .optional = False
      .short_caption = Schonflies symbol plus order (e.g. D2, C7, I) or helical
      .help = Schonflies symbol plus order (e.g. D2, C7, I) or helical. Default is C1, no symmetry
    best_resolution = None
      .type = float(value_min=0,value_max=100)
      .optional = True
      .style = tng:regression:match
      .expert_level = 0
      .short_caption = Best resolution in map (optional but recommended)
      .help = Best resolution (smallest d-spacing) found in input map.\
              If zero it will be set automatically
    sequence_composition = None
      .expert_level = 0
      .optional = True
      .type = path
      .short_caption = Sequence file for entire reconstruction
      .help = Sequence file for entire EM reconstruction. Ignored if fixed_scattering_ratio is set, in which case total composition is set to model composition times fixed_scattering_ratio.
      .style = file_type:seq input_file bold
    fixed_scattering_ratio = None
      .expert_level = 1
      .optional = True
      .type = float
      .short_caption = Factor to convert model composition to total
      .help = If set, total composition is model composition times this factor

    region_approach = *automatic manual rigid_body
      .expert_level = 2
      .type = choice(multi=False)
      .short_caption = Method for specifying search regions
      .help = Method for specifying search regions

    region_for_docking
      .expert_level = 2
      .short_caption = Manual specification for region(s)
      .help = Manual specification for region(s)
      .multiple = True
      {
        name = None
          .type = str
          .expert_level = 0
          .short_caption = A name for the region
          .help = A name for the region
        center
        .multiple = False
        {
          x = None
            .type = float
            .style = tng:regression:match
            .expert_level = 0
            .short_caption = x coord center of sphere for docking search
            .help = x coord center of sphere for docking search
          y = None
            .type = float
            .style = tng:regression:match
            .expert_level = 0
            .short_caption = y coord center of sphere for docking search
            .help = y coord center of sphere for docking search
          z = None
            .type = float
            .style = tng:regression:match
            .expert_level = 0
            .short_caption = z coord center of sphere for docking search
            .help = z coord center of sphere for docking search
        }

        radius = None
          .type = float
          .style = tng:regression:match
          .expert_level = 0
          .short_caption = radius for the sphere for anisotropy correction and docking
          .help = radius for the sphere for anisotropy correction and docking
      }
  }

  docking
  {
    frf_rescoring = True
      .type = bool
      .expert_level = 2
      .optional = True
      .short_caption = use frfr after frf or not
      .help = use frfr after frf or not

    frf_clustering = True
      .type = bool
      .expert_level = 2
      .optional = True
      .short_caption = use frf(r) clustering or not
      .help = use clustering or not for frf (or frfr if frf_rescoring)

    frf_target_ellg = 225.
      .type = float
      .expert_level = 1
      .optional = True
      .short_caption = ideal target eLLG for rotation search
      .help = ideal target eLLG for rotation search with strong signal

    minimum_frf_ellg = 7.5
      .type = float
      .expert_level = 1
      .optional = True
      .short_caption = minimum target eLLG for rotation search
      .help = minimum target eLLG for rotation search: controls sub-volume size

    refine_cell_scale = False
      .type = bool
      .expert_level = 1
      .optional = True
      .short_caption = flag for cell scale refinement
      .help = flag for whether or not to refine cell scale

    top_files = 5
      .type = int(value_min=1)
      .expert_level = 1
      .optional = True
      .short_caption = number of top files to keep
      .help = number of top potential solutions to save for output and focused docking

    sigma_cut = 7.
      .type = float
      .expert_level = 1
      .optional = True
      .short_caption = Z-score cutoff for focused docking
      .help = threshold for carrying out focused docking is top score - sigma_cut*sqrt(top score)

    save_output_folder = True
      .type = bool
      .expert_level = 0
      .optional = True
      .short_caption = save results in output folder
      .help = create output folder to save docked models and statistics

    output_folder = None
      .type = path
      .expert_level = 1
      .optional = True
      .short_caption = path to output folder
      .help = non-default path for output folder

    check_full_map = False
      .type = bool
      .expert_level = 1
      .optional = True
      .short_caption = check top_files solutions against full map
      .help = check top_files solutions against full map (or average of half-maps if not provided), including symmetry copies

    brute_approach = *search six_dimensional_full six_dimensional_local
      .type = choice(multi=False)
      .optional = True
      .expert_level = 1
      .short_caption = search strategy (frf or brute)
      .help = search strategy (frf or brute)

    angle_around
      .multiple = False
      {
        alpha = None
          .type = float
          .style = tng:regression:match
          .expert_level = 1
          .short_caption = alpha for brf around angle
          .help = alpha for brute rotation function angle
        beta = None
          .type = float
          .style = tng:regression:match
          .expert_level = 1
          .short_caption = beta for brf around angle
          .help = beta for brute rotation function angle
        gamma = None
          .type = float
          .style = tng:regression:match
          .expert_level = 1
          .short_caption = gamma for brf around angle
          .help = gamma for brute rotation function angle

        range = None
          .type = float
          .style = tng:regression:match
          .expert_level = 1
          .short_caption = angular range for brf around
          .help = angular range for brute rotation function
      }
  }
  biological_unit
  {
    molecule
    {
      molecule_name = None
              .type = str
              .expert_level = 0
              .optional = False
              .short_caption = Model name
              .help = A name for the search model
              .style = menu_item auto_align
      model_file = None
              .expert_level = 0
              .optional = False
              .type = path
              .short_caption = Model
              .help = Coordinate in .pdb or .cif
              .style = file_type:pdb input_file
      starting_model_vrms = 1.2
              .type = float
              .optional = False
              .style = menu_item auto_align tng:regression:match
              .expert_level = 0
              .short_caption = VRMS estimation
              .help = Starting RMSD estimate for the model
    }
    molecule_fixed
        {
          fixed_molecule_name = "fixed"
            .type = str
            .expert_level = 0
            .optional = True
            .short_caption = Fixed model name
            .help = A name for the fixed model
            .style = menu_item auto_align
          fixed_model_file = None
            .expert_level = 0
            .optional = True
            .type = path
            .short_caption = Fixed model
            .help = Fixed model coordinates in .pdb or .cif
            .style = file_type:pdb input_file
        }
  }

  gui
    .help = "GUI-specific parameter required for output directory"
  {
    output_dir = None
    .type = path
    .style = output_dir
  }

  job_title = None
    .type = str
    .input_size = 400
    .help = Job title in PHENIX GUI, not used on command line
    .style = noauto bold

  '''
