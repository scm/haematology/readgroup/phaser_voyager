from __future__ import absolute_import, division, print_function
from libtbx.program_template import ProgramTemplate
# Set up to import voyager functions
import os,sys
module_path = os.path.dirname(os.path.abspath(__file__))
new_voyager = os.path.join(module_path, "../../New_Voyager/")
sys.path.append(new_voyager)
from libtbx import group_args
import json

master_phil_str = '''
model_file = None
  .type = str
  .help = Search model file
fixed_model_file = None
  .type = str
  .help = Fixed model file
sequence_composition = None
  .type = str
  .help = Optional sequence file with composition for full reconstruction, overrides fixed_scattering_ratio
  .short_caption = Optional sequence file with composition for full reconstruction
map = None
  .type = str
  .help = Map file for full map (optional, not used if half-maps provided)
  .short_caption = Map file for full map
map1 = None
  .type = str
  .help = Map file for half-map 1 (optional if full map is provided)
  .short_caption = Map file for half-map 1
map2 = None
  .type = str
  .help = Map file for half-map 2 (optional if full map is provided)
  .short_caption = Map file for half-map 2
d_min = None
  .type = float(value_min=0.)
  .help = Nominal local resolution. Preferred but optional if half-maps provided, required otherwise
  .short_caption = Nominal local resolution
sphere_center = None
  .type = floats(size=3)
  .help = Centre of sphere for docking
  .short_caption = Centre of sphere for docking
model_vrms = 1.5
  .type = float(value_min=0.1)
  .help = estimated RMSD for model
  .short_caption = estimated RMSD for model
fixed_scattering_ratio = 1.5
  .type = float(value_min=1.1)
  .help = Estimated scattering ratio between sphere and model
  .short_caption = Estimated scattering ratio between sphere and model
brute_rotation = False
  .type = bool
  .help = Test all orientations instead of fast rotation function
  .short_caption = Systematic orientation search
refine_scale = False
  .type = bool
  .help = Turn on cell_scale refinement
  .short_caption = Refine cell_scale
level = *process concise summary logfile verbose testing
  .type = choice(multi=False)
  .help = Output level from process (minimum output) to testing
  .short_caption = Output verbosity level
top_files = 5
  .type = int(value_min=1)
  .help = Maximum number of top docking solutions to save and report
  .short_caption = Maximum number of docking solutions saved
output
{
  fileroot = top_model
    .alias = filename root
    .type = str
    .help = Manually set file root
}
'''
# =============================================================================

# Program class (location should be src/New_Voyager/programs)
class Program(ProgramTemplate):

  # Class variables are used to define basic program properties
  description = """
  Program to dock a model within a sphere cut out from input maps or half-maps
  Usage: phenix.voyager.emplace_local map1=halfmap1.map map2=halfmap2.map
              model_file=model.pdb sphere_center=150,140,160 d_min=3.5
  Note: d_min is optional if half-maps are given

  """

  # Datatypes expected by program
  # Datatypes not listed will not be recognized and show up as unused files
  # The list of available datatypes is in iotbx/data_manager
  # Each datatype is in its own file with some basic functions defined
  datatypes = ['phil']

  # Input parameters
  # Define PHIL scope
  master_phil_str = master_phil_str

  # phil_converters = additional_converters

  # Citations
  # The list of known citations is in libtbx/citations.params
  # Custom citations can be added via the "citations" class variable
  known_article_ids = ['emplacement_theory','emplacement_program']

  def validate(self):
    vparams = self.params
    assert (vparams.map is not None or
           (vparams.map1 is not None and vparams.map2 is not None))
    if vparams.map is not None:
      assert os.path.exists(vparams.map)
    if vparams.map1 is not None:
      assert os.path.exists(vparams.map1)
      assert os.path.exists(vparams.map2)
    assert os.path.exists(vparams.model_file)
    assert ((vparams.sequence_composition is not None) or
            (vparams.fixed_scattering_ratio is not None))
    vparams.sequence_composition = vparams.sequence_composition
    vparams.fixed_scattering_ratio = vparams.fixed_scattering_ratio
    if vparams.sequence_composition is not None:
      vparams.fixed_scattering_ratio = None

  # Set any defaults
  def set_defaults(self):
    pass

  def run(self):
    from New_Voyager.scripts.em_placement_script import emplace_local
    vparams = self.params
    self.results = emplace_local(
        vparams.map,
        vparams.map1,
        vparams.map2,
        vparams.d_min,
        vparams.model_file,
        vparams.model_vrms,
        vparams.fixed_model_file,
        vparams.sphere_center,
        vparams.top_files,
        vparams.sequence_composition,
        vparams.fixed_scattering_ratio,
        vparams.brute_rotation,
        vparams.refine_scale,
        vparams.level)
    self.data_manager.set_overwrite(True)
    from iotbx.data_manager import DataManager
    dm = DataManager()
    dm.set_overwrite(True)
    nsolfound = self.results.nsolfound
    print("\nNumber of solutions found: ",nsolfound, file=self.logger)
    for isol in range(nsolfound):
      output_model_file = vparams.output.fileroot + "_" + str(isol+1) + ".pdb"
      dm.write_model_file(self.results.top_models[isol],output_model_file)
      output_map_file = vparams.output.fileroot + "_" + str(isol+1) + ".map"
      self.results.top_models_map[isol].write_map(output_map_file)
      print("Model #",isol+1," written to: ",output_model_file, file=self.logger)
      print("   corresponding map to: ",output_map_file, file=self.logger)
      print("   mapLLG = ",self.results.top_models_mapLLG[isol], file=self.logger)
      print("   mapCC  = ",self.results.top_models_mapCC[isol], file=self.logger)
      if vparams.refine_scale:
        print("   cell_scale = ",self.results.top_models_cell_scale[isol],
              file=self.logger)

  def get_results(self):
    return (self.results)

  def get_results_as_JSON(self):
    # Regenerate lists of filenames for docked models and corresponding maps
    # written out by run method.
    # For backwards compatibility, also output filenames for just top model and
    # top map.
    nsolfound = self.results.nsolfound
    model_filenames = []
    map_filenames = []
    for isol in range(nsolfound):
      output_model_file = self.params.output.fileroot + "_" + str(isol+1) + ".pdb"
      output_map_file   = self.params.output.fileroot + "_" + str(isol+1) + ".map"
      model_filenames.append(output_model_file)
      map_filenames.append(output_map_file)
    result_dict = {
      'model_filename': model_filenames[0],
      'map_filename': map_filenames[0],
      'model_filenames': model_filenames,
      'map_filenames': map_filenames,
      'n_solutions': nsolfound,
      'mapLLG': self.results.top_models_mapLLG,
      'mapCC': self.results.top_models_mapCC,
      'RT': self.results.top_models_RT,
    }
    return json.dumps(result_dict, indent=2)
