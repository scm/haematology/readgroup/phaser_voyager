voyager_scope = '''

  remove_phasertng_folder = True
    .type = bool
    .expert_level = 0
    .optional = True
    .short_caption = Removes the phasertng folder with the databases at the end of the run
    .help = Removes the phasertng folder with the databases at the end of the run

  janeway
   .multiple = False
    {
      post_mortem
      {
        target_structure = None
          .type = path
          .expert_level = 0
          .short_caption = Path to the target solved structure
          .help = Path to the target solved structure
        stop_if_cc_greater_than = 0.3
          .type = float(value_min=0.01)
          .expert_level = 0
          .short_caption = Minimum CC to target
          .help =Minimum CC of the found solution vs the target structure to stop the execution
      }
      ending_criteria
      {
        minimum_llg = 100
          .type = float(value_min=1.0)
          .expert_level = 0
          .short_caption = Minimum LLG to stop
          .help = Minimum LLG to stop execution if not exhaustive
        minimum_zscore = 8
          .type = float(value_min=1.0)
          .expert_level = 0
          .short_caption = Minimum ZSCORE to stop
          .help = Minimum ZSCORE to stop execution if not exhaustive
        exhaustive = True
            .type = bool
            .expert_level = 0
            .optional = True
            .short_caption = Explore all the viable parameterization and open branches before ending
            .help = Explore all the viable parameterization and open branches before ending
      }
    }

  server_pdb = pdb-redo rcsb *pdbe
      .type = choice(multi=False)
      .expert_level = 1
      .optional = True
      .short_caption = Mirror PDB
      .help = Mirror to the PDB server

  local_pdb = None
      .type = path
      .expert_level = 1
      .optional = True
      .short_caption = Path to a local directory with pdbs
      .help = A local directory path to a copy of the PDB.

  level = process concise *summary logfile verbose testing
    .type = choice(multi=False)
    .expert_level = 0
    .short_caption = verbosity level applied to both phasertng and phaservoyager
    .help = Set up the verbosity level
    .caption = Configure the verbosity level for all the output in stdout
    !.style = bold phaser:ignore OnChange:update_phaser_mode renderer:draw_phaser_mode_widget
    !.style = tng:Suite:Level(CCP4:logfile,PHENIX:logfile,VOYAGER:logfile,VALIDATE:logfile)

  perform_domain_analysis = False
      .type = bool
      .expert_level = 0
      .optional = True
      .short_caption = Skip or perform the domain analysis
      .help = Control the activation of the domain analysis

  reflections = None
    .expert_level = 0
    .type = path
    .short_caption = X-ray data
    .help = X-ray data in .cif or .mtz format

  docking
  {
    frf_rescoring = True
      .type = bool
      .expert_level = 0
      .optional = True
      .short_caption = use frfr after frf or not
      .help = use frfr after frf or not

    frf_clustering = True
      .type = bool
      .expert_level = 0
      .optional = True
      .short_caption = use frf(r) clustering or not
      .help = use clustering or not for frf (or frfr if frf_rescoring)

    frf_target_ellg = 225.
      .type = float
      .expert_level = 0
      .optional = True
      .short_caption = ideal target eLLG for rotation search
      .help = ideal target eLLG for rotation search with strong signal

    minimum_frf_ellg = 7.5
      .type = float
      .expert_level = 0
      .optional = True
      .short_caption = minimum target eLLG for rotation search
      .help = minimum target eLLG for rotation search: controls sub-volume size

    refine_cell_scale = False
      .type = bool
      .expert_level = 1
      .optional = True
      .short_caption = flag for cell scale refinement
      .help = flag for whether or not to refine cell scale

    top_files = 5
      .type = int(value_min=1)
      .expert_level = 1
      .optional = True
      .short_caption = number of top files to keep
      .help = number of top potential solutions to save for output and focused docking

    sigma_cut = 7.
      .type = float
      .expert_level = 1
      .optional = True
      .short_caption = Z-score cutoff for focused docking
      .help = threshold for carrying out focused docking is top score - sigma_cut*sqrt(top score)

    save_output_folder = True
      .type = bool
      .expert_level = 0
      .optional = True
      .short_caption = save results in output folder
      .help = create output folder to save docked models and statistics

    output_folder = None
      .type = path
      .expert_level = 1
      .optional = True
      .short_caption = path to output folder
      .help = non-default path for output folder

    check_full_map = False
      .type = bool
      .expert_level = 0
      .optional = True
      .short_caption = check top_files solutions against full map
      .help = check top_files solutions against full map (or average of half-maps if not provided), including symmetry copies

    brute_approach = *search six_dimensional_full six_dimensional_local
      .type = choice(multi=False)
      .optional = True
      .expert_level = 0
      .short_caption = search strategy (frf or brute)
      .help = search strategy (frf or brute)

    angle_around
      .multiple = False
      {
        alpha = None
          .type = float
          .style = tng:regression:match
          .expert_level = 0
          .short_caption = alpha for brf around angle
          .help = alpha for brute rotation function angle
        beta = None
          .type = float
          .style = tng:regression:match
          .expert_level = 0
          .short_caption = beta for brf around angle
          .help = beta for brute rotation function angle
        gamma = None
          .type = float
          .style = tng:regression:match
          .expert_level = 0
          .short_caption = gamma for brf around angle
          .help = gamma for brute rotation function angle

        range = None
          .type = float
          .style = tng:regression:match
          .expert_level = 0
          .short_caption = angular range for brf around
          .help = angular range for brute rotation function
      }
  }

  map_model
  {
    full_map = None
      .type = path
      .help = Optional input full map file for local docking (not recommended) or map correlation calculation
      .short_caption = Optional full map
      .style = file_type:ccp4_map input_file
    half_map = None
      .type = path
      .multiple = True
      .help = Input half map files (optional but recommended for local docking if full map provided)
      .short_caption = Half map
      .style = file_type:ccp4_map input_file
    point_group_symmetry = C1
      .type = str
      .expert_level = 0
      .optional = False
      .short_caption = Schonflies symbol plus order (e.g. D2, C7, I) or helical
      .help = Schonflies symbol plus order (e.g. D2, C7, I) or helical. Default is C1, no symmetry
    best_resolution = None
      .type = float(value_min=0,value_max=100)
      .optional = True
      .style = tng:regression:match
      .expert_level = 0
      .short_caption = Best resolution in map (optional but recommended)
      .help = Best resolution (smallest d-spacing) found in input map.\
                If zero it will be set automatically
    sequence_composition = None
      .expert_level = 0
      .optional = True
      .type = path
      .short_caption = Sequence file for entire reconstruction
      .help = Sequence file for entire EM reconstruction. Ignored if fixed_scattering_ratio is set, in which case total composition is set to model composition times fixed_scattering_ratio.
    fixed_scattering_ratio = None
      .expert_level = 1
      .optional = True
      .type = float
      .short_caption = Factor to convert model composition to total
      .help = If set, total composition is model composition times this factor

    region_approach = *automatic manual rigid_body
      .expert_level = 2
      .type = choice(multi=False)
      .short_caption = Method for specifying search regions
      .help = Method for specifying search regions

    region_for_docking
      .expert_level = 2
      .short_caption = Manual specification for region(s)
      .help = Manual specification for region(s)
      .multiple = True
      {
        name = None
          .type = str
          .expert_level = 0
          .short_caption = A name for the region
          .help = A name for the region
        center
        .multiple = False
        {
          x = None
            .type = float
            .style = tng:regression:match
            .expert_level = 0
            .short_caption = x coord center of sphere for docking search
            .help = x coord center of sphere for docking search
          y = None
            .type = float
            .style = tng:regression:match
            .expert_level = 0
            .short_caption = y coord center of sphere for docking search
            .help = y coord center of sphere for docking search
          z = None
            .type = float
            .style = tng:regression:match
            .expert_level = 0
            .short_caption = z coord center of sphere for docking search
            .help = z coord center of sphere for docking search
        }

        radius = None
          .type = float
          .style = tng:regression:match
          .expert_level = 0
          .short_caption = radius for the sphere for anisotropy correction and docking
          .help = radius for the sphere for anisotropy correction and docking
      }
  }

  biological_unit
    {
      molecule
          {
            molecule_name = None
              .type = str
              .expert_level = 0
              .optional = False
              .short_caption = Model name
              .help = A name for the search model
              .style = menu_item auto_align
            model_file = None
              .expert_level = 0
              .type = path
              .short_caption = Model
              .help = Coordinates in .pdb or .cif
              .style = file_type:pdb input_file
            starting_model_vrms = 1.2
              .type = float
              .optional = False
              .style = tng:regression:match
              .expert_level = 0
              .short_caption = VRMS estimate
              .help = Starting RMSD estimate for the model
            alignment_source = *phmmer hhpred directory multiple_aln
              .type = choice(multi=False)
              .expert_level = 0
              .short_caption = "alignments"
              .help = Alignment and models source
              .caption = Define how phaser.voyager should get the alignment and the models for \
                         the corresponding sequence.
            alignment_path = None
              .expert_level = 0
              .type = path
              .short_caption = Path to the alignment file or to the models directory
              .help = Path to the alignment file or to the models directory
          }
      molecule_fixed
          {
            fixed_molecule_name = "fixed"
              .type = str
              .expert_level = 0
              .optional = True
              .short_caption = Fixed model name
              .help = A name for the fixed model
            fixed_model_file = None
              .expert_level = 0
              .type = path
              .short_caption = Fixed model
              .help = Fixed model coordinates in .pdb or .cif
              .style = file_type:pdb input_file
          }
    }
  generate_dataset {
    target_model = None
          .type = path
          .expert_level = 0
          .short_caption = Path to the target structure
          .help = Path to the coordinate model from which to generate an artificial dataset
    resolution_dataset = 2.0
          .type = float(value_min=1.0)
          .expert_level = 0
          .short_caption = Best resolution of the artificially generated dataset
          .help = Best resolution of the artificially generated dataset
    }
  prepare_casp_models_for_evaluation {
    models = None
      .type = path
      .expert_level = 0
      .optional = False
      .short_caption = path to the model or models to evaluate
      .help = path to the model or models to evaluate. Accepts patterns to match.

    superpose_models = False
      .type = bool
      .expert_level = 0
      .optional = True
      .short_caption = if True, models will be superposed to the target
      .help = if True, models will be superposed to the target
    }

  models_evaluation_against_target
    {
      estimated_vrms = 1.25
          .type = float
          .expert_level = 0
          .short_caption = Estimation of vrms
          .help = Estimation of vrms
      bfactor_treatment = *both all as_bfactors as_lddt as_lddt_coarser as_rmsd bfactor_constant
          .type = choice(multi=False)
          .expert_level = 0
          .optional = True
          .short_caption = how to treat bfactors
          .help = test bfactors as both pLDDT and constant, plus coarser pLDDT for all, or consider them just as bfactors, pLDDT, coarse pLDDT, rms errors, or set to constant value
          .caption = Defines how to deal with the bfactor column of the model
      keep_refined_models = False
          .type = bool
          .expert_level = 0
          .optional = True
          .short_caption = Keep or remove the folder with the refined models
          .help = Keep or remove the folder with the refined models
    }
  analysis_casp_evaluation
    {
      output_csv_filename = None
      .type = str
      .expert_level = 0
      .optional = True
      .short_caption = Name for output csv file
      .help = If not indicated, will be constructed using the target and model info
    }

  isostructure_search
    {
      output_isostructure_to_directory = None
        .type = path
        .expert_level = 0
        .short_caption = Write a copy of all the extracted models in the directory specified
        .help = Write a copy of all the extracted models in the directory specified
      reindex_as = None
        .expert_level = 0
        .type = path
        .short_caption = X-ray data
        .help = X-ray data in .cif or .mtz format
      delta_dim = 10
        .type = int(value_min=0)
        .expert_level = 0
        .optional = True
        .short_caption = Angstrom delta
        .help = Angstrom delta to the unit cell A,B,C parameters

      delta_angle = 5
        .type = int(value_min=0)
        .expert_level = 0
        .optional = True
        .short_caption = Degree delta
        .help = Degree delta to the unit cell alpha,beta,gamma parameters
      max_ncdist = 2.5
        .type = float(value_min=0)
        .expert_level = 0
        .optional = True
        .short_caption = threshold distance
        .help = Maximum NCDIST threshold to explore
      top_explore = 10
        .type = int(value_min=0)
        .expert_level = 0
        .optional = True
        .short_caption = Number of top entries
        .help = Number of top entries to be explored against the intensity correlation check.
      min_corr_coeff = 0.60
        .type = float(value_min=0.0, value_max=1.0)
        .expert_level = 0
        .optional = True
        .short_caption = CC
        .help = Intensities Correlation Coefficient of the given dataset vs isostructure entry
      tutorial_mode = False
        .type = bool
        .expert_level = 0
        .optional = True
        .short_caption = tutorial
        .help = Does not create a dag node even if a model is found
    }

  '''
