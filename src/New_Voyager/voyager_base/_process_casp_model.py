###################################################################################################
#                                          LICENSE                                                #
# PhaserVoyager is distributed under three different licences                                     #
#                                                                                                 #
# [Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)                 #
# [CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)                         #
# [Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)                     #
#                                                                                                 #
# By accessing the source code in this repository you agree to be bound by one of these licences. #
#                   (c) 2000-2024 Cambridge University Technical Services Ltd                     #
#                                                                                                 #
###################################################################################################

import sys, traceback

# Our imports
try:
    from iotbx.data_manager import DataManager
    from cctbx.array_family import flex
    from libtbx.phil import parse
    from phasertng.phil.converter_registry import converter_registry
    from phasertng.phil.phil_to_cards import phil_to_cards
    from phasertng.phil.cards_to_phil import cards_to_phil
except:
    print(sys.exc_info())
    traceback.print_exc(file=sys.stdout)
    print('Some error with the voyager and phasertng imports in _process_casp_model')


def check_zero_occupancy(model):
  occupancy_field = model.get_occ()
  occ_min_max_mean = occupancy_field.min_max_mean()
  if occ_min_max_mean.max <= 0.:
    return -6
  else:
    return 0

def convert_lddt_to_bfac(model, min_plddt_allowed = None):
  import mmtbx.process_predicted_model
  import iotbx
  master_phil = iotbx.phil.parse(mmtbx.process_predicted_model.master_phil_str)
  params = master_phil.extract()
  params.process_predicted_model.b_value_field_is = 'plddt'
  params.process_predicted_model.minimum_plddt = min_plddt_allowed
  params.process_predicted_model.maximum_rmsd = None
  params.process_predicted_model.split_model_by_compact_regions = False # we do not want to do this here
  params.process_predicted_model.subtract_minimum_b = True
  if min_plddt_allowed is not None:
    params.process_predicted_model.remove_low_confidence_residues = True
  else:
    params.process_predicted_model.remove_low_confidence_residues = False
  processed_model_info = mmtbx.process_predicted_model.process_predicted_model(model,params)
  processed_model = processed_model_info.model
  return processed_model

def process_casp_model(voyager_base_obj,model_pdb,choice_treatment,basetng,
                       vrms_estimated,original_reflections,saved_cards,working_params):
    # depending on choice_treatment_dict, we will run one of four different treatments
    # of the bfactor column in the pdb given
    # 1) setting them to a common value
    # 2) taking them as they come assuming they are bfactors
    # 3) taking them as they come assuming they are rmsd errors that need to be converted to bfactors
    # 4) taking them as they come assuming they are pLDDT estimates that need to be converted to bfactors

    dm = DataManager()
    dm.set_overwrite(True)
    working_model = dm.get_model(model_pdb)
    error_code = check_zero_occupancy(working_model)
    if error_code:
      # Return zero because model is defective
      return('ERROR', float(0), error_code)

    # Hard-wire minimum pLDDT for now, but this could be a parameter if desired
    min_plddt_allowed = None
    if choice_treatment == 'bfactor_constant':
        path_modelito = model_pdb[:-4] + '_bfact_const.pdb'
        try:
            working_model.set_b_iso(flex.double(working_model.get_b_iso().size(),25.))
            dm.write_model_file(working_model,path_modelito)
        except:
            print("There was a problem when setting bfactors to constant with ", path_modelito)
            print(sys.exc_info())
            traceback.print_exc(file=sys.stdout)
            voyager_base_obj.success = False
        model_pdb = path_modelito

    elif choice_treatment == 'as_bfactors':
        path_modelito = model_pdb[:-4] + '_as_bfactors.pdb'
        dm.write_model_file(working_model,path_modelito) # Write with no change
        model_pdb = path_modelito

    elif choice_treatment == 'as_lddt':
        path_modelito = model_pdb[:-4] + '_as_lddt.pdb'
        # Catch models with constant B-factor field (often all zero)
        b_value_field = working_model.get_b_iso()
        bmin_max_mean = b_value_field.min_max_mean()
        if bmin_max_mean.min == bmin_max_mean.max:
          # No variation, so not suitable for as_lddt evaluation
          error_code = -5
          return('ERROR', '#N/A', error_code)
        if min_plddt_allowed is not None:
          if bmin_max_mean.max < min_plddt_allowed:
            # Values not in range for plausible model, so score as zero
            error_code = -4
            return('ERROR', float(0), error_code)
        try:
          processed_model = convert_lddt_to_bfac(working_model, min_plddt_allowed=min_plddt_allowed)
          dm.write_model_file(processed_model,path_modelito)
          model_pdb = path_modelito
        except:
          # Most likely too few acceptable residues, so score as zero
          # If this happens a lot, consider possible problem with process_predicted_model?
          error_code = -3
          return('ERROR', float(0), error_code)

    elif choice_treatment == 'as_lddt_coarser':
        path_modelito = model_pdb[:-4] + '_as_lddt_coarser.pdb'
        # Replace pLDDT values for each residue with the mean for the residue
        num_finer = 0
        overall_bmax = 0.
        for chain in working_model.chains():
          for residue_group in chain.residue_groups():
            b_value_field = flex.double()
            atoms = residue_group.atoms()
            for atom in atoms:
              b_value_field.append(atom.b)
            bmin_max_mean = b_value_field.min_max_mean()
            overall_bmax = max(overall_bmax, bmin_max_mean.max)
            if bmin_max_mean.min == bmin_max_mean.max:
              # No variation within this residue
              continue
            num_finer += 1
            for atom in atoms:
              atom.b = bmin_max_mean.mean
        # Catch models that already have coarse (per-residue) pLDDT values
        if num_finer == 0:
          error_code = -7
          # Return #N/A because this criterion can't be evaluated for this model
          return('ERROR', '#N/A', error_code)
        if min_plddt_allowed is not None:
          if overall_bmax < min_plddt_allowed:
            error_code = -4
            # pLDDT values are not compatible with a reasonable model
            return('ERROR', float(0), error_code)
        try:
          processed_model = convert_lddt_to_bfac(working_model, min_plddt_allowed=min_plddt_allowed)
          dm.write_model_file(processed_model,path_modelito)
          model_pdb = path_modelito
        except:
          error_code = -3
          # Like as_lddt case
          return('ERROR', float(0), error_code)

    # For the as_rmsd treatment we will directly call phasertng with the appropriate
    # keyword so that it performs the conversion from rmsd to bfactor

    # Make sure proper setup of the python object.
    # working_params.phasertng.macrm.study_parameters = True
    working_params.phasertng.mode = ['esm', 'put', 'rmr', 'rellg']
    working_params.phasertng.suite.write_data = False
    working_params.phasertng.model.filename = model_pdb
    working_params.phasertng.molecular_transform.reorient = False
    working_params.phasertng.molecular_transform.preparation = ['interpolation', 'monostructure']
    working_params.phasertng.model.vrms_estimate = vrms_estimated
    # The following macrocycles are more robust for reLLG calculation than the default
    working_params.phasertng.macrm.macrocycle1 = ['rotn','tran']
    working_params.phasertng.macrm.macrocycle2 = ['vrms']
    working_params.phasertng.macrm.macrocycle3 = ['rotn','tran','vrms']
    if choice_treatment == 'as_rmsd':
        working_params.phasertng.model.convert_rmsd_to_bfac = True
    else:
        working_params.phasertng.model.convert_rmsd_to_bfac = False

    # here is where we need to reset the dag cards and the reflections
    basetng.DAGDATABASE.parse_cards(saved_cards)
    basetng.REFLECTIONS = original_reflections

    # Convert to cards to call tng
    #print('SHERLOCK working_params',working_params)
    #print('SHERLOCK dir(working_params.phasertng)', dir(working_params.phasertng))
    cards = phil_to_cards(user_phil=working_params, program_name="InputCard")
    #print('SHERLOCK cards',cards)

    # Run tng
    basetng.run(cards)

    if basetng.success():  # The model was processed fine
        print('This model ran fine')
        return ('OK', basetng)
    else:  # There was some error
        error_code = -10
        # This may not be the fault of the predictor
        result = '#N/A'
        if 'Model is extremely extended' in basetng.error_message():
            error_code = -1
            result = float(0)
            # This includes extended and those also flagged as random coil. Warning messages differ
            # if we wanted to catch those and generate different error codes
            # Here we give a score of zero because the model is defective
        return ('ERROR', result, error_code)
