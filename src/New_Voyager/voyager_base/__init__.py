# -*- coding: utf-8 -*-
"""
This module contains the voyager class::

###################################################################################################
#                                          LICENSE                                                #
# PhaserVoyager is distributed under three different licences                                     #
#                                                                                                 #
# [Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)                 #
# [CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)                         #
# [Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)                     #
#                                                                                                 #
# By accessing the source code in this repository you agree to be bound by one of these licences. #
#                   (c) 2000-2024 Cambridge University Technical Services Ltd                     #
#                                                                                                 #
###################################################################################################
"""

# Python 2/3 compatibility imports
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from future.standard_library import install_aliases

# System imports
import sys
import os
import glob
import shutil

try:
    import voyager_phil_keywords
except:
    print('There was some error with the voyager imports')


# Logging (not really used in current functionality)
def print_log(*args, **kwargs):
  print(*args, file=sys.stderr, **kwargs)

install_aliases()

__license__ = "(c) 2000-2024 Cambridge University Technical Services Ltd"
__copyright__ = "University of Cambridge"

class VoyagerBase(object):
    """ A meaningful description of the class """


    def __init__(self):
        #print('SHERLOCK the VoyagerBase class was instantiated')
        self.success = True
        self.voyager_phil_str = voyager_phil_keywords.voyager_scope

    # Imported methods
    from ._prepare_casp_models_for_evaluation import prepare_casp_models_for_evaluation
    from ._process_casp_model import process_casp_model

    # static methods need to be set
    # from ._static_example import something
    # something = staticmethod(something)
    #
    # # Some more small functions
    # def printHi(self):
    #     print("Hello world")