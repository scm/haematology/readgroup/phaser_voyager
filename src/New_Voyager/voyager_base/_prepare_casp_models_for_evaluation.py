import os, sys, traceback
import shutil
import glob
from Bio.PDB.PDBIO import PDBIO


# Our imports
try:
    from New_Voyager import pdb_structure
    from New_Voyager import simple_tools
except:
    print(sys.exc_info())
    traceback.print_exc(file=sys.stdout)
    print('There was some error in the voyager or phasertng imports')

def prepare_casp_models_for_evaluation(voyager_base_obj,directory_models, phasertng_dir, target, superpose_models=False):
    # Prepare the list of models to process
    list_models_to_check = []
    if os.path.isfile(directory_models) and directory_models.endswith('.pdb'):
        # print("SHERLOCK single file case")
        list_models_to_check.append(os.path.abspath(directory_models))
    else:
        if os.path.exists(directory_models):
            # print("SHERLOCK normal folder case")
            fulldir = os.path.abspath(directory_models)
            list_models_to_check = [os.path.join(fulldir, f) for f in os.listdir(fulldir) if
                                    not f.startswith('._') and f.endswith('.pdb')]
        else:
            # print("SHERLOCK selection expression case")
            list_models_to_check = [os.path.abspath(f) for f in glob.glob(directory_models)]

    # process reference
    ppp = pdb_structure.PDB(target)
    ref_stru = ppp.get_structure()
    chains_ref_stru = list(ref_stru.get_chains())
    if len(chains_ref_stru) > 1:
        voyager_base_obj.success = False
        raise Exception('This strategy assumes the reference pdb has a single chain')
    else:
        reference_chain_sequence = ppp.sequence
    maximum_extent_reference = ppp.find_extent_x_y_z()

    # now recursively compare with the rest of the models
    # get the folder where we will keep them and process and save that info for future
    path_modified = simple_tools.create_directory_timestamp("casp_prepare_")
    # self.strategy.arguments.voyager.intermediate_results.casp_prepared_files  = path_modified
    list_model_errors = []
    for modelito in list_models_to_check:
        print("\nProcessing model " + modelito)
        basepath_modelito = os.path.basename(modelito)
        path_modelito = os.path.join(path_modified, basepath_modelito)
        shutil.copy(modelito, path_modelito)
        # First try to open the model to be sure format is right. Just a check
        try:
            ppp_modelito = pdb_structure.PDB(path_modelito)
        except:
            print("This pdb model has some formatting issue and can't be interpreted")
            list_model_errors.append(basepath_modelito)
            continue
        # Now, if the superposition keyword was set to True, perform the superposition
        # pdb_structure in the object method superpose_with_alignment(reference, target, target_rmsd=None)
        # rmsd = pdb_structure.superpose_with_alignment(pdbf, model, target_rmsd=1.5)
        if superpose_models:
            rmsd = pdb_structure.superpose_with_alignment(target, path_modelito, no_prior_processing=True,
                                                          permissive_mainchain=True, threshold_return_none=0.01)
            if rmsd != None:
                print("The RMSD between model and target is " + str(rmsd))
            else:
                voyager_base_obj.success = False
                raise Exception("The superposition to the target could not be performed")
        # Finally, check if the model should be trimmed to match the target
        # If we arrive to this point we know already the models do not have format issues
        ppp_modelito = pdb_structure.PDB(path_modelito)  # Open again the model now must be superposed
        comp_stru = ppp_modelito.get_structure()
        chains_comp_stru = list(comp_stru.get_chains())
        if len(chains_comp_stru) > 1:
            print("This strategy assumes the model pdb has a single chain, so this model will be skipped")
            list_model_errors.append(basepath_modelito)
            continue
        else:
            model_chain_sequence = ppp_modelito.sequence
        if len(model_chain_sequence) > len(reference_chain_sequence):
            print('      ' + modelito + " was not trimmed according to target, doing it now")
            area_sequence, area_rangeres, alignment = pdb_structure.get_area_covered(chain_a=reference_chain_sequence,
                                                                                     chain_b=model_chain_sequence,
                                                                                     return_alignment=True)
            count_res = 0
            for model in comp_stru.get_list():
                for chain in model.get_list():
                    for residue in chain.get_list():
                        full_id = residue.get_full_id()
                        if alignment.b[count_res] == '-':
                            mini_id = residue.id
                            chain.detach_child(mini_id)
                        count_res = count_res + 1
            # Now save the edited model
            # Save the model before the next iteration
            io = PDBIO()
            io.set_structure(comp_stru)
            io.save(path_modelito)
        # Perform now a check of the maximum dimensions in x,y,z of the model and compare with the target
        maximum_extent_modelito = ppp_modelito.find_extent_x_y_z()
        tol = 80  # tolerance for difference with dimensions
        if (maximum_extent_modelito[0] > maximum_extent_reference[0] + tol) or (
                maximum_extent_modelito[1] > maximum_extent_reference[1] + tol) or (
                maximum_extent_modelito[2] > maximum_extent_reference[2] + tol):
            print("This pdb model is too extended compared to the target and will not be considered")
            list_model_errors.append(basepath_modelito)
            continue
    voyager_base_obj.success = True
    return list_model_errors,path_modified
