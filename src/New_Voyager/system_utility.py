#! /usr/bin/env python
# -*- coding: utf-8 -*-

# # Python 2 and 3: easiest option
from __future__ import absolute_import

from future.standard_library import install_aliases
install_aliases()

import sys
info_p = sys.version_info
info_g = (sys.version).splitlines()
PYTHON_V = info_p.major

import ctypes
import inspect
import math
import os
import platform
import signal
import subprocess
import threading
import time
import traceback
import re
import multiprocessing
import functools
import inspect
import warnings
import cProfile
import json
import requests
import socket
import psutil

from urllib.parse import urlparse, urlencode
from urllib.request import urlopen, Request, build_opener, install_opener, HTTPRedirectHandler, ProxyHandler
from urllib.error import HTTPError
from six import string_types

#TODO: here it is where can be configured the proxy for voyager
# # set up authentication info
# authinfo = urllib.request.HTTPBasicAuthHandler()
# authinfo.add_password(realm='PDQ Application',
#                       uri='https://mahler:8092/site-updates.py',
#                       user='klem',
#                       passwd='geheim$parole')
# proxy_support = urllib.request.ProxyHandler({"http" : "http://ahad-haam:3128"})
# proxy_support = ProxyHandler({})
#
# # build a new opener that adds authentication and caching FTP handlers
# opener = build_opener(proxy_support)
#
# # install it
# install_opener(opener)

#NOTE: this should be done only when proxy should not be configured
os.environ['NO_PROXY'] = '*'


#######################################################################################################
#                                           SUPPORT FUNC                                              #
#######################################################################################################

#string_types = (type(b''), type(u''))

STOP_AND_RESET = "STOP_AND_RESET"
STOP_NOW = "STOP_NOW"



def deprecated(reason):
    """
    This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used.
    """

    if isinstance(reason, string_types):

        # The @deprecated is used with a 'reason'.
        #
        # .. code-block:: python
        #
        #    @deprecated("please, use another function")
        #    def old_function(x, y):
        #      pass

        def decorator(func1):

            if inspect.isclass(func1):
                fmt1 = "Call to deprecated class {name} ({reason})."
            else:
                fmt1 = "Call to deprecated function {name} ({reason})."

            @functools.wraps(func1)
            def new_func1(*args, **kwargs):
                warnings.simplefilter('always', DeprecationWarning)
                warnings.warn(
                    fmt1.format(name=func1.__name__, reason=reason),
                    category=DeprecationWarning,
                    stacklevel=2
                )
                warnings.simplefilter('default', DeprecationWarning)
                return func1(*args, **kwargs)

            return new_func1

        return decorator

    elif inspect.isclass(reason) or inspect.isfunction(reason):

        # The @deprecated is used without any 'reason'.
        #
        # .. code-block:: python
        #
        #    @deprecated
        #    def old_function(x, y):
        #      pass

        func2 = reason

        if inspect.isclass(func2):
            fmt2 = "Call to deprecated class {name}."
        else:
            fmt2 = "Call to deprecated function {name}."

        @functools.wraps(func2)
        def new_func2(*args, **kwargs):
            warnings.simplefilter('always', DeprecationWarning)
            warnings.warn(
                fmt2.format(name=func2.__name__),
                category=DeprecationWarning,
                stacklevel=2
            )
            warnings.simplefilter('default', DeprecationWarning)
            return func2(*args, **kwargs)

        return new_func2

    else:
        raise TypeError(repr(type(reason)))

def timing(f):
    """

    :param f:
    :type f:
    :return:
    :rtype:
    """

    def wrap(*args, **kwds):
        time1 = time.time()
        ret = f(*args, **kwds)
        time2 = time.time()
        # if time2-time1>=1:
       #print('%s function took %0.3f s' % (f.__name__, (time2 - time1)))
        return ret

    return wrap

def profileit(func):
    """

    :param func:
    :type func:
    :return:
    :rtype:
    """

    def wrapper(*args, **kwargs):
        datafn = func.__name__ + ".profile"  # Name the data file sensibly
        prof = cProfile.Profile()
        retval = prof.runcall(func, *args, **kwargs)
        prof.dump_stats(datafn)
        return retval

    return wrapper

def safe_call(funct, *args, **kwargs):
    try:
        return funct(args, kwargs)
    except:
        return None

def explicit_checker(f):
    varnames = f.__code__.co_varnames
    def wrapper(*a, **kw):
        kw['explicit_params'] = set(list(varnames[:len(a)]) + list(kw.keys()))
        return f(*a, **kw)
    return wrapper

#######################################################################################################
#                                                                                                     #
#######################################################################################################

import io
import locale


def is_str_or_unicode(text):
    return isinstance(text, string_types)

def get_encoding():
    return "utf-8"
    #return locale.getpreferredencoding()

def guess_encoding(openfile):
    """guess the encoding of the given file"""
    import io
    import locale
    with io.open(openfile, "rb") as f:
        data = f.read(5)
    if data.startswith(b"\xEF\xBB\xBF"):  # UTF-8 with a "BOM"
        return "utf-8-sig"
    elif data.startswith(b"\xFF\xFE") or data.startswith(b"\xFE\xFF"):
        return "utf-16"
    else:  # in Windows, guessing utf-8 doesn't work, so we have to try
        try:
            with io.open(openfile, encoding="utf-8") as f:
                preview = f.read(222222)
                return "utf-8"
        except:
            return locale.getdefaultlocale()[1]


import hashlib
from functools import partial

def md5sum(filename, include_filename=False):
    try:
        if os.path.exists(filename):
            with open(filename, mode='rb') as f:
                d = hashlib.md5()
                if include_filename: d.update(filename.encode(get_encoding()))
                for buf in iter(partial(f.read, 128), b''):
                    d.update(buf)
        else:
            d = hashlib.md5()
            text = io.StringIO(py2_3_unicode(filename))
            for buf in iter(partial(text.read, 128), b''):
                d.update(buf)

        return d.hexdigest()
    except:
        #print(sys.exc_info())
        #traceback.print_exc(file=sys.stdout)
        raise Exception("Could not create md5 for the given input")

def special_request(search_string, rootsite, data, topn=10):
    # install a custom handler to prevent following of redirects automatically.
    class SmartRedirectHandler(HTTPRedirectHandler):
        def http_error_302(self, req, fp, code, msg, headers):
            return headers

    opener = build_opener(SmartRedirectHandler())
    install_opener(opener)

    parameters = data
    enc_params = urlencode(parameters).encode(get_encoding())

    # post the seqrch request to the server
    request = Request('https://www.ebi.ac.uk/Tools/hmmer/search/phmmer', enc_params)

    # get the url where the results can be fetched from
    #results_url = urlopen(request).getheader('location')
    results_url = urlopen(request)["location"]
    print(results_url)

    # modify the range, format and presence of alignments in your results here
    res_params = {
        'output': 'json',
        'range': '1,'+str(topn)
    }

    # add the parameters to your request for the results
    enc_res_params = urlencode(res_params).encode(get_encoding())
    modified_res_url = results_url + (b'?' + enc_res_params).decode(get_encoding())

    # send a GET request to the server
    results_request = Request(modified_res_url)
    data = urlopen(results_request)

    # print out the results
    return data.read()

def download_url(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)

def request_urlrequest(search_string, rootsite, method="get", headers=None, data=None, files=None):
    z = None
    tries = 10
    for trial in range(tries):
        try:
            if method == "get" and data is None:
                #print("METHOD A GET:",search_string)
                req = requests.get(search_string, files={} if files is None else files, timeout=1)
                #print("DONE")
                try:
                    z = req.json()
                except:
                    z = req
            elif method == "get":
                #print("METHOD B GET:",search_string)
                req = requests.get(search_string, data, files={} if files is None else files, timeout=1)
                #print("DONE")
                try:
                    z = req.json()
                except:
                    z = req
            elif method == "post" and headers is None:
                req = requests.post(search_string, data=data, files={} if files is None else files, timeout=1)
                # print("DONE")
                try:
                    z = req.json()
                except:
                    z = req
            elif method == "post" and headers is not None:
                req = requests.post(search_string, data=data, headers=headers, files={} if files is None else files, timeout=1)
                # print("DONE")
                try:
                    z = req.json()
                except:
                    z = req
            break
        except:
            print(sys.exc_info())
            #traceback.print_exc(file=sys.stdout)
            print("Error contacting",rootsite,"...Attempt ", trial + 1, "/", tries, search_string)
            #time.sleep(1)
    if not z:
        print("Connection Error")
        return None
    else:
        return z

def request_url(search_string, rootsite, data=None):
    z = None
    tries = 10
    for trial in range(tries):
        try:
            if data is None:
                #print("METHOD A", search_string, data)
                req = Request(search_string)
                z = urlopen(req).read()
            else:
                #print("METHOD B", search_string, data)
                req = Request(search_string)
                z = urlopen(req, data).read()
            break
        except:
            # print(sys.exc_info())
            # traceback.print_exc(file=sys.stdout)
            print("Error contacting",rootsite,"...Attempt ", trial + 1, "/", tries, search_string, data)
            #time.sleep(1)
    if not z:
        print("Connection Error")
        return None
    else:
        return z

def warning(objs):
    sys.stderr.write("WARNING: " + str(objs) + "\n")

def findInSubdirectory(filename, subdirectory=''):
    if subdirectory:
        path = subdirectory
    else:
        path = os.getcwd()
    for root, subFolders, files in os.walk(path):
        for fileu in files:
            if fileu == filename:
                return os.path.join(root, fileu)
    raise Exception()

def mem(size="rss"):
    """Generalization; memory sizes: rss, rsz, vsz."""
    return int(os.popen('ps -p %d -o %s | tail -1' % (os.getpid(), size)).read())

def rss():
    """Return ps -o rss (resident) memory in kB."""
    return mem("rss")

def rsz():
    """Return ps -o rsz (resident + text) memory in kB."""
    return mem("rsz")

def vsz():
    """Return ps -o vsz (virtual) memory in kB."""
    return mem("vsz")

def find_free_port():
    s = socket.socket()
    s.bind(('', 0))      # Bind to a free port provided by the host.
    port = s.getsockname()[1]
    s.close()
    return port


class OutputThreading(threading.Thread):
    def __init__(self, externalCallable, *args, **kwds):
        threading.Thread.__init__(self, **kwds)
        self.externalCallable = externalCallable
        self.args = args
        self.kwds = kwds

    def run(self):
        ar = self.args
        kw = self.kwds
        self.externalCallable(*ar, **kw)

    def _get_my_tid(self):
        """determines this (self's) thread id

        CAREFUL : this function is executed in the context of the caller
        thread, to get the identity of the thread represented by this
        instance.
        """
        if not self.isAlive():
            raise threading.ThreadError("the thread is not active")

        # do we have it cached?
        if hasattr(self, "_thread_id"):
            return self._thread_id

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():
            if tobj is self:
                self._thread_id = tid
                return tid

        # TODO: in python 2.6, there's a simpler way to do : self.ident

        raise AssertionError("could not determine the thread's id")

    def raiseExc(self, exctype):
        """Raises the given exception type in the context of this thread.

        If the thread is busy in a system call (time.sleep(),
        socket.accept(), ...), the exception is simply ignored.

        If you are sure that your exception should terminate the thread,
        one way to ensure that it works is:

            t = ThreadWithExc( ... )
            ...
            t.raiseExc( SomeException )
            while t.isAlive():
                time.sleep( 0.1 )
                t.raiseExc( SomeException )

        If the exception is to be caught by the thread, you need a way to
        check that your thread has caught it.

        CAREFUL : this function is executed in the context of the
        caller thread, to raise an excpetion in the context of the
        thread represented by this instance.
        """
        _async_raise(self._get_my_tid(), exctype)


def _async_raise(tid, exctype):
    '''Raises an exception in the threads with id tid'''
    if not inspect.isclass(exctype):
        raise TypeError("Only types can be raised (not instances)")
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(tid),
                                                     ctypes.py_object(exctype))
    if res == 0:
        raise ValueError("invalid thread id")
    elif res != 1:
        # "if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"
        ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(tid), None)
        raise SystemError("PyThreadState_SetAsyncExc failed")

def signal_term_handler(signal,frame):
    print ('\nThe program received a signal of termination')
    if hasattr(sys, '_MEIPASS'):
        print ("Temp file was ", sys._MEIPASS, " and was removed before exiting")
    sys.exit(0)

def get_number_of_cpus():
    return multiprocessing.cpu_count()

def spawn_function_with_multiprocessing(target, args=None, kwargs=None, name=None, blocking=True):
        try:
            if multiprocessing.cpu_count() > 0:
                while 1:
                    if len(multiprocessing.active_children()) < multiprocessing.cpu_count():
                        p2 = multiprocessing.Process(name=target.__name__  if name is None else name, target=target, args=args if args is not None else [], kwargs=kwargs if kwargs is not None else {})
                        p2.start()
                        return p2
                    if not blocking: return False
            else:
                print("FATAL ERROR: I cannot load correctly information of CPUs.")
                sys.exit(0)
        except KeyboardInterrupt:
            print("The user requires to exit from the program.")
            sys.exit(0)

def py2_3_unicode(value):
    global PYTHON_V

    if PYTHON_V == 2:
        return unicode(value)
    else:
        return value

def py2_3_str(value):
    global PYTHON_V

    if PYTHON_V == 2:
        return str(value)
    else:
        return value

def py2_3_bytes(value):
    global PYTHON_V

    if PYTHON_V == 2:
        return bytes(value).encode(u'utf_8')
    else:
        return value

def killtree(pid, including_parent=True):
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
       #print ("Killing process:", child)
        try:
            child.kill()
        except:
            pass

    if including_parent:
        try:
            parent.kill()
        except:
            pass

def get_virtual_memory():
    return psutil.virtual_memory()

def is_available_memory_less_than(value):
    mem = get_virtual_memory()
    if mem.available <= value:
       #print("-----MEMORY-----",mem.available, value)
        return True
    else:
        return False
