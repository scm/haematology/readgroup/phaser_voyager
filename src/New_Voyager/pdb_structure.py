#! /usr/bin/env python
# -*- coding: utf-8 -*-

#####################################################################
#  Copyright and authorship belong to:                              #
#  @author: Massimo Sammito                                         #
#  @email: msacri@ibmb.csic.es / massimo.sammito@gmail.com          #
#  @author: Claudia Millan                                          #
#  @email: cm844@cam.ac.uk / clmilneb@gmail.com
#####################################################################
#####################################################################
#                              LICENCE                              #
#                                                                   #
#                                                                   #
#####################################################################

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# System imports
import sys
import os
import io
import copy
import tempfile
import traceback

#Compatibility imports
from six import string_types

# scientific imports
from numpy.linalg import svd, det, norm
from numpy import dot, sum
import numpy as np
# from sklearn.decomposition import PCA
from collections import defaultdict
# import pyntcloud

# support libraries
from New_Voyager import system_utility

# Bioinformatics imports
import Bio.PDB
import Bio.PDB.Residue
# import Bio.PDB.QCPSuperimposer # Import where used to avoid unnecessary deprecation warnings
# from Bio import pairwise2 # Import where used to avoid unnecessary deprecation warnings
# from Bio.Align import substitution_matrices as matlist # Ditto

info_p = sys.version_info
info_g = (sys.version).splitlines()
PYTHON_V = info_p.major
GLOBAL_POLY_ALA = False
SEPARATOR_FOR_MULTI_ENSEMBLES_PDB = "================================"
_seqalign_cache = {}


def check_if_translation_is_required(file_solution, center_map):
    # we need to make sure the solution is in the cell of the original map we read
    # to do that, just take the center of the cell that we used for the map extraction, and
    # the center of the solution coordinates, and then use the unit cell dimensions to
    # get that distance to be the smallest!
    unit_cell = read_cell_and_sg_from_pdb(file_solution)
    ppp = PDB(file_solution)
    extent, center_mol = ppp.find_extent_x_y_z(return_center=True)
    center_x = center_map[0]
    center_y = center_map[1]
    center_z = center_map[2]
    starting_vector = [(center_x - center_mol[0]), (center_y - center_mol[1]), (center_z - center_mol[2])]
    translation_to_apply = [(round(starting_vector[0] / unit_cell[0][0]) * unit_cell[0][0]),
                            (round(starting_vector[1] / unit_cell[0][1]) * unit_cell[0][1]),
                            (round(starting_vector[2] / unit_cell[0][2]) * unit_cell[0][2])]
    return translation_to_apply

def shifting_coordinates_orthogonal(shift, pdb_object, pathito,inverse=False):
    structure_to_shift_atoms = pdb_object.get_list_of_atoms()
    for atom in structure_to_shift_atoms:
        # print("Before:",atom.get_coord())
        coord_before = atom.get_coord()
        x = coord_before[0]
        y = coord_before[1]
        z = coord_before[2]
        new_coord = [0, 0, 0]
        if not inverse:
            new_coord[0] = x + float(shift[0])
            new_coord[1] = y + float(shift[1])
            new_coord[2] = z + float(shift[2])
        else:
            new_coord[0] = x - float(shift[0])
            new_coord[1] = y - float(shift[1])
            new_coord[2] = z - float(shift[2])
        atom.set_coord(new_coord)
        # print("After: ",atom.get_coord())
    pdb_object.write_pdb(pathito)

def read_cell_and_sg_from_pdb(path_pdb):
    pdb_file = open(path_pdb, 'r')
    pdb_content = pdb_file.read()
    del pdb_file
    pdb_lines = pdb_content.split("\n")
    for linea in pdb_lines:
        if linea.startswith("CRYST1"):
            cryst_card = linea
            space_group = (cryst_card[55:67]).strip()  # SG in characters 56 - 66
            list_values = cryst_card.split()  # Cell elements
            cell = [list_values[1], list_values[2], list_values[3], list_values[4], list_values[5],
                    list_values[6]]
            float_cell = [float(i) for i in cell]
            return float_cell, space_group

class PDB(object):
    AADICMAP = {'ALA': 'A', 'CYS': 'C', 'ASP': 'D', 'GLU': 'E', 'PHE': 'F', 'GLY': 'G', 'HIS': 'H', 'ILE': 'I',
                'LYS': 'K', 'CSO': 'C', 'OCS': 'C', 'SAM': 'M', 'KCX': 'K', 'FOL': 'X', 'DAL': 'A',
                'LEU': 'L', 'MET': 'M', 'MSE': 'M', 'ASN': 'N', 'PRO': 'P', 'GLN': 'Q', 'ARG': 'R', 'SER': 'S',
                'THR': 'T',
                'VAL': 'V', 'TRP': 'W', 'TYR': 'Y', 'UNK': '-', 'SEC': 'U', 'PYL': 'O', 'ASX': 'B', 'GLX': 'Z',
                'XLE': 'J', 'MLY': 'K', 'M3L': 'K',
                'XAA': 'X', 'ME0': 'M', 'CGU': '-', "LLP": "K", "PTR": "Y", "SEP": "S", "TPO": "T", "TYS": "Y"}

    AADICMAP = defaultdict(lambda: "X", **AADICMAP)

    AAList = AADICMAP.keys()
    ATOAAAMAP = dict((v, k) for k, v in AADICMAP.items())
    AALISTOL = AADICMAP.values()

    ID_CHAIN = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "Z",
            "J", "K", "X", "Y", "W", "a", "b", "c", "d", "e", "f", "g", "h", "i", "l", "m", "n", "o", "p", "q", "r",
            "s", "t", "u", "v", "z", "j", "k", "x", "y", "w", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

    def __init__(self, filepath, chains=[], fast=False):
        """  filepath can be a pdb file, cif file, BioPDB object, or
            cctbx model object """

        self.poly_ala=GLOBAL_POLY_ALA

        self.filepath = filepath
        try:
            self.name = os.path.basename(filepath)
        except:
            self.name = "structure"

        if os.path.isfile(str(self.filepath)) and self.filepath.endswith(".pdb"):
            # A PDB file
            self.rename_hetatm_and_blank_chain() #self.pdbtext is created here

            self.pdbtext = io.StringIO(
              system_utility.py2_3_unicode(self.pdbtext))
            parser = Bio.PDB.PDBParser(QUIET=True) # quiet warning to avoid pdb construction warnings
            self.structure = parser.get_structure(self.name, self.pdbtext)

        elif os.path.isfile(str(self.filepath)) and self.filepath.endswith(".cif"):
            # A CIF file
            parser = Bio.PDB.FastMMCIFParser()
            self.name = os.path.basename(self.filepath)[:-4]
            self.structure = parser.get_structure(self.name, self.filepath)

        elif isinstance(self.filepath, Bio.PDB.Structure.Structure):
            # A BioPDB object
            self.structure = self.filepath
            self.filepath = "./structure.pdb"
            self.pdbtext = io.StringIO(
              system_utility.py2_3_unicode(self.as_text()))

        else:  # Try this last because import takes time
            import mmtbx.model
            if isinstance(self.filepath, mmtbx.model.manager):
                # A cctbx model
                self.pdbtext = cctbx_model_as_text(self.filepath)
                #print('type(self.filepath)',type(self.filepath))
                # self.pdbtext = io.StringIO(
                #    system_utility.py2_3_unicode(
                #      cctbx_model_as_text(self.filepath)))
                self.pdbtext = cctbx_model_as_text(self.filepath)
                self.structure = cctbx_model_as_structure(self.filepath)
                self.filepath = "./structure.pdb"
            else:
                raise Exception("ERROR: Unknown model file or type %s" %(
                str(self.filepath)))


        self.precomputed_surfaces = {}
        self.defined_chain = chains
        self.modelsid = [m.id for m in self.structure]

        if len(self.defined_chain) > 0:
            for m in self.structure:
                todel = []
                for c in m:
                    if c.id not in chains: todel.append(c.id)
                for c in todel: del m[c]

        if not fast:
            if len(self.modelsid) > 0:
                self.nres = len(self.get_list_of_atoms(type="CA", model=self.modelsid[0]))
            else:
                self.nres = 0

            self.sequence = "".join(
                [self.AADICMAP[d.get_parent().get_resname()] for d in self.get_list_of_atoms(type="CA") if
                 d.get_parent().get_resname() in self.AADICMAP])
            self.sequenceAAA = "*".join(
                [d.get_parent().get_resname() for d in self.get_list_of_atoms(type="CA") if
                 d.get_parent().get_resname()])

            self.seqbychain = {chai.get_id(): "".join(
                [self.AADICMAP[d.get_parent().get_resname()] for d in self.get_list_of_atoms(type="CA") if
                 d.get_parent().get_resname() in self.AADICMAP and d.get_parent().get_full_id()[2] == chai.get_id()]) for
                               chai in Bio.PDB.Selection.unfold_entities(self.structure, "C")}
            self.mapbychain = {
            chai.get_id(): [(d.get_parent().get_full_id(), self.AADICMAP[d.get_parent().get_resname()]) for d in self.get_list_of_atoms(type="CA") if
                            d.get_parent().get_resname() in self.AADICMAP and d.get_parent().get_full_id()[2] == chai.get_id()] for chai in Bio.PDB.Selection.unfold_entities(self.structure, "C")}
            self.mapindex = [d.get_parent().get_full_id() for d in self.get_list_of_atoms(type="CA") if
                             d.get_parent().get_resname() in self.AADICMAP]
            if hasattr(self, "pdbtext"):
                self.pdbtext.seek(0)
                self.remarks = "\n".join([line for line in self.pdbtext.read().splitlines() if line.startswith("REMARK")])
                self.pdbtext.seek(0)
            else:
                self.remarks = ""

    def as_text(self):
        pdb_io = Bio.PDB.PDBIO()
        pdb_io.set_structure(self.structure)
        f = io.StringIO()
        pdb_io.save(f, write_end = False)  # fails if write_end=True
        return f.getvalue()


    def as_cctbx_model(self):
        """ Convert to cctbx model through text string"""

        pdb_text = self.as_text()
        import iotbx.pdb
        import mmtbx.model
        pdb_inp = iotbx.pdb.input(source_info = None,
          lines = str(pdb_text).splitlines())
        model = mmtbx.model.manager(
           model_input = pdb_inp)
        return model


    def get_structure(self):
        return self.structure

    def get_accessible_surface_area(self,  algorithm="phaser"):
        if algorithm == "phaser":
            try:
                import phaser.chisel
                import phaser.mmt
                import iotbx

                pdbstring = self.get_pdb_from_list_of_atoms(must_have_backbone=True, write_pdb=False, header=self.remarks)[0]
                tf = tempfile.NamedTemporaryFile(delete=False)
                with open(tf.name, "w") as f:
                    f.write(pdbstring)

                pdb_in = iotbx.pdb.hierarchy.input(file_name=tf.name)
                access = []
                for cha in pdb_in.hierarchy.only_model().chains():
                    mmt = phaser.mmt.determine(cha)
                    chm = phaser.chisel.ChainSample(cha, mmt)
                    access += list(chm.accessible_surface_area(None, None).values)
                    # atoms = [a.id_str() for a in list(cha.atoms())]
                print("Phaser accessible area:", sum(access))

                os.remove(tf.name)
                return sum(access)
            except:
                print(sys.exc_info())
                traceback.print_exc(file=sys.stdout)
                return None
        else:
            try:
                import freesasa

                pdbstring = self.get_pdb_from_list_of_atoms(must_have_backbone=True,
                                                            write_pdb=False, header=self.remarks)[0]
                tf = tempfile.NamedTemporaryFile(delete=False)
                with open(tf.name, "w") as f:
                    f.write(pdbstring)

                structuresasa = freesasa.Structure(tf.name)
                resultsasa = freesasa.calc(structuresasa)
                area_classes = freesasa.classifyResults(resultsasa, structuresasa)
                print("FreeSASA accessible area:", area_classes)
                os.remove(tf.name)
                return area_classes['Polar']+area_classes['Apolar']
            except:
                print(sys.exc_info())
                traceback.print_exc(file=sys.stdout)
                return None

    def find_extent_x_y_z(self,return_center=False):
        # Find maximum extent in x,y,z
        # Optionally also find the center
        stru = self.get_structure()
        all_coord = [atom.get_coord() for atom in stru.get_atoms()]
        x_ref_array = [all_coord[i][0] for i in range(len(all_coord))]
        y_ref_array = [all_coord[i][1] for i in range(len(all_coord))]
        z_ref_array = [all_coord[i][2] for i in range(len(all_coord))]
        min_x = min(x_ref_array)
        min_y = min(y_ref_array)
        min_z = min(z_ref_array)
        max_dist_x = max(x_ref_array) - min_x
        max_dist_y = max(y_ref_array) - min_y
        max_dist_z = max(z_ref_array) - min_z
        if not return_center:
            return (max_dist_x, max_dist_y, max_dist_z)
        else:
            center_x = min_x + (max_dist_x/2.0)
            center_y = min_y + (max_dist_y / 2.0)
            center_z = min_z + (max_dist_z / 2.0)
            return (max_dist_x, max_dist_y, max_dist_z),(center_x,center_y,center_z)


    def are_chains_interacting(self, chains, algorithm="phaser"):
        MIN_BURIED_SURFACE = 500

        if algorithm == "phaser":
            try:
                import phaser.chisel
                import phaser.mmt
                import iotbx

                chains = sorted(chains)
                interacting = set([])
                for i,c1 in enumerate(chains):
                    if c1 in interacting: continue

                    found = False
                    access1 = None
                    if c1 not in self.precomputed_surfaces:
                        pdbstring1 = self.get_pdb_from_list_of_atoms(extract_only_chain=[c1],
                                                                    must_have_backbone=True,
                                                                    write_pdb=False, header="")[0]

                        tf1 = tempfile.NamedTemporaryFile(delete=False)
                        with open(tf1.name, "w") as f: f.write(pdbstring1)

                        pdb_in1 = iotbx.pdb.hierarchy.input(file_name=tf1.name)
                        access1 = []
                        for cha in pdb_in1.hierarchy.only_model().chains():
                            mmt = phaser.mmt.determine(cha)
                            chm = phaser.chisel.ChainSample(cha, mmt)
                            access1 += list(chm.accessible_surface_area(None, None).values)
                            # atoms = [a.id_str() for a in list(cha.atoms())]

                        os.remove(tf1.name)
                        access1 = sum(access1)
                        self.precomputed_surfaces[c1] = access1
                    else:
                        access1 = self.precomputed_surfaces[c1]

                    print("Phaser chain:", c1, access1)

                    for j,c2 in enumerate(chains):
                        if j<=i: continue

                        access2 = None
                        if c2 not in self.precomputed_surfaces:
                            pdbstring2 = self.get_pdb_from_list_of_atoms(extract_only_chain=[c2],
                                                                         must_have_backbone=True,
                                                                         write_pdb=False, header="")[0]

                            tf2 = tempfile.NamedTemporaryFile(delete=False)
                            with open(tf2.name, "w") as f: f.write(pdbstring2)

                            pdb_in2 = iotbx.pdb.hierarchy.input(file_name=tf2.name)
                            access2 = []
                            for cha in pdb_in2.hierarchy.only_model().chains():
                                mmt = phaser.mmt.determine(cha)
                                chm = phaser.chisel.ChainSample(cha, mmt)
                                access2 += list(chm.accessible_surface_area(None, None).values)
                                # atoms = [a.id_str() for a in list(cha.atoms())]
                            os.remove(tf2.name)
                            access2 = sum(access2)
                            self.precomputed_surfaces[c2] = access2
                        else:
                            access2 = self.precomputed_surfaces[c2]

                        print("Phaser chain:", c2, access2)

                        pdbstring12 = self.get_pdb_from_list_of_atoms(extract_only_chain=[c1,c2],
                                                                     renumber=True,
                                                                     uniqueChain=True,
                                                                     must_have_backbone=True,
                                                                     write_pdb=False, header="")[0]

                        tf3 = tempfile.NamedTemporaryFile(delete=False)
                        with open(tf3.name, "w") as f:
                            f.write(pdbstring12)

                        pdb_in3 = iotbx.pdb.hierarchy.input(file_name=tf3.name)
                        access3 = []
                        for cha in pdb_in3.hierarchy.only_model().chains():
                            mmt = phaser.mmt.determine(cha)
                            chm = phaser.chisel.ChainSample(cha, mmt)
                            access3 += list(chm.accessible_surface_area(None, None).values)
                            # atoms = [a.id_str() for a in list(cha.atoms())]
                        access3 = sum(access3)
                        print("Phaser chains:", c1, c2, access3)
                        os.remove(tf3.name)

                        buried = ((access1+access2)-(access3))/2.0

                        print("Phaser Interface buried surface:", c1,c2, buried)

                        if buried >= MIN_BURIED_SURFACE:
                            found = True
                            interacting.add(c1)
                            interacting.add(c2)
                            break

                    if not found: return False

                return True
            except:
                print(sys.exc_info())
                traceback.print_exc(file=sys.stdout)
                return None

        else:
            try:
                import freesasa

                chains = sorted(chains)
                interacting = set([])

                for i, c1 in enumerate(chains):
                    if c1 in interacting: continue
                    found = False
                    pdbstring1 = self.get_pdb_from_list_of_atoms(extract_only_chain=[c1],
                must_have_backbone = True,
                write_pdb = False, header = "")[0]

                    tf1 = tempfile.NamedTemporaryFile(delete=False)
                    with open(tf1.name, "w") as f:
                        f.write(pdbstring1)

                    structuresasa1 = freesasa.Structure(tf1.name)
                    resultsasa1 = freesasa.calc(structuresasa1)
                    area_classes1 = freesasa.classifyResults(resultsasa1, structuresasa1)
                    print("FreeSASA accessible area:", c1, area_classes1)
                    os.remove(tf1.name)

                    for j, c2 in enumerate(chains):
                        if j <= i: continue

                        pdbstring2 = self.get_pdb_from_list_of_atoms(extract_only_chain=[c2],
                                                                     must_have_backbone=True,
                                                                     write_pdb=False, header="")[0]

                        tf2 = tempfile.NamedTemporaryFile(delete=False)
                        with open(tf2.name, "w") as f:
                            f.write(pdbstring2)

                        structuresasa2 = freesasa.Structure(tf2.name)
                        resultsasa2 = freesasa.calc(structuresasa2)
                        area_classes2 = freesasa.classifyResults(resultsasa2, structuresasa2)
                        print("FreeSASA accessible area:", c2, area_classes2)
                        os.remove(tf2.name)

                        pdbstring12 = self.get_pdb_from_list_of_atoms(extract_only_chain=[c1, c2],
                                                                      renumber=True,
                                                                      uniqueChain=True,
                                                                      must_have_backbone=True,
                                                                      write_pdb=False, header="")[0]

                        tf3 = tempfile.NamedTemporaryFile(delete=False)
                        with open(tf3.name, "w") as f:
                            f.write(pdbstring12)

                        structuresasa3 = freesasa.Structure(tf3.name)
                        resultsasa3 = freesasa.calc(structuresasa3)
                        area_classes3 = freesasa.classifyResults(resultsasa3, structuresasa3)
                        print("FreeSASA accessible area:", c1, c2, area_classes3)
                        os.remove(tf3.name)

                        buried1 = ((area_classes1['Polar'] + area_classes2['Polar']) - (area_classes3['Polar'])) / 2.0
                        buried2 = ((area_classes1['Apolar'] + area_classes2['Apolar']) - (area_classes3['Apolar'])) / 2.0
                        buried3 = ((area_classes1['Polar'] + area_classes1['Apolar'] + area_classes2['Polar'] + area_classes2['Apolar']) - (area_classes3['Polar'] + area_classes3['Apolar'])) / 2.0

                        print("FreeSASA Interface buried surface:", c1, c2, buried1, buried2, buried3)

                        if buried3 >= MIN_BURIED_SURFACE:
                            found = True
                            interacting.add(c1)
                            interacting.add(c2)
                            break

                    if not found: return False

                return True
            except:
                print(sys.exc_info())
                traceback.print_exc(file=sys.stdout)
                return None

    def count_residues_with_occupancy_larger_than(self, thresh):
        z = [a for x in self.get_list_of_residues() for a in x if a.get_occupancy()>thresh]
        return len(z)

    def validate_residue(self, residue, ca_list, o_list, all_atom_ca, number_of_residues, ignore):
        """
        Validates a residue and update the passed lists. It allows only valid residues according Bioinformatics.AAList with atoms with at least an occupancy of 10%

        :param residue: Residue object
        :type residue: Bio.PDB.Residue
        :param ca_list: list of coords and properties for CA atoms
        :type ca_list: list [0] pos_X [1] pos_Y [2] pos_Z [3] residue full id [4] residue name code in 3 letters
        :param o_list: list of coords and properties for O atoms
        :type o_list: list [0] pos_X [1] pos_Y [2] pos_Z [3] residue full id [4] residue name code in 3 letters
        :param all_atom_ca: array of CA atoms
        :type all_atom_ca: list of Bio.PDB.Atom
        :param number_of_residues: number of residues read
        :type number_of_residues: int
        :param ignore: set of residue full ids to be ignored
        :type ignore: set
        :return coord_ca, o_list, all_atom_ca, number_of_residues:
        :rtype (list, list, list, int):
        """

        if (residue.get_resname().upper() in self.AAList) and (residue.has_id("CA")) and (
        residue.has_id("O")) and (residue.has_id("C")) and (residue.has_id("N")):
            if residue.get_full_id() in ignore:
                return ca_list, o_list, all_atom_ca, number_of_residues

            ca = residue["CA"]
            o = residue["O"]
            if any(map(lambda x: x.get_occupancy() < 0.1, [ca, o, residue["C"], residue["N"]])):
                return ca_list, o_list, all_atom_ca, number_of_residues

            number_of_residues += 1

            # for each atom 4 values are saved:
            # [0] pos_X [1] pos_Y [2] pos_Z [3] residue full id [4] residue name code in 3 letters
            co_ca = ca.get_coord()
            co_o = o.get_coord()
            ca_list.append([float(co_ca[0]), float(co_ca[1]), float(co_ca[2]), residue.get_full_id(), residue.get_resname()])
            all_atom_ca.append(ca)
            o_list.append([float(co_o[0]), float(co_o[1]), float(co_o[2]), residue.get_full_id(), residue.get_resname()])

        return ca_list, o_list, all_atom_ca, number_of_residues

    def get_cvs(self, use_list=[], ignore_list=[], length_fragment=3, one_model_per_nmr=False, process_only_chains=[]):
        """
        Parse a structure and generate for every consecutive 'length_fragment' residues a CV with a window f 1 overlapping residue.

        :param structure: The structure object of the structure
        :type structure: Bio.PDB.Structure
        :param use_list:
        :type use_list: list
        :param ignore_list:
        :type ignore_list: list
        :param length_fragment: Define the peptide length for computing a CV
        :type length_fragment: int
        :param one_model_per_nmr:
        :type one_model_per_nmr: bool
        :return (cvs, separating_chains): List of CVs and the id separation for each chain
        :rtype: (list,list)
        """

        structure = self.structure
        allAtomCA = []
        ca_list = []
        o_list = []
        numberOfResidues = 0
        separating_chains = []
        start_separa = 0
        end_separa = 0
        cvs = []

        if len(use_list) > 0:
            for fra in use_list:
                lir = [residue for residue in
                       Bio.PDB.Selection.unfold_entities(structure[fra["model"]][fra["chain"]], "R")
                       if residue.get_id() in fra["resIdList"]]
                for residue in lir:
                    if process_only_chains is not None and len(process_only_chains) > 0 and residue.get_full_id()[
                        2] not in process_only_chains: continue
                    ca_list, o_list, allAtomCA, numberOfResidues = self.validate_residue(residue, ca_list, o_list, allAtomCA,
                                                                                    numberOfResidues, ignore_list)
        elif one_model_per_nmr:
            lir = Bio.PDB.Selection.unfold_entities(structure.get_list()[0], "R")
            for residue in lir:
                if process_only_chains is not None and len(process_only_chains) > 0 and residue.get_full_id()[
                    2] not in process_only_chains: continue

                ca_list, o_list, allAtomCA, numberOfResidues = self.validate_residue(residue, ca_list, o_list, allAtomCA,
                                                                                numberOfResidues, ignore_list)
        else:
            lir = Bio.PDB.Selection.unfold_entities(structure, "R")
            for residue in lir:
                if process_only_chains is not None and len(process_only_chains) > 0 and residue.get_full_id()[
                    2] not in process_only_chains: continue

                ca_list, o_list, allAtomCA, numberOfResidues = self.validate_residue(residue, ca_list, o_list, allAtomCA,
                                                                                numberOfResidues, ignore_list)

        # This is the number of possible cvs n_cvs = n_aa - (n_peptides - 1) = n_aa - n_peptides + 1
        numberOfSegments = numberOfResidues - length_fragment + 1

        coordCA = np.array([c[:3] for c in ca_list])
        coordO = np.array([c[:3] for c in o_list])

        if numberOfSegments <= 0:
            # print ("\t\t\tNo enough residues available to create a fragment")
            return cvs, separating_chains

        vectorsCA = np.empty((numberOfSegments, 3))
        vectorsCA.fill(0.0)
        vectorsO = np.empty((numberOfSegments, 3))
        vectorsO.fill(0.0)
        vectorsH = np.empty((numberOfSegments, 1))
        vectorsH.fill(0.0)

        # a  b  c  d  e  f
        # |--0--|
        #    |--1--|
        #       |--2--|
        #          |--3--|
        # ==================
        # 4 vectors (from 0 to 3)
        #
        # |--0--| = a/3+b/3+c/3
        # |--1--| = |--0--|+(d-a)/3 = a/3 + b/3 + c/3 + d/3 -a/3 = b/3 + c/3 + d/3
        # |--2--| = |--1--|+(e-b)/3 = b/3 + c/3 + d/3 + e/3 -b/3 = c/3 + d/3 + e/3

        vectorsCA[0] = vectorsCA[0] + (coordCA[:length_fragment, :] / float(length_fragment)).sum(axis=0)
        vectorsO[0] = vectorsO[0] + (coordO[:length_fragment, :] / float(length_fragment)).sum(axis=0)
        for i in range(1, len(vectorsCA)):
            vectorsCA[i] = vectorsCA[i - 1] + (coordCA[i + length_fragment - 1] - coordCA[i - 1]) / float(
                length_fragment)
            vectorsO[i] = vectorsO[i - 1] + (coordO[i + length_fragment - 1] - coordO[i - 1]) / float(length_fragment)

        H = vectorsCA - vectorsO
        vectorsH = np.sqrt((H ** 2).sum(axis=1))

        last_chain = None

        for i in range(len(vectorsCA)):
            # if same occupancy take the lowest bfactor
            prevRes = (" ", None, " ")
            ncontigRes = 0
            resids = []
            prev_model = None
            prev_chain = None

            for yui in range(i, i + length_fragment):  # quindi arrivo a i+lengthFragment-1
                resan = (ca_list[yui])[3]
                resids.append(list(resan) + [ca_list[yui][4]])
                resa = resan[3]

                if prevRes == (" ", None, " "):
                    ncontigRes += 1
                elif prev_chain == None or prev_chain == resan[2]:
                    resaN = self.get_residue(resan[1], resan[2], resan[3])
                    prevResC = self.get_residue(prev_model, prev_chain, prevRes)
                    if check_continuity(prevResC, resaN):
                        ncontigRes += 1
                prevRes = resa
                prev_model = resan[1]
                prev_chain = resan[2]

            if ncontigRes != length_fragment:
                vectorsH[i] = 100  # this value identify a not reliable measure for cv
            else:
                cvs.append([i, vectorsH[i], vectorsCA[i], vectorsO[i], resids])

            if prev_chain == last_chain:
                end_separa = len(cvs)
            else:
                last_chain = prev_chain
                if start_separa < end_separa:
                    separating_chains.append((start_separa, end_separa))
                start_separa = end_separa

        if start_separa < end_separa:
            separating_chains.append((start_separa, end_separa))

        if len(separating_chains) > 0 and separating_chains[0] == (0, 0):
            separating_chains = separating_chains[1:]  # this is done to eliminate the first (0,0) that is unuseful

        return cvs, separating_chains

    # @SystemUtility.timing
    # @SystemUtility.profileit
    def format_and_remove_redundance(self, cvs_global, sep_chains, only_reformat=False):
        """
        Format CVs and remove redundance for chains with identical values of CVs

        :param cvs_global: list of CVs
        :type cvs_global: list of lists
        :param sep_chains: list of delimiters ids for different chains
        :type sep_chains: list of tuples
        :param only_reformat: True if only reformat must be done but not removing, False to allow removing
        :type only_reformat: bool
        :return:
        :rtype:
        :raise: ValueError if sep_chains is empty
        """

        if len(sep_chains) == 0:
            raise ValueError('sep_chains cannot be empty')

        if len(sep_chains) == 1:
            return [cvs_global]

        done = []
        equals = {}
        for i in range(len(sep_chains)):
            a = sep_chains[i]
            if i not in equals:
                equals[i] = []
                done.append(i)
            for j in range(i + 1, len(sep_chains)):
                if j in done:
                    continue

                b = sep_chains[j]
                equal = True
                if only_reformat:
                    equal = False
                else:
                    if a[1] - a[0] == b[1] - b[0]:
                        for p in range(a[1] - a[0] - 1):
                            in_i_a = cvs_global[a[0] + p]
                            in_j_a = cvs_global[a[0] + p + 1]
                            in_i_b = cvs_global[b[0] + p]
                            in_j_b = cvs_global[b[0] + p + 1]
                            dip_a = self.compute_instruction(in_i_a, in_j_a)
                            dip_b = self.compute_instruction(in_i_b, in_j_b)

                            if not self.is_compatible(dip_a, dip_b, verbose=False, test_equality=True):
                                equal = False
                                break
                    else:
                        equal = False

                if equal:
                    if i in equals:
                        equals[i].append(j)
                    else:
                        equals[i] = [j]
                    done.append(j)

        cvs_list = []
        for key in equals:
            cvs_list.append(cvs_global[sep_chains[key][0]:sep_chains[key][1]])

        return cvs_list

    def parse_pdb_and_generate_cvs(self, peptide_length=3, one_model_per_nmr=True, only_reformat=True):
        strucc = self.structure
        correct = []
        if not only_reformat:
            seqs = self.seqbychain
            lich = []
            for s, seqr1 in enumerate(seqs):
                r1, seq1 = seqr1
                if r1 in lich: continue
                correct.append(r1)
                for t, seqr2 in enumerate(seqs):
                    r2, seq2 = seqr2
                    if t <= s or r2 in lich: continue
                    z = galign(seq1, seq2)
                    q = sum([1.0 for g in range(len(z[0][0])) if
                             z[0][0][g] != "-" and z[0][1][g] != "-" and z[0][0][g] == z[0][1][g]]) / len(z[0][0]) if len(z) > 0 else 0.0
                    # print(z[0][0])
                    # print(z[0][1])
                    # print(z[0][2],q)
                    # print()
                    if q >= 0.9: lich.append(r2)
            # for seq in seqs:
            #     print(seq)
            # print(correct)
        cvs_global, sep_chains = self.get_cvs(length_fragment=peptide_length, one_model_per_nmr=one_model_per_nmr, process_only_chains=correct)
        # print(sep_chains)
        cvs_list = self.format_and_remove_redundance(cvs_global, sep_chains, only_reformat=only_reformat)
        return cvs_list

    def rename_hetatm_and_blank_chain(self):
        #print("FUNCTION RENAME HETATM AND BLACK CHAIN----",self.filepath)
        allreadlines = None
        with open(self.filepath, "r") as f:
            allreadlines = f.readlines()

        for i, line in enumerate(allreadlines):
            if "RESOLUTION" and "NOT APPLICABLE" in line: line = "\n"

            if line.startswith("HETATM"):
                line = "ATOM  " + line[6:]
            if line.startswith("HETATM") or line.startswith("ATOM"):
                if line[21] == " ":
                    line = line[:21]+"0"+line[22:]

            allreadlines[i] = line

        pdb = "".join(allreadlines)

        self.pdbtext = pdb

    def get_residue(self, model, chain, residue):
        lir = [resil for resil in Bio.PDB.Selection.unfold_entities(self.structure[model][chain], "R") if
               resil.get_id() == residue]
        if len(lir) > 0:
            return lir[0]
        else:
            # print(model,chain,residue)
            # print()
            # for resil in Bio.PDB.Selection.unfold_entities(structure[model][chain], "R"):
            #     print(resil.get_full_id())
            return None

    def get_list_of_residues(self, model=None, idreslist=None, notidreslist=None):
        lir = [resil for resil in
               Bio.PDB.Selection.unfold_entities(self.structure if model is None else self.structure[model], "R") if
               (not self.defined_chain or resil.get_full_id()[2] in self.defined_chain) and (idreslist is None or resil.get_parent().get_full_id() in idreslist)
               and (notidreslist is None or resil.get_full_id() not in notidreslist)]
        return lir

    def get_model_from_structure(self, modelid):
        for mod in self.structure:
            if mod.get_id() == modelid:
                return mod

    def get_list_of_atoms(self, model=None, type=None, idreslist=None, notidreslist=None, get_coords=False):
        lir = [resil if not get_coords else resil.get_coord() for resil in Bio.PDB.Selection.unfold_entities(
            self.structure if model is None else self.get_model_from_structure(model), "A") if
               (type is None or (isinstance(type, list) and resil.get_name().lower() in [xx.lower() for xx in
                                                                                         type]) or (not isinstance(type, list) and resil.get_name().lower() == type.lower())) and (
                           idreslist is None or resil.get_parent().get_full_id() in idreslist)
               and (notidreslist is None or resil.get_parent().get_full_id() not in notidreslist) and (
                           not self.defined_chain or resil.get_full_id()[2] in self.defined_chain)]

        return lir

    def get_atom(self, model, chain, residue, atom):
        lir = [atoml for atoml in Bio.PDB.Selection.unfold_entities(self.structure[model][chain][residue], "A") if
               atoml.get_id() == atom]
        if len(lir) > 0:
            return lir[0]
        else:
            return None

    def get_backbone(self, residue, without_CB=True):
        if without_CB or not residue.has_id("CB"):
            return [residue["CA"], residue["C"], residue["O"], residue["N"]]
        else:
            return [residue["CA"], residue["C"], residue["O"], residue["N"], residue["CB"]]

    def convert_lddt_to_bfac(self, min_lddt_allowed=None, subtract_minimum_b=False):
        model = self.as_cctbx_model()

        import mmtbx.process_predicted_model
        import iotbx
        master_phil = iotbx.phil.parse(mmtbx.process_predicted_model.master_phil_str)
        params = master_phil.extract()
        params.process_predicted_model.b_value_field_is = 'plddt'
        params.process_predicted_model.minimum_plddt = min_lddt_allowed
        params.process_predicted_model.remove_low_confidence_residues = (min_lddt_allowed is not None)
        params.process_predicted_model.split_model_by_compact_regions = False # we do not want to do this here
        params.process_predicted_model.subtract_minimum_b = subtract_minimum_b
        processed_model_info = mmtbx.process_predicted_model.process_predicted_model(model,params)
        self.__init__(processed_model_info.model)

    def convert_err_estimation_to_bfac(self, max_rms_allowed=None):
        model = self.as_cctbx_model()

        # from mmtbx.process_predicted_model import process_predicted_model
        # processed_model_info = process_predicted_model(model,
        #    b_value_field_is = 'rmsd',
        #    maximum_rmsd= max_rms_allowed,
        #    remove_low_confidence_residues = (max_rms_allowed is not None),
        #    subtract_minimum_b = True, # B-values offset to make smallest 0
        #   )
        import iotbx
        import mmtbx.process_predicted_model
        master_phil = iotbx.phil.parse(mmtbx.process_predicted_model.master_phil_str)
        params = master_phil.extract()
        params.process_predicted_model.b_value_field_is = 'rmsd'
        params.process_predicted_model.maximum_rmsd = max_rms_allowed
        params.process_predicted_model.remove_low_confidence_residues = (max_rms_allowed is not None)
        #print('SHERLOCK params.process_predicted_model.remove_low_confidence_residues is ',params.process_predicted_model.remove_low_confidence_residues)
        params.process_predicted_model.split_model_by_compact_regions = False  # we do not wand to do this here
        params.process_predicted_model.subtract_minimum_b = True # B-values offset to make smallest 0
        processed_model_info = mmtbx.process_predicted_model.process_predicted_model(model, params)
        self.__init__(processed_model_info.model)

    def write_pdb(self, path, remove_unusual_resids=False, remove_icode=False):
        class Sele(Bio.PDB.Select):

            STRICTAA = {'ALA': 'A', 'CYS': 'C', 'ASP': 'D', 'GLU': 'E', 'PHE': 'F', 'GLY': 'G', 'HIS': 'H', 'ILE': 'I',
                        'LYS': 'K', 'CSO': 'C', 'OCS': 'C', 'SAM': 'M', 'KCX': 'K', 'FOL': 'X', 'DAL': 'A',
                        'LEU': 'L', 'MET': 'M', 'MSE': 'M', 'ASN': 'N', 'PRO': 'P', 'GLN': 'Q', 'ARG': 'R', 'SER': 'S',
                        'THR': 'T',
                        'VAL': 'V', 'TRP': 'W', 'TYR': 'Y'}

            def __init__(self, remove_unusual_resids, remove_icode):
                self.remove_unusual_resids = remove_unusual_resids
                self.remove_icode = remove_icode

            def accept_residue(self, residue):
                """Overload this to reject residues for output."""
                if self.remove_unusual_resids and residue.get_resname() not in self.STRICTAA: return 0
                if self.remove_icode and residue.get_id()[2] not in ["", " "]: return 0
                return 1

        io = Bio.PDB.PDBIO()
        io.set_structure(self.structure)
        io.save(path,select=Sele(remove_unusual_resids, remove_icode))

        l = ""
        with open(path,"r") as c: l = c.read()
        if self.remarks!='':
            with open(path, "w") as c:
                c.write(self.remarks + "\n" + l)
        else:
            with open(path, "w") as c:
                c.write(l)


    def set_cloud_points(self, pcl_object):
        self.cloud_points = pcl_object

    def set_kmeans_reductions_and_pcl_geom_descriptors(self, structure_point_clouds, centers, labels):
        self.kcenters = np.array(centers)
        self.klabels = labels
        self.kclouds = structure_point_clouds

        # for t,cloud in self.kclouds.items():
        #    cloud.compute_geometry_param()

    @system_utility.explicit_checker
    def get_pdb_from_list_of_atoms(self, renumber=False, uniqueChain=False, chainId="A", chainFragment=False,
                                   diffchain=None, polyala=False, maintainCys=False, normalize=False,
                                   sort_reference=True,
                                   remove_non_res_hetatm=False, dictio_chains={}, write_pdb=False, path_output_pdb='',
                                   idreslist=None, notidreslist=None, applyRt=None, header="", extract_only_chain=None,
                                   must_have_backbone=True, extract_only_models=None, add_this_list_of_atoms=None,
                                   remove_no_occupied_atoms=False, explicit_params=None, force_write_model_card=False):

        if self.poly_ala and 'polyala' not in explicit_params: polyala = True

        pdbString = ""
        numero = 1
        resn = {}
        nur = 1
        lastRes = None
        prevChain = 0
        lich = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "Z",
                "J", "K", "X", "Y", "W", "a", "b", "c", "d", "e", "f", "g", "h", "i", "l", "m", "n", "o", "p", "q", "r",
                "s", "t", "u", "v", "z", "j", "k", "x", "y", "w", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
        # if chainFragment or uniqueChain:
        #   renumber = True
        q = 1
        if extract_only_models and isinstance(extract_only_models,list):
            limo = [model for werte,model in enumerate(self.structure) if werte in extract_only_models]
        else:
            limo = [model for model in self.structure]

        for model in limo:
            if len(limo) > 1 or force_write_model_card:
                pdbString += "MODEL        " + str(q) + "\n"
            q += 1
            reference = self.get_list_of_atoms(idreslist=idreslist, notidreslist=notidreslist, model=model.get_id())
            if add_this_list_of_atoms:
                reference += add_this_list_of_atoms

            # print(model.get_id(),len(reference))
            if extract_only_chain:
                if isinstance(extract_only_chain, str): extract_only_chain = [extract_only_chain]
                # print("Before filtering by chain",len(reference),"chain is",str(extract_only_chain),"example of one res chain is",str(reference[0].get_full_id()[2]))
                reference = [re for re in reference if str(re.get_full_id()[2]) in [str(zzz) for zzz in extract_only_chain]]
                # print("After filtering by chain",len(reference))

            if must_have_backbone:
                # print("Before filtering for backbone",len(reference))
                reference = [re for re in reference if
                             re.get_parent().has_id("CA") and re.get_parent().has_id("C") and re.get_parent().has_id(
                                 "O") and re.get_parent().has_id("N")]
                # print("After filtering for backbone",len(reference))

            if remove_non_res_hetatm:
                reference = [atm for atm in reference if atm.get_parent().get_resname().upper() in PDB.AAList]

            if not polyala:
                reference = [atm for res in reference for atm in res.get_parent()]
            elif maintainCys:
                reference = [atm if res.get_resname().lower() == "cys" else res for res in reference for atm in
                             res.get_parent()]

            if polyala:
                reference = [atm for atm in reference if atm.get_name().lower() in ["ca", "c", "o", "n", "cb"]]

            if sort_reference:
                reference = Bio.PDB.Selection.uniqueify(reference)
                listore = sorted(reference, key=lambda x: x.get_full_id())
            else:
                listore = reference

            for item in listore:
                # print(item.get_full_id())
                # if item.get_full_id() in set(seen):
                #    continue

                # seen.append(item.get_full_id())
                orig_atom_num = item.get_serial_number()
                hetfield, resseq, icode = item.get_parent().get_id()
                if icode not in [" ", ""]: resseq = str(resseq)+icode
                segid = item.get_parent().get_segid()
                chain_id = item.get_parent().get_parent().get_id()
                hetfield = " "
                if (resseq, chain_id) not in resn.keys():
                    if lastRes != None:
                        # print "Checking Continuity",lastRes.get_parent().get_full_id(),item.get_parent().get_full_id(),checkContinuity(lastRes.get_parent(),item.get_parent())
                        # print "Checking Continuity",item.get_parent().get_full_id(),lastRes.get_parent().get_full_id(),checkContinuity(item.get_parent(),lastRes.get_parent())
                        # print lich[prevChain]
                        # print
                        if not check_continuity(lastRes.get_parent(), item.get_parent()):
                            if renumber:
                                # print("LASTRES", lastRes.get_parent().get_full_id(), "ITEM:", item.get_parent().get_full_id())
                                nur += 10
                            if chainFragment:
                                prevChain += 1
                    new_chain_id = chain_id
                    if dictio_chains:
                        if isinstance(list(dictio_chains.keys())[0],tuple):
                            new_chain_id = dictio_chains[item.get_parent().get_full_id()]
                        elif isinstance(list(dictio_chains.keys())[0],str):
                            new_chain_id = dictio_chains[item.get_parent().get_full_id()[2]]

                        # sys.exit(0)
                    elif uniqueChain:
                        new_chain_id = chainId
                    elif chainFragment:
                        new_chain_id = lich[prevChain]
                    if renumber:
                        resn[(resseq, chain_id)] = (nur, new_chain_id)
                    else:
                        resn[(resseq, chain_id)] = (resseq, new_chain_id)
                    lastRes = item
                    nur += 1
                tuplr = resn[(resseq, chain_id)]
                resseq = tuplr[0]
                chain_id = tuplr[1]
                icode = " "
                orig_atom_num = numero
                numero += 1

                resname = item.get_parent().get_resname()
                element = item.get_name()
                if remove_no_occupied_atoms and item.get_occupancy() <= 0.0: continue

                pdbString += get_atom_line(item, element, hetfield, segid, orig_atom_num, resname, resseq, icode,
                                           chain_id,
                                           normalize=normalize, applyRt=applyRt)

            if diffchain != None and len(diffchain) > 0:
                prevChain += 1
                lastRes = None
                for item in diffchain:
                    orig_atom_num = item.get_serial_number()
                    hetfield, resseq, icode = item.get_parent().get_id()
                    if icode not in [" ", ""]: resseq = str(resseq) + icode
                    segid = item.get_parent().get_segid()
                    chain_id = item.get_parent().get_parent().get_id()
                    hetfield = " "
                    if (resseq, chain_id) not in resn.keys():
                        if lastRes != None:
                            if not check_continuity(lastRes.get_parent(), item.get_parent()):
                                if renumber:
                                    # print("LASTRES",lastRes.get_parent().get_full_id(),"ITEM:",item.get_parent().get_full_id())
                                    nur += 10
                                if chainFragment:
                                    prevChain += 1
                        new_chain_id = chain_id
                        if uniqueChain:
                            new_chain_id = chainId
                        elif chainFragment:
                            new_chain_id = lich[prevChain]

                        if renumber:
                            resn[(resseq, chain_id)] = (nur, new_chain_id)
                        else:
                            resn[(resseq, chain_id)] = (resseq, new_chain_id)
                        lastRes = item
                        nur += 1
                    tuplr = resn[(resseq, chain_id)]
                    resseq = tuplr[0]
                    chain_id = tuplr[1]
                    icode = " "
                    orig_atom_num = numero
                    numero += 1

                    resname = item.get_parent().get_resname()
                    element = item.get_name()
                    if remove_no_occupied_atoms and item.get_occupancy() <= 0.0: continue
                    pdbString += get_atom_line(item, element, hetfield, segid, orig_atom_num, resname, resseq, icode,
                                               chain_id, normalize=normalize, applyRt=applyRt)
            if len(limo) > 1 or force_write_model_card:
                pdbString += "ENDMDL\n"

        pdbString = header + "\n" + pdbString if len(header) > 0 else pdbString

        if write_pdb:
            fichmodel = open(path_output_pdb, 'w')
            fichmodel.write(pdbString)
            fichmodel.close()
        return pdbString, resn

class Atom(object):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com

    """

    def __init__(self, id=None, coord=None, fullname="C", altloc=" ", occupancy=1.0, bfactor=25.0):
        self.coord = coord
        self.fullname = fullname
        self.altloc = altloc
        self.occupancy = occupancy
        self.bfactor = bfactor
        self.xyz = coord
        self.id = id

    def set_coord(self, coordi):
        self.coord = coordi

    def get_coord(self):
        return self.coord

    def get_fullname(self):
        return self.fullname

    def get_altloc(self):
        return self.altloc

    def get_occupancy(self):
        return self.occupancy

    def get_bfactor(self):
        return self.bfactor

    def get_full_id(self):
        return self.id


class StructurePointsCloud():
    # @SystemUtility.timing
    def __init__(self, structure, radius=10.0, center=None, from_structure=None, indexes=None):
        self.radius = radius

        if isinstance(structure, PDB):
            self.structure = structure
            self.listCA = [resi["CA"] for resi in self.structure.get_list_of_residues() if resi.has_id("CA")]
            self.listCB = [atom.get_parent()["CB"] for atom in self.listCA if atom.get_parent().has_id("CB")]
            self.listC = [atom.get_parent()["C"] for atom in self.listCA if atom.get_parent().has_id("C")]
            self.listN = [atom.get_parent()["N"] for atom in self.listCA if atom.get_parent().has_id("N")]
            self.listO = [atom.get_parent()["O"] for atom in self.listCA if atom.get_parent().has_id("O")]
            knn = Bio.PDB.NeighborSearch(self.listCA + self.listCB + self.listC + self.listN + self.listO,
                                         bucket_size=10)
            self.near_residues_ori = {
            atom.get_full_id(): [q for q in knn.search(atom.get_coord(), self.radius, level="A")] for atom in self.listCA}
            self.near_residues = {k:[q.get_coord() for q in v] for k,v in self.near_residues_ori.items()}
            self.listCAcoords = np.array([a.get_coord() for a in self.listCA])
        elif center is None and isinstance(structure, list) and len(structure) > 0:  # and isinstance(structure[0],Bio.PDB.Residue):
            self.listCA = [resi["CA"] for resi in self.structure if resi.has_id("CA")]
            self.listCB = [atom.get_parent()["CB"] for atom in self.listCA if atom.get_parent().has_id("CB")]
            self.listC = [atom.get_parent()["C"] for atom in self.listCA if atom.get_parent().has_id("C")]
            self.listN = [atom.get_parent()["N"] for atom in self.listCA if atom.get_parent().has_id("N")]
            self.listO = [atom.get_parent()["O"] for atom in self.listCA if atom.get_parent().has_id("O")]
            knn = Bio.PDB.NeighborSearch(self.listCA + self.listCB + self.listC + self.listN + self.listO,
                                         bucket_size=10)
            self.near_residues_ori = {
            atom.get_full_id(): [q for q in knn.search(atom.get_coord(), self.radius, level="A")] for atom in self.listCA}
            self.near_residues = {k:[q.get_coord() for q in v] for k,v in self.near_residues_ori.items()}
            self.listCAcoords = np.array([a.get_coord() for a in self.listCA])
        elif center is not None and from_structure is not None and isinstance(structure, list) and len(structure) > 0 and isinstance(structure[0], tuple):
            self.indexes, self.listres = list(zip(*structure))
            self.indexes = np.array(self.indexes)
            self.structure = from_structure
            self.listCA = [resi["CA"] for resi in self.structure.get_list_of_residues() if
                           str(resi.get_full_id()) in self.listres and resi.has_id("CA")]
            if isinstance(center, string_types) and center == "mean_atoms": center = np.array(center_of_mass(self.listCA).coord)
            self.center = center
            self.listCB = [atom.get_parent()["CB"] for atom in self.listCA if atom.get_parent().has_id("CB")]
            self.listC = [atom.get_parent()["C"] for atom in self.listCA if atom.get_parent().has_id("C")]
            self.listN = [atom.get_parent()["N"] for atom in self.listCA if atom.get_parent().has_id("N")]
            self.listO = [atom.get_parent()["O"] for atom in self.listCA if atom.get_parent().has_id("O")]
            # print("center",self.center,"lenCA",len(self.listCA),"lenCB",len(self.listCB),"lenO",len(self.listO),"lenC",len(self.listC),"lenN",len(self.listN),"Radius",self.radius,self.radius<np.inf)
            if self.radius < np.inf:
                knn = Bio.PDB.NeighborSearch(self.listCA + self.listCB + self.listC + self.listN + self.listO,
                                             bucket_size=10)
                self.near_residues_ori = {
                    tuple(center): np.array([q for q in knn.search(center, self.radius, level="A")])}
                self.near_residues = {k: [q.get_coord() for q in v] for k, v in self.near_residues_ori.items()}
            else:
                self.near_residues_ori = {tuple(center): np.array(
                    [q for q in self.listCA + self.listCB + self.listC + self.listN + self.listO])}
                self.near_residues = {k: [q.get_coord() for q in v] for k, v in self.near_residues_ori.items()}

            self.listCAcoords = np.array([a.get_coord() for a in self.listCA])

    def PCA_scaled(self, x):
        x = np.array(x)
        cov = (1.0 / (x.shape[0] + 1.0)) * np.cov(x.T)
        ev, eig = np.linalg.eig(cov)

        # from sklearn.decomposition import PCA as pcados
        # b = pcados(n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
        #           random_state=0).fit(x)

        return np.sort(ev)

    # @SystemUtility.timing
    def compute_PCA(self):
        self.pca_analisys = {
        key: PCA(n_components=None, copy=True, whiten=True, svd_solver='auto', tol=0.0, iterated_power='auto',
                 random_state=0).fit(resinear) for key, resinear in self.near_residues.items()}
        #self.pca_analisys = {key: self.PCA_scaled(resinear) for key, resinear in self.near_residues.items()}

        return self.pca_analisys

    def compute_geometry_param(self):
        if not hasattr(self, "pca_analisys"):
            self.compute_PCA()

        linearity = lambda x: (x[0] - x[1]) / x[0]
        planarity = lambda x: (x[1] - x[2]) / x[0]
        sphericity = lambda x: x[2] / x[0]
        omnivariance = lambda x: np.cbrt(x[0] * x[1] * x[2])
        anisotropy = lambda x: (x[0] - x[2]) / x[0]
        eigenentropy = lambda x: -1 * ((x[0] * np.log(x[0])) + (x[1] * np.log(x[1])) + (x[2] * np.log(x[2])))
        sumoflambdas = lambda x: x[0] + x[1] + x[2]
        changecurvature = lambda x: x[2] / (x[0] + x[1] + x[2])

        self.pca_geom_param = {key: np.array(
            [linearity(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33])),
             planarity(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33])),
             sphericity(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33])),
             omnivariance(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33])),
             anisotropy(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33])),
             eigenentropy(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33])),
             sumoflambdas(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33])),
             changecurvature(item.explained_variance_ if len(item.explained_variance_) == 3 else np.array([33, 33, 33]))]) for
                               key, item in self.pca_analisys.items()}
        return self.pca_geom_param

def cctbx_model_as_text(model):
    """ Convert from cctbx model object to structure object """
    #print('type(model.model_as_pdb())',type(model.model_as_pdb()))
    return io.StringIO(system_utility.py2_3_unicode(model.model_as_pdb()))

def cctbx_model_as_structure(model):
    """ Convert from cctbx model object to structure object """
    pdbtext = io.StringIO(system_utility.py2_3_unicode(model.model_as_pdb()))
    parser = Bio.PDB.PDBParser(QUIET=True) # the quiet is to supress warnings (https://lists.open-bio.org/pipermail/biopython/2014-July/015371.html)
    structure = parser.get_structure('structure', pdbtext)
    return structure

def get_area_covered(chain_a, chain_b, global_alignment=True, return_alignment=False):
    # in seqalign, the first chain is the moving one, and the second the fixed, hence the inversion here before
    # the alignment
    alignment = seqalign(chain_b, chain_a, global_alignment=global_alignment)
    bl = [i for i, x in enumerate(alignment.b) if x != "-"]
    sb = bl[0]
    eb = bl[-1]
    #print("-----------------------")
    #print(alignment.a)
    #print("sb,eb",sb,eb)
    #print("alignment.a[sb:eb+1]")
    #print(alignment.a[sb:eb+1])
    #print("-----------------------")
    if return_alignment:
        return "".join([x for x in alignment.a[sb:eb + 1] if x != "-"]), (sb, eb), alignment
    else:
        return "".join([x for x in alignment.a[sb:eb + 1] if x != "-"]), (sb, eb)

def galign(s1, s2, gap_open=-10):
    from Bio.Align import substitution_matrices as matlist
    matrix = matlist.load("BLOSUM62")
    # HOLMES correcting a deprecation warning
    #matrix = matlist.blosum62  # NOTE: matlist.blosum45
    gap_open = gap_open  # NOTE #-10
    gap_extend = -1  # NOTE #-0.5
    s1 = s1.replace("-", "X")
    s2 = s2.replace("-", "X")
    s1 = s1.replace("U", "X")
    s2 = s2.replace("U", "X")
    s1 = s1.replace("O", "K")
    s2 = s2.replace("O", "K")
    s1 = s1.replace("B", "X")
    s2 = s2.replace("B", "X")
    s1 = s1.replace("Z", "X")
    s2 = s2.replace("Z", "X")
    s1 = s1.replace("J", "X")
    s2 = s2.replace("J", "X")

    # if "X" in s1 or "X" in s2 or "X" in s3:
    #    return 0

    from Bio import pairwise2
    q1 = pairwise2.align.globalds(s1, s2, matrix, gap_open, gap_extend)

    return q1

def seqalign(seq_moving, seq_fixed, quiet=False, cache=True, global_alignment=True,
             similarity_function="blosum50"):
    """Perform sequence alignment using mmtbx.alignment. Return alignment.

    Sequence alignments are cached; use cache=False to disable.
    """
    import mmtbx.alignment
    global _seqalign_cache

    key = (seq_moving, seq_fixed)
    if _seqalign_cache.get(key) and cache:
        return _seqalign_cache[key]

    opts = {
        'gap_opening_penalty': 12.0,
        'gap_extension_penalty': 1.0,
        'similarity_function': similarity_function,  # identity
        'style': 'global' if global_alignment else 'local'
    }
    opts['seq_a'] = seq_moving
    opts['seq_b'] = seq_fixed
    align_obj = mmtbx.alignment.align(**opts)
    alignment = align_obj.extract_alignment()
    if cache:
        _seqalign_cache[key] = alignment
    return alignment


def superpose_with_alignment(reference, target, target_rmsd=None, no_prior_processing=False,
                             permissive_mainchain=False, threshold_return_none=0.3):
    ref = PDB(reference)
    tar = PDB(target)

    allpossi = []
    z1 = galign(ref.sequence, tar.sequence)
    q1 = sum([1.0 for g in range(len(z1[0][0])) if z1[0][0][g] != "-" and z1[0][1][g] != "-" and z1[0][0][g] == z1[0][1][g]]) / len(z1[0][0]) if len(z1) > 0 else 0.0
    allpossi.append((q1,z1))

    # AGATHA NOTE: I think this section is sensible when this function is being called
    # in other contexts of voyager, but can be skipped when models do not come from there
    if not no_prior_processing:

        sculptseq = "".join([PDB.AADICMAP[j] for j in [x.split()[4].split("*") for x in tar.remarks.splitlines() if
                                                       x.startswith("REMARK VOYAGER SEQMAP SCULPT")][0]])
        if len(sculptseq) > 0:
            z2 = galign(ref.sequence, sculptseq)
            q2 = sum([1.0 for g in range(len(z2[0][0])) if z2[0][0][g] != "-" and z2[0][1][g] != "-" and z2[0][0][g] == z2[0][1][g]]) / len(z2[0][0]) if len(z2) > 0 else 0.0
            allpossi.append((q2,z2))

        targetseq = [x.split()[4] for x in tar.remarks.splitlines() if x.startswith("REMARK VOYAGER SEQMAP TARGET")][0]
        if len(targetseq) > 0:
            z3 = galign(ref.sequence, targetseq)
            q3 = sum([1.0 for g in range(len(z3[0][0])) if z3[0][0][g] != "-" and z3[0][1][g] != "-" and z3[0][0][g] == z3[0][1][g]]) / len(z3[0][0]) if len(z3) > 0 else 0.0
            allpossi.append((q3,z3))

    q,z = sorted(allpossi, key=lambda c: c[0], reverse=True)[0]
    #print("SHERLOCK q",q)
    #print("SHERLOCK z",z)
    if q < threshold_return_none: return None

    for ko in range(len(z)):
        print(z[ko],"id",q, ko, "of", len(z))
        s1 = [t for t,p in enumerate(z[ko][0]) if p != "-"]
        s2 = [t for t,p in enumerate(z[ko][1]) if p != "-"]
        assert(len(ref.mapindex)==len(s1))
        assert(len(tar.mapindex)==len(s2))
        assert(len(z[ko][0])==len(z[ko][1]))

        s1s2 = set(s1).intersection(set(s2))
        # s1s2 = simple_tools.consecutive_groups(sorted((list(s1s2))))
        # s1s2 = [g for g in s1s2 if len(g)>3]
        # s1s2 = set([aa for bb in s1s2 for aa in bb])

        l1 = dict(zip(s1, ref.mapindex))
        l2 = dict(zip(s2, tar.mapindex))

        # for a in sorted(l1.keys()):
        #     print(a, l1[a])
        # print("--------------------------------------------------")
        # for a in sorted(l1.keys()):
        #     print(a, l2[a])

        ids1 = []
        ids2 = []
        for a in sorted(l1.keys()):
            b = l1[a]
            if a in s1s2:
                c = l2[a]
                ids1.append(b)
                ids2.append(c)
                #print("+++++++++++++++",a,z[ko][0][a],z[ko][1][a],b,c)

        assert(len(ids1)==len(ids2))

        # for kk in zip(ids1,ids2):
        #     print(kk)

        atoms1 = ref.get_list_of_residues()
        atoms2 = tar.get_list_of_residues()

        check_reference_too = False
        ser = [s for l in ids1 for s in atoms1 if s.get_full_id() == l]
        try:
            ser = [(res["N"], res["CA"], res["C"], res["O"]) for res in ser]
        except:
            if permissive_mainchain:
                new_ser = []
                incomplete_residues = []
                for res in ser:
                    atomitos = res.get_atoms()
                    selected_atomitos = []
                    for atomi in atomitos:
                        if atomi.get_name() in ['CA','C','O','N']:
                            selected_atomitos.append(atomi)
                    if len(selected_atomitos)<4:
                        incomplete_residues.append((res.get_id(),[mit.get_name() for mit in selected_atomitos]))
                    new_ser.append(tuple(selected_atomitos))
                ser = new_ser
                check_reference_too = True
            else:
                 raise Exception("ERROR: Some residues do not have all mainchain atoms")
        ser = [atom for group in ser for atom in group]

        ger = [s for l in ids2 for s in atoms2 if s.get_full_id() == l]
        if check_reference_too:
            only_id = [ele[0] for ele in incomplete_residues]
            new_ger = []
            for res in ger:
                atomitos = res.get_atoms()
                selected_atomitos = []
                id_res = res.get_id()
                if id_res in only_id:
                    for index,element in enumerate(incomplete_residues):
                        if element[0]==id_res:
                            keys_to_select = incomplete_residues[index][1]
                            for key in keys_to_select:
                                selected_atomitos.append(res[key])
                            break
                else:
                    for atomi in atomitos:
                        if atomi.get_name() in ['CA', 'C', 'O', 'N']:
                            selected_atomitos.append(atomi)
                new_ger.append(tuple(selected_atomitos))
            ger = new_ger
        else:
            ger = [(res["N"], res["CA"], res["C"], res["O"]) for res in ger]
        ger = [atom for group in ger for atom in group]

        rmsd, R, t = get_rmsd_and_RT(ser, ger, n_iter=None)

        if target_rmsd is not None and rmsd > target_rmsd:
            ser = [s for l in ids1 for s in atoms1 if s.get_full_id() == l]
            ser = [res["CA"] for res in ser]

            ger = [s for l in ids2 for s in atoms2 if s.get_full_id() == l]
            ger = [res["CA"] for res in ger]

            rmsd, R, t = get_rmsd_and_RT(ser, ger, n_iter=None)

        if target_rmsd is not None and rmsd > target_rmsd and ko < len(z)-1: continue

        tar.get_pdb_from_list_of_atoms(write_pdb=True, path_output_pdb=tar.filepath, applyRt=(R, t), header=tar.remarks)

        print("Superposition of pdb", reference, "onto ", target, "with rmsd of", rmsd, "core size",len(ids1), "size1",len(atoms1),"size2",len(atoms2))
        return rmsd


def get_atoms_distance(in_a, in_b):
    """
    Return the euclidean distance between the two 3d coordinates
    :param in_a: 3d coord a
    :type in_a: numpy.array
    :param in_b: 3d coord b
    :type in_b: numpy.array
    :return d,D: distance and vector distance
    :rtype d,D: (float,numpy.array)
    """

    D = in_a - in_b
    d = np.sqrt((D ** 2).sum(axis=0))
    return d, D

def euclidean_distance(u ,v):
    dist = norm(u - v)
    return dist

def extract_single_components_from_solution(solutionpdb):
    global SEPARATOR_FOR_MULTI_ENSEMBLES_PDB

    allread = open(solutionpdb, "r").read()
    singles = allread.split(SEPARATOR_FOR_MULTI_ENSEMBLES_PDB)
    singles = [s.strip() for s in singles if len(s.strip())>0]
    basedir = os.path.split(solutionpdb)[0]
    returnlist = []
    for i, single in enumerate(singles):
        a = os.path.join(basedir,os.path.basename(solutionpdb)[:-4]+"__"+str(i)+".pdb")
        with open(a, "w") as f: f.write(single)
        c = PDB(a, fast=True).count_residues_with_occupancy_larger_than(0.1)
        if c > 0: returnlist.append(a)
    return returnlist

def get_distance(res1, res2, swap=True):
    try:
        resaN = res2["N"]
        prevResC = res1["C"]
    except:
        return 100.0

    #checkCont = np.sqrt(((resaN.get_coord() - prevResC.get_coord()) ** 2).sum(axis=0))
    checkCont = resaN-prevResC
    if swap:
        t = get_distance(res2, res1, swap=False)
        checkCont = min([checkCont, t])
    return checkCont

def check_continuity(res1, res2, swap=True, combine_with_seqid=True, seqid_jump=1,verbose=False):
    try:
        resaN = res2["N"]
        prevResC = res1["C"]
    except:
        return False

    #checkCont = np.sqrt(((resaN.get_coord() - prevResC.get_coord()) ** 2).sum(axis=0))
    checkCont = resaN-prevResC

    id = abs(resaN.get_full_id()[3][1]-prevResC.get_full_id()[3][1])
    if not combine_with_seqid: id = -1

    if verbose:
        print("CHECKING", resaN.get_full_id(), prevResC.get_full_id(), checkCont)

    if checkCont <= 20 and id <= seqid_jump:
        return True
    else:
        if swap:
            return check_continuity(res2, res1, combine_with_seqid=combine_with_seqid, seqid_jump=seqid_jump, swap=False)
        else:
            return False

def get_contigous_spans(list_residues):
    spans = [[]]
    list_residues = sorted(list_residues, key=lambda x: x.get_full_id())

    for i in range(len(list_residues)-1):
        if len(spans[-1]) == 0: spans[-1].append(list_residues[i])

        if check_continuity(list_residues[i], list_residues[i+1], combine_with_seqid=True, seqid_jump=5, swap=True):
            spans[-1].append(list_residues[i+1])
        else:
            print(list_residues[i].get_full_id(),"--",list_residues[i+1].get_full_id(), get_distance(list_residues[i], list_residues[i+1], swap=True))
            spans.append([list_residues[i+1]])

    return spans

def get_helix_line(idnumber, idname, restartname, chainstart, restartnumber, resicodestart, resendname, chainend,
                   resendnumber, resicodeend, typehelix, comment, lenhelix):
    HELIX_FORMAT_STRING = "{:<6s} {:>3d} {:>3s} {:>3s} {:1s} {:>4d}{:1s} {:>3s} {:1s} {:>4d}{:1s}{:>2d}{:>30s} {:>5d}"
    return HELIX_FORMAT_STRING.format("HELIX", idnumber, idname, restartname, chainstart, restartnumber, resicodestart,
                                      resendname, chainend, resendnumber, resicodeend, typehelix, comment,
                                      lenhelix) + "\n"


def get_sheet_line(idnumber, idnamesheet, nofstrandsinsheet, restartname, chainstart, restartnumber, resicodestart,
                   resendname, chainend, resendnumber, resicodeend, sense):
    SHEET_FORMAT_STRING = "{:<6s} {:>3d} {:>3s}{:>2d} {:>3s} {:1s}{:>4d}{:1s} {:>3s} {:1s}{:>4d}{:1s}{:>2d} {:>4s}{:>3s} {:1s}{:>4s}{:1s} {:>4s}{:>3s} {:1s}{:>4s}{:1s}"
    return SHEET_FORMAT_STRING.format("SHEET", idnumber, idnamesheet, nofstrandsinsheet, restartname, chainstart,
                                      restartnumber, resicodestart, resendname, chainend, resendnumber, resicodeend,
                                      sense, "", "", "", "", "", "", "", "", "", "") + "\n"


def get_atom_line(atom, element, hetfield, segid, atom_number, resname, resseq, icode, chain_id, normalize=False,
                  bfactorNor=25.00, charge=" ", applyRt=None):
    ATOM_FORMAT_STRING = "%s%6i %-4s%c%3s %c%4i%c   %8.3f%8.3f%8.3f%6.2f%6.2f      %4s%2s%2s\n"

    if isinstance(resseq, string_types):
        resseq = int(str(resseq[:-1]))
        icode = str(resseq)[-1]
    else:
        resseq = int(resseq)
        icode = u" "

    if hetfield != " ":
        record_type = "HETATM"
    else:
        record_type = "ATOM "

    element = element.strip().upper()
    # print "ELEMENT value was: ",type(element),element
    element = element[0]
    # print "ELEMENT value is: ",type(element),element

    name = atom.get_fullname()
    altloc = atom.get_altloc()
    if applyRt is not None:
        coo = atom.get_coord()
        voo = copy.deepcopy(coo)
        voo = transform(voo, applyRt[0], applyRt[1])
        x, y, z = voo
    else:
        x, y, z = atom.get_coord()

    occupancy = atom.get_occupancy()
    if not normalize:
        bfactor = atom.get_bfactor()
    else:
        bfactor = bfactorNor
    args = (
    record_type, atom_number, name, altloc, resname, chain_id, resseq, icode, x, y, z, occupancy, bfactor, segid,
    element, charge)
    ala = ATOM_FORMAT_STRING % args

    if record_type == "HETATM":
        clu = ala.split()
        spaziBianchi = 5 - len(clu[1])
        stri = "HETATM"
        for ulu in range(spaziBianchi):
            stri += " "
        stri += clu[1]
        ala = stri + ala[12:]
    return ala

def generate_single_ensemble_model_from_superposed_pdbs(directory, path_file, get_atoms_from_pdbs_list=[], use_remarks={}):
    #print("Creating ensemble from", directory, "saved to", path_file)
    #print("Remarks saved:", use_remarks)
    gi = 1
    remarks_voyager = {}
    with open(path_file, "w") as f:
        for root, subFolders, files in os.walk(directory):
            for fileu in files:
                pdbf = os.path.join(root, fileu)
                if pdbf.endswith(".pdb"):
                    atoms = ""
                    if len(get_atoms_from_pdbs_list) > 0:
                        for pd in get_atoms_from_pdbs_list:
                            #print("FILE TO MERGE---",pd,pdbf,os.path.basename(pd)[:-4].lower(),fileu[:-4].lower())
                            if os.path.basename(pd)[:-4].lower() == fileu[:-4].lower():
                                use_remarks[os.path.basename(pdbf)] = ""
                                remarks_voyager[os.path.basename(pdbf)] = ""
                                with open(pd, "r") as ff:
                                    for line in ff.readlines():
                                        if line.startswith("MODEL") or line.startswith("ENDMDL") or line.startswith("END"): continue
                                        if line.startswith("REMARK PHASER ENSEMBLE MODEL"):
                                            use_remarks[os.path.basename(pdbf)] += line
                                            continue
                                        elif line.startswith("REMARK"):
                                            remarks_voyager[os.path.basename(pdbf)] += line
                                        #print("ADDING",line)
                                        atoms += line
                                break

                    with open(pdbf, "r") as d:
                        #print("READING FROM PDB FILE",pdbf)
                        lines = d.readlines()
                        if len(lines) <= 0: continue
                        f.write("MODEL     " + str(gi) + "\n")
                        f.write("REMARK STRUCTURE NAME " + os.path.basename(pdbf) + "\n")
                        if use_remarks and os.path.basename(pdbf) in use_remarks and len(use_remarks[os.path.basename(pdbf)])>0:
                            line = use_remarks[os.path.basename(pdbf)]
                            #print(line)
                            e = line.split()
                            e[4] = str(gi)
                            line = " ".join(e) + "\n"
                            f.write(line)
                        if remarks_voyager:
                            line = remarks_voyager[os.path.basename(pdbf)]
                            f.write(line)

                        for line in lines:
                            if line.startswith("REMARK PHASER ENSEMBLE MODEL"):
                                # rms =
                                # newline = "REMARK PHASER ENSEMBLE MODEL "+str(gi)+" RMS "+str(rms)+"\n"
                                e = line.split()
                                e[4] = str(gi)
                                line = " ".join(e) + "\n"
                                f.write(line)
                            elif len(atoms) == 0 and not line.startswith("MODEL") and not line.startswith(
                                    "ENDMDL") and not line.startswith("END"):
                                f.write(line)
                        if len(atoms) > 0:
                            f.write(atoms)
                        f.write("\nENDMDL\n")
                        gi += 1
    return gi-1

def extract_single_models_from(ensemble, newdir, rename_with_original_pdb_name=False,
                                   rename_with_original_pdb_name_and_num="", sample=-1,
                                   recover_original_sequence=False, original_models=None,
                                   modifications=None, logger=None, add_remarks=None, get_tuple_with_model_id=False):
        # NOTE: This function relies on having the REMARK STRUCTURE NAME referred to each MODEL in the same list order.

        ids = []
        models_name1 = []
        models_name2 = []
        ids_1 = []
        ids_2 = []
        with open(ensemble, "r") as f:
            for line in f.readlines():
                if line.startswith("MODEL"):
                    ids_1.append(int(line.split()[1]))
                if line.startswith("REMARK STRUCTURE NAME"):
                    models_name1.append(line.split()[3])
                if line.startswith("REMARK PHASER ENSEMBLE MODEL"):
                    try:
                        if line.split()[14] + ".pdb" not in models_name2:
                            models_name2.append(line.split()[14] + ".pdb")
                            ids_2.append(int(line.split()[4]))
                    except:
                        pass

        models_name = []
        ids = []
        modelidf = ""
        if len(models_name2) > 0:
            models_name = models_name2
            ids = ids_2
            modelidf = "REMARK PHASER ENSEMBLE MODEL"
        else:
            models_name = models_name1
            ids = ids_1
            modelidf = "MODEL"

        list_pdbs = []

        if sample > 0:
            if len(ids) > sample:
                ids = np.random.choice(ids, size=sample, replace=False)
            else:
                ids = ids
        ids = list(sorted(set(ids)))

        #print("-------",models_name)
        for oo, id in enumerate(ids):
            if not rename_with_original_pdb_name:
                pdbname = os.path.join(newdir, str(id) + ".pdb")

            if len(rename_with_original_pdb_name_and_num) > 0:
                pdbname = os.path.join(newdir,
                                       str(id) + "." + str(os.path.basename(rename_with_original_pdb_name_and_num)))
            else:
                pdbname = os.path.join(newdir, models_name[oo])

            extract_subgroup_from(ensemble, [id], pdbname, modelidf,
                                       recover_original_sequence=recover_original_sequence,
                                       original_models=original_models, modifications=modifications, logger=logger, add_remarks=add_remarks)

            if not get_tuple_with_model_id:
                list_pdbs.append(pdbname)
            else:
                list_pdbs.append((id, pdbname))

        return list_pdbs

def extract_subgroup_from(ensemble, modelids, newpath, modelid_from, recover_original_sequence=False,
                              original_models=None, modifications=None, logger=None, add_remarks=None):
        text = ""
        collector = []
        rmsd = 0.0
        remark_line = ""

        with open(ensemble, "r") as f:
            startsave = False
            # iddic = {p: i + 1 for i, p in enumerate(sorted(modelids))}
            # lastm = ""

            for line in f.readlines():
                if modelid_from == "MODEL" and line.startswith("MODEL"):
                    if modelids == "all" or int(line.split()[1]) in modelids:
                        # lastm = str(iddic[int(line.split()[1])])
                        # line = "MODEL     " + str(iddic[int(line.split()[1])]) + "\n"
                        line = "MODEL     " + "SSSSSS" + "\n"
                        startsave = True
                        rmsd = 0.0
                        remark_line = copy.deepcopy(line)
                        text = ""

                if line.startswith("REMARK PHASER ENSEMBLE MODEL"):
                    e = line.split()
                    if modelid_from == "REMARK PHASER ENSEMBLE MODEL":
                        if modelids == "all" or int(e[4]) in modelids:
                            startsave = True
                            # e[4] = str(iddic[int(e[4])])
                            # line = "MODEL     " + str(e[4]) + "\n" + " ".join(e) + "\n"
                            e[4] = "SSSSSS"
                            line = "MODEL     " + "SSSSSS" + "\n" + " ".join(e) + "\n"
                            rmsd = float(e[6])
                            remark_line = copy.deepcopy(line)
                            text = ""
                    elif startsave:
                        # e[4] = lastm
                        # lastm = ""
                        e[4] = "SSSSSS"
                        line = " ".join(e) + "\n"
                        remark_line += copy.deepcopy(line)

                if startsave and not line.startswith("MODEL") and not line.startswith("REMARK PHASER ENSEMBLE MODEL"):
                    text += line

                if line.startswith("ENDMDL"):
                    if startsave: collector.append((rmsd, remark_line, text))
                    startsave = False

        allt = ""

        for i, tup in enumerate(sorted(collector, key=lambda x: x[0])):
            rmsd, remark_line, text = tup
            remark_line = remark_line.replace("SSSSSS", str(i + 1))
            allt += remark_line
            allt += text

        with open(newpath, "w") as f:
            f.write("REMARK VOYAGER CLUSTER FROM "+os.path.basename(ensemble)[:-4]+" ID_MODELS: ")
            f.write("_".join([str(x) for x in modelids])+"\n" if modelids != "all" else modelids+"\n")
            if add_remarks is not None:
                f.write("\n"+add_remarks+"\n")
            f.write(allt)

        if recover_original_sequence: recover_original_numbering_and_names([newpath], original_models,
                                                                                modifications,
                                                                                recover_numbering=True,
                                                                                recover_deletions=False, logger=logger)

def recover_original_numbering_and_names_old(pdblist, nosculpted, modifications, recover_deletions=False, recover_numbering=False, recover_sequence=True, logger=None):
        #print("========================================RECOVERING NAMES AND NUMBERS======================================================================")
        for pdb in pdblist:
            noscul = None
            usename = None
            for nosculpt in nosculpted:
                if os.path.basename(pdb) == os.path.basename(nosculpt):
                    noscul = nosculpt
                    usename = os.path.basename(pdb)
                    break
                #elif os.path.basename(pdb)[2:] == os.path.basename(nosculpt):
                q = os.path.basename(pdb)[:-4].split("-")
                #print(q,os.path.basename(nosculpt).split("-")[-1].split("."))

                #TODO: The elif conditions looks strange to me now
                #But I do not wnat to delete them until I did not test against everything
                #In case there is something I am not remembering.
                if os.path.basename(pdb) == os.path.basename(nosculpt):
                    noscul = nosculpt
                    usename = os.path.basename(nosculpt)
                    break
                elif len(q)==1 and q[0] == os.path.basename(nosculpt)[:4]:
                    noscul = nosculpt
                    usename = os.path.basename(nosculpt)
                    break
                elif len(q)>1 and q[0] == os.path.basename(nosculpt)[:4] and q[1].split(".")[0] == os.path.basename(nosculpt).split("-")[-1].split(".")[0]:
                    noscul = nosculpt
                    usename = os.path.basename(nosculpt)
                    break
            if logger is None:
                print("Recovering:", pdb, noscul)
            else:
                logger.info("Recovering:"+pdb+" "+noscul)
            # shutil.copyfile(pdb,"medora.pdb")
            p1 = PDB(pdb)
            p2 = PDB(noscul)

            residues_to_retrieve = set([])
            converted = set([])
            correspondance = {}
            deleted = set([])
            # renames = {}
            # resids = {}
            savedseq = p1.sequenceAAA
            zmap = {c: {iii:rrr for (iii,rrr) in m} for c, m in p1.mapbychain.items()}

            for instruction in modifications[usename]:
                #if "2i0k" in os.path.basename(p1.filepath): print(instruction)
                # print(instruction)
                if instruction[0].startswith("delete"):
                    try:
                        residues_to_retrieve.add((int(instruction[1]), u' '))
                        deleted.add((int(instruction[1]), u' '))
                    except:
                        residues_to_retrieve.add((int(instruction[1][:-1]), instruction[1][-1]))
                        deleted.add((int(instruction[1][:-1]), instruction[1][-1]))
                elif instruction[0].startswith("set resname"):
                    # renames[int(instruction[1][1])] = instruction[1][0]
                    if int(instruction[1][1]) not in converted:
                        try:
                            residues_to_retrieve.add((int(instruction[1][1]),u' '))
                            correspondance[(int(instruction[1][1]),' ')] = (int(instruction[1][1]),u' ')
                        except:
                            residues_to_retrieve.add((int(instruction[1][1][:-1]), instruction[1][1][-1]))
                            correspondance[(int(instruction[1][1][:-1]), instruction[1][1][-1])] = (int(instruction[1][1]), instruction[1][1][-1])
                elif instruction[0].startswith("set resseq"):
                    # resids[int(instruction[0].split()[3][1:-1])] = instruction[1]
                    if instruction[1][-1] == " ":
                        residues_to_retrieve.add((int(instruction[1]), u' '))
                        correspondance[(int(instruction[1]), u' ')] = (int(instruction[0].split()[3][1:-1]), u' ')
                    else:
                        residues_to_retrieve.add((int(instruction[1][:-1]),instruction[1][-1]))
                        #correspondance[(int(instruction[1][:-1]),instruction[1][-1])] = (int(instruction[0].split()[3][1:-2]), instruction[0].split()[3][-2])
                        correspondance[(int(instruction[1][:-1]),instruction[1][-1])] = (int(instruction[0].split()[3][1:-1]),  u' ')

                    converted.add(int(instruction[0].split()[3][1:-1]))

            id1 = {(int(r.get_id()[1]),r.get_id()[2]): r for r in p1.get_list_of_residues()}
            # print("----ID1----",sorted(id1.keys()))
            id2 = {(int(r.get_id()[1]),r.get_id()[2]): r for r in p2.get_list_of_residues()}
            # print("----ID2----",sorted(id2.keys()))
            # print("----CORRESPONDANCE----")
            # for kk in sorted(correspondance.keys()):
            #     print(kk,correspondance[kk])
            # print("----RESIDUES TO RETRIEVE----",sorted(residues_to_retrieve))

            id12 = {id1[correspondance[r]]: id2[r] for r in residues_to_retrieve if
                    r in correspondance.keys() and correspondance[r] in id1.keys()}
            iddel = [id2[r] for r in deleted]

            tmap = zmap.copy()

            # for kk in sorted(id12.keys()):
            #      vv = id12[kk]
            #      print(pdb,"+++",kk,"-->",vv)

            while 1:
                iterative_assignments = {}
                for i1 in sorted(id12.keys()):
                    i2 = id12[i1]
                    #print(pdb,"+++",i1,"-->",i2)

                    try:
                        if i1.get_full_id()[2] not in tmap: tmap[i1.get_full_id()[2]] = {}
                        nn1 = i1.get_full_id()
                        rli = (i2.id[0], i2.id[1]+5000, i2.id[2])
                        if recover_numbering: i1.id = rli
                        #print(pdb,noscul,"**RENAME**",i1.id, rli, "--", i1.resname, i2.resname)
                        if recover_sequence: i1.resname = i2.resname
                        #print(i1.resname, i2.resname)
                        nn2 = PDB.AADICMAP[i2.resname]
                        #print("i2.resname",i2.resname,"nn2",nn2)
                        tmap[i1.get_full_id()[2]][nn1] = nn2
                    except:
                        # print(sys.exc_info())
                        # traceback.print_exc(file=sys.stdout)
                        iterative_assignments[i1] = i2
                        print("did not work",i1,i2)

                if len(iterative_assignments.keys()) == 0: break
                if [gg.id for gg in id12.keys()] == [gg.id for gg in iterative_assignments.keys()]:
                    if logger is None:
                        print("ERROR: Cannot recover correctly the following residues:")
                        print(id12)
                        print(iterative_assignments)
                        for key, item in iterative_assignments.items():
                            print(key, item)
                    else:
                        logger.info("ERROR: Cannot recover correctly the following residues:")
                        logger.info(str(id12))
                        logger.info(str(iterative_assignments))
                        for key, item in iterative_assignments.items():
                            logger.info(str(key)+" "+str(item))
                    raise Exception("ERROR: Cannot recover correctly the sequence")

                id12 = copy.deepcopy(iterative_assignments)


            atoms = []
            if recover_deletions and len(iddel) > 0:
                ser = []
                ger = []
                for k, v in id12.items():
                    ser.append((k["N"], k["CA"], k["C"], k["O"]))
                    ger.append((v["N"], v["CA"], v["C"], v["O"]))
                ser = [atom for group in ser for atom in group]
                ger = [atom for group in ger for atom in group]

                rmsd, R, t = get_rmsd_and_RT(ser, ger)
                ratom = transform_atoms(p2.get_list_of_atoms(), R, t)
                # NOTE: To recover correctly the deletions it is fundamental to first superpose p2 on top of p1 to write right coordinates for the atoms

                for de in iddel:
                    atoms += [a for a in de]

            # for m in p1.structure:
            #     for c in m:
            #         for r in c:
            #             print(r.get_full_id(), r.get_resname())
            #
            # print("----------")

            for m in p1.structure:
                for c in m:
                    for r in c:
                        if r.id[1]>5000: r.id = (r.id[0], r.id[1]-5000, r.id[2])

            # print("len(atoms)",len(atoms))
            # p1.get_pdb_from_list_of_atoms(write_pdb=True, path_output_pdb=p1.filepath+"copy.pdb", header=p1.remarks, add_this_list_of_atoms=atoms)
            rems1 = "REMARK VOYAGER SEQMAP TARGET "
            rems2 = "REMARK VOYAGER SEQMAP ORIGIN "
            rems3 = "REMARK VOYAGER SEQMAP SCULPT "+savedseq+"\n"
            for chain, map1 in zmap.items():
                #rems1 += " "+chain+" : "
                #rems2 += " "+chain+" : "
                map2 = tmap[chain]
                for resid in sorted(map1.keys()):
                    aa = map1[resid]
                    rems1 += aa if len(aa)==1 else aa+"*"
                    if resid in map2: rems2 += map2[resid]+"*" if len(map2[resid])>1 else map2[resid]
                    else: rems2 += "-"
            rems1 += "\n"
            rems2 = rems2[:-1] #eliminating last *
            rems2 += "\n"

            p1.get_pdb_from_list_of_atoms(write_pdb=True, path_output_pdb=p1.filepath, header=p1.remarks+"\n"+rems1+rems2+rems3,
                                          add_this_list_of_atoms=atoms)
            # if "2i0k" in os.path.basename(p1.filepath): quit()
            print("Recovered file written at", p1.filepath)


def recover_original_numbering_and_names(pdblist, nosculpted, modifications, recover_deletions=False,
                                         recover_numbering=False, recover_sequence=True, logger=None):
    # print("========================================RECOVERING NAMES AND NUMBERS======================================================================")
    #TODO: It should be possible to get rid completely of the SEQMAP records from everywhere in the code
    retmap = {}
    for pdb in pdblist:
        noscul = None
        usename = None
        for nosculpt in nosculpted:
            if os.path.basename(pdb) == os.path.basename(nosculpt):
                noscul = nosculpt
                usename = os.path.basename(pdb)
                break
            # elif os.path.basename(pdb)[2:] == os.path.basename(nosculpt):
            q = os.path.basename(pdb)[:-4].split("-")
            # print(q,os.path.basename(nosculpt).split("-")[-1].split("."))

            # TODO: The elif conditions looks strange to me now
            # But I do not wnat to delete them until I did not test against everything
            # In case there is something I am not remembering.
            if os.path.basename(pdb) == os.path.basename(nosculpt):
                noscul = nosculpt
                usename = os.path.basename(nosculpt)
                break
            elif len(q) == 1 and q[0] == os.path.basename(nosculpt)[:4]:
                noscul = nosculpt
                usename = os.path.basename(nosculpt)
                break
            elif len(q) > 1 and q[0] == os.path.basename(nosculpt)[:4] and q[1].split(".")[0] == \
                    os.path.basename(nosculpt).split("-")[-1].split(".")[0]:
                noscul = nosculpt
                usename = os.path.basename(nosculpt)
                break
        if logger is None:
            print("Recovering:", pdb, noscul)
        else:
            logger.info("Recovering:" + pdb + " " + noscul)
        # shutil.copyfile(pdb,"medora.pdb")
        p1 = PDB(pdb)
        p2 = PDB(noscul)
        p3 = PDB(pdb)

        residues_to_retrieve = set([])
        converted = set([])
        correspondance = {}
        deleted = set([])
        # renames = {}
        # resids = {}
        savedseq = p1.sequenceAAA
        zmap = {c: {iii: rrr for (iii, rrr) in m} for c, m in p1.mapbychain.items()}

        for instruction in modifications[usename]:
            # if "2i0k" in os.path.basename(p1.filepath): print(instruction)
            # print(instruction)
            if instruction[0].startswith("delete"):
                try:
                    residues_to_retrieve.add((int(instruction[1]), u' '))
                    deleted.add((int(instruction[1]), u' '))
                except:
                    residues_to_retrieve.add((int(instruction[1][:-1]), instruction[1][-1]))
                    deleted.add((int(instruction[1][:-1]), instruction[1][-1]))
            elif instruction[0].startswith("set resname"):
                # renames[int(instruction[1][1])] = instruction[1][0]
                if int(instruction[1][1]) not in converted:
                    try:
                        residues_to_retrieve.add((int(instruction[1][1]), u' '))
                        correspondance[(int(instruction[1][1]), ' ')] = (int(instruction[1][1]), u' ')
                    except:
                        residues_to_retrieve.add((int(instruction[1][1][:-1]), instruction[1][1][-1]))
                        correspondance[(int(instruction[1][1][:-1]), instruction[1][1][-1])] = (
                        int(instruction[1][1]), instruction[1][1][-1])
            elif instruction[0].startswith("set resseq"):
                # resids[int(instruction[0].split()[3][1:-1])] = instruction[1]
                if instruction[1][-1] == " ":
                    residues_to_retrieve.add((int(instruction[1]), u' '))
                    correspondance[(int(instruction[1]), u' ')] = (int(instruction[0].split()[3][1:-1]), u' ')
                else:
                    residues_to_retrieve.add((int(instruction[1][:-1]), instruction[1][-1]))
                    # correspondance[(int(instruction[1][:-1]),instruction[1][-1])] = (int(instruction[0].split()[3][1:-2]), instruction[0].split()[3][-2])
                    correspondance[(int(instruction[1][:-1]), instruction[1][-1])] = (
                    int(instruction[0].split()[3][1:-1]), u' ')

                converted.add(int(instruction[0].split()[3][1:-1]))

        id1 = {(int(r.get_id()[1]), r.get_id()[2]): r for r in p1.get_list_of_residues()}
        id2 = {(int(r.get_id()[1]), r.get_id()[2]): r for r in p2.get_list_of_residues()}
        id3 = {(int(r.get_id()[1]), r.get_id()[2]): r for r in p3.get_list_of_residues()}
        # print("----ID1----",sorted(id1.keys()))
        # print("----ID2----",sorted(id2.keys()))
        # print("----CORRESPONDANCE----")
        # for kk in sorted(correspondance.keys()):
        #     print(kk,correspondance[kk])
        # print("----RESIDUES TO RETRIEVE----",sorted(residues_to_retrieve))

        id12 = {id1[correspondance[r]]: id2[r] for r in residues_to_retrieve if
                r in correspondance.keys() and correspondance[r] in id1.keys()}
        id13 = {i1:i3 for i1 in sorted(id12.keys()) for i3 in p3.get_list_of_residues() if i3.get_full_id()==i1.get_full_id()}
        iddel = [id2[r] for r in deleted]

        tmap = zmap.copy()

        # for kk in sorted(id12.keys()):
        #      vv = id12[kk]
        #      print(pdb,"+++",kk,"-->",vv)

        while 1:
            iterative_assignments = {}
            for i1 in sorted(id12.keys()):
                i2 = id12[i1]
                i3 = id13[i1]
                #print(pdb,"+++",i1,"-->",i2,"-->",i3)

                try:
                    if i1.get_full_id()[2] not in tmap: tmap[i1.get_full_id()[2]] = {}
                    nn1 = i1.get_full_id()
                    rli = (i2.id[0], i2.id[1] + 5000, i2.id[2])
                    if recover_numbering: i1.id = rli
                    i3.id = rli
                    #print(pdb,noscul,"**RENAME**",i1.id, rli, "--", i1.resname, i2.resname)
                    if recover_sequence: i1.resname = i2.resname
                    i3.resname = i2.resname
                    #print(i1.resname, i2.resname)
                    nn2 = PDB.AADICMAP[i2.resname]
                    #print("i2.resname",i2.resname,"nn2",nn2)
                    tmap[i1.get_full_id()[2]][nn1] = nn2
                except:
                    # print(sys.exc_info())
                    # traceback.print_exc(file=sys.stdout)
                    iterative_assignments[i1] = i2
                    print("did not work", i1, i2)

            if len(iterative_assignments.keys()) == 0: break
            if [gg.id for gg in id12.keys()] == [gg.id for gg in iterative_assignments.keys()]:
                if logger is None:
                    print("ERROR: Cannot recover correctly the following residues:")
                    print(id12)
                    print(iterative_assignments)
                    for key, item in iterative_assignments.items():
                        print(key, item)
                else:
                    logger.info("ERROR: Cannot recover correctly the following residues:")
                    logger.info(str(id12))
                    logger.info(str(iterative_assignments))
                    for key, item in iterative_assignments.items():
                        logger.info(str(key) + " " + str(item))
                raise Exception("ERROR: Cannot recover correctly the sequence")

            id12 = copy.deepcopy(iterative_assignments)

        atoms = []
        if recover_deletions and len(iddel) > 0:
            ser = []
            ger = []
            for k, v in id12.items():
                ser.append((k["N"], k["CA"], k["C"], k["O"]))
                ger.append((v["N"], v["CA"], v["C"], v["O"]))
            ser = [atom for group in ser for atom in group]
            ger = [atom for group in ger for atom in group]

            rmsd, R, t = get_rmsd_and_RT(ser, ger)
            ratom = transform_atoms(p2.get_list_of_atoms(), R, t)
            # NOTE: To recover correctly the deletions it is fundamental to first superpose p2 on top of p1 to write right coordinates for the atoms

            for de in iddel:
                atoms += [a for a in de]

        # for m in p1.structure:
        #     for c in m:
        #         for r in c:
        #             print(r.get_full_id(), r.get_resname())
        #
        # print("----------")

        for m in p1.structure:
            for c in m:
                for r in c:
                    try:
                        if r.id[1] > 5000: r.id = (r.id[0], r.id[1] - 5000, r.id[2])
                    except:
                        print("Cannot replace ",r.id,"as ",(r.id[0], r.id[1] - 5000, r.id[2]),"already exist")


        for m in p3.structure:
            for c in m:
                for r in c:
                    try:
                        if r.id[1] > 5000: r.id = (r.id[0], r.id[1] - 5000, r.id[2])
                    except:
                        print("Cannot replace ",r.id,"as ",(r.id[0], r.id[1] - 5000, r.id[2]),"already exist")



        # print("len(atoms)",len(atoms))
        # p1.get_pdb_from_list_of_atoms(write_pdb=True, path_output_pdb=p1.filepath+"copy.pdb", header=p1.remarks, add_this_list_of_atoms=atoms)
        rems1 = ""
        rems2 = ""
        for chain, map1 in zmap.items():
            # rems1 += " "+chain+" : "
            # rems2 += " "+chain+" : "
            map2 = tmap[chain]
            for resid in sorted(map1.keys()):
                aa = map1[resid]
                rems1 += aa if len(aa) == 1 else aa + "*"
                if resid in map2:
                    rems2 += map2[resid] + "*" if len(map2[resid]) > 1 else map2[resid]
                else:
                    rems2 += "-"

        if rems1[-1] == "*": rems1 = rems1[:-1]
        if rems2[-1] == "*": rems2 = rems2[:-1]

        p3.get_pdb_from_list_of_atoms(write_pdb=True, path_output_pdb=p3.filepath,
                                      header=p3.remarks,
                                      add_this_list_of_atoms=atoms)
        p3 = PDB(p3.filepath)
        origin_seq = p3.sequenceAAA

        lire = [tuple(resi.get_full_id()[2:]) for model in p1.structure for chain in model for resi in chain]
        sri = savedseq.split("*")
        orig = origin_seq.split("*")
        # print("==============SCULPTED=============")
        # print(savedseq)
        # print("===================================")
        # print("==============ORIGINAL=============")
        # print(origin_seq)
        # print("===================================")
        assert(len(lire)==len(sri))
        assert(len(lire)==len(orig))
        retmap[os.path.basename(p1.filepath)[:-4]] = {"origdict":{lire[x]:orig[x] for x in range(len(lire))},"origin":rems2,"target":rems1,"sculpt":{lire[x]:sri[x] for x in range(len(lire))}}

        p1.get_pdb_from_list_of_atoms(write_pdb=True, path_output_pdb=p1.filepath,
                                      header=p1.remarks,
                                      add_this_list_of_atoms=atoms)
        # if "2i0k" in os.path.basename(p1.filepath): quit()
        print("Recovered file written at", p1.filepath)

    return retmap


def get_special_position_atom_lines(list_of_coords, RTapply=None):
    text = ""
    for i, coords in enumerate(list_of_coords):
        if RTapply is not None:
            R, t = RTapply
            text += get_atom_line(Atom(coords, fullname="O"), "O", " ", str(i), i, "ALA", i, " ", "T", normalize=True)
            coords = dot(coords, R.T) - t
        text += get_atom_line(Atom(coords), "CA", " ", str(i), i, "ALA", i, " ", "Z", normalize=True)

    return text


def distance_sq(X, Y):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com
    :param X:
    :param Y:
    :return:
    """
    return ((np.asarray(X) - Y) ** 2).sum(-1)


def get_TM_SCORE(size, hashes):
    #### TM-score formula http://www.blopig.com/blog/2017/01/tm-score/
    d0 = 1.24 * np.cbrt(size - 15) - 1.8
    TM_Score = (1.0 / size) * (np.sum([1.0 / (1.0 + (di[0] / d0) ** 2) for di in hashes.values()]))
    return TM_Score


def wfit(X, Y, w):
    ## center configurations

    norm = sum(w)
    x = dot(w, X) / norm
    y = dot(w, Y) / norm

    ## SVD of correlation matrix

    V, _L, U = svd(dot((X - x).T * w, Y - y))

    ## calculate rotation and translation

    R = dot(V, U)

    if det(R) < 0.:
        U[2] *= -1
        R = dot(V, U)

    t = x - dot(R, y)

    return R, t


def xfit(X, Y, n_iter=10, seed=False, full_output=False):
    """
    Maximum likelihood superposition of two coordinate arrays. Works similar
    to U{Theseus<http://theseus3d.org>} and to L{bfit}.

    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com
    :param X:
    :param Y:
    :param n_iter:
    :param seed:
    :param full_output:
    :return:
    """
    if seed:
        R, t = np.identity(3), np.zeros(3)
    else:
        R, t = fit(X, Y)

    for _ in range(n_iter):
        data = distance_sq(X, transform(Y, R, t))
        scales = 1.0 / data.clip(1e-9)
        R, t = wfit(X, Y, scales)

    if full_output:
        return (R, t), scales
    else:
        return (R, t)


def fit(X, Y):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com

    :param X:
    :param Y:
    :return:
    """

    ## center configurations

    x = X.mean(0)
    y = Y.mean(0)

    ## SVD of correlation matrix

    V, _L, U = svd(dot((X - x).T, Y - y))

    ## calculate rotation and translation

    R = dot(V, U)

    if det(R) < 0.:
        U[-1] *= -1
        R = dot(V, U)

    t = x - dot(R, y)

    return R, t


def transform_atoms(atoms, R, t, s=None, invert=False):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com
    :param atoms:
    :param R:
    :param t:
    :param s:
    :param invert:
    :return:
    """
    Y = []

    for atom in atoms:
        Y.append(atom.get_coord())

    Y = transform(Y, R, t, s=s, invert=invert)

    for i in range(len(Y)):
        y = Y[i]
        atoms[i].set_coord(y)

    return atoms


def transform(Y, R, t, s=None, invert=False):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com
    :param Y:
    :param R:
    :param t:
    :param s:
    :param invert:
    :return:
    """
    if invert:
        x = np.dot(Y - t, R)
        if s is not None:
            s = 1. / s
    else:
        x = np.dot(Y, R.T) + t
    if s is not None:
        x *= s
    return x


def get_pseudo_rmsd(X, Y):
    summatory = 0
    distance_hash = [[distance_sq(x, y) for x in X] for y in Y]
    for subli in distance_hash:
        summatory = summatory + min(subli)
    n = len(distance_hash)
    rmsd = np.sqrt(summatory / n)
    return rmsd


def fit_wellordered(X, Y, n_iter=None, n_stdv=2, tol_rmsd=.5, tol_stdv=0.05, full_output=False):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com
    :param X:
    :param Y:
    :param n_iter:
    :param n_stdv:
    :param tol_rmsd:
    :param tol_stdv:
    :param full_output:
    :return:
    """

    from numpy import ones, compress, dot, sqrt, sum, nonzero, std, average

    rmsd_list = []

    rmsd_old = 0.
    stdv_old = 0.

    n = 0
    converged = False

    mask = ones(X.shape[0])
    while not converged:
        ## find transformation for best match
        if n_iter == None or n_iter <= 0:
            R, t = fit(compress(mask, X, 0), compress(mask, Y, 0))
        else:
            R, t = xfit(compress(mask, X, 0), compress(mask, Y, 0), n_iter=n_iter)

        ## calculate RMSD profile

        d = sqrt(sum((X - dot(Y, R.T) - t) ** 2, 1))

        ## calculate rmsd and stdv

        rmsd = sqrt(average(compress(mask, d) ** 2, 0))
        # if rmsd < 1.0:
        #    print("D",d,len(X),len(Y),mask)

        stdv = std(compress(mask, d))
        # print "Best rmsd",best_rmsd

        ## check conditions for convergence

        if stdv < 1e-10: break

        d_rmsd = abs(rmsd - rmsd_old)
        d_stdv = abs(1 - stdv_old / stdv)

        if d_rmsd < tol_rmsd:
            if d_stdv < tol_stdv:
                converged = 1
            else:
                stdv_old = stdv
        else:
            rmsd_old = rmsd
            stdv_old = stdv

        ## store result

        perc = average(1. * mask)

        # print ("N is",n,"n_iter is",n_iter,"rmsd_list",rmsd_list,"mask",mask, "lens",len(X), len(Y), "perc",perc )
        # if perc < 0.96: break
        ###if n_iter == 0: break

        ## throw out non-matching rows
        new_mask = mask * (d < rmsd + n_stdv * stdv)
        outliers = nonzero(mask - new_mask)
        rmsd_list.append([perc, rmsd, outliers])
        mask = new_mask

        perc = average(1. * mask)
        if n_iter is None or n_iter <= 0 or n >= n_iter: break

        n += 1

    # print("Iter number: ",n)

    if full_output:
        return (R, t), rmsd_list, rmsd
    else:
        return (R, t)


def get_rmsd_and_RT_from_coords(atom_list_a, atom_list_b, transform=False, n_iter=5, full_ca=[], get_full_list_rmsd=False):
    """
        :author: Dr. Massimo Domenico Sammito
        :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com
        :param ca_list1:
        :type ca_list1:
        :param ca_list2:
        :type ca_list2:
        :param full_ca:
        :type full_ca:
        :return:
        :rtype:
    """
    atom_list_a = np.asarray(atom_list_a)
    atom_list_b = np.asarray(atom_list_b)
    transf, rmsd_list, rmsd = fit_wellordered(atom_list_a, atom_list_b, n_iter=n_iter, full_output=True,
                                              n_stdv=2, tol_rmsd=0.005, tol_stdv=0.0005)
    R, t = transf
    # print("---",full_ca[0].get_full_id(),full_ca[0].get_coord())
    if transform:
        allAtoms = transform_atoms(full_ca, R, t)
        # print(ca_list1[0].get_full_id(),ca_list2[0].get_full_id(),rmsd,allAtoms[0].get_full_id(),allAtoms[0].get_coord())

        if get_full_list_rmsd:
            return allAtoms, rmsd, rmsd_list, R, t
        else:
            return allAtoms, rmsd, R, t
    else:
        if get_full_list_rmsd:
            return rmsd, rmsd_list, R, t
        else:
            return rmsd, R, t


def get_rmsd_and_RT(ca_list1, ca_list2, transform=False, n_iter=5, full_ca=[], get_full_list_rmsd=False):
    atom_list_a = [atom.get_coord() for atom in ca_list1]
    atom_list_b = [atom.get_coord() for atom in ca_list2]
    return get_rmsd_and_RT_from_coords(atom_list_a, atom_list_b, n_iter=n_iter, transform=transform, full_ca=full_ca, get_full_list_rmsd=get_full_list_rmsd)


def iterative_core_optimization_based_on_distance(atoms_ref, atoms_targ, max_rmsd=1.0, last_rmsd=1.5):
    at = np.array([a.get_coord() for a in atoms_targ])
    ar = np.array([a.get_coord() for a in atoms_ref])

    rmsd2, size, hashes, Final_R, Final_t = None, None, None, None, None
    for i in range(3):
        rmsd1, R1, t1 = get_rmsd_and_RT_from_coords(ar, at)
        Final_R = R1 if Final_R is None else np.dot(Final_R, R1)
        Final_t = t1 if Final_t is None else Final_t + t1

        at = transform(at, R1, t1)
        rmsd2, size, hashes = search_best_core_and_find_rmsd(ar, at, max_rmsd=max_rmsd if i == 0 else last_rmsd,
                                                             return_hashtable=True, allVSall=False)
        # print("CYCLE",i,"RMSD1",rmsd1,"RMSD2",rmsd2)
        ar = [e for ie, e in enumerate(ar) if ie in hashes.keys()]
        at = [e for ie, e in enumerate(at) if ie in hashes.keys()]

    return Final_R, Final_t, rmsd2, size, hashes


def search_best_core_and_find_rmsd(list1, list2, max_rmsd=1.0, return_hashtable=False, allVSall=True):
    distance_hash2 = None
    if allVSall:
        distance_hash = {ica1: [(distance_sq(ca1, ca2), ica2) for ica2, ca2 in enumerate(list2)] for ica1, ca1 in
                         enumerate(list1)}
        distance_hash2 = {key: sorted(distance_hash[key], key=lambda x: x[0]) for key in distance_hash}
    else:
        distance_hash2 = {ica1: [(distance_sq(ca1, list2[ica1]), ica1)] for ica1, ca1 in enumerate(list1)}

    used = set([])
    todel = set([])

    for key in sorted(distance_hash2.keys(), key=lambda x: distance_hash2[x][0][0]):
        found = False
        for ele in distance_hash2[key]:
            if ele[1] not in used:
                used.add(ele[1])
                distance_hash2[key] = ele
                found = True
                break
        if not found:
            todel.add(key)
            # print("Impossible it should have been found something")
            # sys.exit(1)

    distance_hash2 = {key: item for key, item in distance_hash2.items() if
                      key not in todel and (max_rmsd is None or item[0] <= max_rmsd ** 2)}

    summatory = 0.0
    # arrayo = np.ones((len(list1),len(list2)))*100.0
    for key, item in distance_hash2.items():
        summatory = summatory + item[0]
        # arrayo[key][item[1]] = item[0]

    n = len(distance_hash2)
    if n > 0:
        rmsd = np.sqrt(summatory / n)
        if return_hashtable:
            distance_hash2 = {key: (np.sqrt(item[0]), item[1]) for key, item in distance_hash2.items()}
            return rmsd, n, distance_hash2
        else:
            return rmsd, n
    else:
        if return_hashtable:
            distance_hash2 = {key: (np.sqrt(item[0]), item[1]) for key, item in distance_hash2.items()}
            return 0.0, 0, distance_hash2
        else:
            return 0.0, 0


def get_CA_distance_dictionary(pdb_model1, pdb_model2, max_rmsd=0.5, last_rmsd=1.0, recompute_rmsd=True, cycles=3,
                               cycle=1, before_apply=None, get_superposed_atoms=False, force_reference_residues=False,
                               data_tuple=None):
    list_CA1 = pdb_model1
    list_CA2 = pdb_model2

    if cycle == cycles:
        max_rmsd = last_rmsd
    # print("PDBMODEL1",type(pdb_model1),"PDBMODEL2",type(pdb_model2),"len(1)",len(list_CA1),"len(2)",len(list_CA2),"type(1)",type(list_CA1),"type(2)",type(list_CA2))
    rmsd = 100

    if before_apply is not None and (isinstance(before_apply, tuple) or isinstance(before_apply, list)):
        R, t = before_apply
        # print("---",full_ca[0].get_full_id(),full_ca[0].get_coord())
        # NOTE: it is essential that R,t corresponds to the transformation of the pdb_model2 onto pdb_mopdel1
        allatoms_ana = [atm for res in list_CA2 for atm in res.get_parent()]
        list_CA2 = [atom for atom in transform_atoms(allatoms_ana, R, t) if atom.get_name().lower() == "ca"]
    elif before_apply is not None and isinstance(before_apply, str) and before_apply == "automatic":
        allatoms_ana = [atm for res in list_CA2 for atm in res.get_parent()]
        list_CA2, rmsd, R, t = get_rmsd_and_RT(list_CA1, list_CA2, allatoms_ana)
        list_CA2 = [atom for atom in allatoms_ana if atom.get_name().lower() == "ca"]
        print("rmsd initial using all atoms is", rmsd)

    distance_hash = {ca1.get_full_id(): [
        (euclidean_distance(ca1.get_coord(), ca2.get_coord()), ca2.get_full_id(), ca1, ca2) for ca2 in
        list_CA2] for ca1 in list_CA1}

    if not force_reference_residues or data_tuple is None:
        distance_hash2 = {key: sorted(distance_hash[key], key=lambda x: x[0])[0] for key in distance_hash if
                          (sorted(distance_hash[key], key=lambda x: x[0])[0][0] <= max_rmsd) or (
                                      sorted(distance_hash[key], key=lambda x: x[0])[0][0] <= max_rmsd + 1.0 and
                                      (key[0], key[1], key[2], (key[3][0], key[3][1] - 1, key[3][2]),
                                       key[4]) in distance_hash and
                                      sorted(distance_hash[(
                                      key[0], key[1], key[2], (key[3][0], key[3][1] - 1, key[3][2]), key[4])],
                                             key=lambda x: x[0])[0][0] <= max_rmsd and
                                      (key[0], key[1], key[2], (key[3][0], key[3][1] + 1, key[3][2]),
                                       key[4]) in distance_hash and
                                      sorted(distance_hash[(
                                      key[0], key[1], key[2], (key[3][0], key[3][1] + 1, key[3][2]), key[4])],
                                             key=lambda x: x[0])[0][0] <= max_rmsd)}
        todelete = []
        for i, key in enumerate(sorted(distance_hash2.keys())):
            prev = False
            post = False
            if i > 0 and (key[0], key[1], key[2], (key[3][0], key[3][1] - 1, key[3][2]), key[4]) in distance_hash2:
                prev = True
            if i < len(distance_hash2.keys()) - 1 and (
                    key[0], key[1], key[2], (key[3][0], key[3][1] + 1, key[3][2]), key[4]) in distance_hash2:
                post = True

            if not (prev or post):
                todelete.append(key)

        for key in todelete:
            del distance_hash2[key]
    else:
        distance_hash2 = {key: sorted(distance_hash[key], key=lambda x: x[0]) for key in distance_hash if
                          (sorted(distance_hash[key], key=lambda x: x[0])[0][0] <= max_rmsd) or (
                                      sorted(distance_hash[key], key=lambda x: x[0])[0][0] <= max_rmsd + 1.0 and (
                              key[0], key[1], key[2], (key[3][0], key[3][1] - 1, key[3][2]),
                              key[4]) in distance_hash and
                                      sorted(distance_hash[(
                                      key[0], key[1], key[2], (key[3][0], key[3][1] - 1, key[3][2]), key[4])],
                                             key=lambda x: x[0])[0][0] <= max_rmsd and
                                      (key[0], key[1], key[2], (key[3][0], key[3][1] + 1, key[3][2]),
                                       key[4]) in distance_hash and
                                      sorted(distance_hash[(
                                      key[0], key[1], key[2], (key[3][0], key[3][1] + 1, key[3][2]), key[4])],
                                             key=lambda x: x[0])[0][0] <= max_rmsd)}

        # 1: Find the contact points between paired strands:
        (graph_ref, graph_targ, associations_list, map_reference, map_target) = data_tuple

        map_target = {k[2:4]: v for k, v in map_target.items()}
        map_reference = {k[2:4]: v for k, v in map_reference.items()}

        possibi = []
        for associations in associations_list:
            centers_ref = []
            corresp = []
            mins = []
            for fragr in graph_ref.vs:
                fragrl = [tuple(tr[2:4]) for tr in fragr["reslist"]]
                namer = fragr["name"]
                corre = associations[namer]
                lipol = []
                for key in distance_hash2.keys():
                    if key[2:4] in fragrl:
                        for q, value in enumerate(distance_hash2[key]):
                            if map_target[value[1][2:4]] == corre:
                                lipol.append((value, key[2:4]))
                                break
                lipo = sorted(lipol, key=lambda x: x[0][0])[0]
                diffe = [abs(lipol[c + 1][0][0] - lipol[c][0][0]) for c in range(len(lipol) - 1)]
                # print("DIFFE",diffe)
                where = fragrl.index(lipo[1])
                centers_ref.append(((0, where, len(fragrl)), fragr["reslist"][where]))
                corresp.append((centers_ref[-1], lipo[0]))
                mins.append(lipo[0][0])
            print("ASSO", associations)
            print("MINS", mins)
            possibi.append((sum(mins) / len(mins), associations, centers_ref, corresp))

        (score_possi, associations, centers_ref, corresp) = sorted(possibi, key=lambda x: x[0])[0]
        # centers_ref = [((0,round(int(len(frag["reslist"])/2)),len(frag["reslist"])),frag["reslist"][round(int(len(frag["reslist"])/2))]) for frag in graph_ref.vs]
        map_secstr_ref = {tuple(key[1:4]): frag.index for frag in graph_ref.vs for key in
                          [tuple(k[1:4]) for k in frag["reslist"]]}
        map_secstr_targ = {tuple(key[1:4]): frag.index for frag in graph_targ.vs for key in
                           [tuple(k[1:4]) for k in frag["reslist"]]}
        # corresp = [(center,distance_hash2[tr][0]) for center in centers_ref for tr in distance_hash2.keys() if tuple(tr[2:4])==tuple(center[1][2:4])]

        # print("CENTERS_REF",centers_ref)
        # print("MAP SECSTR REF",map_secstr_ref)
        # print("MAP SECSTR TARG",map_secstr_targ)

        toadd = []
        for corre in corresp:
            lista_res_targ = graph_targ.vs[map_secstr_targ[tuple(corre[1][1][2:4])]]["reslist"]
            central = [c for c, d in enumerate(lista_res_targ) if tuple(d[2:4]) == tuple(corre[1][1][2:4])][0]
            sublist_ref = graph_ref.vs[map_secstr_ref[tuple(corre[0][1][2:4])]]["reslist"]
            start = central - corre[0][0][1]
            fine = central + (corre[0][0][2] - corre[0][0][1])
            add_at_start = 0
            add_at_fine = 0
            if start < 0:
                add_at_start = abs(start)
                start = 0
            if fine > len(lista_res_targ):
                add_at_fine = fine - len(lista_res_targ)
                fine = len(lista_res_targ)
            sublist_tar = lista_res_targ[start:fine]
            if len(sublist_ref) != len(sublist_tar):
                # print("CORRE", corre)
                # print("LISTA RES TARG", lista_res_targ)
                # print("CENTRAL", central)
                # print("SUBLIST REF", sublist_ref)
                # print("START", start, "FINE", fine)
                # print("START", start, "FINE", fine, "START ADD", add_at_start, "FINE ADD", add_at_fine)
                # print("SUBLIST TARG", sublist_tar)
                if add_at_start > 0 and add_at_fine > 0:
                    sublist_tar = sublist_tar + [sublist_tar[-1] for _ in range(add_at_start)]
                    sublist_tar = sublist_tar + [sublist_tar[-1] for _ in range(add_at_fine)]
                elif add_at_start == 0:
                    libre = start
                    if add_at_fine > libre:
                        sublist_tar = sublist_tar + lista_res_targ[0:libre]
                        sublist_tar = sublist_tar + [sublist_tar[-1] for _ in range(add_at_fine - libre)]
                    else:
                        sublist_tar = sublist_tar + lista_res_targ[libre - add_at_fine:libre]
                elif add_at_fine == 0:
                    libre = len(lista_res_targ) - fine
                    if add_at_start > libre:
                        sublist_tar = sublist_tar + lista_res_targ[fine:]
                        sublist_tar = sublist_tar + [sublist_tar[-1] for _ in range(add_at_start - libre)]
                    else:
                        sublist_tar = sublist_tar + lista_res_targ[fine:fine + add_at_start]

                # print("SUBLIST TAR CORRECTED",sublist_tar)
                if len(sublist_ref) != len(sublist_tar):
                    print("ERROR: they should be of the same size")
                    print(sublist_ref)
                    print(sublist_tar)
                    sys.exit(1)
            toadd.append(zip(sublist_ref, sublist_tar))

        for iterate in toadd:
            for sr, st in iterate:
                found = False
                for key in distance_hash2:
                    if key[1:4] == tuple(sr[1:4]):
                        for q, vlo in enumerate(distance_hash2[key]):
                            if vlo[1][1:4] == tuple(st[1:4]):
                                distance_hash2[key] = distance_hash2[key][q]
                                found = True
                                break
                        if found:
                            break

        # for k,v, in distance_hash2.items():
        #      print(k,v)
        # quit()

    distance_hash = distance_hash2
    # for key,value in distance_hash.items():
    #      print("--",key,value)
    # print("|||||||")

    allAtoms = []

    if recompute_rmsd:
        l1 = sorted(distance_hash.keys())
        lit1 = [distance_hash[c1][2] for c1 in l1]
        lit2 = [distance_hash[c1][3] for c1 in l1]
        # print("LIT1",len(lit1),"LIT2",len(lit2))
        if len(lit1) > 0 and len(lit1) == len(lit2):
            allatoms_ana = [atm for res in list_CA2 for atm in res.get_parent()]
            allAtoms, rmsd, R, t = get_rmsd_and_RT(lit1, lit2, allatoms_ana)
            allAtoms = [atom for atom in allatoms_ana if atom.get_name().lower() == "ca"]
            return get_CA_distance_dictionary(list_CA1, allAtoms,
                                              max_rmsd=max_rmsd if cycle + 1 <= cycles else last_rmsd,
                                              recompute_rmsd=True if cycle + 1 <= cycles else False, cycle=cycle + 1,
                                              get_superposed_atoms=get_superposed_atoms)

    if get_superposed_atoms:
        l1 = sorted(distance_hash.keys())
        lit1 = [distance_hash[c1][2] for c1 in l1]
        lit2 = [distance_hash[c1][3] for c1 in l1]
        if len(lit1) > 0 and len(lit1) == len(lit2):
            allatoms_ana = [atm for res in list_CA2 for atm in res.get_parent()]
            allAtoms, rmsd, R, t = get_rmsd_and_RT(lit1, lit2, allatoms_ana)
            allAtoms = [atom for atom in allatoms_ana if atom.get_name().lower() == "ca"]
            distance_hash = {key: distance_hash[key][:-2] for key in distance_hash}
            return distance_hash, allAtoms, rmsd
        else:
            distance_hash = {key: distance_hash[key][:-2] for key in distance_hash}
            return distance_hash, list_CA2, rmsd
    else:
        distance_hash = {key: distance_hash[key][:-2] for key in distance_hash}
        return distance_hash


def center_of_mass(entity, geometric=False):
    """
    Returns gravitic [default] or geometric center of mass of an Entity.
    Geometric assumes all masses are equal (geometric=True)
    """
    global atom_weights

    # Structure, Model, Chain, Residue
    if isinstance(entity, Bio.PDB.Entity.Entity):
        atom_list = entity.get_atoms()
    # List of Atoms
    elif hasattr(entity, '__iter__'):  # and [x for x in entity if x.level == 'A']:
        atom_list = entity
    else:  # Some other weirdo object
        raise ValueError("Center of Mass can only be calculated from the following objects:\n"
                         "Structure, Model, Chain, Residue, list of Atoms.")

    class COM:
        def __init__(self, coord):
            self.coord = coord

    positions = [[], [], []]  # [ [X1, X2, ..] , [Y1, Y2, ...] , [Z1, Z2, ...] ]
    masses = []

    for atom in atom_list:
        try:
            atom.mass = atom_weights[atom.element.capitalize()]
        except:
            atom.mass = 1.0

        masses.append(atom.mass)

        for i, coord in enumerate(atom.coord.tolist()):
            positions[i].append(coord)

    # If there is a single atom with undefined mass complain loudly.
    if 'ukn' in set(masses) and not geometric:
        raise ValueError("Some Atoms don't have an element assigned.\n"
                         "Try adding them manually or calculate the geometrical center of mass instead.")

    if geometric:
        com = COM([sum(coord_list) / len(masses) for coord_list in positions])
        return com
    else:
        w_pos = [[], [], []]
        for atom_index, atom_mass in enumerate(masses):
            w_pos[0].append(positions[0][atom_index] * atom_mass)
            w_pos[1].append(positions[1][atom_index] * atom_mass)
            w_pos[2].append(positions[2][atom_index] * atom_mass)
        com = COM([sum(coord_list) / sum(masses) for coord_list in w_pos])
        return com


def calculate_shape_param(structure, rounding=None):
    """
    Calculates the gyration tensor of a structure.
    Returns a tuple containing shape parameters:

      ((a,b,c), rg, A, S)

      (a,b,c) - dimensions of the semi-axis of the ellipsoid
      rg    - radius of gyration of the structure
      A     - sphericity value
      S     - anisotropy value

      References:
           1.  Rawat N, Biswas P (2011) Shape, flexibility and packing of proteins and nucleic acids in complexes. Phys Chem Chem Phys 13:9632-9643
           2.  Thirumalai D (2004) Asymmetry in the Shapes of Folded and Denatured States of Proteinss - The Journal of Physical Chemistry B
           3.  Vondrasek J (2011) Gyration- and Inertia-Tensor-Based Collective Coordinates for Metadynamics. Application on the Conformational Behavior of Polyalanine Peptides and Trp-Cage Folding - The Journal of Physical Chemistry A
    """

    com = center_of_mass(structure, True)
    cx, cy, cz = com.coord

    n_atoms = 0
    tensor_xx, tensor_xy, tensor_xz = 0, 0, 0
    tensor_yx, tensor_yy, tensor_yz = 0, 0, 0
    tensor_zx, tensor_zy, tensor_zz = 0, 0, 0

    if isinstance(structure, Bio.PDB.Entity.Entity):
        atom_list = structure.get_atoms()
    # List of Atoms
    elif hasattr(structure, '__iter__'):  # and [x for x in structure if x.level == 'A']:
        atom_list = structure
    else:  # Some other weirdo object
        raise ValueError("Center of Mass can only be calculated from the following objects:\n"
                         "Structure, Model, Chain, Residue, list of Atoms.")

    for atom in atom_list:
        ax, ay, az = atom.coord
        tensor_xx += (ax - cx) * (ax - cx)
        tensor_yx += (ax - cx) * (ay - cy)
        tensor_xz += (ax - cx) * (az - cz)
        tensor_yy += (ay - cy) * (ay - cy)
        tensor_yz += (ay - cy) * (az - cz)
        tensor_zz += (az - cz) * (az - cz)
        n_atoms += 1

    gy_tensor = np.mat(
        [[tensor_xx, tensor_yx, tensor_xz], [tensor_yx, tensor_yy, tensor_yz], [tensor_xz, tensor_yz, tensor_zz]])
    gy_tensor = (1.0 / n_atoms) * gy_tensor

    D, V = np.linalg.eig(gy_tensor)
    [a, b, c] = sorted(np.sqrt(D))  # lengths of the ellipsoid semi-axis
    rg = np.sqrt(sum(D))

    l = np.average([D[0], D[1], D[2]])
    A = (((D[0] - l) ** 2 + (D[1] - l) ** 2 + (D[2] - l) ** 2) / l ** 2) * 1 / 6
    S = (((D[0] - l) * (D[1] - l) * (D[2] - l)) / l ** 3)

    if rounding is None:
        return ((a * 2, b * 2, c * 2), rg, A, S)
    else:
        return ((round(a * 2, rounding), round(b * 2, rounding), round(c * 2, rounding)), round(rg, rounding),
                round(A, rounding), round(S, rounding))

    # print "%s" % '#Dimensions(a,b,c) #Rg #Anisotropy'
    # print "%.2f" % round(a,2), round(b,2), round(c,2) , round(rg,2) , round(A,2)


def calculate_moment_of_intertia_tensor(structure):
    """
    Calculates the moment of inertia tensor from the molecule.
    Returns a numpy matrix.
    """
    global atom_weights

    if isinstance(structure, Bio.PDB.Entity.Entity):
        atom_list = structure.get_atoms()
        # List of Atoms
    elif hasattr(structure, '__iter__') and [x for x in structure if x.level == 'A']:
        atom_list = structure
    else:  # Some other weirdo object
        raise ValueError("Center of Mass can only be calculated from the following objects:\n"
                         "Structure, Model, Chain, Residue, list of Atoms.")
    s_mass = 0.0
    for atom in atom_list:
        atom.mass = atom_weights[atom.element.capitalize()]
        s_mass += atom.mass

    com = center_of_mass(structure, False)
    cx, cy, cz = com.coord

    n_atoms = 0
    tensor_xx, tensor_xy, tensor_xz = 0, 0, 0
    tensor_yx, tensor_yy, tensor_yz = 0, 0, 0
    tensor_zx, tensor_zy, tensor_zz = 0, 0, 0
    # s_mass = sum([a.mass for a in atom_list])

    if isinstance(structure, Bio.PDB.Entity.Entity):
        atom_list = structure.get_atoms()
    elif hasattr(structure, '__iter__') and [x for x in structure if x.level == 'A']:
        atom_list = structure

    for atom in atom_list:
        ax, ay, az = atom.coord
        tensor_xx += ((ay - cy) ** 2 + (az - cz) ** 2) * atom_weights[atom.element.capitalize()]
        tensor_xy += -1 * (ax - cx) * (ay - cy) * atom_weights[atom.element.capitalize()]
        tensor_xz += -1 * (ax - cx) * (az - cz) * atom_weights[atom.element.capitalize()]
        tensor_yy += ((ax - cx) ** 2 + (az - cz) ** 2) * atom_weights[atom.element.capitalize()]
        tensor_yz += -1 * (ay - cy) * (az - cz) * atom_weights[atom.element.capitalize()]
        tensor_zz += ((ax - cx) ** 2 + (ay - cy) ** 2) * atom_weights[atom.element.capitalize()]

    in_tensor = np.mat([[tensor_xx, tensor_xy, tensor_xz], [tensor_xy, tensor_yy, tensor_yz], [tensor_xz,
                                                                                               tensor_yz, tensor_zz]])
    D, V = np.linalg.eig(in_tensor)

    a = np.sqrt((5 / (2 * s_mass)) * (D[0] - D[1] + D[2]))
    b = np.sqrt((5 / (2 * s_mass)) * (D[2] - D[0] + D[1]))
    c = np.sqrt((5 / (2 * s_mass)) * (D[1] - D[2] + D[0]))
    return sorted([a, b, c]), D, V


def get_rmsd_from_distance_hash(distance_hash, squared_distances=False):
    """
    :author: Dr. Massimo Domenico Sammito
    :email: msacri@ibmb.csic.es / massimo.sammito@gmail.com
    :param distance_hash:
    :type distance_hash:
    :return: rmsd
    :rtype: float
    """
    summatory = 0
    for key in distance_hash.keys():
        summatory += distance_hash[key][0] if squared_distances else distance_hash[key][0] ** 2
    n = len(distance_hash)
    if n > 0:
        rmsd = np.sqrt(summatory / n)
        return rmsd
    else:
        return None


# NOTE: This is currently broken... it looks like there is a bug in biopython Bio/PDB/QCPSuperimposer/floppy.py", line 53, in _rms
def get_quaternion_superposition(list_atom_ref, list_atom_targ):
    import Bio.PDB.QCPSuperimposer
    qpc = Bio.PDB.QCPSuperimposer.QCPSuperimposer()
    serc = np.array([np.array(a.get_coord()) for a in list_atom_ref])
    gerc = np.array([np.array(a.get_coord()) for a in list_atom_targ])
    print(serc.shape, gerc.shape)
    qpc.set(serc, gerc)
    qpc.run()
    return qpc.get_rotran(), qpc.get_init_rms(), qpc.get_rms()

def get_specific_remarks_as_string(pdbf, remark):
    z = []
    q = None
    with open(pdbf, "r") as f:
        q = f.read()
    for line in q.splitlines():
        if line.startswith(remark):
            z.append(line)

    return "\n".join(z)


def get_voyager_remarks_as_string(pdbf, include_phaser_remarks=False):
    z = []
    q = None
    with open(pdbf, "r") as f:
        q = f.read()
    for line in q.splitlines():
        if line.startswith("REMARK VOYAGER") or (include_phaser_remarks and line.startswith("REMARK PHASER")):
            z.append(line)

    return "\n".join(z)


def get_voyager_remarks_as_dict(pdbf):
    z = get_voyager_remarks_as_string(pdbf)
    dictio = {}
    for line in z.splitlines():
        if line.startswith("REMARK VOYAGER"):
            lil = line.split()
            tp = lil[2]
            lil = lil[3:]
            s = {}
            for key,value in enumerate(lil):
                if key%2==0:
                    if value.endswith(":"): value = value[:-1]
                    s[value] = lil[key+1]
            dictio[tp] = s
    return dictio
