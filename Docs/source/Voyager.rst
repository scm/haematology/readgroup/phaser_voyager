Voyager package
===============

Subpackages
-----------

.. toctree::

    Voyager.Actions
    Voyager.Strategies
    Voyager.gui

Submodules
----------

.. toctree::

   Voyager.AIagent
   Voyager.phaser.voyager
   Voyager.voyager_actions
   Voyager.voyager_dag
   Voyager.voyager_strategy

Module contents
---------------

.. automodule:: Voyager
    :members:
    :undoc-members:
    :show-inheritance:
