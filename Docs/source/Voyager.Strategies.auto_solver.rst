Voyager.Strategies.auto\_solver module
======================================

.. automodule:: Voyager.Strategies.auto_solver
    :members:
    :undoc-members:
    :show-inheritance:
