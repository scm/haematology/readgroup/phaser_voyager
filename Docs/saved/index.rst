.. PhaserVoyager documentation master file, created by
   sphinx-quickstart on Fri Jul 31 18:52:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PhaserVoyager's documentation!
=========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
