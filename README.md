DISCLAIMER: 
Currently tested in Mac and Linux with Python3 and Python2. Still pending to test in 
Windows. Also would be ideal to set up some continuous integration system.


To start with Voyager follow these instructions:

1) Download the latest bootstrap here:

```
wget https://raw.githubusercontent.com/cctbx/cctbx_project/master/libtbx/auto_build/bootstrap.py
```

2) Create a directory for the phenix_voyager

```
mkdir phenix_voyager
mv bootstrap.py phenix_voyager
cd phenix_voyager
```


3) Install phenix_voyager with bootstrap.py

```
python bootstrap.py --builder=phenix_voyager --use-conda --cciuser=myuser --nproc=6 --python=37
```

3.5) The first time you build phenix_voyager from scratch you should also:
   - ```cd ../phaser_voyager```
   - ```git checkout master``` #the default branch for voyager is distribution which comntain the last distributed version (not the one with the recents commits)
   - As you have changed some branches you need to tell phenix to recreate the links for the new programs eg.: phenix.voyager.data_driven_ensembler .
     If you do not know how to do that you can just redo: ``` python bootstrap.py --builder=phenix_voyager --use-conda --cciuser=myuser --nproc=6 --python=38 ```
     from now on (if you do not remove directories or change branches) bootstrap will update the branches and you have checkout

4) Run Voyager as following:

```
phenix.phaser.voyager --show-defaults=3
```

5) When you have chosen a strategy you can:

```
phenix.voyager.strategyname --show-defaults=3
```

6) When you have created a .phil configuration file you can:

```
phenix.voyager.strategyname input.phil
```

7) Voyager assumes that the current directory is the working directory so run Voyager in the place where you want it can write
directories and files.


Strategies that should be deployed are:

```
phenix.voyager.isostructure
phenix.voyager.sculptor_ensembler
phenix.voyager.homologs
phenix.voyager.casp_rel_ellg
phenix.voyager.xtricorder
```




