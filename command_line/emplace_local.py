# LIBTBX_SET_DISPATCHER_NAME phenix.voyager.emplace_local

#                                          LICENSE                                                #
# PhaserVoyager is distributed under three different licences                                     #
#                                                                                                 #
# [Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)                 #
# [CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)                         #
# [Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)                     #
#                                                                                                 #
# By accessing the source code in this repository you agree to be bound by one of these licences. #
#                   (c) 2000-2024 Cambridge University Technical Services Ltd                     #
#                                                                                                 #

from __future__ import absolute_import, division, print_function

import os,sys
module_path = os.path.dirname(os.path.abspath(__file__))
src = os.path.join(module_path, "../src/")
sys.path.append(src)

from iotbx.cli_parser import run_program
from New_Voyager.programs import emplace_local

if (__name__ == '__main__'):
  run_program(emplace_local.Program)
