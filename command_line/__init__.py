# -*- coding: utf-8 -*-
"""
#                                          LICENSE                                                #
# PhaserVoyager is distributed under three different licences                                     #
#                                                                                                 #
# [Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)                 #
# [CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)                         #
# [Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)                     #
#                                                                                                 #
# By accessing the source code in this repository you agree to be bound by one of these licences. #
#                   (c) 2000-2020 Cambridge University Technical Services Ltd                     #
#                                                                                                 #
"""

from __future__ import print_function
from __future__ import division
from future.standard_library import install_aliases

install_aliases()

__license__ = "(c) 2000-2020 Cambridge University Technical Services Ltd"
__revision__ = "0.0.1"
__author__ = "Massimo D. Sammito"
__docformat__ = 'reStructuredText'
__copyright__ = "University of Cambridge"
__credits__ = ["Massimo D. Sammito", "Randy J. Read"]
__version__ = "0.0.1"
__maintainer__ = "Massimo D. Sammito"
__email__ = "mds83@cam.ac.uk"
__status__ = "Prototype"
