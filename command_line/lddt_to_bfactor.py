# LIBTBX_SET_DISPATCHER_NAME phenix.voyager.lddt_to_bfactor

#####################################################################
#  @author: Claudia Millan                                          #
#  @email: cm844@cam.ac.uk                                          #
#####################################################################

#                                          LICENSE                                                #
# PhaserVoyager is distributed under three different licences                                     #
#                                                                                                 #
# [Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)                 #
# [CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)                         #
# [Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)                     #
#                                                                                                 #
# By accessing the source code in this repository you agree to be bound by one of these licences. #
#                   (c) 2000-2024 Cambridge University Technical Services Ltd                     #
#                                                                                                 #

# System imports
from __future__ import print_function
from __future__ import division

import sys
import os

module_path = os.path.dirname(os.path.abspath(__file__))
src = os.path.join(module_path, "../src/")
sys.path.append(src)
import argparse
import time
import shutil

import New_Voyager.pdb_structure

if __name__ == '__main__':
    start_time = time.time()
    errstream = sys.stderr
    sys.stderr = sys.stdout

    parser = argparse.ArgumentParser(description='Command line options for phenix.voyager.lddt_to_bfactor')
    parser.add_argument("--inputfile", help="Input path of the pdb file to be processed", required=True)
    parser.add_argument("--outputfile", help="Output path for the processed pdb file",required=True)
    parser.add_argument("--min_lddt", help="Minimum threshold of LDDT for accepted atoms . Default=-1 and means no trimming", type=float,
                        default=-1)


    print("******************************************* Command line used **************************************************")
    print(" ".join(sys.argv))
    print("*********************************************************************************************************")

    args = parser.parse_args()
    sys.stderr = errstream

    # First I generate a copy of my input file
    shutil.copy(args.inputfile, 'tempfile.pdb')
    # Then I read this as an object
    pdb_obj = pdb_structure.PDB('tempfile.pdb')
    if args.min_lddt == -1: # just nothing to be removed
        pdb_obj.convert_lddt_to_bfac(min_lddt_allowed=None)
    else: # remove things above that value
        pdb_obj.convert_lddt_to_bfac(min_lddt_allowed=args.min_lddt)
    pdb_obj.write_pdb(args.outputfile)
    try:
        os.remove('tempfile.pdb')
    except:
        pass
